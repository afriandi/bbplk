-- phpMyAdmin SQL Dump
-- version 3.2.0.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 08, 2017 at 12:12 PM
-- Server version: 5.1.37
-- PHP Version: 5.3.0

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `blk_bandung`
--

-- --------------------------------------------------------

--
-- Table structure for table `asesmen`
--

CREATE TABLE IF NOT EXISTS `asesmen` (
  `id_asesmen` int(11) NOT NULL AUTO_INCREMENT,
  `id_kejuruan` varchar(6) DEFAULT NULL,
  `no_skema` varchar(20) DEFAULT NULL,
  `judul_skema` varchar(75) DEFAULT NULL,
  `deskripsi` text,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_asesmen`),
  KEY `id_kejuruan` (`id_kejuruan`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `asesmen`
--

INSERT INTO `asesmen` (`id_asesmen`, `id_kejuruan`, `no_skema`, `judul_skema`, `deskripsi`, `last_update`) VALUES
(1, 'oto', '', 'PEMELIHARAAN BERKALA KENDARAAN RINGAN', 'Terdiri dari 24 Unit Kompetensi, yaitu :\r\nMelaksanakan Pemeliharaan Servis Komponen, Membaca dan Memahami Gambar Teknik, Menggunakan dan memelihara alat ukur, Mengikuti Prosedur  Kesehatan & Keselamatan Kerja, Menggunakan dan Memelihara Peralatan dan Perlengkapan Tempat Kerja, Kontribusi Komunikasi di Tempat Kerja, Memelihara/Servis Engine dan Komponen - Komponennya, Memelihara/servis sistem pendingin dan komponennya, Memelihara/Servis Sistem Bahan Bakar Bensin, Memelihara/Servis Sistem Injeksi, Bahan Bakar Diesel, Pemeliharaan/Servis Sistem Kontrol Emisi, Memelihara/Servis Unit Kopling dan Komponen – Komponennya Sistem Pengoperasian, Memelihara/servis transmisi manual, Memelihara/Servis Transmisi Otomatis, Memelihara/Servis Unit Final Drive / Gardan, Memelihara/Servis sistem rem, Memelihara/Servis Sistem Kemudi, Memelihara/Servis Sistem Suspensi\r\nMelepas, memasang, dan Menyetel Roda\r\nMenguji, Memelihara/Servis dan Mengganti Baterai, Memperbaiki sistem starter dan pengisian, Memasang, menguji dan memperbaiki sistem penerangan dan wiring, Memperbaiki sistem pengapian, Memelihara/Servis dan Memperbaiki Engine Manajemen Sistem.\r\n', '2017-06-14 16:42:38'),
(2, 'oto', '', 'SERVIS SEPEDA MOTOR KONVENSIONAL', '', '2017-06-14 16:42:38'),
(3, 'oto', '', 'PEMELIHARAAN KENDARAAN RINGAN SISTEM INJEKSI', 'Terdiri dari 7 Unit Kompetensi, yaitu :\r\nMembaca dan Memahami Gambar Teknik, Menggunakan dan Memelihara Alat Ukur, Mengikuti prosedur  kesehatan & keselamatan, Memelihara/Servis Sistem Bahan Bakar Bensin, Memelihara/Servis Sistem Kontrol Emisi, Memperbaiki Sistem Pengapian, Memelihara/Servis dan Memperbaiki Engine Management System.', '2017-06-14 16:42:38'),
(4, 'oto', '', 'PERBAIKAN BODY KENDARAAN RINGAN', 'Terdiri dari 5 Unit Kompetensi, yaitu :\r\nMengikuti Prosedur  Kesehatan & Keselamatan Kerja, Melaksanakan prosedur pengelasan, pemotongan termal dan pemanasan, Memperbaiki panel-panel body, Mempersiapkan bahan dan peralatan pengecatan, Pelaksanakan pengkilapan dan pemolesan.', '2017-06-14 16:42:38'),
(6, 'oto', '', 'SPOORING BALANCING KENDARAAN RINGAN', 'Terdiri dari 10 Unit Kompetensi, yaitu :\r\nMembaca dan Memahami Gambar Teknik, Menggunakan dan Memelihara Alat Ukur, Mengikuti Prosedur Kesehatan & Keselamatan Kerja, Menggunakan dan Memelihara Peralatan dan Perlengkapan Tempat Kerja, Melaksanakan Operasi Penanganan Secara Manual, Memelihara/Servis Sistem Kemudi, Memelihara/Servis Sistem Suspensi, Melaksanakan Pekerjaan Pelurusan, Roda/Spooring, Membalance Roda/Ban\r\nMelepas, Memasang dan Menyetel Roda.\r\n', '2017-06-14 16:42:38'),
(7, 'oto', NULL, 'SERVICE SPEDA MOTOR INJEKSI', NULL, '2017-06-14 16:44:45'),
(8, 'oto', NULL, 'PEMELIHARAAN KENDARAAN RINGAN SISTEM KONVENSIONAL', NULL, '2017-06-14 16:44:45'),
(9, 'oto', NULL, 'TUNE UP SEPEDA MOTOR KONVENSIONAL', 'Terdiri dari 9 unit kompetensi, yaitu :\r\nMengikuti Prosedur Keselamatan, Kesehatan Kerja dan Lingkungan, Menggunakan dan Memelihara Alat Ukur, Melepas Kepala Silinder, Menilai Komponen-Komponennya serta Merakit Kepala Silinder, Memperbaiki dan Melakukan Overhaul Komponen Sistem Bahan Bakar Bensin, Memelihara Unit Kopling Manual dan Otomatik berikut Komponen-Komponen Sistem Pengoperasiannya, Memelihara Sistem Rem, Memelihara Rantai / Chain, Memasang, Menguji dan Memperbaiki Sistem Penerangan dan Wiring, Memperbaiki Sistem Pengapian.\r\n', '2017-06-14 16:48:10'),
(10, 'oto', NULL, 'PEMELIHARAAN SEPEDA MOTOR', NULL, '2017-06-14 16:48:10'),
(11, 'tekman', NULL, 'PENGOPERASIAN MESIN BUBUT CNC', 'Terdiri dari 11 unit, antara lain :\r\nMenerapkan prinsip-prinsip K3, Menerapkan prosedur-prosedur mutu, Mengukur dengan menggunakan alat ukur, Mengoperasikan dan mengamati Mesin/Proses, Bekerja Dengan Mesin Umum, Membaca gambar teknik, Menggunakan perkakas tangan, Mengeset mesin dan program NC/CNC, Mengeset dan Mengedit Program Mesin NC/CNC, Memprogram mesin NC/CNC (dasar), Mengoperasikan mesin  NC/CNC (dasar).\r\n', '2017-06-14 17:08:35'),
(12, 'tekman', NULL, 'PEMBUATAN JIG AND FIXTURE', 'Terdiri dari 10 unit, antara lain :\r\nMenerapkan prinsip-prinsip, Keselamatan dan kesehatan kerja di tempat kerja, Menerapkan prosedur-prosedur Mutu, Membaca gambar teknik, Mengukur dengan menggunakan alat ukur, Menggunakan perkakas tangan, Melakukan pekerjaan dengan mesin bubut, Melakukan pekerjaan dengan mesin frais, Melakukan pekerjaan dengan mesin gerinda, Melakukan pekerjaan presisi dengan jig boring, Perakitan presisi.', '2017-06-14 17:08:35'),
(13, 'tekman', NULL, 'PEMBUATAN PRECISION TOOL', 'Terdiri dari 9 unit, yaitu :\r\nMenerapkan prinsip-prinsip keselamatan dan kesehatan kerja di tempat kerja, Menerapkan prosedur-prosedur Mutu, Mengukur dengan menggunakan alat ukur, Membaca gambar teknik, Menggunakan perkakas tangan, Menggerinda pahat dan alat potong, Melakukan pekerjaan dengan mesin bubut, Melakukan pekerjaan dengan mesin frais, Melakukan pekerjaan dengan mesin gerinda.', '2017-06-14 17:08:35'),
(14, 'tekman', NULL, 'PEMBUATAN PRESS TOOL', 'Terdiri dari 9 unit kompetensi, antara lain :\r\nMenerapkan prinsip-prinsip  keselamatan dan kesehatan kerja di tempat kerja, Menerapkan prosedur-prosedur Mutu, Mengukur dengan menggunakan alat ukur, Menggunakan peralatan pembanding dan/ atau alat ukur dasar, Membaca gambar teknik, Menggunakan perkakas tangan, Melakukan pekerjaan dengan mesin bubut, Melakukan pekerjaan dengan mesin frais, Melakukan pekerjaan dengan mesin gerinda.\r\n', '2017-06-14 17:08:35'),
(15, 'tekman', NULL, 'PEMBUATAN (PEMBENTUKAN) SHEET METAL', 'Terdiri dari 10 unit, yaitu :\r\nKeselamatan kerja  Dilingkungan tempat kerja, Menerapkan prosedur mutu, Mengukur dengan Menggunakan alat ukur, Merakit pelat dan lembaran, Melakukan pemotongan secara mekanik, Melaksanakan fabrikasi, pembentukan, pelengkungan, dan pencetakan, Membuat bukaan / bentangan geometri, Menggambar dan menginterpretasikan sketsa, Membaca gambar teknik, Menggunakan perkakas tangan.\r\n', '2017-06-14 17:08:35'),
(16, 'tekman', NULL, 'PERAWATAN MESIN PERKAKAS', 'Terdiri dari 9 unit kompetensi, antara lain :\r\nMenerapkan prinsip-prinsip keselamatan dan kesehatan kerja di tempat kerja, Menerapkan prosedur-prosedur Mutu, Mengukur dengan alat ukur, Membaca gambar teknik, \r\nMenggunakan perkakas tangan, Mendiagnosa kesalahan, memasang, dan melepas bantalan, Mendatarkan dan menyebariskan komponen pemesinan, Memasang dan melepas seal mekanis, Membongkar /mengganti dan merakit, komponen komponen pemesinan.\r\n', '2017-06-14 17:08:35'),
(17, 'tekman', NULL, 'PENGOPERASIAN MESIN FRAIS', 'Terdiri dari 6 unit kompetensi, yaitu :\r\nMenerapkan Prinsip – Prinsip Keselamatan Dan Kesehatan Kerja di Lingkungan Kerja, Menerapkan Prosedur – Prosedur Mutu, Mengukur Dengan Menggunakan Alat Ukur, Membaca Gambar Teknik, Menggunakan Perkakas Tangan, \r\nMelakukan Pekerjaan Dengan Mesin Frais\r\n', '2017-06-14 17:08:35'),
(18, 'tekman', NULL, 'PENGOPERASIAN MESIN BUBUT', 'Terdiri dari 6 unit kompetensi, antara lain :\r\nMenerapkan Prinsip – Prinsip Keselamatan Dan Kesehatan Kerja di Lingkungan Kerja, Menerapkan Prosedur – Prosedur Mutu, Mengukur Dengan Menggunakan Alat Ukur, Membaca Gambar Teknik, Menggunakan Perkakas Tangan, Melakukan Pekerjaan Dengan Mesin Bubut.\r\n', '2017-06-14 17:08:35'),
(19, 'tekman', NULL, 'PEMBUATAN KODE FILE 3D DENGAN SISTEM CAM', 'Terdiri dari 10 unit kompetensi, yaitu :\r\nMenerapkan prinsip-prinsip K3, Menerapkan prosedur-prosedur mutu, Mengukur dengan menggunakan alat ukur, Mengoperasikan Komputer, Membaca gambar teknik, Mempersiapkan Gambar Teknik Dasar, Menggambar 2D dengan sistem CAD, Menggambar 3D dengan sistem CAD, Membuat File Kode 2D Sistem CAM, Membuat File Kode 3D Sistem CAM.', '2017-06-14 17:08:35'),
(20, 'tekman', NULL, 'PENGOPERASIAN MESIN MILLING CNC', 'Terdiri dari 11 unit kompetensi, antara lain :\r\nMenerapkan prinsip-prinsip K3, Menerapkan prosedur-prosedur mutu, Mengukur dengan menggunakan alat ukur, Mengoperasikan dan mengamati Mesin/Proses, Bekerja Dengan Mesin Umum, Membaca gambar teknik, Menggunakan perkakas tangan, Mengeset mesin dan program NC/CNC, Mengeset dan Mengedit Program Mesin NC/CNC, Memprogram mesin NC/CNC (dasar), Mengoperasikan mesin proses NC/CNC (dasar).\r\n', '2017-06-14 17:08:35'),
(21, 'tekman', NULL, 'PENGOPERASIAN MESIN CNC DENGAN PROGRAM CAM', 'Terdiri dari 15 unit kompetensi, antara lain :\r\nMenerapkan prinsip-prinsip K3, Menerapkan prosedur-prosedur mutu, Membaca gambar teknik, Menggunakan peralatan pembanding dan atau alat ukur dasar, Menggunakan perkakas tangan, Bekerja Dengan Mesin Umum, Mengoperasikan dan mengamati Mesin/Proses, Mengoperasikan Komputer\r\nMemprogram mesin CNC/NC, Mengeset dan mengedit program mesin NC/CNC, Mengoperasikan mesin CNC, Menggambar 2D dengan sistem CAD, Menggambar 3D dengan sistem CAD, Membuat File Kode 2D Sistem CAM, Membuat File Kode 3D Sistem CAM.\r\n', '2017-06-14 17:13:14'),
(22, 'tekman', NULL, 'DRAFTER MESIN', 'Terdiri dari 7 unit kompetensi, yaitu :\r\nMenerapkan prinsip-prinsip K3, Mengukur dengan menggunakan alat ukur, Mengoperasikan Komputer, Membaca gambar teknik, Menggambar dan menginterpretasikan sketsa, Menggambar 2D dengan sistem CAD, Menggambar 3D dengan sistem CAD.\r\n', '2017-06-14 17:13:14'),
(23, 'tekman', NULL, 'PEMBUATAN MOULD', 'Teridiri dari 17 unit kompetensi, antara lain :\r\nMenerapkan prinsip-prinsip K3, Menerapkan prosedur-prosedur mutu, Membaca gambar teknik, Menggunakan peralatan pembanding dan atau alat ukur dasar, Menggunakan perkakas tangan, Bekerja Dengan Mesin Umum, Mengoperasikan dan mengamati Mesin/Proses, Mengoperasikan Komputer, Memprogram mesin CNC/NC, Mengeset dan mengedit program mesin NC/CNC, Mengoperasikan mesin CNC, Menggambar 2D dengan sistem CAD, Menggambar 3D dengan sistem CAD, Membuat File Kode 2D Sistem CAM, Membuat File Kode 3D Sistem CAM, Mengoperasikan Mesin EDM, Menyelesaikan/memoles material secara manual.\r\n', '2017-06-14 17:15:09');

-- --------------------------------------------------------

--
-- Table structure for table `asesor`
--

CREATE TABLE IF NOT EXISTS `asesor` (
  `id_asesor` int(11) NOT NULL AUTO_INCREMENT,
  `nip` varchar(18) DEFAULT NULL,
  `no_met` varchar(25) DEFAULT NULL,
  `no_lsp` varchar(25) DEFAULT NULL,
  `tgl_exp` date DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_asesor`),
  KEY `nip` (`nip`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `asesor`
--


-- --------------------------------------------------------

--
-- Table structure for table `berita`
--

CREATE TABLE IF NOT EXISTS `berita` (
  `id_berita` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) DEFAULT NULL,
  `judul` varchar(100) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `deskripsi` text,
  `gambar` varchar(100) DEFAULT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_berita`),
  KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `berita`
--

INSERT INTO `berita` (`id_berita`, `username`, `judul`, `tanggal`, `deskripsi`, `gambar`, `last_update`) VALUES
(5, 'admin', 'PENDAFTARAN PELATIHAN KERJA & SERTIFIKASI TAHAP VI BULAN JULI 2017 KEJURUAN TEKNIK MANUFAKTUR', '2017-06-12', 'BBPLK Bandung telah membuka pendaftaran Pelatihan Kerja & Sertifikasi Tahap VI Tahun 2017, pilih program pelatihan yang kalian minat,lengkapi persyaratannya dan langsung daftar ke BBPLK Bandung atau via online di www.kios3in1.net/001 , Pelaksanaan Tes Rekruitmen akan dilaksanakan pada tanggal 7 Juli 2017.\r\n\r\nPelatihan Kerja & Sertifikasi ini diselenggarakan secara Gratis...', '1.jpg', '2017-06-12 00:00:00'),
(6, 'admin', 'Kunjungan Senior Manager PT. Mitra Pinasthika Mustika, Tbk ke BBPLK Bandung', '2017-06-12', 'Kunjungan Senior Manager PT. Mitra Pinasthika Mustika, Tbk ke BBPLK Bandung dalam rangka penjajakan kerjasama pelatihan dan penempatan alumni pelatihan BBPLK Bandung. Kunjungan ini diterima langsung Kepala BBPLK Bandung Bapak Edy Hernanto, S.Pd', '2.jpg', '2017-06-12 00:00:00'),
(7, 'admin', 'Kunjungan dari SMK Karanggayam Kebumen ke BBPLK Bandung', '2017-06-12', 'Pada tanggal 8 Mei 2017 SMK Karanggayam Kebumen berkunjung ke BBPLK Bandung.Kunjungan ini dimaksudkan untuk menambah pengetahuan siswa mengenai Balai Latihan Kerja, Program Pelatihan dan Dunia Industri.\r\nWorkshop Otomotif menjadi tujuan utama siswa SMK Karanggayam Kebumen, karena sesuai kejuruan yang mereka ikuti saat ini. Namun Siswa SMK Karanggayam Kebumen juga sangat tertarik untuk datang ke workshop Teknik Manufaktur.\r\nSiswa SMK Karanggayam Kebumen sangat senang berkunjung ke BBPLK Bandung terlebih fasilitas dan program pelatihan di BBPLK Bandung sangat bagus dan Program Pelatihan diselenggarakan secara gratis sehingga menarik minat mereka untuk mengikutinya.', '3.jpg', '2017-06-12 00:00:00'),
(8, 'admin', 'Partisipasi BBPLK Bandung dalam kegiatan IIMS di JIEXPO Kemayoran Jakarta', '2017-06-12', 'Kegiatan Indonesia International Motor Show yang diselenggarakan di JIEXPO Kemayoran Jakarta menampilkan berbagai produk Otomotif dari berbagai perusahaan otomotif, di kegiatan ini BBPLK Bandung mendapat undangan Melalui Kementerian Ketenagakerjaan untuk hadir dan diberi kesempatan untuk mensosialisasikan Balai Latihan Kerja dan Program – program yang ada di BBPLK Bandung kepada pengunjung yang hadir.\r\nKegiatan IIMS dilaksanakan selama 10 hari dari tanggal 27 April - 7 May 2017.\r\n', '4.jpg', '2017-06-12 00:00:00'),
(9, 'admin', 'Tata Cara Mendaftar Pelatihan Kerja di BBPLK Bandung', '2017-06-12', 'Tahun 2017 ini BBPLK Bandung membuka kembali Program Pelatihan Kerja & Sertifikasi, Pelatihan kerja ini dibiayai pemerintah dalam hal ini Kementerian Ketenagakerjaan. Pada Tahun 2017 ini BBPLK Bandung Membuka Pelatihan untuk kejuruan Teknik Manufaktur dan Otomotif .\r\nPelatihan ini dibuka untuk umum dan beberapa paket pelatihan akan diselenggarakan dengan menyediakan fasilitas asrama bagi peserta yang berasal dari luar kota Bandung.untuk mendaftar ada dua cara yaitu dengan mendatangi langsung kantor BBPLK Bandung dengan melengkapi persyaratan yang diperlukan dan juga dapat mendaftar secara online melalui website www.kios3in1.net/001', '5.jpg', '2017-06-12 00:00:00'),
(10, 'admin', 'Pembukaan Pelatihan Kerja & Sertifikasi Tahap 4 Tahun 2017', '2017-06-12', 'Pembukaan Pelatihan Kerja & Sertifikasi Tahap 4 ini dibuka langsung oleh kepala BBPLK Bandung, Bpk. Edy Hernanto, S.Pd pada hari Selasa, 25 April 2017 di Aula BBPLK Bandung. Pada tahap 4 ini dibuka 10 paket pelatihan.', '6.jpg', '2017-06-12 00:00:00'),
(11, 'admin', 'Tes Recruitment PT. Altinex', '2017-06-12', 'Tes Recruitment calon karyawan  PT. Altinex di BBPLK Bandung yang di ikuti oleh alumni BBPLK Bandung Kejuruan Teknik Manufaktur. Tes Recruitmen ini diselenggarakan di Aula BBPLK Bandung Rabu, 19 April 2017.', '7.jpg', '2017-06-12 00:00:00'),
(12, 'admin', 'Job & Education Fair 2017 SMKN 03 kota cimahi', '2017-06-12', 'Rekan BBPLK Bandung, hari ini kami hadir di acara Job & Education Fair 2017 SMKN 03 kota cimahi..ingin tahu tentang Pelatihan Kerja Gratis BBPLK Bandung atau ingin daftar langsung Pelatihan Kerja Gratis di BBPLK Bandung datang aja ke Job & Education Fair 2017 SMKN 03 kota cimahi.\r\nPengunjung booth BBPLK Bandung dapat langsung mendaftar Pelatihan Kerja Gratis di acara Job & Education Fair 2017 SMKN 03 kota cimahi.', '8.jpg', '2017-06-12 00:00:00'),
(13, 'admin', 'Kunjungan Universitas Lampung ke BBPLK Bandung', '2017-06-12', 'Pada hari selasa, 18 April 2017 BBPLK Bandung mendapatkan Kunjungan dari Universitas Lampung dalam rangka menambah wawasan mahasiswa tentang Balai Latihan Kerja,Program Pelatihan dan dunia Industri.', '9.jpg', '2017-06-12 00:00:00'),
(14, 'admin', 'Tes Rekruitmnen Calon Karyawan PT. Inti Ganda Perdana', '2017-06-12', 'Kegiatan Tes Rekruitmnen Calon Karyawan PT. Inti Ganda Perdana di BBPLK Bandung yang diikuti oleh Alumni BBPLK Bandung dan dilaksanakan selama dua hari yaitu pada hari Senin 17 April 2017 sampai dengan selasa, 18 April 2017 yang di selenggarakan di Aula BBPLK Bandung.\r\nTes rekruitmen ini dilaksanakan untuk memenuhi kebutuhan beberapa posisi di PT. Inti Ganda Perdana.', '10.jpg', '2017-06-12 00:00:00'),
(15, 'admin', 'Job & Education Fair 2017 SMKN 1 Bandung', '2017-06-12', 'Rekan BBPLK Bandung, Besok hari sabtu 8 April 2017 BBPLK Bandung akan hadir di acara Job & Education Fair 2017 yang diselenggarakan di SMKN 1 Bandung Jl. Wastukancana no 3 Bandung..ayo datang dan kunjungi booth BBPLK Bandung, disana kalian akan mendapatkan banyak informasi tentang Pelatihan Kerja Gratis dan bisa langsung mendaftar juga untuk ikut Pelatihan Kerja Gratis di BBPLK Bandung..jangan lupa membawa persyaratan yang diperlukan ya..sampai jumpa di acara Job & Education Fair 2017 SMKN 1 Bandung.', '11.jpg', '2017-06-12 00:00:00'),
(16, 'admin', 'BLK Bandung Membuka Pelatihan Kerja Gratis', '2017-07-07', 'Balai Besar Pengembangan Latihan Kerja (BBPLK) Bandung atau yang kita kenal sebagai BLK Bandung merupakan unit pelaksanaan teknis pusat dibidang pelatihan kerja dibawah Direktorat Jenderal Pembinaan Pelatihan dan Produktivitas Kementerian Ketenagakerjaan Republik Indonesia yang berlokasi di Jalan Gatot Subroto Nomor 170 kota Bandung. \r\nPada tahun 2017 BLK Bandung hanya menyelenggarakan pelatihan di 2 (dua) kejuruan yaitu Teknik Manufaktur dan Otomotif, sesuai dengan Program Kementerian mengenai 3R ( Reorientasi, Revitalisasi dan Rebranding) dibeberapa Balai Pelatihan di bawah Kementerian Ketenagakerjaan Republik Indonesia. \r\n\r\nKejuruan Otomotif memiliki 4 jenis Program Pelatihan yaitu:\r\nmekanik mobil bensin (480 JP/ 3 Bulan)\r\nmekanik mobil EFI (160 JP/ 1 Bulan)\r\nmekanik spooring balancing (160 JP/ 1 Bulan)\r\nmekanik body repair (240 JP/ 1,5 Bulan)\r\n\r\nSedangkan Kejuruan Teknik Manufaktur memiliki 12 jenis Program Pelatihan yaitu :\r\nOperator Mesin Produksi (320 JP/ 2 Bulan)\r\nDrafter Mesin (240 JP/ 1,5 Bulan)\r\nOperator CAD CAM (240 JP/ 1,5 Bulan)\r\nOperator CNC CAD CAM (320 JP / 2 Bulan)\r\nOperator Produksi CNC (240 JP/ 1,5 Bulan)\r\nOperator Kerja Plat (240 JP/ 1,5 Bulan)\r\nPress Tool Maker (240JP/ 1,5 Bulan)\r\nPerawatan Mesin Perkakas (240 JP/ 1,5 Bulan)\r\nOperator Jig dan Fixture Maker (240 JP/ 1,5 Bulan)\r\nPembuatan Perkakas Presisi (240 JP/ 1,5 Bulan)\r\nOperator CNC Milling (240 JP/ 1,5 Bulan)\r\nMold Maker (320 JP/ 2 Bulan)\r\nPelatihan dibiayai Pemerintah\r\n\r\nPeserta pelatihan tidak dipungut biaya apapun!! (100% Gratis). Adapun fasilitas yang akan didapat adalah \r\nProgram Pelatihan Non Boarding (tidak diasramakan) :\r\nMakan siang per hari\r\nBantuan transport harian Rp 10.000\r\nModul pelatihan\r\nSepatu praktik\r\nBaju praktik\r\nATK\r\n\r\nProgram Pelatihan Boarding (diasramakan) :\r\nPenginapan\r\nMakan 3 x sehari\r\nPergantian biaya transport keberangkatan dan kepulangan dari BLK Bandung ke wilayah asal\r\nModul pelatihan\r\nSepatu praktik\r\nBaju praktik\r\nATK\r\n\r\nProgram pelatihan boarding digunakan oleh peserta yang berasal dari luar Kota Bandung, sedangkan program pelatihan non boarding digunakan oleh peserta yang berasal dari Kota Bandung. Kuota tiap gelombang terbatas, pastikan anda terdaftar disalah satu gelombang yang tersedia, adapun jadwal pelatihan bisa di lihat pada link berikut ini http://blkbandung.kemnaker.go.id/pendaftaran\r\nProses Pendaftaran Pelatihan\r\n\r\nAda 2 (dua) mekanisme pendaftaran pelatihan, yaitu datang langsung ke BLK Bandung dengan membawa persyaratan dan mengisi form pendaftaran, serta cara yang kedua secara online menggunakan link http://blkbandung.kemnaker.go.id/pendaftaran/daftar \r\nUntuk info lanjut dapat mengunjungi website kami di http://blkbandung.kemnaker.go.id/ atau http://www.kios3in1.net/001/ dan Admin Kios kami :\r\nPak Asep (0812-2153-3345)\r\nIbu Yusnita (0856-5928-0995)\r\nAdmin Medsos (0878-2190-6504)', '5.jpg', '2017-07-07 15:42:54');

-- --------------------------------------------------------

--
-- Table structure for table `blk`
--

CREATE TABLE IF NOT EXISTS `blk` (
  `id_blk` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT NULL,
  `alamat` text,
  `no_tlp` varchar(15) DEFAULT NULL,
  `email` varchar(35) DEFAULT NULL,
  `jam_kerja` varchar(50) DEFAULT NULL,
  `facebook` varchar(50) DEFAULT NULL,
  `twitter` varchar(50) DEFAULT NULL,
  `instagram` varchar(50) DEFAULT NULL,
  `info` text,
  `tentang` text,
  `visi` text,
  `misi` text,
  `struktur_org` varchar(100) DEFAULT NULL,
  `foto` varchar(100) DEFAULT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_blk`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `blk`
--

INSERT INTO `blk` (`id_blk`, `nama`, `alamat`, `no_tlp`, `email`, `jam_kerja`, `facebook`, `twitter`, `instagram`, `info`, `tentang`, `visi`, `misi`, `struktur_org`, `foto`, `last_update`) VALUES
(1, 'BLK Bandung', 'Jl. Jenderal Gatot Subroto No. 170 Bandung', '022 731 2564', 'admin@blkbandung.com', 'Senin - Jumat: 07:30 s.d 16:00', 'https://web.facebook.com/pelatihan.kerja.gratis.ba', 'https://twitter.com/bbplk_bdg', 'https://www.instagram.com/bbplk_bdg/', '<p>Untuk informasi pelatihan dan sertifikasi dapat langsung mengunjungi Kantor Balai Besar Pengembangan Latihan Kerja Bandung di alamat berikut :</p><br>\r\n<ul>\r\n<li><strong>Jl. Jenderal Gatot Subroto No. 170 Bandung</strong></li>\r\n<li>No Telepon - (022) 731 2564</li>\r\n<li>Fax - (022) 731 2564</li>\r\n</ul>\r\n<br>\r\n<p>Kantor BLK dapat dikunjungi pada hari Senin s.d. Jumat pada pukul 07.30 s.d. 16.00.</p>\r\n<p>Calon peserta pelatihan diharapkan membawa kelengkapan dokumen berupa fotocopy KTP, fotocopy ijazah terakhir, dan pasfoto ukuran 3x4 berwarna background merah sebanyak 4 rangkap.</p>', '<p>BLK Bandung adalah pusat pelatihan terlengkap untuk menyelenggarakan program pelatihan dan pengembangan kompetensi dan keterampilan sumber daya manusia di bidang industri jasa dan manufaktur. Kemitraan dengan berbagai institusi di dalam maupun Luar Negeri merupakan salah satu langkah strategi BLK Bandung guna menunjang kualitas program pelatihan serta menambah ruang lingkup pelayanannya. BLK Bandung adalah UPTP di bidang pengembangan pelatihan instruktur, tenaga pelatih serta tenaga kerja yang berada di bawah dan bertanggung jawab kepada Direktur Jenderal Pembinan Pelatihan dan Produktivitas Kemenakertrans R.I.</p>', 'Mewujudkan BLK Bandung sebagai "Center of Excelence, Center of Development, Center of Empowerment" di bidang pendidikan dan pelatihan dalam rangka mendukung kebijakan dan program ketenagakerjaan.', '<ul>\r\n<li>Melaksanakan Diklat Instruktur dan Tenaga Kerja.</li>\r\n<li>Melaksanakan Pengembangan Sumber Daya Pelatihan</li>\r\n<li>Melaksanakan Konsultasi dan Bimbingan Penyelenggaraan Diklat</li></ul><ul>\r\n</ul>', 'struktur_org.jpg', '1.jpg', '2017-07-07 11:42:30');

-- --------------------------------------------------------

--
-- Table structure for table `detail_asesmen`
--

CREATE TABLE IF NOT EXISTS `detail_asesmen` (
  `id_detail_asesmen` int(11) NOT NULL AUTO_INCREMENT,
  `kode_unit` varchar(20) DEFAULT NULL,
  `id_asesor1` int(11) DEFAULT NULL,
  `id_asesor2` int(11) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_detail_asesmen`),
  KEY `kode_unit` (`kode_unit`),
  KEY `id_asesor1` (`id_asesor1`),
  KEY `id_asesor2` (`id_asesor2`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `detail_asesmen`
--


-- --------------------------------------------------------

--
-- Table structure for table `detail_fasilitas`
--

CREATE TABLE IF NOT EXISTS `detail_fasilitas` (
  `id_detail` int(11) NOT NULL AUTO_INCREMENT,
  `id_fasilitas` int(11) NOT NULL,
  `detail` text NOT NULL,
  `foto` varchar(200) NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_detail`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `detail_fasilitas`
--

INSERT INTO `detail_fasilitas` (`id_detail`, `id_fasilitas`, `detail`, `foto`, `last_update`) VALUES
(1, 1, '<ol>\r\n <li>detail 1</li>\r\n<li>detail 2</li>\r\n<li>detail 3</li>\r\n<li>detail 4</li>\r\n</ol>', '1.jpg', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `detail_pelatihan`
--

CREATE TABLE IF NOT EXISTS `detail_pelatihan` (
  `id_detail_pelatihan` int(11) NOT NULL AUTO_INCREMENT,
  `id_pelatihan` int(11) NOT NULL,
  `program_pelatihan` varchar(100) DEFAULT NULL,
  `tahun` year(4) DEFAULT NULL,
  `urutan` int(11) NOT NULL,
  `tahap` int(11) NOT NULL,
  `status` enum('Perencanaan','Pelaksanaan') DEFAULT NULL,
  `asrama` enum('Boarding','Non Boarding') DEFAULT NULL,
  `tgl_pendaftaran_mulai` date DEFAULT NULL,
  `tgl_pendaftaran_selesai` date DEFAULT NULL,
  `tgl_rekrutmen` date DEFAULT NULL,
  `tgl_pengumuman` date DEFAULT NULL,
  `tgl_daftar_ulang` date DEFAULT NULL,
  `tgl_mulai` date DEFAULT NULL,
  `tgl_selesai` date DEFAULT NULL,
  `tgl_ujk` date DEFAULT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `p` int(11) DEFAULT '0',
  `l` int(11) DEFAULT '0',
  `sd` int(11) DEFAULT '0',
  `smp` int(11) DEFAULT '0',
  `sma` int(11) DEFAULT '0',
  `s1` int(11) DEFAULT '0',
  `mandiri` int(11) DEFAULT '0',
  `industri` int(11) DEFAULT '0',
  PRIMARY KEY (`id_detail_pelatihan`),
  KEY `id_pelatihan` (`id_pelatihan`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=59 ;

--
-- Dumping data for table `detail_pelatihan`
--

INSERT INTO `detail_pelatihan` (`id_detail_pelatihan`, `id_pelatihan`, `program_pelatihan`, `tahun`, `urutan`, `tahap`, `status`, `asrama`, `tgl_pendaftaran_mulai`, `tgl_pendaftaran_selesai`, `tgl_rekrutmen`, `tgl_pengumuman`, `tgl_daftar_ulang`, `tgl_mulai`, `tgl_selesai`, `tgl_ujk`, `last_update`, `p`, `l`, `sd`, `smp`, `sma`, `s1`, `mandiri`, `industri`) VALUES
(1, 5, 'Mekanik Mobil Bensin 1', 2017, 1, 1, 'Pelaksanaan', 'Boarding', '2016-12-01', '2017-01-03', '2017-01-05', '2017-01-10', '2017-01-10', '2017-01-23', '2017-04-05', '2017-04-05', '0000-00-00 00:00:00', 0, 16, 1, 0, 14, 1, 0, 0),
(2, 5, 'Mekanik Mobil Bensin 2', 2017, 2, 1, 'Pelaksanaan', 'Boarding', '2016-12-01', '2017-01-03', '2017-01-05', '2017-01-10', '2017-01-10', '2017-01-16', '2017-04-05', '2017-04-06', '2017-06-21 17:41:12', 0, 16, 1, 1, 14, 0, 0, 0),
(3, 3, 'Mekanik Mobil EFI 1', 2017, 1, 1, 'Pelaksanaan', 'Non Boarding', '2016-12-01', '2017-01-03', '2017-01-05', '2017-01-10', '2017-01-10', '2017-01-16', '2017-02-10', '2017-02-13', '2017-06-21 17:41:12', 0, 16, 0, 0, 15, 1, 0, 0),
(4, 2, 'Operator CNC CAD 1', 2017, 1, 1, 'Pelaksanaan', 'Boarding', '2016-12-01', '2017-01-03', '2017-01-05', '2017-01-10', '2017-01-10', '2017-01-16', '2017-03-07', '2017-03-08', '2017-06-22 11:08:44', 1, 15, 0, 0, 12, 4, 0, 2),
(5, 1, 'Operator CAD CAM 1', 2017, 1, 1, 'Pelaksanaan', 'Boarding', '2016-12-01', '2017-01-03', '2017-01-05', '2017-01-10', '2017-01-10', '2017-02-06', '2017-02-21', '2017-02-22', '2017-06-22 11:08:44', 1, 15, 0, 0, 13, 3, 0, 1),
(6, 1, 'Drafter Mesin 1', 2017, 1, 1, 'Pelaksanaan', 'Non Boarding', '2016-12-01', '2017-01-03', '2017-01-05', '2017-01-10', '2017-01-10', '2017-01-16', '2017-02-24', '2017-02-27', '2017-06-22 11:12:55', 0, 16, 0, 1, 14, 1, 0, 2),
(7, 2, 'Operator Mesin Produksi 1', 2017, 1, 1, 'Pelaksanaan', 'Boarding', '2016-12-01', '2017-01-03', '2017-01-05', '2017-01-10', '2017-01-10', '2017-01-16', '2017-03-07', '2017-03-08', '2017-06-22 11:12:55', 1, 15, 0, 0, 13, 3, 0, 3),
(8, 1, 'Operator Kerja Plat 1', 2017, 1, 1, 'Pelaksanaan', 'Non Boarding', '2016-12-01', '2017-01-03', '2017-01-05', '2017-01-10', '2017-01-10', '2017-01-16', '2017-02-24', '2017-02-27', '2017-06-22 12:36:20', 0, 16, 1, 1, 13, 1, 0, 1),
(9, 2, 'Operator Mesin Produksi 1', 2017, 1, 1, 'Pelaksanaan', 'Non Boarding', '2016-12-01', '2017-01-03', '2017-01-05', '2017-01-10', '2017-01-10', '2017-02-06', '2017-03-14', '2017-03-15', '2017-06-22 12:36:20', 0, 16, 0, 0, 14, 2, 0, 2),
(10, 4, 'Mekanik Body Repair 1', 2017, 1, 2, 'Pelaksanaan', 'Non Boarding', '2017-01-16', '2017-01-27', '2017-01-31', '2017-02-01', '2017-02-02', '2017-02-27', '2017-04-10', '2017-04-11', '2017-06-22 12:46:55', 0, 16, 0, 1, 13, 2, 0, 0),
(11, 5, 'Mekanik Mobil Bensin 1', 2017, 1, 2, 'Pelaksanaan', 'Non Boarding', '2017-01-16', '2017-01-27', '2017-01-31', '2017-02-01', '2017-02-02', '2017-02-06', '2017-05-04', '2017-05-05', '2017-06-22 12:46:55', 0, 16, 0, 2, 12, 2, 0, 0),
(12, 5, 'Mekanik Mobil Bensin 3', 2017, 3, 2, 'Pelaksanaan', 'Boarding', '2017-01-16', '2017-01-27', '2017-01-31', '2017-02-01', '2017-02-02', '2017-02-06', '2017-04-15', '2017-04-17', '2017-06-22 12:51:26', 0, 16, 1, 3, 11, 1, 0, 1),
(13, 3, 'Mekanik Mobil EFI 2', 2017, 2, 2, 'Pelaksanaan', 'Non Boarding', '2017-01-16', '2017-01-27', '2017-01-31', '2017-02-01', '2017-02-02', '2017-02-06', '2017-03-03', '2017-03-06', '2017-06-22 12:51:26', 0, 16, 0, 0, 16, 0, 0, 1),
(14, 3, 'Mekanik Spooring Balancing 1', 2017, 1, 2, 'Pelaksanaan', 'Boarding', '2017-01-16', '2017-01-27', '2017-01-31', '2017-02-01', '2017-02-02', '2017-02-06', '2017-02-28', '2017-03-01', '2017-06-22 12:53:41', 0, 16, 0, 2, 14, 0, 0, 0),
(15, 1, 'CAD CAM 1', 2017, 1, 2, 'Pelaksanaan', 'Non Boarding', '2017-01-16', '2017-01-27', '2017-02-09', '2017-02-13', '2017-02-13', '2017-02-27', '2017-04-26', '2017-04-27', '2017-06-22 13:00:28', 1, 15, 0, 1, 14, 1, 0, 4),
(16, 1, 'Drafter Mesin 1', 2017, 1, 2, 'Pelaksanaan', 'Boarding', '2017-01-16', '2017-01-27', '2017-02-09', '2017-02-13', '2017-02-13', '2017-02-27', '2017-03-31', '2017-04-01', '2017-06-22 13:00:28', 2, 14, 0, 0, 14, 2, 0, 4),
(17, 2, 'Operator Mesin Produksi 2', 2017, 2, 2, 'Pelaksanaan', 'Boarding', '2017-01-16', '2017-01-27', '2017-02-09', '2017-02-13', '2017-02-13', '2017-02-27', '2017-04-12', '2017-04-13', '2017-06-22 13:00:28', 0, 16, 0, 1, 13, 2, 0, 7),
(18, 2, 'Operator Mesin Produksi 2', 2017, 1, 2, 'Pelaksanaan', 'Non Boarding', '2017-01-16', '2017-01-27', '2017-02-09', '2017-02-13', '2017-02-13', '2017-02-27', '2017-04-26', '2017-04-27', '2017-06-22 13:00:28', 0, 16, 0, 0, 13, 3, 0, 1),
(19, 1, 'Operator Produksi CNC 1', 2017, 1, 2, 'Pelaksanaan', 'Boarding', '2017-01-16', '2017-01-27', '2017-02-09', '2017-02-13', '2017-02-13', '2017-02-27', '2017-03-31', '2017-04-01', '2017-06-22 13:00:28', 0, 16, 0, 2, 13, 1, 0, 0),
(20, 2, 'Operator CNC CAD CAM 1', 2017, 1, 2, 'Pelaksanaan', 'Non Boarding', '2017-01-16', '2017-01-27', '2017-02-09', '2017-02-13', '2017-02-13', '2017-02-27', '2017-04-10', '2017-04-11', '2017-06-22 13:01:48', 0, 16, 0, 0, 11, 5, 0, 0),
(21, 4, 'Body Repair 2', 2017, 2, 3, 'Pelaksanaan', 'Non Boarding', '2017-02-06', '2017-03-10', '2017-03-14', '2017-03-16', '2017-03-16', '2017-03-20', '2017-05-04', '2017-05-05', '2017-06-22 13:09:42', 0, 16, 2, 9, 5, 0, 0, 0),
(22, 3, 'Mekanik Mobil EFI 3', 2017, 3, 3, 'Pelaksanaan', 'Non Boarding', '2017-02-06', '2017-03-10', '2017-03-14', '2017-03-16', '2017-03-16', '2017-03-20', '2017-04-18', '2017-04-19', '2017-06-22 13:09:42', 0, 16, 0, 2, 13, 1, 0, 0),
(23, 5, 'Mekanik Mobil Bensin 2', 2017, 2, 3, 'Pelaksanaan', 'Non Boarding', '2017-02-06', '2017-03-10', '2017-03-14', '2017-03-16', '2017-03-16', '2017-03-20', '2017-06-20', '2017-06-21', '2017-06-22 13:11:29', 1, 15, 0, 2, 13, 2, 0, 0),
(24, 4, 'Body Repair 1', 2017, 1, 3, 'Pelaksanaan', 'Boarding', '2017-02-06', '2017-03-10', '2017-03-14', '2017-03-16', '2017-03-16', '2017-03-20', '2017-04-21', '2017-04-22', '2017-06-22 13:11:29', 0, 16, 3, 7, 6, 0, 0, 1),
(25, 4, 'Body Repair 2', 2017, 2, 3, 'Pelaksanaan', 'Boarding', '2017-02-06', '2017-03-10', '2017-03-14', '2017-03-16', '2017-03-16', '2017-03-20', '2017-04-21', '2017-04-22', '2017-06-22 13:13:02', 0, 16, 6, 9, 1, 0, 0, 0),
(26, 4, 'Body Repair 3', 2017, 3, 3, 'Pelaksanaan', 'Boarding', '2017-02-06', '2017-03-10', '2017-03-14', '2017-03-16', '2017-03-16', '2017-03-20', '2017-04-21', '2017-04-22', '2017-06-22 13:13:02', 0, 16, 2, 13, 1, 0, 0, 0),
(27, 1, 'Drafter Mesin 2', 2017, 2, 3, 'Pelaksanaan', 'Non Boarding', '2017-02-27', '2017-03-10', '2017-03-14', '2017-03-16', '2017-03-16', '2017-03-20', '2017-05-04', '2017-05-05', '2017-06-22 13:20:14', 2, 14, 0, 0, 13, 3, 0, 0),
(28, 2, 'Operator CNC CAD CAM 2', 2017, 2, 3, 'Pelaksanaan', 'Non Boarding', '2017-02-27', '2017-03-10', '2017-03-14', '2017-03-16', '2017-03-16', '2017-03-20', '2017-05-19', '2017-05-22', '2017-06-22 13:20:14', 0, 16, 0, 0, 9, 7, 0, 1),
(29, 1, 'Operator CNC Milling 1', 2017, 1, 3, 'Pelaksanaan', 'Boarding', '2017-02-27', '2017-03-10', '2017-03-14', '2017-03-16', '2017-03-16', '2017-03-20', '2017-04-21', '2017-04-22', '2017-06-22 13:20:14', 0, 16, 0, 5, 9, 2, 0, 0),
(30, 1, 'Operator Proses Produksi CNC 1', 2017, 1, 3, 'Pelaksanaan', 'Non Boarding', '2017-02-27', '2017-03-10', '2017-03-14', '2017-03-16', '2017-03-16', '2017-03-20', '2017-05-04', '2017-05-05', '2017-06-22 13:20:14', 0, 16, 0, 3, 13, 0, 0, 0),
(31, 2, 'Operator Mesin Produksi 3', 2017, 3, 3, 'Pelaksanaan', 'Non Boarding', '2017-02-27', '2017-03-10', '2017-03-14', '2017-03-16', '2017-03-16', '2017-03-20', '2017-05-19', '2017-05-22', '2017-06-22 13:20:14', 0, 16, 0, 0, 15, 1, 0, 0),
(32, 3, 'Mekanik Spooring Balancing 1', 2017, 1, 4, 'Pelaksanaan', 'Non Boarding', '2017-03-20', '2017-04-07', '2017-04-10', '2017-04-13', '2017-04-17', '2017-04-25', '2017-05-24', '2017-05-26', '2017-06-22 13:25:46', 0, 16, 0, 1, 15, 0, 0, 0),
(33, 5, 'Mekanik Mobil Bensin 3', 2017, 3, 4, 'Pelaksanaan', 'Non Boarding', '2017-03-20', '2017-04-07', '2017-04-10', '2017-04-13', '2017-04-17', '2017-04-25', '2017-08-04', '2017-08-07', '2017-06-22 13:25:46', 0, 16, 0, 0, 16, 0, 0, 0),
(34, 5, 'Mekanik Mobil Bensin 4', 2017, 4, 4, 'Pelaksanaan', 'Boarding', '2017-03-20', '2017-04-07', '2017-04-10', '2017-04-13', '2017-04-17', '2017-04-25', '2017-06-05', '2017-06-06', '2017-06-22 13:25:46', 0, 16, 0, 0, 16, 0, 0, 0),
(35, 3, 'Mekanik EFI Sistem 1', 2017, 1, 4, 'Pelaksanaan', 'Boarding', '2017-03-20', '2017-04-07', '2017-04-10', '2017-04-13', '2017-04-17', '2017-04-25', '2017-05-18', '2017-05-19', '2017-06-22 13:25:46', 0, 16, 0, 0, 16, 0, 0, 0),
(36, 3, 'Mekanik Spooring Balancing 2', 2017, 2, 4, 'Pelaksanaan', 'Boarding', '2017-03-20', '2017-04-07', '2017-04-10', '2017-04-13', '2017-04-17', '2017-04-25', '2017-05-18', '2017-05-19', '2017-06-22 13:25:46', 0, 16, 3, 4, 9, 0, 0, 0),
(37, 2, 'Operator CNC CAD CAM 3', 2017, 3, 4, 'Pelaksanaan', 'Non Boarding', '2017-03-20', '2017-04-07', '2017-04-10', '2017-04-13', '2017-04-17', '2017-04-25', '2017-07-07', '2017-07-10', '2017-06-22 13:40:37', 0, 16, 0, 0, 15, 1, 0, 0),
(38, 1, 'Operator Kerja Plat 1', 2017, 1, 4, 'Pelaksanaan', 'Boarding', '2017-03-20', '2017-04-07', '2017-04-10', '2017-04-13', '2017-04-17', '2017-04-25', '2017-05-29', '2017-05-30', '2017-06-22 13:40:37', 0, 16, 0, 1, 15, 0, 0, 0),
(39, 2, 'Operator Mesin Produksi 3', 2017, 3, 4, 'Pelaksanaan', 'Boarding', '2017-03-20', '2017-04-07', '2017-04-10', '2017-04-13', '2017-04-17', '2017-04-25', '2017-06-10', '2017-06-12', '2017-06-22 13:40:37', 0, 16, 1, 1, 13, 1, 0, 0),
(40, 2, 'Operator Mesin Produksi 4', 2017, 4, 4, 'Pelaksanaan', 'Non Boarding', '2017-03-20', '2017-04-07', '2017-04-10', '2017-04-13', '2017-04-17', '2017-04-25', '2017-07-07', '2017-07-10', '2017-06-22 13:40:37', 1, 15, 0, 0, 16, 0, 0, 0),
(41, 1, 'Operator Produksi CNC 2', 2017, 2, 4, 'Pelaksanaan', 'Non Boarding', '2017-03-20', '2017-04-07', '2017-04-10', '2017-04-13', '2017-04-17', '2017-04-25', '2017-05-29', '2017-05-30', '2017-06-22 13:40:37', 1, 15, 0, 0, 16, 0, 0, 0),
(42, 3, 'Mekanik Mobil EFI 4', 2017, 4, 5, 'Pelaksanaan', 'Non Boarding', '2017-04-20', '2017-05-03', '2017-05-05', '2017-05-08', '2017-05-09', '2017-05-15', '2017-06-13', '2017-06-14', '2017-06-22 15:13:37', 1, 15, 0, 0, 16, 0, 0, 0),
(43, 3, 'Mekanik Mobil EFI 5', 2017, 5, 5, 'Pelaksanaan', 'Non Boarding', '2017-04-20', '2017-05-03', '2017-05-05', '2017-05-08', '2017-05-09', '2017-05-15', '2017-06-13', '2017-06-14', '2017-06-22 15:13:37', 0, 16, 0, 0, 16, 0, 0, 0),
(44, 5, 'Mekanik Mobil Bensin 4', 2017, 4, 5, 'Pelaksanaan', 'Non Boarding', '2017-04-20', '2017-05-03', '2017-05-05', '2017-05-08', '2017-05-09', '2017-05-15', '2017-08-23', '2017-08-24', '2017-06-22 15:13:37', 0, 16, 0, 0, 15, 1, 0, 0),
(45, 5, 'Mekanik Mobil Bensin 5', 2017, 5, 5, 'Pelaksanaan', 'Non Boarding', '2017-04-20', '2017-05-03', '2017-05-05', '2017-05-08', '2017-05-09', '2017-05-15', '2017-08-23', '2017-08-24', '2017-06-22 15:13:37', 0, 16, 0, 0, 16, 0, 0, 0),
(46, 4, 'Body Repair 3', 2017, 3, 5, 'Pelaksanaan', 'Non Boarding', '2017-04-20', '2017-05-03', '2017-05-05', '2017-05-08', '2017-05-09', '2017-05-15', '2017-07-11', '2017-07-12', '2017-06-22 15:13:37', 1, 15, 0, 1, 14, 0, 0, 0),
(47, 3, 'Mekanik EFI Sistem 3', 2017, 3, 5, 'Pelaksanaan', 'Boarding', '2017-04-20', '2017-05-03', '2017-05-05', '2017-05-08', '2017-05-09', '2017-05-22', '2017-06-13', '2017-06-14', '2017-06-22 15:17:25', 0, 16, 3, 1, 12, 0, 0, 0),
(48, 3, 'Mekanik Efi Sistem 4', 2017, 4, 5, 'Pelaksanaan', 'Boarding', '2017-04-20', '2017-05-03', '2017-05-05', '2017-05-08', '2017-05-09', '2017-05-22', '2017-06-13', '2017-06-14', '2017-06-22 15:17:25', 0, 16, 0, 3, 12, 1, 0, 0),
(49, 3, 'Mekanik Spooring Balancing 3', 2017, 3, 5, 'Pelaksanaan', 'Boarding', '2017-04-20', '2017-05-03', '2017-05-05', '2017-05-08', '2017-05-09', '2017-05-22', '2017-06-13', '2017-06-14', '2017-06-22 15:17:25', 0, 16, 0, 0, 16, 0, 0, 0),
(50, 1, 'Operator CNC Milling 1', 2017, 1, 5, 'Pelaksanaan', 'Non Boarding', '2017-04-20', '2017-05-03', '2017-05-05', '2017-05-08', '2017-05-09', '2017-05-15', '2017-07-11', '2017-07-12', '2017-06-22 15:45:22', 3, 13, 0, 0, 14, 2, 0, 0),
(51, 1, 'Operator CNC Milling 2', 2017, 2, 5, 'Pelaksanaan', 'Non Boarding', '2017-04-20', '2017-05-03', '2017-05-05', '2017-05-08', '2017-05-09', '2017-05-15', '2017-07-11', '2017-07-12', '2017-06-22 15:45:22', 2, 14, 0, 0, 16, 0, 0, 0),
(52, 1, 'Operator CAD CAM 2', 2017, 2, 5, 'Pelaksanaan', 'Non Boarding', '2017-04-20', '2017-05-03', '2017-05-05', '2017-05-08', '2017-05-09', '2017-05-15', '2017-07-11', '2017-07-12', '2017-06-22 15:45:22', 3, 13, 0, 0, 14, 2, 0, 0),
(53, 2, 'Operator CNC CAD CAM 4', 2017, 4, 5, 'Pelaksanaan', 'Non Boarding', '2017-04-20', '2017-05-03', '2017-05-05', '2017-05-08', '2017-05-09', '2017-05-15', '2017-07-25', '2017-07-26', '2017-06-22 15:45:22', 0, 16, 0, 0, 16, 0, 0, 0),
(54, 2, 'Operator CNC CAD CAM 5', 2017, 5, 5, 'Pelaksanaan', 'Non Boarding', '2017-04-20', '2017-05-03', '2017-05-05', '2017-05-08', '2017-05-09', '2017-05-15', '2017-07-25', '2017-07-26', '2017-06-22 15:45:22', 1, 15, 0, 0, 16, 0, 0, 0),
(55, 2, 'Operator Mesin Produksi 4', 2017, 4, 5, 'Pelaksanaan', 'Non Boarding', '2017-04-20', '2017-05-03', '2017-05-05', '2017-05-08', '2017-05-09', '2017-05-15', '2017-07-25', '2017-07-26', '2017-06-22 15:47:49', 0, 16, 0, 1, 15, 0, 0, 0),
(56, 2, 'Operator Mesin Produksi 5', 2017, 5, 5, 'Pelaksanaan', 'Non Boarding', '2017-04-20', '2017-05-03', '2017-05-05', '2017-05-08', '2017-05-09', '2017-05-15', '2017-07-25', '2017-07-26', '2017-06-22 15:47:49', 1, 15, 0, 0, 16, 0, 0, 0),
(57, 1, 'Drafter Mesin 2', 2017, 2, 5, 'Pelaksanaan', 'Boarding', '2017-04-20', '2017-05-03', '2017-05-05', '2017-05-08', '2017-05-09', '2017-05-12', '2017-06-13', '2017-06-14', '2017-06-22 15:52:59', 0, 16, 0, 1, 15, 0, 0, 0),
(58, 1, 'Operator Kerja Plat 2', 2017, 2, 5, 'Pelaksanaan', 'Boarding', '2017-04-20', '2017-05-03', '2017-05-05', '2017-05-08', '2017-05-09', '2017-05-12', '2017-06-13', '2017-06-14', '2017-06-22 15:52:59', 0, 16, 0, 2, 13, 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `fasilitas`
--

CREATE TABLE IF NOT EXISTS `fasilitas` (
  `id_fasilitas` int(11) NOT NULL AUTO_INCREMENT,
  `kepemilikan` varchar(6) DEFAULT NULL,
  `judul` varchar(30) DEFAULT NULL,
  `deskripsi` text,
  `foto` varchar(100) DEFAULT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_fasilitas`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `fasilitas`
--

INSERT INTO `fasilitas` (`id_fasilitas`, `kepemilikan`, `judul`, `deskripsi`, `foto`, `last_update`) VALUES
(1, 'umum', 'KANTIN', 'Luas Bangunan 356 M2 I Kapasitas Ruangan : 50 Orang.', '1.jpg', '2017-06-13 11:14:01'),
(2, 'umum', 'TEMPAT OLAHRAGA', 'Luas Bangunan 396 M2 I Kapasitas Ruangan : 300 Orang.', '2.jpg', '2017-06-13 11:14:01'),
(3, 'umum', 'AULA', 'Luas Bangunan 400 M2 I Kapasitas Ruangan : 100 Orang', '3.jpg', '2017-06-13 11:14:01'),
(4, 'umum', 'ASRAMA CENDRAWASIH', 'Luas Bangunan 878 M2 I Kapasitas Ruangan : 48 Orang', '4.jpg', '2017-06-13 11:14:01'),
(5, 'umum', 'ASRAMA GELATIK', 'Luas Bangunan 488 M2 I Kapasitas Ruangan : 56 Orang', '5.jpg', '2017-06-13 11:14:01'),
(6, 'umum', 'ASRAMA MALEO', 'Luas Bangunan 488 M2 I Kapasitas Ruangan : 56 Orang', '6.jpg', '2017-06-13 11:14:01'),
(7, 'umum', 'TEMPAT IBADAH', 'Luas Bangunan 122 M2 I Kapasitas Ruangan : 100 Orang.', '7.jpg', '2017-06-13 11:18:33'),
(8, 'umum', 'KIOS 3IN1', 'Luas Bangunan 150 M2 I Kapasitas Ruangan : 40 Orang', '8.jpg', '2017-06-13 11:18:33'),
(9, 'umum', 'PERPUSTAKAAN', 'Luas Bangunan 70 M2 I Kapasitas Ruangan : 20 Orang.', '9.jpg', '2017-06-13 11:18:33'),
(10, 'tekman', 'WORKSHOP TEKNIK MANUFAKTUR', 'Luas Bangunan 1250 M2 I Kapasitas Ruangan : 64 Orang.', '10.jpg', '2017-06-13 11:18:33'),
(11, 'tekman', 'WORKSHOP TEKNIK MANUFAKTUR - C', 'Luas Bangunan 100 M2 I Kapasitas Ruangan : 16 Orang.', '11.jpg', '2017-06-13 11:18:33'),
(12, 'tekman', 'RUANG KELAS TEKNIK MANUFAKTUR', 'Luas Bangunan 100 M2 I Kapasitas Ruangan : 16 Orang.', '12.jpg', '2017-06-13 11:23:47'),
(13, 'oto', 'WORKSHOP OTOMOTIF', 'Luas Bangunan 400 M2 I Kapasitas Ruangan : 48 Orang.', '13.jpg', '2017-06-13 11:23:47'),
(14, 'oto', 'WORKSHOP OTOMOTIF OTOMOTIF MOT', 'Luas Bangunan 200 M2 I Kapasitas Ruangan : 32 Orang.', '14.jpg', '2017-06-13 11:23:47'),
(15, 'oto', 'RUANG KELAS TEKNIK. OTOMOTIF –', 'Luas Bangunan 200 M2 I Kapasitas Ruangan : 32 Orang.', '15.jpg', '2017-06-13 11:23:47'),
(16, 'oto', 'RUANG KELAS TEKNIK. OTOMOTIF –', 'Luas Bangunan 100 M2 I Kapasitas Ruangan : 16 Orang', '16.jpg', '2017-06-13 11:23:47');

-- --------------------------------------------------------

--
-- Table structure for table `galeri`
--

CREATE TABLE IF NOT EXISTS `galeri` (
  `id_galeri` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(100) DEFAULT NULL,
  `foto` varchar(100) DEFAULT NULL,
  `tgl_upload` date DEFAULT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_galeri`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `galeri`
--

INSERT INTO `galeri` (`id_galeri`, `judul`, `foto`, `tgl_upload`, `last_update`) VALUES
(3, 'Apel Pagi', '2.jpg', '2016-11-16', '2017-06-03 13:40:21'),
(4, 'BLK Bandung', '9.jpg', '2016-11-16', '2017-06-03 13:40:21'),
(7, 'Pelatihan Tahun 2017', '10.jpg', '2016-11-16', '2017-06-15 12:26:33'),
(8, 'Seleksi Calon Peserta PBK', '13.jpg', '2016-11-16', '2017-06-03 13:40:21'),
(9, 'Pelatihan Otomotif', '14.jpg', '2016-11-16', '2017-06-03 13:40:21');

-- --------------------------------------------------------

--
-- Table structure for table `golongan`
--

CREATE TABLE IF NOT EXISTS `golongan` (
  `id_golongan` int(11) NOT NULL AUTO_INCREMENT,
  `golongan` varchar(4) DEFAULT NULL,
  `pangkat` varchar(50) DEFAULT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_golongan`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `golongan`
--

INSERT INTO `golongan` (`id_golongan`, `golongan`, `pangkat`, `last_update`) VALUES
(1, 'IIIa', 'Penata Muda', '0000-00-00 00:00:00'),
(2, 'IIIb', 'Penata Muda Tk. 1', '0000-00-00 00:00:00'),
(3, 'IIIc', 'Penata', '0000-00-00 00:00:00'),
(4, 'IIId', 'Penata Tk. 1', '0000-00-00 00:00:00'),
(5, 'IVa', 'Pembina', '0000-00-00 00:00:00'),
(6, 'IVb', 'Pembina Tk. 1', '0000-00-00 00:00:00'),
(7, 'IVc', 'Pembina Utama Muda', '0000-00-00 00:00:00'),
(8, 'IIa', 'Pengatur Muda', '2017-06-14 11:27:52'),
(9, 'IIb', 'Pengatur Muda Tk.I', '2017-06-14 11:27:52'),
(10, 'IIc', 'Pengatur', '2017-06-14 11:27:52'),
(11, 'IId', 'Pengatur Tk.I', '2017-06-14 11:27:52'),
(12, 'IVd', 'Pembina Utama Madya', '2017-06-14 11:27:52');

-- --------------------------------------------------------

--
-- Table structure for table `instruktur`
--

CREATE TABLE IF NOT EXISTS `instruktur` (
  `id_instruktur` int(11) NOT NULL AUTO_INCREMENT,
  `nip` varchar(18) DEFAULT NULL,
  `id_kejuruan` varchar(6) DEFAULT NULL,
  `keterangan` text,
  `facebook` varchar(100) DEFAULT NULL,
  `linkedin` varchar(100) DEFAULT NULL,
  `twitter` varchar(100) DEFAULT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_instruktur`),
  KEY `nip` (`nip`),
  KEY `id_kejuruan` (`id_kejuruan`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=46 ;

--
-- Dumping data for table `instruktur`
--

INSERT INTO `instruktur` (`id_instruktur`, `nip`, `id_kejuruan`, `keterangan`, `facebook`, `linkedin`, `twitter`, `last_update`) VALUES
(17, '198409162012121001', 'tekman', 'Kompetensi : K3, Permesinan Konvensional, Asesor Kompetensi.', '', '', '', '2017-06-14 11:02:52'),
(18, '196404201986031003', 'oto', 'Kompetensi :\r\nTeknisi Servis Sepeda Motor, Teknisi Engine Tune Sistem Injeksi, Teknisi Wheel Alignment, Teknisi Brake System, Teknisi Junior Kendaraan Ringan, Asesor Kompetensi Teknik Kendaraan Ringan dan Sepeda Motor.', NULL, NULL, NULL, '2017-06-14 11:02:52'),
(19, '196412091986021001', 'oto', 'Kompetensi :\r\nTeknik Otomotif Kendaraan Ringan\r\nTeknik Otomotif Sepeda Motor\r\nAsesor Kompetensi', '', '', '', '2017-06-13 18:41:18'),
(20, '196208141983031003', 'oto', 'Kompetensi :\r\nEmisi Gas Buang, Wheel Alignment, Engine Tune Up.', '', '', '', '2017-06-14 11:02:52'),
(21, '197901192003121002', 'oto', 'Kompetensi :\r\nTeknik Kendaraan Ringan, Asesor Kompetensi.', '', '', '', '2017-06-14 11:02:52'),
(22, '196512011986031002', 'tekman', 'Kompetensi :\r\nMesin Proses, Autocad, Asesor Kompetensi', '', '', '', '2017-06-14 11:02:52'),
(23, '197510242006041001', 'tekman', 'Kompetensi :\r\nBubut Dasar, Bubut Kompleks, Drafter CAD 3D, Asesor Kompetensi.', '', '', '', '2017-06-14 11:02:52'),
(24, '198505122015031003', 'tekman', 'Kompetensi :\r\nMesin Bubut, Mesin Frais.', '', '', '', '2017-06-14 11:02:52'),
(25, '198108232015031002', 'tekman', 'Kompetensi :\r\nMesin Bubut, Mesin Frais.', '', '', '', '2017-06-14 11:02:52'),
(26, '197205212005011001', 'tekman', 'Kompetensi :\r\nCNC CADCAM, Mould Maker, Drafter, CNC Milling, CNC Bubut, EDM, Wirecut, Asesor Kompetensi.', '', '', '', '2017-06-14 11:02:52'),
(27, '198107312003121002', 'tekman', 'Kompetensi : Metodologi petlaker, CDD & Pedagogy, CAD 3D (Inventor & Solidwork), Bubut dan Frais Konvensional, Asesor Kompetensi Logam Mesin, Trainer Softskill.', NULL, NULL, NULL, '2017-06-14 11:13:21'),
(28, '197610012003121002', 'tekman', 'Kompetensi : Mesin Produksi, CAD-CAM, Mesin CNC', NULL, NULL, NULL, '2017-06-14 11:20:24'),
(29, '199008042012122003', 'tekman', 'Kompetensi : Drafter, CNC CAD CAM, Bubut Dasar, Milling Dasar', NULL, NULL, NULL, '2017-06-14 11:31:38'),
(30, '198009222003121002', 'tekman', 'Kompetensi : Permesinan, T.Perawatan, Prestools, JIG & Fixture, Asesor.', NULL, NULL, NULL, '2017-06-14 11:37:56'),
(31, '196811132003121001', 'tekman', 'Kompetensi : Permesinan.', NULL, NULL, NULL, '2017-06-14 11:37:56'),
(32, '198908102012121003', 'tekman', 'Kompetensi : Permesinan (Bubut, Frais, CNC), Maintenance (Mesin Perkakas), Metodologi, Asesor Kompetensi.', NULL, NULL, NULL, '2017-06-14 11:42:46'),
(33, '197910192003122001', 'tekman', 'Kompetensi : Pesmesinan Konvensional, Perawatan Mesin Perkakas, Metodologi.', NULL, NULL, NULL, '2017-06-14 11:42:46'),
(34, '196910271998031001', 'tekman', 'Kompetensi : Permesinan, Perawatan, Metodologi, Asesor Kompetensi.', NULL, NULL, NULL, '2017-06-14 11:45:26'),
(35, '197505042007121001', 'oto', 'Kompetensi : Eto Injeksi, Brake Sis', NULL, NULL, NULL, '2017-06-14 12:46:49'),
(36, '197405222006041004', 'oto', 'Kompetensi : Teknik Sepeda Motor, Teknik Otomotif Kendaraan Ringan, Asesor Kompetensi.', NULL, NULL, NULL, '2017-06-14 12:46:49'),
(37, '198110282011011010', 'oto', 'Kompetensi : Kendaraan Ringan, Brake System, Metodologi Pelatihan, Asesor Kompetensi.', NULL, NULL, NULL, '2017-06-14 12:46:49'),
(38, '198201162009011007', 'oto', 'Kompetensi : Teknik Otomotif Kendaraan Ringan, Teknik Otomotif Sepeda Motor, Metodologi Pelatihan, Asesor Kompetensi.', NULL, NULL, NULL, '2017-06-14 12:46:49'),
(39, '197707012006041004', 'oto', 'Kompetensi : Engine Tune-Up injeksi, Wheel Alignment, Sepeda Motor, Body Repair.', NULL, NULL, NULL, '2017-06-14 12:46:49'),
(40, '198002072006041005', 'oto', 'Kompetensi : Engine Tune-Up injeksi, Wheel Alignment, Brake System, Asesor Kompetensi.', NULL, NULL, NULL, '2017-06-14 12:46:49'),
(41, '198412192012121001', 'oto', 'Kompetensi : Teknik Otomotif Kendaraan Ringan, Teknik Otomotif Sepeda Motor, Metodologi Pelatihan, Asesor Kompetensi.', NULL, NULL, NULL, '2017-06-14 12:46:49'),
(42, '198712132015031003', 'oto', 'Kompetensi : Sepeda Motor.', NULL, NULL, NULL, '2017-06-14 12:46:49'),
(43, '198001022009011006', 'oto', 'Kompetensi : Mobil Bensin / Diesel, Spooring Balancing, Efi System.', NULL, NULL, NULL, '2017-06-14 12:46:49'),
(44, '195802071981030100', 'oto', 'Kompetensi : Sepeda Motor.', NULL, NULL, NULL, '2017-06-14 12:46:49');

-- --------------------------------------------------------

--
-- Table structure for table `jabatan`
--

CREATE TABLE IF NOT EXISTS `jabatan` (
  `id_jabatan` int(11) NOT NULL AUTO_INCREMENT,
  `jabatan` varchar(50) DEFAULT NULL,
  `jobdesc` text,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_jabatan`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `jabatan`
--

INSERT INTO `jabatan` (`id_jabatan`, `jabatan`, `jobdesc`, `last_update`) VALUES
(1, 'Instruktur Pelaksana', 'Mengajar dan melatih siswa pelatihan', '0000-00-00 00:00:00'),
(2, 'Instruktur Pelaksana Lanjutan', 'Mengajar dan melatih siswa pelatihan', '0000-00-00 00:00:00'),
(3, 'Instruktur Penyelia', 'Mengajar dan melatih siswa pelatihan', '0000-00-00 00:00:00'),
(4, 'Instruktur Pertama', 'Mengajar dan melatih siswa pelatihan', '0000-00-00 00:00:00'),
(5, 'Instruktur Muda', 'Mengajar dan melatih siswa pelatihan', '0000-00-00 00:00:00'),
(6, 'Instruktur Madya', 'Mengajar dan melatih siswa pelatihan', '0000-00-00 00:00:00'),
(7, 'Staf Fungsional Umum', 'Staf Umum', '0000-00-00 00:00:00'),
(8, 'Calon Instruktur', 'Mengajar dan melatih siswa', '2017-06-14 11:54:04');

-- --------------------------------------------------------

--
-- Table structure for table `kejuruan`
--

CREATE TABLE IF NOT EXISTS `kejuruan` (
  `id_kejuruan` varchar(6) NOT NULL,
  `nama` varchar(30) DEFAULT NULL,
  `deskripsi` text,
  `foto` varchar(100) DEFAULT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_kejuruan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kejuruan`
--

INSERT INTO `kejuruan` (`id_kejuruan`, `nama`, `deskripsi`, `foto`, `last_update`) VALUES
('oto', 'Otomotif', 'Kejuruan Otomitif saat ini memiliki 15 tenaga pengajar/ instruktur dengan latar belakang D3,S1 dan S2 yang memiliki pengalaman belajar di dalam maupun luar negeri. Tenaga pengajar/ instruktur memiliki sertifikat kompetensi dari Lembaga Sertifikasi Profesi (LSP) dan memiliki lisensi asesor dari BNSP dan LSP Otomotif.<br>\r\nPelatihan yang ada untuk sub sektor kendaraan ringan yaitu Mekanik Mobil Bensin/Diesel, Mekanik Tune-Up EFI, Mekanik Spooring Balancing, Mekanik Body Repair.\r\nPeralatan yang tersedia antara lain : Mobil, Spooring Balancing, Scanner, Dyno Test, Oven, dan Motor.', '14.jpg', '2017-07-06 18:18:50'),
('tekman', 'Teknik Manufaktur', 'Kejuruan Teknik Manufaktur saat ini memiliki 16 tenaga pengajar/ instruktur dengan latar belakang D3,S1 dan S2 yang memiliki pengalaman belajar di dalam maupun luar negeri. Tenaga pengajar/ instruktur memiliki sertifikat kompetensi dari Lembaga Sertifikasi Profesi (LSP) dan memiliki lisensi asesor dari BNSP dan LSP Otomotif.<br>\r\nPeralatan yang tersedia antara lain : CNC Milling, CNC Bubut, Wire Cut, EDM, Mesin Potong, Tool Cutter Grinder, Plastic Injection Machine, dan Kerja Plat. ', NULL, '2017-06-19 14:28:06');

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE IF NOT EXISTS `log` (
  `id_log` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) DEFAULT NULL,
  `waktu` datetime DEFAULT NULL,
  `ip` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id_log`),
  KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=43 ;

--
-- Dumping data for table `log`
--

INSERT INTO `log` (`id_log`, `username`, `waktu`, `ip`) VALUES
(1, 'admin', '2016-09-01 09:17:09', '::1'),
(2, 'manajemen', '2016-10-11 12:33:49', '::1'),
(3, 'admin', '2016-10-15 16:14:24', '::1'),
(4, 'kajur', '2016-11-10 14:12:43', '::1'),
(5, 'manajemen', '2016-11-10 14:12:59', '::1'),
(6, 'admin', '2016-11-15 09:31:47', '::1'),
(7, 'admin', '2016-11-15 17:25:50', '::1'),
(8, 'manajemen', '2016-11-15 17:36:34', '::1'),
(9, 'pegawai', '2016-11-15 17:37:35', '::1'),
(10, 'pegawai', '2016-11-16 09:06:09', '::1'),
(11, 'manajemen', '2016-11-16 09:42:14', '::1'),
(12, 'kajur', '2016-11-16 10:24:56', '::1'),
(13, 'kajur', '2016-11-16 10:58:23', '::1'),
(14, 'admin', '2016-11-16 11:21:20', '::1'),
(15, 'pegawai', '2016-11-16 16:40:39', '::1'),
(16, 'kajur', '2016-11-16 16:46:21', '::1'),
(17, 'admin', '2016-11-16 16:55:01', '::1'),
(18, 'manajemen', '2016-11-16 16:57:54', '::1'),
(19, 'admin', '2016-11-16 17:40:11', '::1'),
(20, 'admin', '2016-11-17 07:45:21', '::1'),
(21, 'pegawai', '2016-11-17 07:57:06', '::1'),
(22, 'admin', '2016-11-17 07:57:36', '::1'),
(23, 'admin', '2016-11-17 12:03:27', '::1'),
(24, 'admin', '2016-11-17 12:06:27', '::1'),
(25, 'kajur', '2016-11-17 13:39:39', '::1'),
(26, 'admin', '2016-11-17 13:43:35', '::1'),
(27, 'manajemen', '2016-11-17 13:52:30', '::1'),
(28, 'admin', '2016-11-17 14:05:04', '::1'),
(29, 'pegawai', '2016-11-17 14:23:12', '::1'),
(30, 'admin', '2016-11-21 09:05:42', '::1'),
(31, 'admin', '2017-05-15 10:41:31', '::1'),
(32, 'admin', '2017-05-27 12:19:25', '::1'),
(33, 'admin', '2017-05-27 12:32:38', '::1'),
(34, 'admin', '2017-05-28 15:34:19', '::1'),
(35, 'admin', '2017-05-29 18:20:09', '::1'),
(36, 'admin', '2017-05-31 05:15:09', '::1'),
(37, 'kajur', '2017-05-31 05:16:58', '::1'),
(38, 'manajemen', '2017-05-31 05:17:20', '::1'),
(39, 'admin', '2017-05-31 05:17:56', '::1'),
(40, 'kajur', '2017-05-31 05:34:54', '::1'),
(41, 'admin', '2017-05-31 05:35:36', '::1'),
(42, 'admin', '2017-06-06 07:54:20', '::1');

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE IF NOT EXISTS `pegawai` (
  `nama` varchar(50) DEFAULT NULL,
  `nip` varchar(18) NOT NULL,
  `tempat_lahir` varchar(30) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `id_jabatan` int(11) DEFAULT NULL,
  `id_golongan` int(11) DEFAULT NULL,
  `tmt` date DEFAULT NULL,
  `pendidikan` enum('SMA','D3','S1','S2','S3') DEFAULT NULL,
  `jk` enum('Perempuan','Laki-Laki') DEFAULT NULL,
  `foto` varchar(100) DEFAULT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`nip`),
  KEY `id_jabatan` (`id_jabatan`),
  KEY `id_golongan` (`id_golongan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`nama`, `nip`, `tempat_lahir`, `tgl_lahir`, `id_jabatan`, `id_golongan`, `tmt`, `pendidikan`, `jk`, `foto`, `last_update`) VALUES
('Jaka Suherman Setia S., S.Pd.', '195802071981030100', 'Sumedang', '1958-02-07', 6, 5, '1981-03-01', 'S1', 'Laki-Laki', '195802071981030100.png', '2017-06-14 12:22:03'),
('Acep Suandi, S.Pd.', '196208141983031003', 'Bandung', '1962-08-14', 6, 5, '2015-04-01', 'S1', 'Laki-Laki', '196208141983031003.png', '2017-06-13 18:40:19'),
('Mujianto, S.T.', '196404201986031003', 'Blitar', '1964-04-20', 3, 3, '2015-10-01', 'S1', 'Laki-Laki', '196404201986031003.png', '2017-06-13 18:40:19'),
('Sariman, S.Pd.', '196412091986021001', 'Cilacap', '1964-12-09', 6, 5, '2015-10-01', 'S1', 'Laki-Laki', '196412091986021001.png', '2017-06-13 18:40:19'),
('Habib Tawang, S.Pd.', '196512011986031002', 'Takalar', '1965-12-01', 6, 5, '2011-04-01', 'S1', 'Laki-Laki', '196512011986031002.png', '2017-06-13 18:40:19'),
('Saeful Rohim', '196811132003121001', 'Bandung', '1968-11-13', 3, 3, NULL, 'S1', 'Laki-Laki', '196811132003121001.png', '2017-06-14 11:36:12'),
('Wahyono', '196910271998031001', 'Klaten', '1969-10-27', 6, 5, '2015-03-01', 'S1', 'Laki-Laki', '196910271998031001.png', '2017-06-14 11:44:18'),
('Heri Budiman, S.T.', '197205212005011001', 'Garut', '1972-05-21', 5, 3, '0000-00-00', 'S1', 'Laki-Laki', '197205212005011001.png', '2017-06-13 18:40:19'),
('Wahyu Firmansyah', '197405222006041004', 'Jakarta', '1974-05-22', 5, 4, '2015-04-01', 'S1', 'Laki-Laki', '197405222006041004.png', '2017-06-14 12:29:31'),
('Akhmad Ropik', '197505042007121001', 'Indramayu', '1975-05-04', 5, 4, '2016-10-01', 'S2', 'Laki-Laki', '197505042007121001.png', '2017-06-14 12:29:31'),
('Oktosa Indriatmoko', '197510242006041001', 'Batang', '1975-10-24', 5, 3, '2006-04-01', 'S1', 'Laki-Laki', '197510242006041001.png', '2017-06-13 18:40:19'),
('Taberian ', '197610012003121002', 'Jakarta', '1976-10-01', 5, 3, NULL, 'S2', 'Laki-Laki', '197610012003121002.png', '2017-06-14 11:19:27'),
('Hadi Prianto, S.T.', '197707012006041004', 'Tulungagung', '1977-07-01', 5, 3, NULL, 'S1', 'Laki-Laki', '197707012006041004.png', '2017-06-14 12:29:31'),
('Heru Wijayanto', '197901192003121002', 'Jakarta', '1979-01-19', 3, 2, '0000-00-00', 'D3', 'Laki-Laki', '197901192003121002.png', '2017-06-13 18:40:19'),
('Farida Noor Fatimah', '197910192003122001', 'Surabaya', '1979-10-19', 3, 3, '2015-04-01', 'S1', 'Perempuan', '197910192003122001.png', '2017-06-14 11:40:52'),
('Ari Kustiandi Ariffien', '198001022009011006', 'Tasikmalaya', '1980-01-02', 5, 3, '2014-04-01', 'S2', 'Laki-Laki', '198001022009011006.png', '2017-06-14 12:22:03'),
('Medianto, S.T., M.Si.', '198002072006041005', 'Jakarta', '1980-02-07', 5, 4, '2016-10-01', 'S2', 'Laki-Laki', '198002072006041005.png', '2017-06-14 12:22:03'),
('Ariyono Eko Prasetya', '198009222003121002', 'Klaten', '1980-09-22', 3, 3, '2015-04-01', 'S1', 'Laki-Laki', '198009222003121002.png', '2017-06-14 11:36:12'),
('Nur Akbarudin, S.T., M.T.', '198107312003121002', 'Jakarta', '1981-07-31', 5, 4, '2016-10-01', 'S2', 'Laki-Laki', '198107312003121002.png', '2017-06-14 11:10:57'),
('Agustin, S.Kom.', '198108232015031002', 'Klaten', '1981-08-23', 4, 1, '0000-00-00', 'S1', 'Perempuan', '198108232015031002.png', '2017-06-13 18:40:19'),
('Yudyana Prasetya W., S.T.', '198110282011011010', 'Yogyakarta', '1981-10-28', 5, 2, NULL, 'S1', 'Laki-Laki', '198110282011011010.png', '2017-06-14 12:29:31'),
('Andri, S.T.', '198201162009011007', 'Cirebon', '1982-01-16', 5, 1, '2015-04-01', 'S1', 'Laki-Laki', '198201162009011007.png', '2017-06-14 12:29:31'),
('Adhe Prihandana .G', '198409162012121001', 'Banyumas', '1984-09-16', 4, 1, '2012-12-01', 'S1', 'Laki-Laki', '198409162012121001.png', '2017-06-13 18:40:19'),
('Andi Prasetyo', '198412192012121001', 'Bogor', '1984-12-19', 2, 11, '2012-12-01', 'D3', 'Laki-Laki', '198412192012121001.png', '2017-06-14 12:22:03'),
('Wawan Prasetyo', '198505122015031003', 'Jakarta', '1985-05-12', 4, 1, '0000-00-00', 'S1', 'Laki-Laki', '198505122015031003.png', '2017-06-13 18:40:19'),
('Purwo Hadi Trapsilo', '198712132015031003', 'Jakarta', '1987-12-13', 8, 10, '2015-03-01', 'D3', 'Laki-Laki', '198712132015031003.png', '2017-06-14 12:22:03'),
('Belly Rachmat', '198810032012122004', '', '0000-00-00', 7, 1, '0000-00-00', 'S1', 'Laki-Laki', 'belly.jpg', '2017-07-08 12:11:55'),
('Dimas Ari Prasetyo', '198908102012121003', 'Cilacap', '1989-08-10', 4, 1, '2012-12-01', 'S1', 'Laki-Laki', '198908102012121003.png', '2017-06-14 11:40:52'),
('Furi Aida Nisa', '199008042012122003', 'Cimahi', '1990-08-04', 1, 10, '2012-12-01', 'D3', 'Perempuan', '199008042012122003.png', '2017-06-14 11:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `pelatihan`
--

CREATE TABLE IF NOT EXISTS `pelatihan` (
  `id_pelatihan` int(11) NOT NULL AUTO_INCREMENT,
  `id_kejuruan` varchar(6) DEFAULT NULL,
  `jml_jp` int(11) DEFAULT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_pelatihan`),
  KEY `id_kejuruan` (`id_kejuruan`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `pelatihan`
--

INSERT INTO `pelatihan` (`id_pelatihan`, `id_kejuruan`, `jml_jp`, `last_update`) VALUES
(1, 'tekman', 240, '0000-00-00 00:00:00'),
(2, 'tekman', 320, '0000-00-00 00:00:00'),
(3, 'oto', 160, '0000-00-00 00:00:00'),
(4, 'oto', 240, '0000-00-00 00:00:00'),
(5, 'oto', 480, '0000-00-00 00:00:00'),
(6, 'oto', 960, '2017-06-16 12:55:55'),
(7, 'oto', 1200, '2017-06-16 12:55:55');

-- --------------------------------------------------------

--
-- Table structure for table `pengumuman`
--

CREATE TABLE IF NOT EXISTS `pengumuman` (
  `id_pengumuman` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(75) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `deskripsi` text,
  `gambar` varchar(100) DEFAULT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_pengumuman`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `pengumuman`
--

INSERT INTO `pengumuman` (`id_pengumuman`, `judul`, `tanggal`, `deskripsi`, `gambar`, `last_update`) VALUES
(1, 'PENDAFTARAN PELATIHAN KERJA & SERTIFIKASI TAHAP VI BULAN JULI 2017 KEJURUAN', '2016-11-02', 'BBPLK Bandung telah membuka pendaftaran Pelatihan Kerja & Sertifikasi Tahap VI Tahun 2017, pilih program pelatihan yang kalian minat,lengkapi persyaratannya dan langsung daftar ke BBPLK Bandung atau via online di www.kios3in1.net/001 , Pelaksanaan Tes Rekruitmen akan dilaksanakan pada tanggal 7 Juli 2017.\r\n\r\nPelatihan Kerja & Sertifikasi ini diselenggarakan secara Gratis!', '1.jpg', '2017-06-15 12:49:07'),
(2, 'PROGRAM PELATIHAN & SERTIFIKASI TAHAP VI TAHUN 2017 KEJURUAN TEKNIK MANUFAK', '2016-11-03', 'LBBPLK Bandung telah membuka pendaftaran untuk Program Pelatihan & Sertifikasi Tahap VI Tahun 2017 yang akan dilaksanakan pada bulan Juli 2017.Berikut daftar sub kejuran dari kejuruan Teknik Manufaktur yang akan dibuka pada tahap VI ini.', '2.png', '2017-06-13 11:50:14'),
(3, 'PROGRAM PELATIHAN & SERTIFIKASI TAHAP VI TAHUN 2017 KEJURUAN OTOMOTIF', '2016-11-03', 'BBPLK Bandung telah membuka pendaftaran untuk Program Pelatihan & Sertifikasi Tahap VI Tahun 2017 yang akan dilaksanakan pada bulan Juli 2017.Berikut daftar sub kejuran dari kejuruan Otomotif  yang akan dibuka pada tahap VI ini.', '3.png', '2017-06-13 11:50:14'),
(4, 'JADWAL DAFTAR ULANG PROGRAM PELATIHAN TAHAP V TAHUN 2017', '2016-11-11', 'Program Pelatihan yang akan dibuka pada tahap V tahun 2017. \r\nBagi peserta yang lulus tes diharap segera melakukan Daftar ulang sesuai dengan jadwal yang ditentukan.', '4.jpg', '2017-06-13 11:50:14'),
(5, 'HASIL SELEKSI WAWANCARA CALON PESERTA PELATIHAN KERJA & SERTIFIKASI TAHAP V', '2017-06-13', 'Berikut daftar peserta Lulus tes wawancara peserta Program Pelatihan Kerja & Sertifikasi Tahap V Tahun 2017 sub kejuruan CNC Milling II 240 JP (Tidak Asrama). Bagi peserta yang Lulus diwajibkan segera melakukan Daftar Ulang.', '5.jpg', '2017-06-13 11:56:14'),
(6, 'HASIL SELEKSI WAWANCARA CALON PESERTA PELATIHAN KERJA & SERTIFIKASI TAHAP V', '2017-06-13', 'Berikut daftar peserta Lulus tes wawancara peserta Program Pelatihan Kerja & Sertifikasi Tahap V Tahun 2017 sub kejuruan CNC Milling I 240 JP (Tidak Asrama). Bagi peserta yang Lulus diwajibkan segera melakukan Daftar Ulang.', '6.jpg', '2017-06-13 11:56:14'),
(7, 'HASIL SELEKSI WAWANCARA CALON PESERTA PELATIHAN KERJA & SERTIFIKASI TAHAP V', '2017-06-13', 'Berikut daftar peserta Lulus tes wawancara peserta Program Pelatihan Kerja & Sertifikasi Tahap V Tahun 2017 sub kejuruan Opr. Mesin Produksi VI 320 JP (Tidak Asrama). Bagi peserta yang Lulus diwajibkan segera melakukan Daftar Ulang.', '7.jpg', '2017-06-13 11:57:22'),
(8, 'HASIL SELEKSI WAWANCARA CALON PESERTA PELATIHAN KERJA & SERTIFIKASI TAHAP V', '2017-06-13', 'Berikut daftar peserta Lulus tes wawancara peserta Program Pelatihan Kerja & Sertifikasi Tahap V Tahun 2017 sub kejuruan Opr. Mesin Produksi V 320 JP (Tidak Asrama). Bagi peserta yang Lulus diwajibkan segera melakukan Daftar Ulang.', '8.jpg', '2017-06-13 11:57:22'),
(9, 'HASIL SELEKSI WAWANCARA CALON PESERTA PELATIHAN KERJA & SERTIFIKASI TAHAP V', '2017-06-13', 'Berikut daftar peserta Lulus tes wawancara peserta Program Pelatihan Kerja & Sertifikasi Tahap V Tahun 2017 sub kejuruan Opr. CAD CAM III 240 JP (Tidak Asrama). Bagi peserta yang Lulus diwajibkan segera melakukan Daftar Ulang.', '9.jpg', '2017-06-13 11:58:34'),
(10, 'HASIL SELEKSI WAWANCARA CALON PESERTA PELATIHAN KERJA & SERTIFIKASI TAHAP V', '2017-06-13', 'Berikut daftar peserta Lulus tes wawancara peserta Program Pelatihan Kerja & Sertifikasi Tahap V Tahun 2017 sub kejuruan Opr. CAD CAM V 320 JP (Tidak Asrama). Bagi peserta yang Lulus diwajibkan segera melakukan Daftar Ulang.', '10.jpg', '2017-06-13 11:58:34'),
(11, 'HASIL SELEKSI WAWANCARA CALON PESERTA PELATIHAN KERJA & SERTIFIKASI TAHAP V', '2017-06-13', 'Berikut daftar peserta Lulus tes wawancara peserta Program Pelatihan Kerja & Sertifikasi Tahap V Tahun 2017 sub kejuruan Opr. CAD CAM IV 320 JP (Tidak Asrama). Bagi peserta yang Lulus diwajibkan segera melakukan Daftar Ulang.', '11.jpg', '2017-06-13 12:01:02'),
(12, 'HASIL SELEKSI WAWANCARA CALON PESERTA PELATIHAN KERJA & SERTIFIKASI TAHAP V', '2017-06-13', 'Berikut daftar peserta Lulus tes wawancara peserta Program Pelatihan Kerja & Sertifikasi Tahap V Tahun 2017 sub kejuruan Mekanik Mobil Bensin IV 480 JP (Tidak Asrama). Bagi peserta yang Lulus diwajibkan segera melakukan Daftar Ulang.', '12.jpg', '2017-06-13 12:01:02'),
(13, 'HASIL SELEKSI WAWANCARA CALON PESERTA PELATIHAN KERJA & SERTIFIKASI TAHAP V', '2017-06-13', 'Berikut daftar peserta Lulus tes wawancara peserta Program Pelatihan Kerja & Sertifikasi Tahap V Tahun 2017 sub kejuruan Mekanik Body Repair  III 240 JP (Tidak Asrama). Bagi peserta yang Lulus diwajibkan segera melakukan Daftar Ulang.', '13.jpg', '2017-06-13 12:03:12'),
(14, 'HASIL SELEKSI WAWANCARA CALON PESERTA PELATIHAN KERJA & SERTIFIKASI TAHAP V', '2017-06-13', 'Berikut daftar peserta Lulus tes wawancara peserta Program Pelatihan Kerja & Sertifikasi Tahap V Tahun 2017 sub kejuruan Mekanik Mobil EFI VI 160 JP (Tidak Asrama). Bagi peserta yang Lulus diwajibkan segera melakukan Daftar Ulang.', '14.jpg', '2017-06-13 12:03:12'),
(15, 'HASIL SELEKSI WAWANCARA CALON PESERTA PELATIHAN KERJA & SERTIFIKASI TAHAP V', '2017-06-13', 'Berikut daftar peserta Lulus tes wawancara peserta Program Pelatihan Kerja & Sertifikasi Tahap V Tahun 2017 sub kejuruan Mekanik Mobil EFI V 160 JP (Tidak Asrama). Bagi peserta yang Lulus diwajibkan segera melakukan Daftar Ulang.', '15.jpg', '2017-06-13 12:04:13'),
(16, 'HASIL SELEKSI WAWANCARA CALON PESERTA PELATIHAN KERJA & SERTIFIKASI TAHAP V', '2017-06-13', 'Berikut daftar peserta Lulus tes wawancara peserta Program Pelatihan Kerja & Sertifikasi Tahap V Tahun 2017 sub kejuruan Mekanik Mobil Bensin IV 480 JP (Tidak Asrama). Bagi peserta yang Lulus diwajibkan segera melakukan Daftar Ulang.', '16.jpg', '2017-06-13 12:04:13'),
(17, 'JADWAL PELAKSANAAN TES WAWANCRA PELATIHAN KERJA & SERTIFIKASI TAHAP V TAHUN', '2017-06-13', 'Tes rekruitmen calon peserta Pelatihan Kerja & Sertifikasi BBPLK Bandung Tahap V tahun 2017 akan dilaksanakan pada :\r\nHari/tanggal   :Jum’at, 05 Mei 2017\r\nWaktu            : 08.00 wib\r\nTempat          : BBPLK Bandung\r\nBagi peserta yang telah mendaftar dan mendapatkan sms panggilan tes rekruitmen, diharapkan hadir tepat waktu sesuai jadwal yang telah ditentukan, peserta tes menggunakan pakaian kemeja putih celana hitam, dan membawa alat tulis.', '17.png', '2017-06-13 12:04:50');

-- --------------------------------------------------------

--
-- Table structure for table `regulasi`
--

CREATE TABLE IF NOT EXISTS `regulasi` (
  `id_regulasi` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) DEFAULT NULL,
  `deskripsi` text,
  `file` varchar(100) DEFAULT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_regulasi`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `regulasi`
--

INSERT INTO `regulasi` (`id_regulasi`, `nama`, `deskripsi`, `file`, `last_update`) VALUES
(1, 'Kepmen No.261 Tahun 2004', 'Perusahaan yang wajib melaksanakan pelatihan kerja.', 'Kepmen No.261 Tahun 2004.pdf', '2017-06-18 12:04:33'),
(2, 'Permenakertrans No.2 Tahun 2016', 'Sistem Satndardisasi Kompetensi Kerja Nasional', 'Permenakertrans No.2 Tahun 2016.pdf', '2017-06-18 12:09:16'),
(3, 'Permenakertrans No.6 Tahun 2012', 'Pendanaan Sistem Pelatihan Kerja', 'Permenakertrans No.6 Tahun 2012.pdf', '2017-06-18 12:09:16'),
(4, 'Permenakertrans No.7 Tahun 2012', 'Kerjasama Penggunaan Balai Latihan Kerja Oleh Swasta', 'Permenakertrans No.7 Tahun 2012.pdf', '2017-06-18 12:09:16'),
(5, 'Permenakertrans No.8 Tahun 2014', 'Pedoman Penyelenggaraan PBK', 'Permenakertrans No.8 Tahun 2014.pdf', '2017-06-18 12:09:16'),
(6, 'PP No.31 Tahun 2006', 'Sistem Pelatihan Kerja', 'PP No.31 Tahun 2006.pdf', '2017-06-18 12:10:03');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `id_role` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) NOT NULL,
  `menu` text NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_role`),
  UNIQUE KEY `role_name` (`role_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id_role`, `role_name`, `menu`, `last_update`) VALUES
(1, 'admin', 'a:5:{s:6:"Master";a:3:{i:0;s:3:"BLK";i:1;s:8:"Golongan";i:2;s:7:"Jabatan";}s:5:"Input";a:8:{i:0;s:7:"Pegawai";i:1;s:8:"Kejuruan";i:2;s:9:"Pelatihan";i:3;s:16:"Detail_Pelatihan";i:4;s:10:"Instruktur";i:5;s:7:"Asesmen";i:6;s:6:"Asesor";i:7;s:4:"Unit";}s:3:"Web";a:8:{i:0;s:9:"Informasi";i:1;s:6:"Slider";i:2;s:6:"Galeri";i:3;s:6:"Berita";i:4;s:10:"Pengumuman";i:5;s:9:"Fasilitas";i:6;s:16:"Detail_Fasilitas";i:7;s:8:"Regulasi";}s:7:"Laporan";a:3:{i:0;s:15:"Rekap_Pelatihan";i:1;s:13:"Rekap_Peserta";i:2;s:16:"Rekap_Penempatan";}s:4:"User";a:3:{i:0;s:4:"User";i:1;s:4:"Role";i:2;s:3:"Log";}}', '2017-07-07 14:29:30'),
(5, 'admin kios', 'a:4:{s:6:"Master";a:3:{i:0;s:3:"BLK";i:1;s:8:"Golongan";i:2;s:7:"Jabatan";}s:5:"Input";a:8:{i:0;s:7:"Pegawai";i:1;s:8:"Kejuruan";i:2;s:9:"Pelatihan";i:3;s:16:"Detail_Pelatihan";i:4;s:10:"Instruktur";i:5;s:7:"Asesmen";i:6;s:6:"Asesor";i:7;s:4:"Unit";}s:3:"Web";a:7:{i:0;s:9:"Informasi";i:1;s:6:"Slider";i:2;s:6:"Galeri";i:3;s:6:"Berita";i:4;s:10:"Pengumuman";i:5;s:9:"Fasilitas";i:6;s:8:"Regulasi";}s:7:"Laporan";a:3:{i:0;s:15:"Rekap_Pelatihan";i:1;s:13:"Rekap_Peserta";i:2;s:16:"Rekap_Penempatan";}}', '2017-07-07 18:18:48');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE IF NOT EXISTS `slider` (
  `id_slider` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(100) DEFAULT NULL,
  `foto` varchar(100) DEFAULT NULL,
  `tgl_upload` date DEFAULT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_slider`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id_slider`, `judul`, `foto`, `tgl_upload`, `last_update`) VALUES
(5, '', '1.jpg', '2016-11-16', '0000-00-00 00:00:00'),
(2, '', '2.jpg', '2016-11-16', '0000-00-00 00:00:00'),
(4, '', '3.jpg', '2016-11-16', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `unit`
--

CREATE TABLE IF NOT EXISTS `unit` (
  `id_unit` int(11) NOT NULL AUTO_INCREMENT,
  `kode_unit` varchar(20) NOT NULL,
  `judul` varchar(75) DEFAULT NULL,
  `deskripsi` text,
  `skkni` varchar(100) DEFAULT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_unit`),
  UNIQUE KEY `kode_unit` (`kode_unit`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `unit`
--


-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `username` varchar(20) NOT NULL DEFAULT '',
  `password` varchar(30) DEFAULT NULL,
  `nip` varchar(18) DEFAULT NULL,
  `hak_akses` varchar(30) NOT NULL,
  `status` enum('0','1') NOT NULL COMMENT '0 inactive, 1 active',
  `pertanyaan` varchar(75) DEFAULT NULL,
  `jawaban` varchar(25) DEFAULT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`username`, `password`, `nip`, `hak_akses`, `status`, `pertanyaan`, `jawaban`, `last_update`) VALUES
('admin', 'admin', NULL, 'admin', '1', NULL, NULL, '2017-06-06 10:08:51'),
('kajur', 'kajur', NULL, 'kajur', '1', NULL, NULL, '2017-06-06 10:08:51'),
('manajemen', 'manajemen', '1234567891234', 'manajemen', '1', NULL, NULL, '2017-06-06 12:40:02'),
('pegawai', 'pegawai', '198810032012122004', 'pegawai', '1', 'a', 'b', '2017-06-06 10:08:51');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `nip` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `id_role` int(11) NOT NULL,
  `status` enum('0','1') NOT NULL COMMENT '0 inactive, 1 active',
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id_user`, `nip`, `username`, `password`, `id_role`, `status`, `last_update`) VALUES
(2, '', 'superadmin', '17c4520f6cfd1ab53d8745e84681eb49', 1, '1', '2017-07-05 09:58:50'),
(3, '198810032012122004', 'belly', 'fa83f40479a04df219641b58c911b417', 5, '1', '2017-07-08 12:12:46');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `asesmen`
--
ALTER TABLE `asesmen`
  ADD CONSTRAINT `asesmen_ibfk_1` FOREIGN KEY (`id_kejuruan`) REFERENCES `kejuruan` (`id_kejuruan`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `asesor`
--
ALTER TABLE `asesor`
  ADD CONSTRAINT `asesor_ibfk_1` FOREIGN KEY (`nip`) REFERENCES `pegawai` (`nip`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `berita`
--
ALTER TABLE `berita`
  ADD CONSTRAINT `berita_ibfk_1` FOREIGN KEY (`username`) REFERENCES `user` (`username`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `detail_asesmen`
--
ALTER TABLE `detail_asesmen`
  ADD CONSTRAINT `detail_asesmen_ibfk_1` FOREIGN KEY (`kode_unit`) REFERENCES `unit` (`kode_unit`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `detail_asesmen_ibfk_2` FOREIGN KEY (`id_asesor1`) REFERENCES `asesor` (`id_asesor`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `detail_asesmen_ibfk_3` FOREIGN KEY (`id_asesor2`) REFERENCES `asesor` (`id_asesor`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `detail_pelatihan`
--
ALTER TABLE `detail_pelatihan`
  ADD CONSTRAINT `detail_pelatihan_ibfk_1` FOREIGN KEY (`id_pelatihan`) REFERENCES `pelatihan` (`id_pelatihan`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `instruktur`
--
ALTER TABLE `instruktur`
  ADD CONSTRAINT `instruktur_ibfk_1` FOREIGN KEY (`nip`) REFERENCES `pegawai` (`nip`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `instruktur_ibfk_2` FOREIGN KEY (`id_kejuruan`) REFERENCES `kejuruan` (`id_kejuruan`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `log`
--
ALTER TABLE `log`
  ADD CONSTRAINT `log_ibfk_1` FOREIGN KEY (`username`) REFERENCES `user` (`username`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `pelatihan`
--
ALTER TABLE `pelatihan`
  ADD CONSTRAINT `pelatihan_ibfk_1` FOREIGN KEY (`id_kejuruan`) REFERENCES `kejuruan` (`id_kejuruan`) ON DELETE NO ACTION ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

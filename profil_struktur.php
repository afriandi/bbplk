        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{title_menu}</h1>
                <ol class="breadcrumb">
                    <li><a href="../{base_url}home">Home</a></li>
                    <li><a href="#">{menu}</a></li>
                    <li class="active">{submenu}</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
        <div class="row struktur_org">
          <div class="col-lg-12">
            <img class="img-responsive" src="{profile_struktur_img}" alt="">
          </div>
        </div>
        <!-- /.row -->

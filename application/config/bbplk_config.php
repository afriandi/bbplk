<?php

$config['bbplk_module']['Master'] = array('BLK', 'Golongan', 'Jabatan');
$config['bbplk_module']['Input'] = array('Pegawai', 'Kejuruan', 'Pelatihan', 'Detail Pelatihan', 'Instruktur', 'Asesmen', 'Asesor', 'Unit');
$config['bbplk_module']['Web'] = array('Informasi', 'Slider', 'Galeri', 'Berita', 'Pengumuman', 'Fasilitas', 'Detail Fasilitas','Regulasi');
$config['bbplk_module']['Laporan'] = array('Rekap Pelatihan', 'Rekap Peserta', 'Rekap Penempatan');
$config['bbplk_module']['User'] = array('User', 'Role', 'Log');

?>

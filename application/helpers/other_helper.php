<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('getKejuruan')) {
    function getKejuruan($kejur) {
        $kejuruan = array(
			'oto' => 'Otomotif',
			'tekman' => 'Teknik Manufaktur'
		);
		
		return $kejuruan[$kejur];
    }
}

if (!function_exists('getKepemilikan')) {
    function getKepemilikan($index) {
        $data = array(
			'oto' => 'Otomotif',
			'tekman' => 'Teknik Manufaktur',
			'umum' => 'Umum'
		);
		
		return $data[$index];
    }
}

if (!function_exists('getStatus')) {
    function getStatus($st) {
        $status = array(
			'0' => 'Tidak Aktif',
			'1' => 'Aktif'
		);
		
		return $status[$st];
    }
}


if (!function_exists('getDateShortIndo')) {

    function getDateShortIndo($date) {
        $thisDate = date("Y-m-d", strtotime($date));
        $month = array(
            '01' => 'jan',
            '02' => 'Feb',
            '03' => 'Mar',
            '04' => 'Apr',
            '05' => 'Mei',
            '06' => 'jun',
            '07' => 'jul',
            '08' => 'Agu',
            '09' => 'Sep',
            '10' => 'Okt',
            '11' => 'Nov',
            '12' => 'Des',
        );

        $dt = explode("-", $thisDate);

        return $dt[2] . ' ' . $month[$dt[1]] . ' ' . $dt[0];
    }

}


if (!function_exists('getDueDate')) {

    function getDueDate($endDate) {
        $date = explode('-', $endDate);
        $dueDate = mktime(0, 0, 0, $date[1], $date[2] + 5, $date[0]);
        return getDateShortIndo(date('Y-m-d', $dueDate));
    }

}


if (!function_exists('getDueDateRowStyle')) {

    function getDueDateRowStyle($status, $endDate) {

        $today = date('Y-m-d');
        $dayEnd = date("Y-m-d", strtotime($endDate));

        $arrEnd = explode("-", $dayEnd);
        $arrToday = explode("-", $today);

        $style = '';

        if ($arrToday[0] == $arrEnd[0] && $arrToday[1] == $arrEnd[1]) {
            $countDay = $arrToday[2] - $arrEnd[2];
            if ($countDay > 1 && $countDay <= 3) {
                $style = 'yellow';
            } elseif ($countDay > 3) {
                $style = 'passist';
            } else {
                $style = 'pstatus';
            }
        } elseif ($arrToday[0] == $arrEnd[0] && $arrToday[1] != $arrEnd[1]) {
            $countMonth = $arrEnd[1] - $arrToday[1];
            if ($countMonth > 0) {
                $style = 'pstatus';
            } else {
                $diff = datediff($endDate, $today);
                if ($diff['days_total'] > 1 && $diff['days_total'] <= 3) {
                    $style = 'yellow';
                } elseif ($diff['days_total'] == 1) {
                    $style = 'pstatus';
                } else {
                    $style = 'passist';
                }
            }
        } elseif ($arrToday[0] != $arrEnd[0]) {
            $countYear = $arrEnd[0] - $arrToday[0];
            if ($countYear > 0) {
                $countMonth = $arrEnd[1] - $arrToday[1];
                if ($countMonth > 0) {
                    $diff = datediff($endDate, $today);
                    if ($diff['days_total'] > 1 && $diff['days_total'] <= 3) {
                        $style = 'yellow';
                    } elseif ($diff['days_total'] == 1) {
                        $style = 'pstatus';
                    } else {
                        $style = 'passist';
                    }
                } else {
                    $diff = datediff($endDate, $today);
                    if ($diff['days_total'] > 1 && $diff['days_total'] <= 3) {
                        $style = 'yellow';
                    } elseif ($diff['days_total'] == 1) {
                        $style = 'pstatus';
                    } else {
                        $style = 'passist';
                    }
                }
            } else {
                $style = 'passist';
            }
        }

        return $style;
    }

}

if (!function_exists('datediff')) {

    function datediff($tgl1, $tgl2) {
        $tgl1 = strtotime($tgl1);
        $tgl2 = strtotime($tgl2);
        $diff_secs = abs($tgl1 - $tgl2);
        $base_year = min(date("Y", $tgl1), date("Y", $tgl2));
        $diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);
        return array("years" => date("Y", $diff) - $base_year, "months_total" => (date("Y", $diff) - $base_year) * 12 + date("n", $diff) - 1, "months" => date("n", $diff) - 1, "days_total" => floor($diff_secs / (3600 * 24)), "days" => date("j", $diff) - 1, "hours_total" => floor($diff_secs / 3600), "hours" => date("G", $diff), "minutes_total" => floor($diff_secs / 60), "minutes" => (int) date("i", $diff), "seconds_total" => $diff_secs, "seconds" => (int) date("s", $diff));
    }

}

if (!function_exists('checkExcelExtendedFile')) {

    function checkExcelExtendedFile($filename) {
        $allow = explode('|', 'xls|XLS');
        $arfl = explode(".", $filename);
        $a = count($arfl);
        $ext = $arfl[$a - 1];
        $ret = false;
        if (in_array($ext, $allow)) {
            $ret = true;
        }

        return $ret;
    }

}

if (!function_exists('checkImageExtendedFile')) {

    function checkImageExtendedFile($filename) {
        $allow = explode('|', 'gif|jpg|jpeg|png|pdf|bmp');
        $arfl = explode(".", $filename);
        $a = count($arfl);
        $ext = $arfl[$a - 1];
        $ret = false;
        if (in_array($ext, $allow)) {
            $ret = true;
        }

        return $ret;
    }

}

if (!function_exists('combineArray')) {

    function combineArray($arr1, $arr2) {
        $count = min(count($arr1), count($arr2));
        return array_combine(array_slice($arr1, 0, $count), array_slice($arr2, 0, $count));
    }

}

if (!function_exists('configDataView')) {

    function configDataView($val) {
        $val = strtolower($val);
        $ret = "";
        if ($val == 'y') {
            $ret = 'Yearly';
        } elseif ($val == 'm') {
            $ret = 'Monthly';
        } else {
            $ret = $val;
        }
        return $ret;
    }

}

if (!function_exists('generateRandomString')) {

    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }

}

if (!function_exists('ConvertArrayModule')) {

	function ConvertArrayModule($array) {

        $result = array();

        $i = 0;
        foreach ($array as $module => $sections) {
            $ii = 0;
            foreach ($sections as $section) {
                if ($section == '')
                    continue;
                $module = preg_replace("/[^[:alnum:]]/", "_", strtolower($module));
                $section_short = preg_replace("/[^[:alnum:]]/", "_", strtolower($section));
                $result[$module][$ii] = $section_short;

                $ii++;
            }
            $i++;
        }

        return $result;
    }
}

if (!function_exists('DeConvertArrayModule')) {

	function DeConvertArrayModule($array) {

        $result = array();

        $i = 0;
        foreach ($array as $module => $sections) {
            $ii = 0;
            foreach ($sections as $section) {
                if ($section == '')
                    continue;
                $module = preg_replace("_", "/[^[:alnum:]]/", strtolower($module));
                $section_short = preg_replace("_", "/[^[:alnum:]]/", strtolower($section));
                $result[$module][$ii] = $section_short;

                $ii++;
            }
            $i++;
        }

        return $result;
    }
}


?>

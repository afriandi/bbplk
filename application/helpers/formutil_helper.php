<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


if (!function_exists('datalist_pegawai')) {

    function datalist_pegawai($name, $extra='') {
        $CI = & get_instance();

        $retval = "<datalist name=\"$name\" id=\"$name\" $extra>";

        $CI->db->select('*');
        $CI->db->from('pegawai');
        $query = $CI->db->get();

        foreach ($query->result() as $row) {
            $retval .= '<option value="'.$row->nip.' ('.$row->nama.')" >';
        }
        $retval .= "</datalist>";
        return $retval;
    }
}

if (!function_exists('combo_role')) {

    function combo_role($name, $selected='', $extra='') {
        $CI = & get_instance();

        $retval = "<select name=\"$name\" id=\"$name\" $extra>";

        if ($selected == "")
            $retval .= "<option value=\"\" selected=\"selected\"> -- Hak Akses -- </option>";

        $CI->db->select('*');
        $CI->db->from('role');
        $query = $CI->db->get();

        foreach ($query->result() as $row) {
            $retval .= "<option value=\"" . $row->id_role . "\"";
            if ($row->id_role == $selected)
                $retval .= " selected=\"selected\" ";
            $retval .= " >" . $row->role_name . "</option>";
        }

        $retval .= "</select>";
        return $retval;
    }
}


if (!function_exists('combo_fasilitas')) {

    function combo_fasilitas($name, $selected='', $extra='') {
        $CI = & get_instance();

        $retval = "<select name=\"$name\" id=\"$name\" $extra>";

        if ($selected == "")
            $retval .= "<option value=\"\" selected=\"selected\"> -- Fasilitas -- </option>";

        $CI->db->select('*');
        $CI->db->from('fasilitas');
        $query = $CI->db->get();

        foreach ($query->result() as $row) {
            $retval .= "<option value=\"" . $row->id_fasilitas . "\"";
            if ($row->id_fasilitas == $selected)
                $retval .= " selected=\"selected\" ";
            $retval .= " >" . $row->judul . "</option>";
        }

        $retval .= "</select>";
        return $retval;
    }
}

if (!function_exists('combo_kejuruan')) {

    function combo_kejuruan($name, $selected='', $extra='') {
        $CI = & get_instance();

        $retval = "<select name=\"$name\" id=\"$name\" $extra>";

        if ($selected == "")
            $retval .= "<option value=\"\" selected=\"selected\"> -- Kejuruan -- </option>";

        $CI->db->select('*');
        $CI->db->from('kejuruan');
        $query = $CI->db->get();

        foreach ($query->result() as $row) {
            $retval .= "<option value=\"" . $row->id_kejuruan . "\"";
            if ($row->id_kejuruan == $selected)
                $retval .= " selected=\"selected\" ";
            $retval .= " >" . $row->nama . "</option>";
        }

        $retval .= "</select>";
        return $retval;
    }
}

if (!function_exists('combo_jabatan')) {

    function combo_jabatan($name, $selected='', $extra='') {
        $CI = & get_instance();

        $retval = "<select name=\"$name\" id=\"$name\" $extra>";

        if ($selected == "")
            $retval .= "<option value=\"\" selected=\"selected\"> -- Jabatan -- </option>";

        $CI->db->select('*');
        $CI->db->from('jabatan');
        $query = $CI->db->get();

        foreach ($query->result() as $row) {
            $retval .= "<option value=\"" . $row->id_jabatan . "\"";
            if ($row->id_jabatan == $selected)
                $retval .= " selected=\"selected\" ";
            $retval .= " >" . $row->jabatan . "</option>";
        }

        $retval .= "</select>";
        return $retval;
    }
}


if (!function_exists('combo_pelatihan')) {

    function combo_pelatihan($name, $selected='', $extra='') {
        $CI = & get_instance();

        $retval = "<select name=\"$name\" id=\"$name\" $extra>";

        if ($selected == "")
            $retval .= "<option value=\"\" selected=\"selected\"> -- Pelatihan -- </option>";

        $CI->db->select('*');
        $CI->db->from('pelatihan');
        $CI->db->join('kejuruan', 'kejuruan.id_kejuruan = pelatihan.id_kejuruan', 'left');
		$query = $CI->db->get();

        foreach ($query->result() as $row) {
            $retval .= "<option value=\"" . $row->id_pelatihan . "\"";
            if ($row->id_pelatihan == $selected)
                $retval .= " selected=\"selected\" ";
            $retval .= " >" . $row->nama ." (Jumlah JP " . $row->jml_jp . ")</option>";
        }

        $retval .= "</select>";
        return $retval;
    }
}


if (!function_exists('combo_golongan')) {

    function combo_golongan($name, $selected='', $extra='') {
        $CI = & get_instance();

        $retval = "<select name=\"$name\" id=\"$name\" $extra>";

        if ($selected == "")
            $retval .= "<option value=\"\" selected=\"selected\"> -- Golongan -- </option>";

        $CI->db->select('*');
        $CI->db->from('golongan');
        $query = $CI->db->get();

        foreach ($query->result() as $row) {
            $retval .= "<option value=\"" . $row->id_golongan . "\"";
            if ($row->id_golongan == $selected)
                $retval .= " selected=\"selected\" ";
            $retval .= " >" . $row->golongan . "</option>";
        }

        $retval .= "</select>";
        return $retval;
    }
}

if (!function_exists('combo_asrama')) {

    function combo_asrama($name, $selected='', $extra='') {

        $retval = "<select name=\"$name\" id=\"$name\" $extra>";

        if ($selected == "")
            $retval .= "<option value=\"\" selected=\"selected\"> -- Asrama -- </option>";

		$data = array('Boarding','Non Boarding');

        foreach ($data as $key) {
            $retval .= "<option value=\"" . $key. "\"";
            if (strtolower($key) == strtolower($selected))
                $retval .= " selected=\"selected\" ";
            $retval .= " >" . $key . "</option>";
        }

        $retval .= "</select>";
        return $retval;
    }
}

if (!function_exists('combo_kepemilikan')) {

    function combo_kepemilikan($name, $selected='', $extra='') {

        $retval = "<select name=\"$name\" id=\"$name\" $extra>";

        if ($selected == "")
            $retval .= "<option value=\"\" selected=\"selected\"> -- Kepemilikan -- </option>";

		$data = array(
			'oto'  => 'Otomotif',
			'tekman'  => 'Teknik Manufaktur',
			'umum'  => 'Umum'
		);

        foreach ($data as $key => $value) {
            $retval .= "<option value=\"" . $key. "\"";
            if ($key == $selected)
                $retval .= " selected=\"selected\" ";
            $retval .= " >" . $value . "</option>";
        }

        $retval .= "</select>";
        return $retval;
    }
}

if (!function_exists('combo_pendidikan')) {

    function combo_pendidikan($name, $selected='', $extra='') {

        $retval = "<select name=\"$name\" id=\"$name\" $extra>";

        if ($selected == "")
            $retval .= "<option value=\"\" selected=\"selected\"> -- Pendidikan -- </option>";

		$pendidikan = array(
			0  => 'S1',
			1  => 'D3',
			2  => 'SMA',
			3  => 'SMP',
			4  => 'SD'
		);

        for($i=0;$i<count($pendidikan);$i++) {
            $retval .= "<option value=\"" . $pendidikan[$i]. "\"";
            if ($pendidikan[$i] == $selected)
                $retval .= " selected=\"selected\" ";
            $retval .= " >" . $pendidikan[$i] . "</option>";
        }

        $retval .= "</select>";
        return $retval;
    }
}

if (!function_exists('radio_status')) {

    function radio_status($name ,$val='0') {
        
        $active = array('name' => $name, 'value' => '1');
        $inactive = array('name' => $name, 'value' => '0');
        if (strtolower($val) == '1') {
            $active['checked'] = true;
        } else {
            $inactive['checked'] = true;
        }

        return '<label>'.form_radio($active) . ' Aktif </label> &nbsp;&nbsp;&nbsp;&nbsp; <label>' . form_radio($inactive) . ' Tidak Aktif </label>';
    }

}


if (!function_exists('input_text')) {
    function input_text($name, $value='', $extra='') {
        $retval = '<input type="text" value="'.$value.'" ' . $extra.' >' ;
        return $retval;
    }
}


if (!function_exists('radioGender')) {

    function radioGender($name ,$val='', $disable='') {
        
        $Male = array('name' => $name, 'value' => 'Laki-laki');
        $Female = array('name' => $name, 'value' => 'Perempuan');
		$cheked = "";
        if (strtoupper($val) == 'PEREMPUAN') {
			
            $Female['checked'] = true;
			if($disable != '') $Female['disabled'] = $disable;
        } else {
            $Male['checked'] = true;
			if($disable != '') $Male['disabled'] = $disable;
        }
		
		$return = '<div class="col-sm-2"><label><span class="input-group-addon">'.form_radio($Male) . ' Laki-laki </span></label></div>';
		$return .= '<div class="col-sm-2"><label><span class="input-group-addon">'.form_radio($Female) . ' Perempuan </span></label></div>';
	
		return $return;
    }
}

if (!function_exists('radioStatusPelatihan')) {

    function radioStatusPelatihan($name ,$val='', $disable='') {
        
        $Male = array('name' => $name, 'value' => 'Perencanaan');
        $Female = array('name' => $name, 'value' => 'Pelaksanaan');
		$cheked = "";
        if (strtoupper($val) == 'PELAKSANAAN') {
			
            $Female['checked'] = true;
			if($disable != '') $Female['disabled'] = $disable;
        } else {
            $Male['checked'] = true;
			if($disable != '') $Male['disabled'] = $disable;
        }
		
		$return = '<div class="col-sm-2"><label><span class="input-group-addon">'.form_radio($Male) . ' Perencanaan </span></label></div>';
		$return .= '<div class="col-sm-2"><label><span class="input-group-addon">'.form_radio($Female) . ' Pelaksanaan </span></label></div>';
	
		return $return;
    }

}


if (!function_exists('combo_urutan')) {

    function combo_urutan($name, $selected='', $extra='') {

        $retval = "<select name=\"$name\" id=\"$name\" $extra>";

        if ($selected == "")
            $retval .= "<option value=\"\" selected=\"selected\"> -- Urutan -- </option>";

		$data = array(1,2,3,4,5,6,7,8,9,10);

        foreach ($data as $key) {

            $retval .= "<option value=\"" . $key . "\"";
            if ($key == $selected)
                $retval .= " selected=\"selected\" ";
            $retval .= " >" . $key . "</option>";
        }

        $retval .= "</select>";
        return $retval;
    }
}


if (!function_exists('combo_tahap')) {

    function combo_tahap($name, $selected='', $extra='') {

        $retval = "<select name=\"$name\" id=\"$name\" $extra>";

        if ($selected == "")
            $retval .= "<option value=\"\" selected=\"selected\"> -- Tahap -- </option>";

		$data = array(1,2,3,4,5,6,7,8,9,10);

        foreach ($data as $key) {

            $retval .= "<option value=\"" . $key . "\"";
            if ($key == $selected)
                $retval .= " selected=\"selected\" ";
            $retval .= " >" . $key . "</option>";
        }

        $retval .= "</select>";
        return $retval;
    }
}


if (!function_exists('combo_tahun')) {

    function combo_tahun($name, $selected='', $extra='') {
        $CI = & get_instance();

        $retval = "<select name=\"$name\" id=\"$name\" $extra>";

        $arryear = array();
        $y = date("Y");
        $i=0;
        for ($a = $y; $a >= $y - 5; $a--) {
            $arryear[$i] = $a; 
            $i++;
        }

        if ($selected == "")
            $retval .= "<option value=\"\" selected=\"selected\"> -- Tahun -- </option>";


        foreach ($arryear as $key) {

            $retval .= "<option value=\"" . $key . "\"";
            if ($key == $selected)
                $retval .= " selected=\"selected\" ";
            $retval .= " >" . $key . "</option>";
        }

        $retval .= "</select>";
        return $retval;
    }

}

?>

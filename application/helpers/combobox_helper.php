<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('combo_month')) {

    function combo_month($name, $selected='', $view = false, $extra='') {
        $CI = & get_instance();

        $retval = "<select name=\"$name\" id=\"$name\" $extra>";

        $arrmonth = array(
            '01' => 'January',
            '02' => 'February',
            '03' => 'March',
            '04' => 'April',
            '05' => 'May',
            '06' => 'June',
            '07' => 'July',
            '08' => 'August',
            '09' => 'September',
            '10' => 'October',
            '11' => 'November',
            '12' => 'December',
        );

        if ($view) {
            $retval .= "<option value=\"\"> --- month --- </option>";
        }

        foreach ($arrmonth as $key => $val) {
            $retval .= "<option value=\"" . $key . "\"";
            if ($key == $selected)
                $retval .= " selected=\"selected\" ";
            $retval .= " >" . $val . "</option>";
        }

        $retval .= "</select>";
        return $retval;
    }

}


?>

<div class="col-md-6">
	<div class="box-body">
    	<div class="chart">
        	<canvas id="chart-area" style="height:230px"></canvas>
        </div>
   	</div>
</div>

<script src="<?php echo base_url() . 'assets/plugins/chartjs/Chart.bundle.min.js'; ?>"></script>
<script src="<?php echo base_url() . 'assets/plugins/chartjs/utils.js'; ?>"></script>


<script>
$(function () {
	var color = Chart.helpers.color;
	var reviewconfig = {
        type: 'pie',
        data: {
            datasets: [{
                data: [
                    40,
                    50,
					10
                ],
                backgroundColor: [
                    chartColors.red,
                    chartColors.orange,
					chartColors.white
                ],
                label: 'Dataset 1'
            }],
            labels: [
                "Red",
                "Orange",
				"not yet"
            ]
        },
        options: {
            responsive: true
        }
    };

        var ctxreview = $("#chart-area").get(0).getContext("2d");
        var myPie = new Chart(ctxreview, reviewconfig);


});
</script>

<header id="myCarousel" class="carousel slide">
    <!-- Indicators -->
    <ol class="carousel-indicators">
    	<?php
			for($a=0;$a<$slider_count;$a++){
				$active = $a== 0? 'active' : '';
				echo '<li data-target="#myCarousel" data-slide-to="'.$a.'" class="'.$active.'"></li>';
			}
		?>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
    	<?php
			foreach ($slider as $sld) {
				$act = $sld['slider_id']== 0? 'active' : '';
				echo '<div class="item '.$act.'">';
					echo '<div class="fill" style="background-image:url(\''.$sld['slider_img'].'\');"></div>';
					echo '<div class="carousel-caption"><h2>'.$sld['slider_title'].'</h2></div>';
				echo '</div>';
			}
		?>
    </div>

    <!-- Controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class="icon-prev"></span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class="icon-next"></span>
    </a>
</header>


<div class="container">
  <div class="row">
      <div class="col-lg-12">
          <h2 class="page-header">BLK Bandung Membuka Pelatihan Kerja Gratis</h2>
      </div>
      <div class="col-md-12">
        Balai Besar Pengembangan Latihan Kerja (BBPLK) Bandung atau yang kita kenal sebagai BLK Bandung merupakan unit pelaksanaan teknis pusat dibidang pelatihan kerja dibawah Direktorat Jenderal Pembinaan Pelatihan dan Produktivitas Kementerian Ketenagakerjaan Republik Indonesia yang berlokasi di Jalan Gatot Subroto Nomor 170 kota Bandung.
        <br>
        Pada tahun 2017 BLK Bandung hanya menyelenggarakan pelatihan di 2 (dua) kejuruan yaitu Teknik Manufaktur dan Otomotif, sesuai dengan Program Kementerian mengenai 3R ( Reorientasi, Revitalisasi dan Rebranding) dibeberapa Balai Pelatihan di bawah Kementerian Ketenagakerjaan Republik Indonesia.
        <br><br>
        Kejuruan Otomotif memiliki 4 jenis Program Pelatihan yaitu:
        <ul>
        <li>mekanik mobil bensin (480 JP/ 3 Bulan)</li>
        <li>mekanik mobil EFI (160 JP/ 1 Bulan)</li>
        <li>mekanik spooring balancing (160 JP/ 1 Bulan)</li>
        <li>mekanik body repair (240 JP/ 1,5 Bulan)</li>
        </ul>
        <br>
        Sedangkan Kejuruan Teknik Manufaktur memiliki 12 jenis Program Pelatihan yaitu :
        <ul>
          <li>Operator Mesin Produksi (320 JP/ 2 Bulan)</li>
          <li>Drafter Mesin (240 JP/ 1,5 Bulan)</li>
          <li>Operator CAD CAM (240 JP/ 1,5 Bulan)</li>
          <li>Operator CNC CAD CAM (320 JP / 2 Bulan)</li>
          <li>Operator Produksi CNC (240 JP/ 1,5 Bulan)</li>
          <li>Operator Kerja Plat (240 JP/ 1,5 Bulan)</li>
          <li>Press Tool Maker (240JP/ 1,5 Bulan)</li>
          <li>Perawatan Mesin Perkakas (240 JP/ 1,5 Bulan)</li>
          <li>Operator Jig dan Fixture Maker (240 JP/ 1,5 Bulan)</li>
          <li>Pembuatan Perkakas Presisi (240 JP/ 1,5 Bulan)</li>
          <li>Operator CNC Milling (240 JP/ 1,5 Bulan)</li>
          <li>Mold Maker (320 JP/ 2 Bulan)</li>
        </ul>
        <h3 class="page-header">
        Pelatihan dibiayai Pemerintah
        </h3>
        Peserta pelatihan tidak dipungut biaya apapun!! (100% Gratis). Adapun fasilitas yang akan didapat adalah
        <br>
        Program Pelatihan Non Boarding (tidak diasramakan) :
        <ol type="1">
          <li>Makan siang per hari</li>
          <li>Bantuan transport harian Rp 10.000</li>
          <li>Modul pelatihan</li>
          <li>Sepatu praktik</li>
          <li>Baju praktik</li>
          <li>ATK</li>
        </ol>
        <br>
        Program Pelatihan Boarding (diasramakan) :
        <ol type="1">
          <li>Penginapan</li>
          <li>Makan 3 x sehari</li>
          <li>Pergantian biaya transport keberangkatan dan kepulangan dari BLK Bandung ke wilayah asal</li>
          <li>Modul pelatihan</li>
          <li>Sepatu praktik</li>
          <li>Baju praktik</li>
          <li>ATK</li>
        </ol>
        <br>
        Program pelatihan boarding digunakan oleh peserta yang berasal dari luar Kota Bandung, sedangkan program pelatihan non boarding digunakan oleh peserta yang berasal dari Kota Bandung. Kuota tiap gelombang terbatas, pastikan anda terdaftar disalah satu gelombang yang tersedia, adapun jadwal pelatihan bisa di lihat pada link berikut ini <a href="http://blkbandung.kemnaker.go.id/pendaftaran" target="_blank">http://blkbandung.kemnaker.go.id/pendaftaran</a>
        <h3 class="page-header">
        Proses Pendaftaran Pelatihan
        </h3>
        Ada 2 (dua) mekanisme pendaftaran pelatihan, yaitu datang langsung ke BLK Bandung dengan membawa persyaratan dan mengisi form pendaftaran, serta cara yang kedua secara online menggunakan link <a href="http://blkbandung.kemnaker.go.id/pendaftaran" target="_blank">http://blkbandung.kemnaker.go.id/pendaftaran/daftar</a>
        <br>
        Untuk info lanjut dapat mengunjungi website kami  di <a href="http://blkbandung.kemnaker.go.id/" target="_blank">http://blkbandung.kemnaker.go.id/</a> atau
        <a href="http://www.kios3in1.net/001/" target="_blank">http://www.kios3in1.net/001/</a>
        dan Admin Kios kami :<br>
        <ul>
            <li>Pak Asep (0812-2153-3345)</li>
            <li>Ibu Yusnita (0856-5928-0995)</li>
            <li>Admin Medsos (0878-2190-6504)</li>
        </ul>
        <br>
        <a href="<?php echo base_url().'pendaftaran/daftar'; ?>">
            <img class="img-responsive img-portfolio img-hover" src="<?php echo base_url().'assets/img/home/blk bandung pelatihan gratis.jpg'?>" alt="">
        </a>
      </div>
  </div>

              <!-- Marketing Icons Section -->
        <div class="row">
            <br><br>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4><i class="fa fa-fw fa-compass"></i> Pengumuman</h4>
                    </div>
                    <div class="panel-body">
                        <p>{pengumuman} ...</p>
                        <a href="<?php echo base_url().'pengumuman'; ?>" class="btn btn-default">Baca Lebih Lanjut</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4><i class="fa fa-fw fa-compass"></i> Pelatihan Kerja</h4>
                    </div>
                    <div class="panel-body">
                        <p>Berikut ini merupakan jadwal pelatihan yang akan dibuka. Untuk informasi lebih lanjut silakan klik tombol dibawah ini.</p>
                        <a href="<?php echo base_url().'pendaftaran'; ?>" class="btn btn-default">Baca Lebih Lanjut</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4><i class="fa fa-fw fa-compass"></i> Pendaftaran</h4>
                    </div>
                    <div class="panel-body">
                        <p>Untuk mendaftar pelatihan kerja gratis dapat dilakukan secara online dengan cara mengisi form di website ini. Ayo buruan daftar, kuota terbatas!</p>
                        <a href="<?php echo base_url().'pendaftaran/daftar'; ?>" class="btn btn-default">Daftar Sekarang</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->

<!-- <div class="row">
    <div class="col-lg-12">
        <h2 class="page-header">Proses Pendaftaran Pelatihan Berbasis Kompetensi</h2>
    </div>

        <div class="col-md-12">
        <a href="<?php echo base_url().'pendaftaran/daftar'; ?>">
            <img class="img-responsive img-portfolio img-hover" src="<?php echo base_url().'assets/img/home/prosedur.jpg'?>" alt="">
        </a>
    	  </div>
</div> -->

        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Kantor BLK Bandung</h2>
            </div>
            <div class="col-md-6">
              <p>Untuk informasi pelatihan dan sertifikasi dapat langsung mengunjungi Kantor Balai Besar Pengembangan Latihan Kerja Bandung di alamat berikut :</p><br>
<ul>
<li><strong>Jl. Jenderal Gatot Subroto No. 170 Bandung</strong></li>
<li>No Telepon - (022) 731 2564</li>
<li>Fax - (022) 731 2564</li>
</ul>
<br>
<p>Kantor BLK dapat dikunjungi pada hari Senin s.d. Jumat pada pukul 07.30 s.d. 16.00.</p>
<p>Calon peserta pelatihan diharapkan membawa kelengkapan dokumen berupa fotocopy KTP, fotocopy ijazah terakhir, dan pasfoto ukuran 3x4 berwarna background merah sebanyak 4 rangkap.</p>            

				
                
				
			</div>

            <div class="col-md-6 slideanim gmap slider">
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3960.678760869473!2d107.63561251436752!3d-6.9289469697471135!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e68e87646aa8c91%3A0x86f0777ca52c54f5!2sBBPLKDN+%2F+NVTDC+BANDUNG!5e0!3m2!1sen!2sid!4v1474015075418" style="border:0" allowfullscreen="" width="100%" height="450" frameborder="0"></iframe>
            </div>
		</div>
        
        
        <div class="row" style="margin-top:20px;">
        	<div class="col-lg-6">
            	<div class="col-md-12" style="text-align:center"><h4>Website menyediakan informasi yang cukup jelas</h4></div>
                <div class="chart">
                    <canvas id="chart-area" style="height:230px"></canvas>
                </div>
                <div class="col-md-12">
                	<table width="100%" cellpadding="2" cellspacing="2" style="font-size:12px;">
                    	<tr>
                        	<td>Sangat jelas</td>
                            <td><b><?php echo $jelas['SJ']; ?></b></td>
                        </tr>
                        <tr>
                        	<td>Jelas</td>
                            <td><b><?php echo $jelas['J']; ?></b></td>
                        </tr>
                        <tr>
                        	<td>Cukup jelas</td>
                            <td><b><?php echo $jelas['CJ']; ?></b></td>
                        </tr>
                        <tr>
                        	<td>Tidak jelas</td>
                            <td><b><?php echo $jelas['TJ']; ?></b></td>
                        </tr>
                        <tr>
                        	<td>Sangat tidak jelas</td>
                            <td><b><?php echo $jelas['STJ']; ?></b></td>
                        </tr>
                        <tr>
                        	<td><b>Total Responden</b></td>
                            <td><b><?php echo $total_jelas; ?></b></td>
                        </tr>
                    </table>
                </div>
			</div>
			<div class="col-lg-6">
  	          	<div class="col-md-12" style="text-align:center"><h4>Website memiliki tampilan yang menarik</h4></div>
				<div class="chart">
					<canvas id="piemenarik" style="height:230px"></canvas>
				</div>
                <div class="col-md-12">
                	<table width="100%" cellpadding="2" cellspacing="2" style="font-size:12px;">
                    	<tr>
                        	<td>Sangat menarik</td>
                            <td><b><?php echo $menarik['SM']; ?></b></td>
                        </tr>
                        <tr>
                        	<td>Menarik</td>
                            <td><b><?php echo $menarik['M']; ?></b></td>
                        </tr>
                        <tr>
                        	<td>Cukup menarik</td>
                            <td><b><?php echo $menarik['CM']; ?></b></td>
                        </tr>
                        <tr>
                        	<td>Tidak menarik</td>
                            <td><b><?php echo $menarik['TM']; ?></b></td>
                        </tr>
                        <tr>
                        	<td>Sangat tidak menarik</td>
                            <td><b><?php echo $menarik['STM']; ?></b></td>
                        </tr>
                        <tr >
                        	<td><b>Total Responden</b></td>
                            <td><b><?php echo $total_menarik; ?></b></td>
                        </tr>
                    </table>
                </div>
			</div>
            
            <div class="col-md-12" style="text-align:center; margin-top:20px;"> 
                <button type="button" class="btn btn btn-primary" data-toggle="modal" data-target="#modal-jelas">
                	Berikan Respon Anda
            	</button>
           	</div>
		</div>


		<div class="modal fade" id="modal-jelas">
          <div class="modal-dialog">
            <div class="modal-content">
            <form action="<?php echo base_url().'home/proses_review'?>" method="post" id="respondenForm">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Respon Pengunjung</h4>
              </div>
              <div class="modal-body">
                <p>Apakah Website menyediakan informasi yang cukup jelas ? </p>
                <label><input type="radio" name="jelas" value="SJ" required>Sangat Jelas</label></br>
                <label><input type="radio" name="jelas" value="J" required>Jelas</label></br>
                <label><input type="radio" name="jelas" value="CJ" required>Cukup Jelas</label></br>
                <label><input type="radio" name="jelas" value="TJ" required>Tidak Jelas</label></br>
                <label><input type="radio" name="jelas" value="STJ" required>Sangat Tidak Jelas</label></br>
                </br>
                <p>Apakah Website memiliki tampilan yang menarik ? </p>
                <label><input type="radio" name="menarik" value="SM" required>Sangat Menarik</label></br>
                <label><input type="radio" name="menarik" value="M" required>Menarik</label></br>
                <label><input type="radio" name="menarik" value="CM" required>Cukup Menarik</label></br>
                <label><input type="radio" name="menarik" value="TM" required>Tidak Menarik</label></br>
                <label><input type="radio" name="menarik" value="STM" required>Sangat Tidak Menarik</label></br>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
              </div>
              </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->


<script src="<?php echo base_url() . 'assets/plugins/chartjs/Chart.bundle.min.js'; ?>"></script>
<script src="<?php echo base_url() . 'assets/plugins/chartjs/utils.js'; ?>"></script>


<script>
$(function () {
	var dataJelas = {
		datasets: [{
			data: [
				<?php echo $jelas['SJ']; ?>,
				<?php echo $jelas['J']; ?>,
				<?php echo $jelas['CJ']; ?>,
				<?php echo $jelas['TJ']; ?>,
				<?php echo $jelas['STJ']; ?>
			],
			backgroundColor: [
				"#FF6384",
				"#333333",
				"#FFCE56",
				"#E7E9ED",
				"#36A2EB"
			],
			label: 'My dataset' // for legend
		}],
		labels: [
			"Red",
			"Purple",
			"Yellow",
			"Grey",
			"Blue"
		]
	};
	
	var pieOptionsJelas = {
	  events: true,
	  animation: {
		duration: 500,
		easing: "easeOutQuart",
		onComplete: function () {
		  var ctx = this.chart.ctx;
		  ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);
		  ctx.textAlign = 'center';
		  ctx.textBaseline = 'bottom';
	
		  this.data.datasets.forEach(function (dataset) {
	
			for (var i = 0; i < dataset.data.length; i++) {
			  var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
				  total = dataset._meta[Object.keys(dataset._meta)[0]].total,
				  mid_radius = model.innerRadius + (model.outerRadius - model.innerRadius)/2,
				  start_angle = model.startAngle,
				  end_angle = model.endAngle,
				  mid_angle = start_angle + (end_angle - start_angle)/2;
	
			  var x = mid_radius * Math.cos(mid_angle);
			  var y = mid_radius * Math.sin(mid_angle);
	
			  ctx.fillStyle = '#fff';
			  if (i == 3){ // Darker text color for lighter background
				ctx.fillStyle = '#444';
			  }
			  var percent = String(Math.round(dataset.data[i]/total*100)) + "%";
			  
			  // Display percent in another line, line break doesn't work for fillText
			  ctx.fillText(percent, model.x + x, model.y + y + 15);
			}
		  });               
		}
	  }
	};
	
	var pieChartCanvasJelas = $("#chart-area");
	var pieChartJelas = new Chart(pieChartCanvasJelas, {
	  type: 'pie', // or doughnut
	  data: dataJelas,
	  options: pieOptionsJelas
	});
	
	
	
	var dataMenarik = {
		datasets: [{
			data: [
				<?php echo $menarik['SM']; ?>,
				<?php echo $menarik['M']; ?>,
				<?php echo $menarik['CM']; ?>,
				<?php echo $menarik['TM']; ?>,
				<?php echo $menarik['STM']; ?>
			],
			backgroundColor: [
				"#FF6384",
				"#333333",
				"#FFCE56",
				"#E7E9ED",
				"#36A2EB"
			],
			label: 'My dataset' // for legend
		}],
		labels: [
			"Red",
			"Purple",
			"Yellow",
			"Grey",
			"Blue"
		]
	};
	
	var pieOptionsMenarik = {
	  events: true,
	  animation: {
		duration: 500,
		easing: "easeOutQuart",
		onComplete: function () {
		  var ctx = this.chart.ctx;
		  ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);
		  ctx.textAlign = 'center';
		  ctx.textBaseline = 'bottom';
	
		  this.data.datasets.forEach(function (dataset) {
	
			for (var i = 0; i < dataset.data.length; i++) {
			  var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
				  total = dataset._meta[Object.keys(dataset._meta)[0]].total,
				  mid_radius = model.innerRadius + (model.outerRadius - model.innerRadius)/2,
				  start_angle = model.startAngle,
				  end_angle = model.endAngle,
				  mid_angle = start_angle + (end_angle - start_angle)/2;
	
			  var x = mid_radius * Math.cos(mid_angle);
			  var y = mid_radius * Math.sin(mid_angle);
	
			  ctx.fillStyle = '#fff';
			  if (i == 3){ // Darker text color for lighter background
				ctx.fillStyle = '#444';
			  }
			  var percent = String(Math.round(dataset.data[i]/total*100)) + "%";
			  
			  // Display percent in another line, line break doesn't work for fillText
			  ctx.fillText(percent, model.x + x, model.y + y + 15);
			}
		  });               
		}
	  }
	};
	
	
	var pieChartCanvasMenarik = $("#piemenarik");
	var pieChartMenarik = new Chart(pieChartCanvasMenarik, {
	  type: 'pie', // or doughnut
	  data: dataMenarik,
	  options: pieOptionsMenarik
	});
	/*
	var color = Chart.helpers.color;
	var reviewconfig = {
        type: 'pie',
        data: {
            datasets: [{
                data: [
                    50,
                    35,
					10,
					5,
					0
                ],
                backgroundColor: [
                    chartColors.red,
                    chartColors.blue,
					chartColors.yellow,
					chartColors.purple,
					chartColors.grey
					
                ],
                label: 'Jelas'
            }],
            labels: [
                "Sangat Jelas ",
                "Jelas ",
				"Cukup Jelas ",
				"Tidak Jelas ",
				"Sangat Tidak Jelas "
            ],
			
        },
        options: {
            responsive: true
        }
		
    };

    var ctxreview = $("#chart-area").get(0).getContext("2d");
    var myPie = new Chart(ctxreview, reviewconfig);
	

	var reviewconfigkam = {
        type: 'pie',
        data: {
            datasets: [{
                data: [
                    40%,
                    50%,
					10%,
					3%,
					2%
                ],
                backgroundColor: [
                    chartColors.red,
                    chartColors.blue,
					chartColors.yellow,
					chartColors.purple,
					chartColors.grey
                ],
                label: 'Menarik'
            }],
            labels: [
                "Sangat Menarik ",
                "Menarik ",
				"Cukup Menarik ",
				"Tidak Menarik ",
				"Sangat Tidak Menarik "
            ]
        },
        options: {
            responsive: true
        }
    };

    var ctxkam = $("#piemenarik").get(0).getContext("2d");
    var PieKamp = new Chart(ctxkam, reviewconfigkam);

*/
});
</script>



<script type="text/javascript">
	$( function() {
    	$('.carousel').carousel({
			interval: 4000 //changes the speed
		})
		
  	} );
</script>

<?php
	require_once('footer.php');
?>

</div>

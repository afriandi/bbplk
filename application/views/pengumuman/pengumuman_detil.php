<!-- Page Heading/Breadcrumbs -->
<div class="row">
   	<div class="col-lg-12">
       	<h1 class="page-header">{title_menu}</h1>
        <div class="sharebox" data-title="BLK Bandung" data-url="<?php echo base_url().'pengumuman/detail/'; ?>{pengumuman_id}" style="text-align:right;vertical-align:central;"></div>
        <ol class="breadcrumb">
	        <li><a href="../{base_url}home">Home</a></li>
            <li class="active">{menu}</li>
        </ol>
    </div>
</div>
<!-- /.row -->
<div class="row">
	<!-- Blog Post Content Column -->
    <div class="col-md-12">
	    <!-- Blog Post -->
        <hr>
        <!-- Date/Time -->
        <p><i class="fa fa-clock-o"></i> Diposting pada tanggal {pengumuman_date} </p>
        <hr>

        <!-- Preview Image -->
        <center>
 	       <img class="img-responsive" src="{pengumuman_img}" alt="">
        </center>
        <hr>

        <!-- Post Content -->
        <p class="lead">{pengumuman_title}</p>
        <p>{pengumuman_desc}</p>
        <br>
        <div class="tombol_kanan">
          	<button type="button" name="back" class="btn btn-primary" onclick="history.go(-1);"><span class="glyphicon glyphicon-ok"></span>&nbsp; Kembali &nbsp;</button>
        </div>
	</div>
</div>

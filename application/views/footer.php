<div class="footer_margin">&nbsp; </div>
			<hr>
            <!-- Footer -->
            <div class="row" id="footer">
                <div class="col-sm-12">
                    <p>Copyright &copy; TIK BBPLK 2016</p>
                </div>
            </div>
            <footer class="text-center">
              <a class="up-arrow" href="#atas" data-toggle="tooltip" title="Kembali ke atas">
                <span class="glyphicon glyphicon-chevron-up"></span>
              </a>
            </footer>
		</div>


        <script type="application/javascript">

				// Add smooth scrolling to all links in navbar + footer link
				$("footer a[href='#atas']").on('click', function(event) {
					// Make sure this.hash has a value before overriding default behavior
					if (this.hash !== "") {
					// Prevent default anchor click behavior
					event.preventDefault();

					// Store hash
					var hash = this.hash;

					// Using jQuery's animate() method to add smooth page scroll
					// The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
					$('html, body').animate({
						scrollTop: $(hash).offset().top
					}, 900, function(){

					// Add hash (#) to URL when done scrolling (default click behavior)
					window.location.hash = hash;
				  });
				} // End if
			  });

			  $(window).scroll(function() {
				$(".slideanim").each(function(){
					var pos = $(this).offset().top;

					var winTop = $(window).scrollTop();
					if (pos < winTop + 600) {
					  $(this).addClass("slider");
					}
				});
			  });


			  // Initialize Tooltip
			  $('[data-toggle="tooltip"]').tooltip();

			  // Add smooth scrolling to all links in navbar + footer link
			  $("footer a[href='#atas']").on('click', function(event) {

				// Make sure this.hash has a value before overriding default behavior
				if (this.hash !== "") {

				  // Prevent default anchor click behavior
				  event.preventDefault();

				  // Store hash
				  var hash = this.hash;

				  // Using jQuery's animate() method to add smooth page scroll
				  // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
				  $('html, body').animate({
					scrollTop: $(hash).offset().top
				  }, 900, function(){

					// Add hash (#) to URL when done scrolling (default click behavior)
					window.location.hash = hash;
				  });
				} // End if
			  });


			})
			</script>

			<!--Start of Tawk.to Script-->
			<script type="text/javascript">
			var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
			(function(){
			var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
			s1.async=true;
			s1.src='https://embed.tawk.to/595c4b49e9c6d324a4738d3b/default';
			s1.charset='UTF-8';
			s1.setAttribute('crossorigin','*');
			s0.parentNode.insertBefore(s1,s0);
			})();
			</script>
			<!--End of Tawk.to Script-->

	</body>
</html>

<div class="container">
	<!-- Page Heading/Breadcrumbs -->
    <div class="row">
    	<div class="col-lg-12">
        	<h1 class="page-header">{title_menu}</h1>
                <ol class="breadcrumb">
                    <li><a href="<?php echo base_url() . 'home' ?>">Home</a>
                    </li>
                    <li class="active">{menu}</li>
                </ol>
        </div>
        <div class="col-lg-12">
        	<div class="form_login">
                <div class="jumbotron login">
                  	<form class="login" action="{action}" method="post">
                    	<div class="input-group">
                      		<span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-user"></span></span>
                      		<input type="text" name="username" placeholder="Username" aria-describedby="basic-addon1" class="form-control">
                    	</div>
                    	<br>
                    	<div class="input-group">
                      		<span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-lock"></span></span>
       		               <input type="password" name="password" placeholder="Password" class="form-control">
                    	</div>
                    	<br>
                    	<button type="submit" name="button" class="btn btn-default">Login</button>
                  	</form>
                </div>
            </div>
            <div style="text-align:center;">
	        	<h4>{message}</h4>
            </div>
    	</div>
	</div>
    <!-- /.row -->
</div>

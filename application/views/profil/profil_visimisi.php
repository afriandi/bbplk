<!-- Page Heading/Breadcrumbs -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">{title_menu}</h1>
        <div class="sharebox" data-title="BLK Bandung" data-url="<?php echo base_url().'profil/visimisi'; ?>" style="text-align:right;vertical-align:central;"></div>
        <ol class="breadcrumb">
            <li><a href="../{base_url}home">Home</a></li>
            <li><a href="#">{menu}</a></li>
            <li class="active">{submenu}</li>
        </ol>
    </div>
</div>
<!-- /.row -->
<!-- Intro Content -->
<div class="row">
    <div class="col-md-6">
        <p>
          <!-- Content Row -->
          </p><div class="row">
              <div class="col-lg-12">
                  <div class="panel-group" >
                      <div class="panel panel-default">
                          <div class="panel-heading">
                              <h4 class="panel-title">
                                  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Visi</a>
                              </h4>
                          </div>
                          <div id="collapseOne" class="panel-collapse collapse in">
                              <div class="panel-body">
                                {profile_visi}
                              </div>
                          </div>
                      </div>
                      <!-- /.panel -->
                      <div class="panel panel-default">
                          <div class="panel-heading">
                              <h4 class="panel-title">
                                  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Misi</a>
                              </h4>
                          </div>
                          <div id="collapseTwo" class="panel-collapse collapse in">
                              <div class="panel-body">
                                 {profile_misi}
</ul>                                 </div>
                          </div>
                      </div>
                      <!-- /.panel -->
                  </div>
                  <!-- /.panel-group -->
              </div>
              <!-- /.col-lg-12 -->
          </div>
          <!-- /.row -->
        <p></p>
    </div>
    <div class="col-md-6">
        <br>
        <img class="img-responsive" src="{profile_img}" alt="">
    </div>
</div>
<!-- /.row -->

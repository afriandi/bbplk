<!-- Page Heading/Breadcrumbs -->
	<div class="row">
    	<div class="col-lg-12">
        	<h1 class="page-header">{title_menu}</h1>
            <div class="sharebox" data-title="BLK Bandung" data-url="<?php echo base_url().'profil/detil_fasilitas'; ?>" style="text-align:right;vertical-align:central;"></div>
            <ol class="breadcrumb">
                <li><a href="../{base_url}home">Home</a></li>
                <li><a href="#">{menu}</a></li>
                <li class="active">{submenu}</li>
            </ol>
        </div>
    </div>
<!-- /.row -->
        <!-- Portfolio Item Row -->
        <div class="row">
					<div class="col-md-12">
					 	<!-- Portfolio Item Row -->
					    <img src="{fasilitas_img}" alt="" width="100%">
					    <h3>{fasilitas_title}</h3>
					    <p>{fasilitas_desc}</p>
					    <p>Status Kepemilikan : Kejuruan {fasilitas_milik}</p>
							<div class="tombol_kanan">
          				<button type="button" name="back" class="btn btn-primary" onclick="history.go(-1);"><span class="glyphicon glyphicon-ok"></span>&nbsp; Kembali &nbsp;</button>
        			</div>
					</div>

        </div>
        <!-- /.row -->

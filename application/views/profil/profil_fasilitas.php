<!-- Page Heading/Breadcrumbs -->
	<div class="row">
    	<div class="col-lg-12">
        	<h1 class="page-header">{title_menu}</h1>
            <div class="sharebox" data-title="BLK Bandung" data-url="<?php echo base_url().'profil/fasilitas'; ?>" style="text-align:right;vertical-align:central;"></div>
            <ol class="breadcrumb">
                <li><a href="../{base_url}home">Home</a></li>
                <li><a href="#">{menu}</a></li>
                <li class="active">{submenu}</li>
            </ol>
        </div>
    </div>
<!-- /.row -->
<!-- Project One -->
	{fasilitas}
    <div class="row">
        <div class="col-md-7">
            <a href="../portfolio-item.html">
                <img class="img-responsive img-hover" src="{fasilitas_img}" alt="" width="600px">
            </a>
        </div>
        <div class="col-md-5">
            <h3>{fasilitas_title}</h3>
            {fasilitas_desc}
            <br>
            <br>
            <a class="btn btn-primary" href="{fasilitas_detail}">Lihat Fasilitas</a>
        </div>
    </div>
    <!-- /.row -->
    <hr>
    {/fasilitas}
		<div class="row">
			<div class="col-md-12" align="right">
				{pagination}
			</div>
		</div>

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{title_menu}</h1>
                <div class="sharebox" data-title="BLK Bandung" data-url="<?php echo base_url().'profile'; ?>" style="text-align:right;vertical-align:central;"></div>
                <ol class="breadcrumb">
                    <li><a href="../{base_url}home">Home</a></li>
                    <li><a href="#">{menu}</a></li>
                    <li class="active">{submenu}</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->

        <!-- Intro Content -->
        <div class="row">
            <div class="col-md-6">
                <br><img class="img-responsive" src="{profile_img}" alt="">
            </div>
            <div class="col-md-6">
                <h2>{profile_title}</h2>
                {profile_desc}
            </div>
        </div>
        <!-- /.row -->
<!-- Page Heading/Breadcrumbs -->
	<div class="row">
    	<div class="col-lg-12">
        	<h1 class="page-header">{title_menu}</h1>
            <div class="sharebox" data-title="BLK Bandung" data-url="<?php echo base_url().'profil/regulasi'; ?>" style="text-align:right;vertical-align:central;"></div>
            <ol class="breadcrumb">
                <li><a href="../{base_url}home">Home</a></li>
                <li><a href="#">{menu}</a></li>
                <li class="active">{submenu}</li>
            </ol>
        </div>
    </div>
<!-- /.row -->
<form class="" action="" method="post">
	<div class="row">
    	<div class="well regulasi">
        {table}
				<div class="row">
					<div class="col-md-12" align="right">
						{pagination}
					</div>
				</div>
      </div>
  </div>
</form>

<!-- /.row -->

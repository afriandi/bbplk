<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8'>
        <meta http-equiv='X-UA-Compatible' content='IE=edge'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <meta name='description' content=''>
        <meta name='author' content=''>
        <title>{title}</title>
         <!-- Bootstrap Core CSS -->
    	<link href='<?php echo base_url() . 'assets/css/bootstrap.css' ?>' rel='stylesheet'>
    	<!-- Custom CSS -->
        <link href='<?php echo base_url() . 'assets/css/modern-business.css' ?>' rel='stylesheet'>
    	<!-- Custom Fonts -->
        <link href='<?php echo base_url() . 'assets/font-awesome/css/font-awesome.min.css' ?>' rel='stylesheet'>
        
        <link href='<?php echo base_url() . 'assets/plugins/fb-shared/src/jquery.sharebox.css' ?>' rel='stylesheet'>
    	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    	<!--[if lt IE 9]>
        <script src='https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js'></script>
        <script src='https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js'></script>
    	<![endif]-->


        <!-- GALERI -->
        <link href='<?php echo base_url() . 'assets/plugins/hydrogen/css/animate.css' ?>' rel='stylesheet'>
        <link href='<?php echo base_url() . 'assets/plugins/hydrogen/css/icomoon.css' ?>' rel='stylesheet'>
        <link href='<?php echo base_url() . 'assets/plugins/hydrogen/css/magnific-popup.css' ?>' rel='stylesheet'>
        <link href='<?php echo base_url() . 'assets/plugins/hydrogen/css/salvattore.css' ?>' rel='stylesheet'>
        <link href='<?php echo base_url() . 'assets/plugins/hydrogen/css/style.css' ?>' rel='stylesheet'>
        

    </head>
    <body>
    <?php
		require_once('menu.php');
	?>
<a href="#" id="atas"></a>
<div class="container">

<!-- Page Heading/Breadcrumbs -->
<div class="row">
   	<div class="col-lg-12">
       	<h1 class="page-header">{title_menu}</h1>
        <div class="sharebox" data-title="BLK Bandung" data-url="<?php echo base_url().'galeri'; ?>" style="text-align:right;vertical-align:central;"></div>
        <ol class="breadcrumb">
            <li><a href="galeri/{base_url}home">Home</a></li>
        	<li class="active">{menu}</li>
    	</ol>
	</div>
</div>
<!-- /.row -->
<div id="fh5co-main">

        <div id="fh5co-board" data-columns>
			{galeri}
        	<div class="item">
        		<div class="animate-box">
	        		<a href="{galeri_img}" class="image-popup fh5co-board-img" title="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo, eos?"><img src="{galeri_img}" alt="Free HTML5 Bootstrap template"></a>
        		</div>
        		<div class="fh5co-desc">{galeri_title}</div>
        	</div>
            {/galeri}
        </div>

	</div>

	<!-- jQuery -->
	<script src="<?php echo base_url() . 'assets/plugins/hydrogen/'; ?>js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="<?php echo base_url() . 'assets/plugins/hydrogen/'; ?>js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="<?php echo base_url() . 'assets/plugins/hydrogen/'; ?>js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="<?php echo base_url() . 'assets/plugins/hydrogen/'; ?>js/jquery.waypoints.min.js"></script>
	<!-- Magnific Popup -->
	<script src="<?php echo base_url() . 'assets/plugins/hydrogen/'; ?>js/jquery.magnific-popup.min.js"></script>
	<!-- Salvattore -->
	<script src="<?php echo base_url() . 'assets/plugins/hydrogen/'; ?>js/salvattore.min.js"></script>
	<!-- Main JS -->
	<script src="<?php echo base_url() . 'assets/plugins/hydrogen/'; ?>js/main.js"></script>
    <script type="application/javascript" src="<?php echo base_url() . 'assets/plugins/fb-shared/src/jquery.sharebox.js' ?>"></script>

   			<hr>
            <!-- Footer -->
            <div class="row">
                <div class="col-sm-12">
                    <p>Copyright &copy; TIK BBPLK 2016</p>
                </div>
            </div>
            <footer class="text-center">
              <a class="up-arrow" href="#atas" data-toggle="tooltip" title="Kembali ke atas">
                <span class="glyphicon glyphicon-chevron-up"></span>
              </a>
            </footer>
		</div>


        <script type="application/javascript">
			$(document).ready(function(){

				// Add smooth scrolling to all links in navbar + footer link
				$("footer a[href='#atas']").on('click', function(event) {
					// Make sure this.hash has a value before overriding default behavior
					if (this.hash !== "") {
					// Prevent default anchor click behavior
					event.preventDefault();

					// Store hash
					var hash = this.hash;

					// Using jQuery's animate() method to add smooth page scroll
					// The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
					$('html, body').animate({
						scrollTop: $(hash).offset().top
					}, 900, function(){

					// Add hash (#) to URL when done scrolling (default click behavior)
					window.location.hash = hash;
				  });
				} // End if
			  });

			  $(window).scroll(function() {
				$(".slideanim").each(function(){
					var pos = $(this).offset().top;

					var winTop = $(window).scrollTop();
					if (pos < winTop + 600) {
					  $(this).addClass("slider");
					}
				});
			  });


			  // Initialize Tooltip
			  $('[data-toggle="tooltip"]').tooltip();

			  // Add smooth scrolling to all links in navbar + footer link
			  $("footer a[href='#atas']").on('click', function(event) {

				// Make sure this.hash has a value before overriding default behavior
				if (this.hash !== "") {

				  // Prevent default anchor click behavior
				  event.preventDefault();

				  // Store hash
				  var hash = this.hash;

				  // Using jQuery's animate() method to add smooth page scroll
				  // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
				  $('html, body').animate({
					scrollTop: $(hash).offset().top
				  }, 900, function(){

					// Add hash (#) to URL when done scrolling (default click behavior)
					window.location.hash = hash;
				  });
				} // End if
			  });


			})
			</script>

	</body>
</html>

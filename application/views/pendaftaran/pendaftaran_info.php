<!-- Page Heading/Breadcrumbs -->
	<div class="row">
    	<div class="col-lg-12">
        	<h1 class="page-header">{title_menu}</h1>
            <div class="sharebox" data-title="BLK Bandung" data-url="<?php echo base_url().'pendaftaran'; ?>" style="text-align:right;vertical-align:central;"></div>
            <ol class="breadcrumb">
                <li><a href="../{base_url}home">Home</a></li>
                <li><a href="../{base_url}pendaftaran">{menu}</a></li>
                <li class="active">{submenu}</li>
            </ol>
        </div>
    </div>
<!-- /.row -->
<div class="row">
		<div class="col-md-12">
				<table class="table table-bordered table-striped cellpadding="1" cellspacing="1">
								<thead>
								<tr>
										<th>
												<label for="no">NO</label>
										</th>
										<th>
												<label for="program_pelatihan">PROGRAM</label>
										</th>
										<th>
												<label for="tahap">TAHAP</label>
										</th>
										<th>
												<label for="asrama">ASRAMA</label>
										</th>
										<th colspan="3">
												<label for="tgl_pendaftaran">PENDAFTARAN</label>
										</th>
										<th colspan="3">
												<label for="tgl_mulai">PELATIHAN</label>
										</th>
										<th>
												<label for="status">STATUS</label>
										</th>
										<th>
												<label for="tombol"></label>
										</th>
								</tr>
								</thead>
								<tbody>
								{info_pendaftaran}
								<tr align="center">
										<td>{no}</td>
										<td>{program_pelatihan}</td>
										<td>{tahap}</td>
										<td>{asrama}</td>
										<td>{tgl_pendaftaran_mulai}</td>
										<td>s.d</td>
										<td>{tgl_pendaftaran_selesai}</td>
										<td>{tgl_mulai}</td>
										<td>s.d</td>
										<td>{tgl_selesai}</td>
										<td>{status}</td>
										<td>
											<a href="<?=base_url().'pendaftaran/detail_pendaftaran/';?>{id_detail_pelatihan}">
												<button class="btn btn-xs btn-info" type="button" name="button">Detail</button>
											</a>
											<a href="<?=base_url().'pendaftaran/daftar/';?>">
												<button class="btn btn-xs btn-success" type="button" name="button">Daftar</button>
											</a>
										</td>
								</tr>
								{/info_pendaftaran}
						</tbody>
				</table>
		</div>
	</div>

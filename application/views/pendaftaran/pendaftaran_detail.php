<!-- Page Heading/Breadcrumbs -->
	<div class="row">
    	<div class="col-lg-12">
        	<h1 class="page-header">{title_menu}</h1>
            <ol class="breadcrumb">
                <li><a href="../{base_url}home">Home</a></li>
                <li><a href="../{base_url}pendaftaran">{menu}</a></li>
                <li class="active">{submenu}</li>
            </ol>
        </div>
    </div>
<!-- /.row -->
<div class="row">
		<div class="col-md-12">
				<div class="well">
          <table class="table table-bordered table-striped" cellpadding="1" cellspacing="1">
              <tbody>
                  <tr>
                    <th colspan="2">DATA PELATIHAN</th>
                  </tr>
                  <tr>
                      <td width="30%"><label for="">PROGRAM PELATIHAN</label></td>
                      <td>{program_pelatihan}</td>
                  </tr>
                  <tr>
                    <td><label for="">KEJURUAN</label></td>
                    <td>{nama}</td>
                  </tr>
                  <tr>
                      <td><label for="">JUMLAH JP</label></td>
                      <td>{jml_jp}</td>
                  </tr>
                  <tr>
                    <td><label for="">TAHUN</label></td>
                    <td>{tahun}</td>
                  </tr>
                  <tr>
                      <td><label for="">URUTAN</label></td>
                      <td>{urutan}</td>
                  </tr>
                  <tr>
                      <td><label for="">TAHAP</label></td>
                      <td>{tahap}</td>
                  </tr>
                  <tr>
                      <td><label for="">STATUS</label></td>
                      <td>{status}</td>
                  </tr>
                  <tr>
                      <td><label for="">ASRAMA</label></td>
                      <td>{asrama}</td>
                  </tr>
                  <tr>
                      <td><label for="">PENDAFTARAN</label></td>
                      <td>{tgl_pendaftaran_mulai} s.d {tgl_pendaftaran_selesai}</td>
                  </tr>
                  <tr>
                      <td><label for="">TANGGAL REKRUTMEN</label></td>
                      <td>{tgl_rekrutmen}</td>
                  </tr>
                  <tr>
                      <td><label for="">TANGGAL PENGUMUMAN</label></td>
                      <td>{tgl_pengumuman}</td>
                  </tr>
                  <tr>
                      <td><label for="">TANGGAL DAFTAR ULANG</label></td>
                      <td>{tgl_daftar_ulang}</td>
                  </tr>
                  <tr>
                      <td><label for="">JADWAL PELATIHAN</label></td>
                      <td>{tgl_mulai} s.d {tgl_selesai}</td>
                  </tr>
                  <tr>
                      <td><label for="">UJI KOMPETENSI</label></td>
                      <td>{tgl_ujk}</td>
                  </tr>
              </tbody>
          </table>
					<div class="tombol_kanan">
							<button type="button" name="back" class="btn btn-sm btn-success" onclick="history.go(-1);"><span class="glyphicon glyphicon-ok"></span>&nbsp; Kembali &nbsp;</button>
					</div>
          <!-- <a href="<?=base_url().'pendaftaran/';?>">
            <button class="btn btn-xs btn-success" type="button" name="button">Kembali</button>
          </a> -->
				</div>
		</div>
	</div>

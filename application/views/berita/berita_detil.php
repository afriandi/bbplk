<!-- Page Heading/Breadcrumbs -->
<div class="row">
   	<div class="col-lg-12">
       	<h1 class="page-header">{title_menu}</h1>
        <div class="sharebox" data-title="BLK Bandung" data-url="<?php echo base_url().'berita/detail/'; ?>{berita_id}" style="text-align:right;vertical-align:central;"></div>
        <ol class="breadcrumb">
	        <li><a href="../{base_url}home">Home</a></li>
            <li class="active">{menu}</li>
        </ol>
    </div>
</div>
<!-- /.row -->
<div class="row">
	<!-- Blog Post Content Column -->
    <div class="col-sm-12">
	    <!-- Blog Post -->
        <hr>
        <!-- Date/Time -->
        <p><i class="fa fa-clock-o"></i> Diposting pada tanggal {berita_date} </p>
        <p><i class="glyphicon glyphicon-user"></i> Oleh {berita_postby} </p>
        <hr>

        <!-- Preview Image -->
        <center>
 	       <img class="img-responsive" src="{berita_img}" alt="">
        </center>
        <hr>

        <!-- Post Content -->
        <p class="lead">{berita_title}</p>
        <p>{berita_desc}</p>
        <br>
        <div class="tombol_kanan">
          	<button type="button" name="back" class="btn btn-primary" onclick="history.go(-1);"><span class="glyphicon glyphicon-ok"></span>&nbsp; Kembali &nbsp;</button>
        </div>
	</div>
</div>

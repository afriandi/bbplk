<!-- Page Heading/Breadcrumbs -->
	<div class="row">
    	<div class="col-lg-12">
        	<h1 class="page-header">{title_menu}</h1>
            <div class="sharebox" data-title="BLK Bandung" data-url="<?php echo base_url().'berita'; ?>" style="text-align:right;vertical-align:central;"></div>
            <ol class="breadcrumb">
                <li><a href="../{base_url}home">Home</a></li>
                <li class="active">{menu}</li>
            </ol>
        </div>
    </div>
<!-- /.row -->
		{berita}
        <!-- /.row -->
        <div class="row">
            <div class="col-md-1 text-center">
                <p><i class="fa fa-calendar fa-4x"></i></p>
                <p>{berita_date}</p>
            </div>
            <div class="col-md-5">
                <a href="{berita_detail}">
                    <img class="img-responsive img-hover" src="{berita_img}" alt="">
                </a>
            </div>
            <div class="col-md-6">
                <h3>
                    <a href="{berita_detail}">{berita_title}</a>
                </h3>
                <p>by <a href="">{berita_postby}</a></p>
                <p>{isi_berita}</p>
                <a class="btn btn-primary" href="{berita_detail}">Baca Lebih Lanjut &nbsp;<i class="fa fa-angle-right"></i></a>
            </div>
        </div>
        <!-- /.row -->
        <hr>
        {/berita}

				<div class="row">
					<div class="col-md-12" align="right">
						<?php echo $pagination;?>
					</div>
				</div>
        <!-- Pager
        <div class="row">
            <ul class="pager">
                <li class="previous"><a href="#">← Older</a>
                </li>
                <li class="next"><a href="#">Newer →</a>
                </li>
            </ul>
        </div> -->
        <!-- /.row -->

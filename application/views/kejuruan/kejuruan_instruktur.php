<div class="col-md-9">
	{instruktur}
	<div class="col-md-4 text-center">
    	<div class="thumbnail" id="thumbnail_instruktur">
				<br>
        	<img class="img-responsive" src="{instruktur_img}" alt="" width="100px">
            <div class="caption" id="instruktur">
            	<h3>{instruktur_name}<br>
                <small>{instruktur_nip}</small>
                </h3>
                <p>{instruktur_desc}</p>
                <ul class="list-inline">
                	<li>
                    	<a href="{instruktur_fb}"><i class="fa fa-2x fa-facebook-square"></i>
						</a>
                    </li>
                    <li>
                    	<a href="{instruktur_linkedin}"><i class="fa fa-2x fa-linkedin-square"></i>
						</a>
                    </li>
                    <li>
                    	<a href="{instruktur_twitter}"><i class="fa fa-2x fa-twitter-square"></i>
						</a>
                    </li>
                </ul>
            </div>
      	</div>
    </div>
    {/instruktur}
		<div class="row">
			<div class="col-md-12" align="right">
				{pagination}
			</div>
		</div>
</div>

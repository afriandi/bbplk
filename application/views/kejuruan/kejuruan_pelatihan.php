<div class="col-md-9">

    	{pelatihan}
    	<div class="col-md-6 col-sm-6">
        	<div class="panel panel-default text-center">
            	<div class="panel-heading operator"></div>
                <div class="panel-body">
                 	<h4>{pelatihan_title}</h4>
                    <p>Merupakan pelatihan berbasi kompetensi dengan jumlah jam {pelatihan_jp} JP.</p>
                    <a href="<?php echo base_url(); ?>kejuruan/detail_pelatihan/{pelatihan_detail_id}" class="btn btn-primary">Lihat Detail</a>
                 </div>
             </div>
         </div>
         {/pelatihan}
         <div class="row">
     			<div class="col-md-12" align="right">
     				{pagination}
     			</div>
     		</div>
</div>

<div class="col-md-9">
	<form class="" action="" method="post">
    	<div class="well detail_pegawai">
        	<table class="table table-bordered table-striped" cellpadding="1" cellspacing="1">
                <tbody>
								<tr>
                  <td class="kolom_detail_pegawai1">
                    <label for="">NO SKEMA</label>
                  </td>
                  <td class="kolom_detail_pegawai4">
                    <!-- <input name="no_skema" value="" class="form-control input-sm" disabled="" type="text"> -->
										{sertifikasi_no_skema}
									</td>
                </tr>
                <tr>
                  <td class="kolom_detail_pegawai1">
                    <label for="">JUDUL SKEMA</label>
                  </td>
                  <td class="kolom_detail_pegawai4">
                    <!-- <input name="judul_skema" value="{sertifikasi_title}" class="form-control input-sm" disabled="" type="text"> -->
										{sertifikasi_title}
                  </td>
                </tr>
                <tr>
                  <td class="kolom_detail_pegawai1">
                    <label for="">DESKRIPSI</label>
                  </td>
                  <td class="kolom_detail_pegawai4">
                    <!-- <textarea name="deskripsi" rows="20" cols="60" class="form-control" disabled=""></textarea> -->
										{sertifikasi_desc}
                  </td>
                </tr>
            </tbody>
        </table>
        <br>
        <div class="tombol_kanan">
          	<button type="button" name="back" class="btn btn-primary" onclick="history.go(-1);"><span class="glyphicon glyphicon-ok"></span>&nbsp; Kembali &nbsp;</button>
        </div>
    	</div>
	</form>
</div>

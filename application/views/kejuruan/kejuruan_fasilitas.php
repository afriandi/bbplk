<div class="col-md-9">
	{fasilitas}
    <div class="row">
        <div class="col-md-7">
            <a href="../portfolio-item.html">
                <img class="img-responsive img-hover" src="{fasilitas_img}" alt="">
            </a>
        </div>
        <div class="col-md-5">
            <h3>{fasilitas_title}</h3>
            <p>{fasilitas_desc}</p>
            <a class="btn btn-primary" href="{fasilitas_detil}">Lihat Fasilitas</a>
        </div>
    </div>
		<br>
    {/fasilitas}
		<div class="row">
			<div class="col-md-12" align="right">
				{pagination}
			</div>
		</div>
</div>

<div class="col-md-9">
 	<!-- Portfolio Item Row -->
    <img src="{fasilitas_img}" alt="" width="100%">
    <h3>{fasilitas_title}</h3>
    <p>{fasilitas_desc}</p>
    <p>Status Kepemilikan : Kejuruan {fasilitas_milik}</p>
    <div class="tombol_kanan">
        <button type="button" name="back" class="btn btn-primary" onclick="history.go(-1);"><span class="glyphicon glyphicon-ok"></span>&nbsp; Kembali &nbsp;</button>
    </div>
</div>

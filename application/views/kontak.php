        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Kontak
                </h1>
                <div class="sharebox" data-title="BLK Bandung" data-url="<?php echo base_url().'kontak'; ?>{berita_id}" style="text-align:right;vertical-align:central;"></div>
                <ol class="breadcrumb">
                    <li><a href="/bbplk/app/index.php">Home</a>
                    </li>
                    <li class="active">Kontak</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
        <!-- Content Row -->
        <div class="row">
            <!-- Map Column -->
            <div class="col-md-8">
                <!-- Embedded Google Map -->
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3960.678760869473!2d107.63561251436752!3d-6.9289469697471135!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e68e87646aa8c91%3A0x86f0777ca52c54f5!2sBBPLKDN+%2F+NVTDC+BANDUNG!5e0!3m2!1sen!2sid!4v1474015075418" style="border:0" allowfullscreen="" width="100%" height="450" frameborder="0"></iframe>
            </div>
            <!-- Contact Details Column -->
            <div class="col-md-4">
                <h3>Alamat Kantor BLK Bandung :</h3>
                <p>Jl. Jenderal Gatot Subroto No. 170 Bandung<br>
                </p>
                <p><i class="fa fa-phone"></i>
                    <abbr title="Phone"></abbr>: 022 731 2564</p>
                <p><i class="fa fa-envelope-o"></i>
                    <abbr title="Email"></abbr>: <a href="mailto:oktinindyati@gmail.com">admin@blkbandung.com</a>
                </p>
                <p><i class="fa fa-clock-o"></i>
                    <abbr title="Hours"></abbr>: Senin - Jumat: 07:30 s.d 16:00</p>
                <ul class="list-unstyled list-inline list-social-icons">
                    <li>
                        <a href=""><i class="fa fa-facebook-square fa-2x"></i></a>
                    </li>
                    <li>
                        <a href=""><i class="fa fa-twitter-square fa-2x"></i></a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /.row -->
<br>

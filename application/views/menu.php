<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <img src='<?php echo base_url() .'assets/img/logo.png';?>' alt="" width="60px"/>
        <a href="https://web.facebook.com/pelatihan.kerja.gratis.bandung/" target="_blank">
          <img src='<?php echo base_url() .'assets/img/facebook.png';?>' alt="" width="25px"/>
        </a>
        <a href="https://twitter.com/bbplk_bdg" target="_blank">
          <img src='<?php echo base_url() .'assets/img/twitter.png';?>' alt="" width="25px"/>
        </a>
        <a href="https://www.instagram.com/bbplk_bdg/" target="_blank">
          <img src='<?php echo base_url() .'assets/img/instagram2.png';?>' alt="" width="25px"/>
        </a>
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href='<?php echo base_url() .'/index.html';?>'>
          </a>
      </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
            	<li>
                    <a href="<?php echo base_url(); ?>home">Home</a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Profil<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="<?php echo base_url(); ?>profil/index">Tentang</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>profil/visimisi">Visi Misi</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>profil/struktur">Struktur Organisasi</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>profil/fasilitas">Fasilitas</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>profil/regulasi">Regulasi</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Kejuruan <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="<?php echo base_url(); ?>kejuruan/profil/oto">Otomotif</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>kejuruan/profil/tekman">Teknik Manufaktur</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Pendaftaran <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="<?php echo base_url(); ?>pendaftaran">Informasi</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>pendaftaran/daftar">Daftar</a>
                        </li>
                    </ul>
                </li>


                <li>
                    <a href="<?php echo base_url(); ?>pengumuman">Pengumuman</a>
                </li>
                <li>
                    <a href="<?php echo base_url(); ?>berita">Berita</a>
                </li>
                <li>
                    <a href="<?php echo base_url(); ?>galeri">Galeri</a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Informasi <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                          	<a href="<?php echo base_url(); ?>info">Rekap Peserta</a>
                        </li>
                        <li>
                          	<a href="<?php echo base_url(); ?>info/pelatihan">Rekap Pelatihan</a>
                        </li>
                        <li>
                          	<a href="<?php echo base_url(); ?>info/penempatan">Rekap Penempatan</a>
                        </li>
                        <li>
                          	<a href="<?php echo base_url(); ?>info/lowker">Lowongan Pekerjaan</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="<?php echo base_url(); ?>kontak">Kontak</a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Link <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="http://www.kemnaker.go.id" target="_blank">Kemnaker</a>
                        </li>
                        <li>
                          <a href="http://www.kios3in1.net/001/" target="_blank">Kios3in1</a>
                        </li>
                    </ul>
                </li>
                {islogin}
        	</ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

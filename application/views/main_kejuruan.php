<?php 
	require_once('header.php');	
	require_once('menu.php'); 
?>
<a href="#" id="atas"></a>
<div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{title_menu}</h1>
                <div class="sharebox" data-title="BLK Bandung" data-url="<?php echo base_url().'kejuruan/oto'; ?>" style="text-align:right;vertical-align:central;"></div>
                <ol class="breadcrumb">
                    <li><a href="<?php echo base_url() ?>/home">Home</a></li>
                    <li><a href="#">{menu}</a>
                    </li>
                    <li class="active">{submenu}</li>
                </ol>
            </div>
        </div>

        <div class="row">
            <?php require_once('kejuruan_menu_left.php')?>
            <!-- Content Column -->
			{content}
        </div>
        <!-- /.row -->
	<?php require_once('footer.php'); ?>
</div>



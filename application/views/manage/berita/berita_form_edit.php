{message}
<div class="col-sm-12">
	<img src="{img}" height="200">
</div>
<div class="footer_margin">&nbsp;</div>
<form method="post" action="{action}"  enctype="multipart/form-data">
    <div class="form-group">
        <label for="judul" class="col-sm-2 control-label">Judul</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="judul" id="judul" placeholder="Judul" value="<?php echo set_value('judul', $this->form_data->judul); ?>">
            <input type="hidden" name="id" id="id" value="<?php echo set_value('id', $this->form_data->id); ?>" >
            <input type="hidden" name="img" id="img" value="<?php echo set_value('img', $this->form_data->img); ?>" >
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="tgl" class="col-sm-2 control-label">Tanggal_Posting</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="tgl" id="tgl" value="<?php echo set_value('tgl', $this->form_data->tgl); ?>" placeholder="Tanggal Posting">
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="pos" class="col-sm-2 control-label">Diposting_Oleh</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="pos" id="pos" value="<?php echo set_value('pos', $this->form_data->pos); ?>" placeholder="Diposting Oleh">
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="desc" class="col-sm-2 control-label">Deskripsi</label>
        <div class="col-sm-10">
            <textarea name="desc" id="desc" placeholder="Berita" class="form-control"><?php echo set_value('desc', $this->form_data->desc); ?></textarea>
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="photo" class="col-sm-2 control-label">Photo</label>
        <div class="col-sm-10">
            <input type="file" class="form-control" name="photo" id="photo" placeholder="Photo">
        </div>
        <div class="cleared"></div>
    </div>
    <div class="footer_margin"></div>
	<div class="form-group right">
	<button type="submit" name="button" class="btn btn-success">&nbsp;&nbsp; Simpan &nbsp;&nbsp;</button>
    <button type="button" name="button" class="btn btn-danger" onclick="location.href='{back_link}';">&nbsp;&nbsp; Kembali &nbsp;&nbsp;</button>
    </div>

</form>

<script>
  $(function () {

    $('#tgl').datepicker({
      autoclose: true,
	   format: 'yyyy-mm-dd'
    });

  });
</script>

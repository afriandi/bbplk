{message}
<form method="post" action="{action}"  enctype="multipart/form-data">
    <div class="form-group">
        <label for="program" class="col-sm-2 control-label">Program_Pelatihan</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="program" id="program" placeholder="Program Pelatihan">
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="pelatihan" class="col-sm-2 control-label">Pelatihan</label>
        <div class="col-sm-10">
            {combo_pelatihan}
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="Urutan" class="col-sm-2 control-label">Urutan</label>
        <div class="col-xs-3">
          	{combo_urutan}
        </div>
        <label for="tahap" class="col-sm-1 control-label">Tahap</label>
        <div class="col-xs-3">
        	{combo_tahap}
        </div>
        <label for="tahun" class="col-sm-1 control-label">Tahun</label>
        <div class="col-xs-2">
        	{combo_tahun}
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="jabatan" class="col-sm-2 control-label">Asrama</label>
        <div class="col-sm-10">
            {combo_asrama}
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="golongan" class="col-sm-2 control-label">Status</label>
        <div class="col-sm-10">
            {radio_status}
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="start_daftar" class="col-sm-2 control-label">Tgl_Pendaftaran</label>
        <div class="col-xs-3">
          	<input type="text" class="form-control" name="start_daftar" id="start_daftar" placeholder="Tanggal Mulai Pendaftaran">
        </div>
        <label for="end_daftar" class="col-sm-1 control-label">&nbsp;&nbsp;s/d</label>
        <div class="col-xs-3">
        	<input type="text" class="form-control" name="end_daftar" id="end_daftar" placeholder="Tanggal Selesai PEndaftaran">
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="tgl_rekrut" class="col-sm-2 control-label">Tgl_Rekrutmen</label>
        <div class="col-xs-3">
          	<input type="text" class="form-control" name="tgl_rekrut" id="tgl_rekrut" placeholder="Tanggal Rekrutmen">
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="tgl_pengumuman" class="col-sm-2 control-label">Tgl_Pengumuman</label>
        <div class="col-xs-3">
          	<input type="text" class="form-control" name="tgl_pengumuman" id="tgl_pengumuman" placeholder="Tanggal Pengumuman">
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="tgl_daftar_ulang" class="col-sm-2 control-label">Tgl_Daftar_Ulang</label>
        <div class="col-xs-3">
          	<input type="text" class="form-control" name="tgl_daftar_ulang" id="tgl_daftar_ulang" placeholder="Tanggal Daftar Ulang">
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="tgl_mulai" class="col-sm-2 control-label">Tgl_Mulai</label>
        <div class="col-xs-3">
          	<input type="text" class="form-control" name="tgl_mulai" id="tgl_mulai" placeholder="Tanggal Mulai">
        </div>
        <label for="tgl_selesai" class="col-sm-1 control-label">Selesai</label>
        <div class="col-xs-3">
          	<input type="text" class="form-control" name="tgl_selesai" id="tgl_selesai" placeholder="Tanggal Selesai">
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="tgl_ujk" class="col-sm-2 control-label">Tgl_UJK</label>
        <div class="col-xs-3">
          	<input type="text" class="form-control" name="tgl_ujk" id="tgl_ujk" placeholder="Tanggal UJK">
        </div>
        <div class="cleared"></div>
    </div>

    <div class="footer_margin"></div>
	<div class="form-group right">
	<button type="submit" name="button" class="btn btn-success">&nbsp;&nbsp; Simpan &nbsp;&nbsp;</button>
    <button type="button" name="button" class="btn btn-danger" onclick="location.href='{back_link}';">&nbsp;&nbsp; Kembali &nbsp;&nbsp;</button>
    </div>
</form>


<script>
  $(function () {

    $('#start_daftar').datepicker({
      autoclose: true,
	   format: 'yyyy-mm-dd'
    });

	$('#end_daftar').datepicker({
      autoclose: true,
	   format: 'yyyy-mm-dd'
    });

	$('#tgl_rekrut').datepicker({
      autoclose: true,
	   format: 'yyyy-mm-dd'
    });

	$('#tgl_pengumuman').datepicker({
      autoclose: true,
	   format: 'yyyy-mm-dd'
    });

	$('#tgl_daftar_ulang').datepicker({
      autoclose: true,
	   format: 'yyyy-mm-dd'
    });

	$('#tgl_mulai').datepicker({
      autoclose: true,
	   format: 'yyyy-mm-dd'
    });

	$('#tgl_selesai').datepicker({
      autoclose: true,
	   format: 'yyyy-mm-dd'
    });

	$('#tgl_ujk').datepicker({
      autoclose: true,
	   format: 'yyyy-mm-dd'
    });

  });
</script>

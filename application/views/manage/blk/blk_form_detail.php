{message}
<form method="post" action="{action}"  enctype="multipart/form-data">
    <div class="form-group">
        <label for="nama" class="col-sm-2 control-label">Nama BLK</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="nama" id="nama" value="<?php echo set_value('nama', $this->form_data->nama); ?>" placeholder="NAMA BLK" disabled>
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="alamat" class="col-sm-2 control-label">Alamat</label>
        <div class="col-sm-10">
        	<textarea name="alamat" id="alamat" class="form-control" placeholder="Alamat" disabled><?php echo set_value('alamat', $this->form_data->alamat); ?></textarea>
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="telp" class="col-sm-2 control-label">Nomor Telepon</label>
        <div class="col-sm-10">
          	<input type="text" class="form-control" name="telp" value="<?php echo set_value('telp', $this->form_data->telp); ?>" id="telp" placeholder="Nomor Telepon" disabled>
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="email" class="col-sm-2 control-label">Email</label>
        <div class="col-sm-10">
            <input type="email" class="form-control" name="email" id="email" value="<?php echo set_value('email', $this->form_data->email); ?>" placeholder="Email" disabled>
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="jam" class="col-sm-2 control-label">Jam Kerja</label>
        <div class="col-sm-10">
          	<input type="text" class="form-control" name="jam" id="jam" value="<?php echo set_value('jam', $this->form_data->jam); ?>" placeholder="Jam Kerja" disabled>
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="fb" class="col-sm-2 control-label">Facebook</label>
        <div class="col-sm-10">
          	<input type="text" class="form-control" name="fb" id="fb" value="<?php echo set_value('fb', $this->form_data->fb); ?>" placeholder="Facebook" disabled>
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="tw" class="col-sm-2 control-label">Twitter</label>
        <div class="col-sm-10">
          	<input type="text" class="form-control" name="tw" id="tw" value="<?php echo set_value('tw', $this->form_data->tw); ?>" placeholder="Twitter" disabled>
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="in" class="col-sm-2 control-label">Instagram</label>
        <div class="col-sm-10">
          	<input type="text" class="form-control" name="in" id="in" value="<?php echo set_value('in', $this->form_data->in); ?>" placeholder="Linkedin" disabled>
        </div>
        <div class="cleared"></div>
    </div>
    <div class="footer_margin"></div>
    <div class="tombol_kanan">
     <button type="button" name="button" class="btn btn-primary" onclick="location.href='{back_link}';">&nbsp;&nbsp; Kembali &nbsp;&nbsp;</button>
    </div>
</form>

<script>
  $(function () {
    $('#tgl_lahir').datepicker({
      autoclose: true,
	   format: 'yyyy-mm-dd'
    });

  });
</script>

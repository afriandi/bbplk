{message}
<form method="post" action="{action}"  enctype="multipart/form-data">
	<table class="table-form col-sm-12">
    	<tr>
        	<td width="20%"><label for="idk" class="col-sm-3 control-label">NIP</label></td>
            <td width="60%">
            	<div class="col-sm-12">
                	<input type="text" class="form-control" disabled name="idk" id="idk" value="<?php echo set_value('id', $this->form_data->id); ?>" placeholder="Id Kejuruan">
                    <input type="hidden" name="id" id="id" value="<?php echo set_value('id', $this->form_data->id); ?>">
                    <input type="hidden" name="img" id="img" value="<?php echo set_value('id', $this->form_data->img); ?>">
            	</div>
            </td>
            <td width="20%" rowspan="3" align="right" valign="top">
            	<img src="{kejuruan_img}" width="150px"/>
            </td>
        </tr>
        <tr>
        	<td><label for="nama" class="col-sm-3 control-label">Nama Kejuruan</label></td>
            <td>
            	<div class="col-sm-12">
                	<input type="text" class="form-control" name="nama" id="nama" placeholder="Nama Lengkap" value="<?php echo set_value('nama', $this->form_data->nama); ?>" >
            	</div>
            </td>
        </tr>

        <tr>
        	<td><label for="desc" class="col-sm-3 control-label">Deskripsi</label ></td>
            <td>
            	<div class="col-sm-12">
                	<textarea name="desc" id="desc"  class="form-control"><?php echo set_value('desc', $this->form_data->desc); ?></textarea>
            	</div>
            </td>
        </tr>
        <tr>
        	<td><label for="photo" class="col-sm-2 control-label">Photo</label></td>
            <td colspan="2">
	            <div class="col-sm-12">
    	            <input type="file" class="form-control" name="photo" id="photo" placeholder="Photo">
        		</div>
            </td>
        </tr>
    </table>
    <div class="footer_margin">&nbsp;</div>
	<div class="form-group right">
    <button type="submit" name="button" class="btn btn-success">&nbsp;&nbsp; Simpan &nbsp;&nbsp;</button>
    <button type="button" name="button" class="btn btn-danger" onclick="location.href='{back_link}';">&nbsp;&nbsp; Kembali &nbsp;&nbsp;</button>
    </div>

</form>

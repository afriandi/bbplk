{message}
<form method="post" action="{action}"  enctype="multipart/form-data">
	<table class="table-form col-sm-12">
    	<tr>
        	<td width="20%"><label for="id" class="col-sm-3 control-label">ID</label></td>
            <td width="60%">
            	<div class="col-sm-12">
                	<input type="text" class="form-control" disabled name="id" id="id" value="<?php echo set_value('id', $this->form_data->id); ?>" placeholder="Id Kejuruan">
            	</div>
            </td>
            <td width="20%" rowspan="3" align="right" valign="top">
            	<img src="{kejuruan_img}" width="150px"/>
            </td>
        </tr>
        <tr>
        	<td><label for="nama" class="col-sm-3 control-label">Kejuruan</label></td>
            <td>
            	<div class="col-sm-12">
                	<input type="text" class="form-control" name="nama" id="nama" placeholder="Nama Lengkap" value="<?php echo set_value('nama', $this->form_data->nama); ?>" disabled>
            	</div>
            </td>
        </tr>

        <tr>
        	<td><label for="desc" class="col-sm-3 control-label">Deskripsi</label ></td>
            <td>
            	<div class="col-sm-12">
                	<textarea name="desc" id="desc" rows="12" disabled class="form-control"><?php echo set_value('desc', $this->form_data->desc); ?></textarea>
            	</div>
            </td>
        </tr>
    </table>
    <div class="footer_margin">&nbsp;</div>
	<div class="form-group right">
    <button type="button" name="button" class="btn btn-danger" onclick="location.href='{back_link}';">&nbsp;&nbsp; Kembali &nbsp;&nbsp;</button>
    </div>

</form>

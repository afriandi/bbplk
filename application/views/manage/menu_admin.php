<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
          <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="" width="60px"/>
          <a href="http://www.facebook.com" target="_blank">
            <img src="<?php echo base_url(); ?>assets/img/facebook.png" alt="" width="25px"/>
          </a>
          <a href="http://www.twitter.com" target="_blank">
            <img src="<?php echo base_url(); ?>assets/img/twitter.png" alt="" width="25px"/>
          </a>
          <a href="http://www.instagram.com" target="_blank">
            <img src="<?php echo base_url(); ?>assets/img/instagram2.png" alt="" width="25px"/>
          </a>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/bbplk/index.html">
            </a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <?php
                	$menuList = $this->session->userdata('menu');

					$i = 1;
					foreach ($menuList as $module => $sections) {
						$awalan = strtolower($module) == 'master' ? 'Data' : '';
						 echo '<li class="dropdown">
      				<a href="#" class="dropdown-toggle" data-toggle="dropdown">Kelola '.$awalan .' '.$module.'<b class="caret"></b></a>
					<ul class="dropdown-menu">';
						foreach ($sections as $section) {
							if ($section == '')
								continue;
								$sumenu = str_replace("_", " ", $section);
							echo '<li>
                          			<a href="'.base_url().'manage/'.$section.'">'.$awalan.' '.$sumenu.'</a>
                      			</li>';
						}
						echo '</ul></li>';
					}
				?>
                  <!--<ul class='dropdown-menu'>
                      <li>
                          <a href='<?php echo base_url(); ?>manage/blk'>Data BLK</a>
                      </li>
                      <li>
                          <a href='<?php echo base_url(); ?>manage/pegawai'>Data Pegawai</a>
                      </li>
                      <li>
                          <a href='<?php echo base_url(); ?>manage/kejuruan'>Data Kejuruan</a>
                      </li>
                      <li>
                          <a href='<?php echo base_url(); ?>manage/instruktur'>Data Instruktur</a>
                      </li>
                      <li>
                          <a href='<?php echo base_url(); ?>manage/asesor'>Data Asesor</a>
                      </li>
                      <li>
                          <a href='<?php echo base_url(); ?>manage/unit'>Data Unit Kompetensi</a>
                      </li>
                      <li>
                          <a href='<?php echo base_url(); ?>manage/pelatihan'>Data Pelatihan</a>
                      </li>
                      <li>
                          <a href='<?php echo base_url(); ?>manage/asesmen'>Data Sertifikasi</a>
                      </li>
                  </ul>
          		</li>
  				<li class='dropdown'>
                  <a href='#' class='dropdown-toggle' data-toggle='dropdown'>Kelola Web<b class='caret'></b></a>
                  <ul class='dropdown-menu'>
                      <li>
                          <a href='<?php echo base_url(); ?>manage/fasilitas'>Fasilitas</a>
                      </li>
                      <li>
                          <a href='<?php echo base_url(); ?>manage/berita'>Berita</a>
                      </li>
                      <li>
                          <a href='<?php echo base_url(); ?>manage/pengumuman'>Pengumuman</a>
                      </li>
                      <li>
                          <a href='<?php echo base_url(); ?>manage/regulasi'>Regulasi</a>
                      </li>
                      <li>
                          <a href='<?php echo base_url(); ?>manage/galeri'>Galeri</a>
                      </li>
                      <li>
                          <a href='<?php echo base_url(); ?>manage/slider'>Slider</a>
                      </li>
                      <li>
                          <a href='<?php echo base_url(); ?>manage/info'>Informasi</a>
                      </li>
                  </ul>
              </li>
              <li class='dropdown'>
                  <a href='#' class='dropdown-toggle' data-toggle='dropdown'>Kelola User<b class='caret'></b></a>
                  <ul class='dropdown-menu'>
                      <li>
                          <a href='<?php echo base_url(); ?>manage/user'>User</a>
                      </li>
                      <li>
                          <a href='<?php echo base_url(); ?>manage/log'>Log</a>
                      </li>
                  </ul>
              </li>
              -->
  			  {islogin}
			</ul>

        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

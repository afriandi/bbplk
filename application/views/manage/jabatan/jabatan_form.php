{message}
<form method="post" action="{action}"  enctype="multipart/form-data">

    <div class="form-group">
        <label for="jabatan" class="col-sm-2 control-label">Jabatan</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="jabatan" id="jabatan" placeholder="Jabatan">
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="jobdes" class="col-sm-2 control-label">Job Desk</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="jobdes" id="jobdes" placeholder="Job Desk">
        </div>
        <div class="cleared"></div>
    </div>
    <div class="footer_margin">&nbsp;</div>
	<div class="form-group right">
	<button type="submit" name="button" class="btn btn-success">&nbsp;&nbsp; Simpan &nbsp;&nbsp;</button>
    <button type="button" name="button" class="btn btn-danger" onclick="location.href='{back_link}';">&nbsp;&nbsp; Kembali &nbsp;&nbsp;</button>
    </div>

</form>

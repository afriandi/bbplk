{message}
<!-- <img src="{img}" height="150" > -->
<div class="footer_margin">&nbsp;</div>
<form method="post" action="{action}"  enctype="multipart/form-data">
    <div class="form-group">
        <label for="nama" class="col-sm-2 control-label">Nama BLK</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="nama" id="nama" value="<?php echo set_value('nama', $this->form_data->nama); ?>" placeholder="Nama">
            <input type="hidden" name="id" id="id" value="<?php echo set_value('id', $this->form_data->id); ?>">
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="info" class="col-sm-2 control-label">Info Pada Menu Home</label>
        <div class="col-sm-10">
        	<div class="box-body pad">
            <textarea name="info" id="info" placeholder="Info Home" class="textedit" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo set_value('info', $this->form_data->info); ?></textarea>
            </div>
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="tentang" class="col-sm-2 control-label">Info Pada Menu Tentang</label>
        <div class="col-sm-10">
        	<div class="box-body pad">
            <textarea name="tentang" id="tentang" placeholder="Tentang" class="textedit" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo set_value('tentang', $this->form_data->tentang); ?></textarea>
            </div>
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="visi" class="col-sm-2 control-label">Visi</label>
        <div class="col-sm-10">
        	<div class="box-body pad">
            <textarea name="visi" id="visi" placeholder="Visi" class="textedit" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo set_value('visi', $this->form_data->visi); ?></textarea>
            </div>
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="misi" class="col-sm-2 control-label">Misi</label>
        <div class="col-sm-10">
        	<div class="box-body pad">
            <textarea name="misi" id="misi" placeholder="Misis" class="textedit" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo set_value('misi', $this->form_data->misi); ?></textarea>
            </div>
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="photo" class="col-sm-2 control-label">Photo</label>
        <div class="col-sm-10">
            <input type="file" name="photo" id="photo" class="form-control"/>
        </div>
        <div class="cleared"></div>
    </div>


    <div class="footer_margin">&nbsp;</div>
	<div class="form-group right">
	<button type="submit" name="button" class="btn btn-success">&nbsp;&nbsp; Simpan &nbsp;&nbsp;</button>
    <button type="button" name="button" class="btn btn-danger" onclick="location.href='{back_link}';">&nbsp;&nbsp; Kembali &nbsp;&nbsp;</button>
    </div>

</form>


<script src="<?php echo base_url() . 'assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js'; ?>"></script>

<script>
  $(function () {

    //$("#detail").wysihtml5();

	var myCustomTemplates = {
    emphasis : function(locale) {
        return "<li>" +
            "<div class='btn-group'>" +
            "<a data-wysihtml5-command='bold' title='Bold' class='btn btn-none btn-default' ><span style='font-weight:700'>B</span></a>" +
            "<a data-wysihtml5-command='italic' title='Italics' class='btn btn-none btn-default' ><span style='font-style:italic'>I</span></a>" +
            "<a data-wysihtml5-command='underline' title='Underline' class='btn btn-none btn-default' ><span style='text-decoration:underline'>U</span></a>" +
            "</div>" +
            "</li>";
    },
    lists : function(locale) {
        return "<li>" +
            "<div class='btn-group'>" +
            "<a data-wysihtml5-command='insertUnorderedList' title='Unordered list' class='btn btn-none btn-default' ><span class='fa fa-list-ul'></span></a>" +
            "<a data-wysihtml5-command='insertOrderedList' title='Ordered list' class='btn btn-none btn-default' ><span class='fa fa-list-ol'></span></a>" +
            "<a data-wysihtml5-command='Outdent' title='Outdent' class='btn btn-none btn-default' ><span class='fa fa-outdent'></span></a>" +
            "<a data-wysihtml5-command='Indent' title='Indent' class='btn btn-none btn-default'><span class='fa fa-indent'></span></a>" +
            "</div>" +
            "</li>";
    }
}

$('.textedit').wysihtml5({
    toolbar: {
        "font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
        "emphasis": true, //Italics, bold, etc. Default true
        "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
        "html": true, //Button which allows you to edit the generated HTML. Default false
        "link": true, //Button to insert a link. Default true
        "image": true, //Button to insert an image. Default true,
        "color": true, //Button to change color of font
        "blockquote": false, //Blockquote
    },
    customTemplates: myCustomTemplates
});

  });
</script>

{message}
<div class="cleared"></div>
<!-- <img src="{img}" height="150" > -->
<div class="footer_margin">&nbsp;</div>
<form method="post" action="{action}"  enctype="multipart/form-data">
    <div class="form-group">
        <label for="nama" class="col-sm-2 control-label">Nama BLK</label>
        <div class="col-sm-10">
        	<div class="box-body form-detail" >
            	<?php echo $this->form_data->nama; ?>
            </div>
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="info" class="col-sm-2 control-label">Info Pada Menu Home</label>
        <div class="col-sm-10">
        	<div class="box-body form-detail" >
            	<?php echo $this->form_data->info; ?>
            </div>
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="tentang" class="col-sm-2 control-label">Info Pada Menu Tentang</label>
        <div class="col-sm-10">
        	<div class="box-body form-detail">
            	<?php echo $this->form_data->tentang; ?>
            </div>
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="visi" class="col-sm-2 control-label">Visi</label>
        <div class="col-sm-10">
        	<div class="box-body form-detail">
            	<?php echo $this->form_data->visi; ?>
            </div>
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="misi" class="col-sm-2 control-label">Misi</label>
        <div class="col-sm-10">
        	<div class="box-body form-detail">
            	<?php echo $this->form_data->misi; ?>
            </div>
        </div>
        <div class="cleared"></div>
    </div>

    <div class="footer_margin">&nbsp;</div>
	<div class="form-group right">
	<button type="button" name="button" onClick="location.href='{action}'" class="btn btn-primary"><span class="glyphicon glyphicon-edit"></span>&nbsp;&nbsp; Edit Data &nbsp;&nbsp;</button>
    </div>

</form>

{message}
<form method="post" action="{action}"  enctype="multipart/form-data">
	<table class="table-form col-sm-12">
    	<tr>
        	<td width="20%"><label for="nip" class="col-sm-3 control-label">NIP</label></td>
            <td width="60%">
            	<div class="col-sm-12">
                	<input type="text" class="form-control" disabled name="nipe" id="nipe" value="<?php echo set_value('nip', $this->form_data->nip); ?>" placeholder="NIP">
                    <input type="hidden" class="form-control" name="nip" id="nip" value="<?php echo set_value('nip', $this->form_data->nip); ?>" placeholder="NIP">
                    <?php echo form_error('nip'); ?>
            	</div>
            </td>
            <td width="20%" rowspan="6" align="right" valign="top">
            	<img src="{pegawai_img}" width="150px"/>
            </td>
        </tr>
        <tr>
        	<td><label for="nama" class="col-sm-3 control-label">Nama Lengkap</label></td>
            <td>
            	<div class="col-sm-12">
                	<input type="text" class="form-control" name="nama" id="nama" placeholder="Nama Lengkap" value="<?php echo set_value('nama', $this->form_data->nama); ?>" >
                    <?php echo form_error('nama'); ?>
            	</div>
            </td>
        </tr>
        <tr>
        	<td><label for="tempat_lahir" class="col-sm-3 control-label">Tempat/Tgl Lahir</label></td>
            <td>
            	<div class="col-xs-7">
                    <input type="text" class="form-control" name="tempat_lahir" id="tempat_lahir" value="<?php echo set_value('tempat_lahir', $this->form_data->tempat_lahir); ?>" placeholder="Tempat Lahir" >
                    <?php echo form_error('tempat_lahir'); ?>
                </div>
                <div class="col-xs-5">
                    <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="text" class="form-control pull-right" name="tgl_lahir" id="tgl_lahir" value="<?php echo set_value('tgl_lahir', $this->form_data->tgl_lahir); ?>" placeholder="Tanggal Lahir" >
                          <?php echo form_error('tgl_lahir'); ?>
                        </div>
                </div>
            </td>

        </tr>
        <tr>
        	<td><label for="jabatan" class="col-sm-3 control-label">Jabatan</label ></td>
            <td>
            	<div class="col-sm-12">
                	{combo_jabatan}
            	</div>
            </td>
        </tr>
        <tr>
        	<td><label for="golongan" class="col-sm-3 control-label">Golongan</label></td>
            <td>
            	<div class="col-sm-12">
                	{combo_golongan}
            	</div>
            </td>
        </tr>
        <tr>
        	<td><label for="tmt" class="col-sm-2 control-label">TMT</label></td>
            <td>
            	<div class="col-sm-12">
                	<input type="text" class="form-control" name="tmt" id="tmt" value="<?php echo set_value('tmt', $this->form_data->tmt); ?>" placeholder="TMT" >
                    <?php echo form_error('tmt'); ?>
            	</div>
            </td>
        </tr>
        <tr>
        	<td><label for="pendidikan" class="col-sm-2 control-label">Pendidikan</label></td>
            <td colspan="2">
            	<div class="col-sm-12">
                	{combo_pendidikan}
            	</div>
            </td>
        </tr>
        <tr>
        	<td><label for="gender" class="col-sm-2 control-label">Jenis Kelamin</label></td>
            <td colspan="2">
                {radio_gender}
            </td>
        </tr>
        <tr>
        	<td><label for="photo" class="col-sm-2 control-label">Photo</label></td>
            <td colspan="2">
	            <div class="col-sm-12">
    	            <input type="file" class="form-control" name="photo" id="photo" placeholder="Photo">
        		</div>
            </td>
        </tr>
        <tr>
        	<td colspan="3">&nbsp;</td>
        </tr>
        <tr>
        	<td></td>
            <td colspan="2" align="right">
            <div class="col-sm-12">
            	<div class="form-group right">
                    <button type="submit" name="button" class="btn btn-success">&nbsp;&nbsp; Simpan &nbsp;&nbsp;</button>
                    <button type="button" name="button" class="btn btn-danger" onclick="location.href='{back_link}';">&nbsp;&nbsp; Kembali &nbsp;&nbsp;</button>
                </div>
               </div>
            </td>
        </tr>
    </table>
    <div class="footer_margin"></div>


</form>


<script>
  $(function () {

    $('#tgl_lahir').datepicker({
      autoclose: true,
	   format: 'yyyy-mm-dd'
    });

  });
</script>

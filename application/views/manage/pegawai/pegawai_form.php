{message}
<form method="post" action="{action}"  enctype="multipart/form-data">
    <div class="form-group">
        <label for="nip" class="col-sm-2 control-label">NIP</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="nip" id="nip" placeholder="NIP">
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="nama" class="col-sm-2 control-label">Nama Lengkap</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama Lengkap">
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="tempat_lahir" class="col-sm-2 control-label">Tempat/Tgl Lahir</label>
        <div class="col-xs-4">
          	<input type="text" class="form-control" name="tempat_lahir" id="tempat_lahir" placeholder="Tempat Lahir">
        </div>
        <div class="col-xs-3">
        	<div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" name="tgl_lahir" id="tgl_lahir">
                </div>
        </div>
        <div class="col-xs-3"></div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="jabatan" class="col-sm-2 control-label">Jabatan</label>
        <div class="col-sm-10">
            {combo_jabatan}
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="golongan" class="col-sm-2 control-label">Golongan</label>
        <div class="col-sm-10">
            {combo_golongan}
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="tmt" class="col-sm-2 control-label">TMT</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="tmt" id="tmt" placeholder="TMT">
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="pendidikan" class="col-sm-2 control-label">Pendidikan</label>
        <div class="col-sm-10">
            {combo_pendidikan}
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="gender" class="col-sm-2 control-label">Jenis Kelamin</label>
        {radio_gender}
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="photo" class="col-sm-2 control-label">Photo</label>
        <div class="col-sm-10">
            <input type="file" class="form-control" name="photo" id="photo" placeholder="Photo">
        </div>
        <div class="cleared"></div>
    </div>
    <div class="footer_margin"></div>
	<div class="form-group right">
	<button type="submit" name="button" class="btn btn-success">&nbsp;&nbsp; Simpan &nbsp;&nbsp;</button>
    <button type="button" name="button" class="btn btn-danger" onclick="location.href='{back_link}';">&nbsp;&nbsp; Kembali &nbsp;&nbsp;</button>
    </div>

</form>


<script>
  $(function () {

    $('#tgl_lahir').datepicker({
      autoclose: true,
	   format: 'yyyy-mm-dd'
    });

  });
</script>

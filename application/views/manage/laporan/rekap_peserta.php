<!-- Page Heading/Breadcrumbs -->
<div class="row">
   	<div class="col-lg-12">
       	<h1 class="page-header">{title_menu}</h1>
        <ol class="breadcrumb">
            <li><a href="{base_url}home">Home</a></li>
        	<li class="active">{menu}</li>
    	</ol>
	</div>
</div>
<!-- /.row -->

<div class="row">
<div class="col-md-6">
<div class="widget-content">
	<table class="table table-striped table-bordered">
    	<tr>
        	<th rowspan="3" valign="bottom">Kejuruan</th>
        	<th colspan="3">2017</th>
      </tr>
      <tr>
        	<th rowspan="2">Jumlah Peserta</th>
          <th colspan="2">Jenis Kelamin</th>
      </tr>
      <tr>
        	<th>Perempuan</th>
        	<th>Laki-Laki</th>
      </tr>
      <tr>
      	<td>Kejuruan Otomotif</td>
        <td align="center">{total_siswa_oto}</td>
        <td align="center">{total_p_oto}</td>
        <td align="center">{total_l_oto}</td>
      </tr>
      <tr>
      	<td>Kejuruan Manufaktur</td>
      	<td align="center">{total_siswa_tekman}</td>
        <td align="center">{total_p_tekman}</td>
        <td align="center">{total_l_tekman}</td>
      </tr>
      <tr>
      	<td align="center">Total</td>
      	<td align="center">{total_siswa}</td>
          <td align="center">{total_p}</td>
          <td align="center">{total_l}</td>
      </tr>
    </table>
    </div>
</div>
<div class="row">
<div class="col-md-6">
	<div class="box-body">
    	<div class="chart">
        	<canvas id="barChart" style="height:230px"></canvas>
        </div>
   	</div>
</div>
</div>
<br><hr><br>
<div class="row">
<div class="col-md-12">
<div class="widget-content">
	<table class="table table-striped table-bordered">
    	<tr>
        	<th rowspan="3" valign="bottom">Kejuruan</th>
        	<th colspan="5">2017</th>
      </tr>
      <tr>
        	<th rowspan="2">Jumlah Peserta</th>
          <th colspan="4">Pendidikan</th>
      </tr>
      <tr>
        	<th><= SD</th>
        	<th>SMP</th>
        	<th>SMA/SMK</th>
        	<th>>= S1</th>
      </tr>
      <tr>
      	<td>Kejuruan Otomotif</td>
        <td align="center">{total_siswa_oto}</td>
        <td align="center">{total_sd_oto}</td>
        <td align="center">{total_smp_oto}</td>
        <td align="center">{total_sma_oto}</td>
        <td align="center">{total_s1_oto}</td>
      </tr>
      <tr>
      	<td>Kejuruan Manufaktur</td>
      	<td align="center">{total_siswa_tekman}</td>
        <td align="center">{total_sd_tekman}</td>
        <td align="center">{total_smp_tekman}</td>
        <td align="center">{total_sma_tekman}</td>
        <td align="center">{total_s1_tekman}</td>
      </tr>
      <tr>
      	<td align="center">Total</td>
      	<td align="center">{total_siswa}</td>
          <td align="center">{total_sd}</td>
          <td align="center">{total_smp}</td>
          <td align="center">{total_sma}</td>
          <td align="center">{total_s1}</td>
      </tr>
    </table>
    </div>
</div>
</div>
<div class="row">
<div class="col-md-12">
	<div class="box-body">
    	<div class="chart">
        	<canvas id="barChart2" style="height:230px"></canvas>
        </div>
   	</div>
</div>
</div>


<script src="<?php echo base_url() . 'assets/plugins/chartjs/Chart.bundle.min.js'; ?>"></script>
<script src="<?php echo base_url() . 'assets/plugins/chartjs/utils.js'; ?>"></script>


<script>
$(function () {
	var color = Chart.helpers.color;
    var barChartData = {
    	labels: ["Kejuruan Otomotif", "Kejuruan Manufaktur"],
        datasets: [{
        	label: 'Perempuan',
            backgroundColor: color(chartColors.red).alpha(10).rgbString(),
            borderColor: chartColors.red,
            borderWidth: 1,
            data: [
                {total_p_oto},
                {total_p_tekman}
            ]
         }, {
            label: 'Laki-Laki',
            backgroundColor: color(chartColors.yellow).alpha(10).rgbString(),
            borderColor: chartColors.yellow,
            borderWidth: 1,
            data: [
                {total_l_oto},
                {total_l_tekman}
            ]
         }]

	};


    var ctx = $("#barChart").get(0).getContext("2d");
    var myBar = new Chart(ctx, {
    	type: 'bar',
        data: barChartData,
        options: {
        	responsive: true,
            legend: {
            	position: 'top',
            },
            title: {
            	display: true,
                text: 'Rekap Jenis Kelamin Peserta'
            }
        }
   });

});
</script>

<script>
$(function () {
	var color = Chart.helpers.color;
    var barChartData = {
    	labels: ["Kejuruan Otomotif", "Kejuruan Manufaktur"],
        datasets: [
          {	label: '<=SD',
            backgroundColor: color(chartColors.red).alpha(10).rgbString(),
            borderColor: chartColors.red,
            borderWidth: 1,
            data: [
                {total_sd_oto},
                {total_sd_tekman}
            ]
          },
          { label: 'SMP',
            backgroundColor: color(chartColors.yellow).alpha(10).rgbString(),
            borderColor: chartColors.yellow,
            borderWidth: 1,
            data: [
                {total_smp_oto},
                {total_smp_tekman}
            ]
         },
         { label: 'SMA',
           backgroundColor: color(chartColors.blue).alpha(10).rgbString(),
           borderColor: chartColors.blue,
           borderWidth: 1,
           data: [
               {total_sma_oto},
               {total_sma_tekman}
           ]
        },
        { label: '>=S1',
          backgroundColor: color(chartColors.green).alpha(10).rgbString(),
          borderColor: chartColors.green,
          borderWidth: 1,
          data: [
              {total_s1_oto},
              {total_s1_tekman}
          ]
       }
       ]

	};


    var ctx = $("#barChart2").get(0).getContext("2d");
    var myBar = new Chart(ctx, {
    	type: 'bar',
        data: barChartData,
        options: {
        	responsive: true,
            legend: {
            	position: 'top',
            },
            title: {
            	display: true,
                text: 'Rekap Pendidikan Peserta'
            }
        }
   });

});
</script>
</div>
<br>
<div class="row">
  <div class="tombol_kanan">
    <a class="btn btn-primary" href="" type="button" onclick="myFunction()">
            <span class="glyphicon glyphicon-print"></span>&nbsp Cetak Data &nbsp
    </a>
  </div>
</div>

<script>
function myFunction() {
    window.print();
}
</script>

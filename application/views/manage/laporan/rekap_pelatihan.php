<!-- Page Heading/Breadcrumbs -->
<div class="row">
   	<div class="col-lg-12">
       	<h1 class="page-header">{title_menu}</h1>
        <ol class="breadcrumb">
            <li><a href="{base_url}home">Home</a></li>
        	<li class="active">{menu}</li>
    	</ol>
	</div>
</div>
<!-- /.row -->
<div class="row">
<div class="col-md-6">
<div class="widget-content">

	<table class="table table-striped table-bordered">
    	<tr>
        	<th rowspan="2" valign="bottom">Kejuruan</th>
        	<th colspan="2">2017</th>
        </tr>
        <tr>

        	<th>Jumlah Paket</th>
          <th>Peserta yg Dilatih</th>
        </tr>
        <tr>
        	<td>Kejuruan Otomotif</td>
        	<td align="center">{total_oto}</td>
            <td align="center">{total_siswa_oto}</td>
        </tr>
        <tr>
        	<td>Kejuruan Manufaktur</td>
        	<td align="center">{total_tekman}</td>
            <td align="center">{total_siswa_tekman}</td>
        </tr>
        <tr>
        	<td align="center">Total</td>
        	<td align="center">{total_program}</td>
          <td align="center">{total_siswa}</td>
        </tr>
    </table>
    </div>
</div>

<div class="col-md-6">
	<div class="box-body">
    	<div class="chart">
        	<canvas id="barChart" style="height:230px"></canvas>
        </div>
   	</div>
</div>

<script src="<?php echo base_url() . 'assets/plugins/chartjs/Chart.bundle.min.js'; ?>"></script>
<script src="<?php echo base_url() . 'assets/plugins/chartjs/utils.js'; ?>"></script>


<script>
$(function () {
	var color = Chart.helpers.color;
    var barChartData = {
    	labels: ["Kejuruan Otomotif", "Kejuruan Manufaktur"],
        datasets: [{
        	label: 'Paket Program Pelatihan',
            backgroundColor: color(chartColors.red).alpha(10).rgbString(),
            borderColor: chartColors.red,
            borderWidth: 1,
            data: [
                {total_oto},
                {total_tekman}
            ]
         }, {
            label: 'Jumlah Siswa yang Dilatih',
            backgroundColor: color(chartColors.yellow).alpha(10).rgbString(),
            borderColor: chartColors.yellow,
            borderWidth: 1,
            data: [
                {total_siswa_oto},
                {total_siswa_tekman}
            ]
         }]

	};


    var ctx = $("#barChart").get(0).getContext("2d");
    var myBar = new Chart(ctx, {
    	type: 'bar',
        data: barChartData,
        options: {
        	responsive: true,
            legend: {
            	position: 'top',
            },
            title: {
            	display: true,
                text: 'Rekap Pelatihan'
            }
        }
   });

});
</script>
</div>
<br>
<div class="row">
  <div class="tombol_kanan">
    <a class="btn btn-primary" href="" type="button" onclick="myFunction()">
            <span class="glyphicon glyphicon-print"></span>&nbsp Cetak Data &nbsp
    </a>
  </div>
</div>

<script>
function myFunction() {
    window.print();
}
</script>

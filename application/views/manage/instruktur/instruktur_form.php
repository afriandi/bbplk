{message}
<form method="post" action="{action}"  enctype="multipart/form-data">
    <div class="form-group">
        <label for="id" class="col-sm-2 control-label">NIP</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="nip" id="nip" placeholder="NIP"  list="dtpeg">
            {datalist_pegawai}
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="nama" class="col-sm-2 control-label">Kejuruan</label>
        <div class="col-sm-10">
            {combo_kejuruan}
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="desc" class="col-sm-2 control-label">Keterangan</label>
        <div class="col-sm-10">
            <textarea name="desc" id="desc" class="form-control"></textarea>
        </div>
        <div class="cleared"></div>
    </div>
    <div class="footer_margin">&nbsp;</div>
	<div class="form-group right">
	  <button type="submit" name="button" class="btn btn-success">&nbsp;&nbsp; Simpan &nbsp;&nbsp;</button>
    <button type="button" name="button" class="btn btn-danger" onclick="location.href='{back_link}';">&nbsp;&nbsp; Kembali &nbsp;&nbsp;</button>
  </div>

</form>

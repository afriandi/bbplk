{message}
<form method="post" action="{action}"  enctype="multipart/form-data">
    <div class="form-group">
        <label for="nama" class="col-sm-2 control-label">Nama Role</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama Role">
        </div>
        <div class="cleared"></div>
    </div>
	<div class="col-sm-12" style="text-align:center;padding:10px;font-weight:bold;">
    	HAK AKSES
    </div>
    <div class="form-group">
    	{modules}
        <div class="col-sm-2">
        	DATA  {module}
        </div>
        <div class="col-sm-10">
        	{module_section}
        	<label for="{label}"><span class="input-group-addon"><input  name="module[{module_short}][]" value="{section_short}" id="{label}" {sec_checked} type="checkbox"> {section} </span></label>
            {/module_section}
        </div>
        {/modules}
        <div class="cleared"></div>
    </div>
    <div class="footer_margin"></div>
	<div class="form-group right">
	<button type="submit" name="button" class="btn btn-success">&nbsp;&nbsp; Simpan &nbsp;&nbsp;</button>
    <button type="button" name="button" class="btn btn-danger" onclick="location.href='{back_link}';">&nbsp;&nbsp; Kembali &nbsp;&nbsp;</button>
    </div>

</form>

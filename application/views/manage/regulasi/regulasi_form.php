{message}
<form method="post" action="{action}"  enctype="multipart/form-data">

    <div class="form-group">
        <label for="nama" class="col-sm-2 control-label">Nama Regulasi</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="nama" id="nama" placeholder="Regulasi">
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="desc" class="col-sm-2 control-label">Deskripsi</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="desc" id="desc" placeholder="Deskripsi">
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="photo" class="col-sm-2 control-label">File</label>
        <div class="col-sm-10">
            <input type="file" class="form-control" name="photo" id="photo" placeholder="Photo">
        </div>
        <div class="cleared"></div>
    </div>
    <div class="footer_margin">&nbsp;</div>
	<div class="form-group right">
	<button type="submit" name="button" class="btn btn-success">&nbsp;&nbsp; Simpan &nbsp;&nbsp;</button>
    <button type="button" name="button" class="btn btn-danger" onclick="location.href='{back_link}';">&nbsp;&nbsp; Kembali &nbsp;&nbsp;</button>
    </div>

</form>

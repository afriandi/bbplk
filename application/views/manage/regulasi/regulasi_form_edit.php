{message}
<form method="post" action="{action}"  enctype="multipart/form-data">

    <div class="form-group">
        <label for="nama" class="col-sm-2 control-label">Jabatan</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="nama" id="nama" value="<?php echo set_value('nama', $this->form_data->nama); ?>" placeholder="Nama Regulasi">
            <input type="hidden" name="id" id="id" value="<?php echo set_value('id', $this->form_data->id); ?>" >
            <input type="hidden" name="img" id="img" value="<?php echo set_value('img', $this->form_data->img); ?>" >
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="desc" class="col-sm-2 control-label">Deskripsi</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="desc" id="desc" value="<?php echo set_value('desc', $this->form_data->desc); ?>" placeholder="Deskripsi">
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="photo" class="col-sm-2 control-label">File</label>
        <div class="col-sm-10">
        	{regulasi_file}
            <input type="file" class="form-control" name="photo" id="photo" placeholder="Photo">
        </div>
        <div class="cleared"></div>
    </div>
    <div class="footer_margin">&nbsp;</div>
	<div class="form-group right">
	<button type="submit" name="button" class="btn btn-success">&nbsp;&nbsp; Simpan &nbsp;&nbsp;</button>
    <button type="button" name="button" class="btn btn-danger" onclick="location.href='{back_link}';">&nbsp;&nbsp; Kembali &nbsp;&nbsp;</button>
    </div>

</form>

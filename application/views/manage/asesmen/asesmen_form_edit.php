{message}
<form method="post" action="{action}"  enctype="multipart/form-data">
    <div class="form-group">
        <label for="no" class="col-sm-2 control-label">Nomor Skema</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="no" id="no" value="<?php echo set_value('skema', $this->form_data->skema); ?>" placeholder="Nomor Skema">
            <input type="hidden" name="id" id="id" value="<?php echo set_value('id', $this->form_data->id); ?>" >
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="judul" class="col-sm-2 control-label">Judul Skema</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="judul" id="judul" value="<?php echo set_value('judul', $this->form_data->judul); ?>" placeholder="Judul Skema">
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="kejuruan" class="col-sm-2 control-label">Kejuruan</label>
        <div class="col-sm-10">
            {combo_kejuruan}
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="desc" class="col-sm-2 control-label">Deskripsi</label>
        <div class="col-sm-10">
            <textarea name="desc" id="desc" class="form-control" placeholder="Deskripsi" ><?php echo set_value('desc', $this->form_data->desc); ?></textarea>
        </div>
        <div class="cleared"></div>
    </div>
    <div class="footer_margin">&nbsp;</div>
	<div class="form-group right">
	<button type="submit" name="button" class="btn btn-success">&nbsp;&nbsp; Simpan &nbsp;&nbsp;</button>
    <button type="button" name="button" class="btn btn-danger" onclick="location.href='{back_link}';">&nbsp;&nbsp; Kembali &nbsp;&nbsp;</button>
    </div>

</form>

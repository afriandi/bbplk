{message}
<form method="post" action="{action}"  enctype="multipart/form-data">
    <div class="form-group">
        <label for="nip" class="col-sm-2 control-label">NIP</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="nip" id="nip" placeholder="NIP">
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="username" class="col-sm-2 control-label">Username</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="username" id="username" placeholder="Username">
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="password" class="col-sm-2 control-label">Password</label>
        <div class="col-sm-10">
            <input type="password" class="form-control" name="password" id="password" placeholder="Password">
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="jabatan" class="col-sm-2 control-label">Role</label>
        <div class="col-sm-10">
            {combo_role}
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="jabatan" class="col-sm-2 control-label">status</label>
        <div class="col-sm-10">
            {radio_status}
        </div>
        <div class="cleared"></div>
    </div>
    <div class="footer_margin"></div>
	<div class="form-group right">
	<button type="submit" name="button" class="btn btn-success">&nbsp;&nbsp; Simpan &nbsp;&nbsp;</button>
    <button type="button" name="button" class="btn btn-danger" onclick="location.href='{back_link}';">&nbsp;&nbsp; Kembali &nbsp;&nbsp;</button>
    </div>

</form>

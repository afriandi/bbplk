{message}
<div class="col-sm-12">
	<img src="{img}" height="200">
</div>
<div style="margin:10px;">&nbsp;</div>
<form method="post" action="{action}"  enctype="multipart/form-data">
    <div class="form-group">
        <label for="nip" class="col-sm-2 control-label">Judul</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="judul" id="judul" value="<?php echo set_value('judul', $this->form_data->judul); ?>" placeholder="Judul">
            <input type="hidden" name="id" id="id" value="<?php echo set_value('id', $this->form_data->id); ?>" >
            <input type="hidden" name="img" id="img" value="<?php echo set_value('img', $this->form_data->img); ?>" >
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="photo" class="col-sm-2 control-label">Photo</label>
        <div class="col-sm-10">
            <input type="file" class="form-control" name="photo" id="photo" placeholder="Photo">
        </div>
        <div class="cleared"></div>
    </div>
    <div class="footer_margin"></div>
	<div class="form-group right">
	<button type="submit" name="button" class="btn btn-success">&nbsp;&nbsp; Simpan &nbsp;&nbsp;</button>
    <button type="button" name="button" class="btn btn-danger" onclick="location.href='{back_link}';">&nbsp;&nbsp; Kembali &nbsp;&nbsp;</button>
    </div>

</form>

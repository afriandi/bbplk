{message}
<form method="post" action="{action}"  enctype="multipart/form-data">
    <div class="form-group">
        <label for="gol" class="col-sm-2 control-label">Golongan</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="gol" id="gol" placeholder="Golongan">
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="pangkat" class="col-sm-2 control-label">Pangkat</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="pangkat" id="pangkat" placeholder="Pangkat">
        </div>
        <div class="cleared"></div>
    </div>
    <div class="footer_margin">&nbsp;</div>
	<div class="form-group right">
	   <button type="submit" name="button" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp; Simpan &nbsp;&nbsp;</button>
     <button type="button" name="button" class="btn btn-danger" onclick="location.href='{back_link}';">&nbsp;&nbsp; Kembali &nbsp;&nbsp;</button>
  </div>
</form>

<?php
	require_once('header.php');
	require_once('menu_admin.php');
?>
<a href="#" id="atas"></a>
<div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{title_menu}</h1>
                <ol class="breadcrumb">
                    <li><a href="<?php echo base_url() ?>/dashboard.php">Home</a></li>
                    <li><a href="#">{menu}</a>
                    </li>
                    <li class="active">{submenu}</li>
                </ol>
            </div>
        </div>

        <div class="row">
            <?php require_once('menu_left_adm.php');?>
            <!-- Content Column -->
            <div class="col-md-9">
            	<div class="row">
					<div class="well laporan">
						<?php
								if($message!=''){
									echo '<div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'.$message.'</div>';
								}
						?>
						{table}
            <div style="text-align:right;padding: 10px;">
                {pagination}
            </div>
            {add_btn} {print_btn}
					</div>
                </div>
            </div>
        </div>
        <!-- /.row -->
	<?php require_once('footer.php'); ?>
</div>

<?php 
	require_once('header.php');	
	require_once('menu_admin.php'); 
?>
<a href="#" id="atas"></a>
<div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{title_menu}</h1>
                <ol class="breadcrumb">
                    <li><a href="<?php echo base_url() ?>/dashboard.php">Home</a></li>
                    <li><a href="#">{menu}</a>
                    </li>
                    <li class="active">{submenu}</li>
                </ol>
            </div>
        </div>

        <div class="row">
            <?php require_once('menu_left_adm.php')?>
            <!-- Content Column -->
            <div class="col-md-9">
            	<div class="row well">
                	{content}
                </div>
            </div>
        </div>
        <!-- /.row -->
	<?php require_once('footer.php'); ?>
</div>



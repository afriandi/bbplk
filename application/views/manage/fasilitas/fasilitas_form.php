{message}
<form method="post" action="{action}"  enctype="multipart/form-data">
    <div class="form-group">
        <label for="judul" class="col-sm-2 control-label">Judul</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="judul" id="judul" placeholder="Judul">
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="milik" class="col-sm-2 control-label">Kepemilikan</label>
        <div class="col-sm-10">
            {combo_kepemilikan}
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="desc" class="col-sm-2 control-label">Deskripsi</label>
        <div class="col-sm-10">
            <textarea name="desc" id="desc" placeholder="Berita" class="form-control"> </textarea>
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="photo" class="col-sm-2 control-label">Photo</label>
        <div class="col-sm-10">
            <input type="file" class="form-control" name="photo" id="photo" placeholder="Photo">
        </div>
        <div class="cleared"></div>
    </div>
    <div class="footer_margin"></div>
	<div class="form-group right">
	<button type="submit" name="button" class="btn btn-success">&nbsp;&nbsp; Simpan &nbsp;&nbsp;</button>
    <button type="button" name="button" class="btn btn-danger" onclick="location.href='{back_link}';">&nbsp;&nbsp; Kembali &nbsp;&nbsp;</button>
    </div>

</form>

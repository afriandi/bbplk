{message}
<form method="post" action="{action}"  enctype="multipart/form-data">
    <div class="form-group">
        <label for="id" class="col-sm-2 control-label">NIP</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="nip" id="nip" placeholder="NIP"  list="dtpeg">
            {datalist_pegawai}
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="met" class="col-sm-2 control-label">No Met</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="met" id="met" placeholder="No Met" >
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="lsp" class="col-sm-2 control-label">No LSP</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="lsp" id="lsp" placeholder="No LSP" >
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="exp" class="col-sm-2 control-label">Tanggal Expire</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="exp" id="exp" placeholder="Tanggal Expire" >
        </div>
        <div class="cleared"></div>
    </div>
    <div class="footer_margin">&nbsp;</div>
	<div class="form-group right">
	<button type="submit" name="button" class="btn btn-success">&nbsp;&nbsp; Simpan &nbsp;&nbsp;</button>
    <button type="button" name="button" class="btn btn-danger" onclick="location.href='{back_link}';">&nbsp;&nbsp; Kembali &nbsp;&nbsp;</button>
    </div>

</form>

<script>
  $(function () {

    $('#exp').datepicker({
      autoclose: true,
	   format: 'yyyy-mm-dd'
    });

  });
</script>

{message}
<form method="post" action="{action}"  enctype="multipart/form-data">
	<table class="table-form col-sm-12">
    	<tr>
        	<td width="20%"><label for="nip" class="col-sm-3 control-label">NIP</label></td>
            <td width="60%">
            	<div class="col-sm-12">
                	<input type="text" class="form-control" disabled name="nip" id="nip" value="<?php echo set_value('nip', $this->form_data->nip); ?>" placeholder="NIP">
            	</div>
            </td>
            <td width="20%" rowspan="6" align="right" valign="top">
            	<img src="{photo}" width="150px"/>
            </td>
        </tr>
        <tr>
        	<td><label for="met" class="col-sm-3 control-label">No_Met</label></td>
            <td>
            	<div class="col-sm-12">
                	<input type="text" class="form-control" name="met" id="met" placeholder="No Met" value="<?php echo set_value('met', $this->form_data->met); ?>" disabled>
            	</div>
            </td>
        </tr>
        <tr>
        	<td><label for="lsp" class="col-sm-3 control-label">No_Lsp</label></td>
            <td>
            	<div class="col-sm-12">
                	<input type="text" class="form-control" name="lsp" id="lsp" placeholder="No Lsp" value="<?php echo set_value('lsp', $this->form_data->lsp); ?>" disabled>
            	</div>
            </td>
        </tr>
        <tr>
        	<td><label for="exp" class="col-sm-3 control-label">Tanggal_Expire</label></td>
            <td>
            	<div class="col-sm-12">
                	<input type="text" class="form-control" name="exp" id="exp" placeholder="Tanggal Exp" value="<?php echo set_value('exp', $this->form_data->exp); ?>" disabled>
            	</div>
            </td>
        </tr>
        <tr>
        	<td><label for="nama" class="col-sm-3 control-label">Nama_Lengkap</label></td>
            <td>
            	<div class="col-sm-12">
                	<input type="text" class="form-control" name="nama" id="nama" placeholder="Nama Lengkap" value="<?php echo set_value('nama', $this->form_data->nama); ?>" disabled>
            	</div>
            </td>
        </tr>
        <tr>
        	<td><label for="tempat_lahir" class="col-sm-3 control-label">Tempat/Tgl_Lahir</label></td>
            <td >
            	<div class="col-xs-7">
                    <input type="text" class="form-control" name="tempat_lahir" id="tempat_lahir" value="<?php echo set_value('tempat_lahir', $this->form_data->tempat_lahir); ?>" placeholder="Tempat Lahir" disabled>
                </div>
                <div class="col-xs-5">
                    <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="text" class="form-control pull-right" name="tgl_lahir" id="tgl_lahir" value="<?php echo set_value('tempat_lahir', $this->form_data->tgl_lahir); ?>" placeholder="Tanggal Lahir" disabled>
                        </div>
                </div>
            </td>

        </tr>
        <tr>
        	<td><label for="jabatan" class="col-sm-3 control-label">Jabatan</label ></td>
            <td colspan="2">
            	<div class="col-sm-12">
                	{combo_jabatan}
            	</div>
            </td>
        </tr>
        <tr>
        	<td><label for="golongan" class="col-sm-3 control-label">Golongan</label></td>
            <td colspan="2">
            	<div class="col-sm-12">
                	{combo_golongan}
            	</div>
            </td>
        </tr>
        <tr>
        	<td><label for="tmt" class="col-sm-2 control-label">TMT</label></td>
            <td colspan="2">
            	<div class="col-sm-12">
                	<input type="text" class="form-control" name="tmt" id="tmt" value="<?php echo set_value('tmt', $this->form_data->tmt); ?>" placeholder="TMT" disabled>
            	</div>
            </td>
        </tr>
        <tr>
        	<td><label for="pendidikan" class="col-sm-2 control-label">Pendidikan</label></td>
            <td colspan="2">
            	<div class="col-sm-12">
                	{combo_pendidikan}
            	</div>
            </td>
        </tr>
        <tr>
        	<td><label for="gender" class="col-sm-2 control-label">Jenis_Kelamin</label></td>
            <td colspan="2">
                {radio_gender}
            </td>
        </tr>
    </table>
    <div class="footer_margin">&nbsp;</div>
	<div class="form-group right">
    <button type="button" name="button" class="btn btn-danger" onclick="location.href='{back_link}';">&nbsp;&nbsp; Kembali &nbsp;&nbsp;</button>
    </div>

</form>

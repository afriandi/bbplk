{message}
<img src="{img}" height="150" >
<div class="footer_margin">&nbsp;</div>
<form method="post" action="{action}"  enctype="multipart/form-data">
    <div class="form-group">
        <label for="fasilitas" class="col-sm-2 control-label">Fasilitas</label>
        <div class="col-sm-10">
        	<input type="hidden" name="id" id="id" value="<?php echo set_value('id', $this->form_data->id); ?>" class="form-control"/>
            <input type="hidden" name="img" id="img" value="<?php echo set_value('img', $this->form_data->img); ?>" class="form-control"/>
            {combo_fasilitas}
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="detail" class="col-sm-2 control-label">Detail</label>
        <div class="col-sm-10">
        	<div class="box-body pad">
            <textarea name="detail" id="detail" placeholder="Detail Fasilitas" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo set_value('detail', $this->form_data->detail); ?></textarea>
            </div>
        </div>
        <div class="cleared"></div>
    </div>
    <div class="form-group">
        <label for="foto" class="col-sm-2 control-label">Photo</label>
        <div class="col-sm-10">
            <input type="file" name="photo" id="photo" class="form-control"/>
        </div>
        <div class="cleared"></div>
    </div>


    <div class="footer_margin">&nbsp;</div>
	<div class="form-group right">
	<button type="submit" name="button" class="btn btn-success">&nbsp;&nbsp; Simpan &nbsp;&nbsp;</button>
    <button type="button" name="button" class="btn btn-danger" onclick="location.href='{back_link}';">&nbsp;&nbsp; Kembali &nbsp;&nbsp;</button>
    </div>

</form>


<script src="<?php echo base_url() . 'assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js'; ?>"></script>

<script>
  $(function () {

    //$("#detail").wysihtml5();

	var myCustomTemplates = {
    emphasis : function(locale) {
        return "<li>" +
            "<div class='btn-group'>" +
            "<a data-wysihtml5-command='bold' title='Bold' class='btn btn-none btn-default' ><span style='font-weight:700'>B</span></a>" +
            "<a data-wysihtml5-command='italic' title='Italics' class='btn btn-none btn-default' ><span style='font-style:italic'>I</span></a>" +
            "<a data-wysihtml5-command='underline' title='Underline' class='btn btn-none btn-default' ><span style='text-decoration:underline'>U</span></a>" +
            "</div>" +
            "</li>";
    },
    lists : function(locale) {
        return "<li>" +
            "<div class='btn-group'>" +
            "<a data-wysihtml5-command='insertUnorderedList' title='Unordered list' class='btn btn-none btn-default' ><span class='fa fa-list-ul'></span></a>" +
            "<a data-wysihtml5-command='insertOrderedList' title='Ordered list' class='btn btn-none btn-default' ><span class='fa fa-list-ol'></span></a>" +
            "<a data-wysihtml5-command='Outdent' title='Outdent' class='btn btn-none btn-default' ><span class='fa fa-outdent'></span></a>" +
            "<a data-wysihtml5-command='Indent' title='Indent' class='btn btn-none btn-default'><span class='fa fa-indent'></span></a>" +
            "</div>" +
            "</li>";
    }
}

$('#detail').wysihtml5({
    toolbar: {
        "font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
        "emphasis": true, //Italics, bold, etc. Default true
        "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
        "html": true, //Button which allows you to edit the generated HTML. Default false
        "link": true, //Button to insert a link. Default true
        "image": true, //Button to insert an image. Default true,
        "color": true, //Button to change color of font
        "blockquote": false, //Blockquote
    },
    customTemplates: myCustomTemplates
});

  });
</script>

<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kontak extends CI_Controller {

    public function index()
    {

		$islogin = '<li><a href="'.base_url().'login"><span class="glyphicon glyphicon-lock"></span>Login</a></li>';

		if ($this->session->userdata('username')) {
            $islogin = ' <li><a href="'.base_url().'login/logout"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>';
        }

        $contentVars['base_url'] = base_url();

		$pagedata = array(
			'title' => ' >> Kontak',
			'islogin' => $islogin,
			'content' => $this->parser->parse('kontak', $contentVars, TRUE)
		);

        $this->parser->parse('main', $pagedata);
    }

}

?>

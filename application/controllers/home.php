<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->load->model('slider_model', '', TRUE);
        $this->load->model('pengumuman_model', '', TRUE);
		$this->load->model('galeri_model', '', TRUE);
		$this->load->model('responden_model', '', TRUE);
        $this->tableName = 'slider';
		$this->tableName2 = 'pengumuman';
    }

    public function index()
    {

		$islogin = '<li><a href="'.base_url().'login"><span class="glyphicon glyphicon-lock"></span>Login</a></li>';

		if ($this->session->userdata('username')) {
            $islogin = ' <li><a href="'.base_url().'login/logout"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>';
        }

		$slider = slider_model::list_all($this->tableName)->result();
		$list = array();
		$i=0;
		foreach ($slider as $sld) {
			$list[$i]['slider_id'] = $i;
      $list[$i]['slider_title'] = $sld->judul;
			$list[$i]['slider_img'] = base_url().'assets/img/slider/'.$sld->foto;
			$list[$i]['slider_uploaded'] = $sld->tgl_upload;
      $i++;
		}

		$galeri = galeri_model::get_paged_list('galeri', 6)->result();
		$galer = array();
		$a=0;
		foreach ($galeri as $gal) {
			$galer[$a]['galeri_id'] = $a;
      $galer[$a]['galeri_title'] = $gal->judul;
			$galer[$a]['galeri_img'] = base_url().'assets/img/galeri/'.$gal->foto;
			$galer[$a]['galeri_uploaded'] = $gal->tgl_upload;
        	$a++;
		}

		$pengumuman = pengumuman_model::terbaru($this->tableName2)->result();
		$list_pengumuman = array();
		$p=0;
		foreach ($pengumuman as $pgmn) {
			$list_pengumuman[$p]['deskripsi'] = $pgmn->deskripsi;
			$isi_pengumuman = $list_pengumuman[$p]['deskripsi'];
			$p++;
		}
		$isi_pengumuman = substr($isi_pengumuman,0,135);


		$responden = responden_model::list_all('responden')->result();
		$list_resp_jelas = array('SJ' => 0, 'J' => 0, 'CJ' => 0,'TJ' => 0,'STJ' => 0);
		$list_resp_menarik = array('SM' => 0, 'M' => 0, 'CM' => 0,'TM' => 0,'STM' => 0);
		$total_jelas = 0;
		$total_menarik = 0;
		foreach ($responden as $rsp) {
			if($rsp->jelas == 'SJ'){
				$list_resp_jelas['SJ'] = $list_resp_jelas['SJ'] + 1;
				$total_jelas = $total_jelas + 1;
			}
			if($rsp->jelas == 'J'){
				$list_resp_jelas['J'] = $list_resp_jelas['J'] + 1;
				$total_jelas = $total_jelas + 1;
			}
			if($rsp->jelas == 'CJ'){
				$list_resp_jelas['CJ'] = $list_resp_jelas['CJ'] + 1;
				$total_jelas = $total_jelas + 1;
			}
			if($rsp->jelas == 'TJ'){
				$list_resp_jelas['TJ'] = $list_resp_jelas['TJ'] + 1;
				$total_jelas = $total_jelas + 1;
			}
			if($rsp->jelas == 'STJ'){
				$list_resp_jelas['STJ'] = $list_resp_jelas['STJ'] + 1;
				$total_jelas = $total_jelas + 1;
			}
			if($rsp->menarik == 'SM'){
				$list_resp_menarik['SM'] = $list_resp_menarik['SM'] + 1;
				$total_menarik = $total_menarik + 1;
			}
			if($rsp->menarik == 'M'){
				$list_resp_menarik['M'] = $list_resp_menarik['M'] + 1;
				$total_menarik = $total_menarik + 1;
			}
			if($rsp->menarik == 'CM'){
				$list_resp_menarik['CM'] = $list_resp_menarik['CM'] + 1;
				$total_menarik = $total_menarik + 1;
			}
			if($rsp->menarik == 'TM'){
				$list_resp_menarik['TM'] = $list_resp_menarik['TM'] + 1;
				$total_menarik = $total_menarik + 1;
			}
			if($rsp->menarik == 'STM'){
				$list_resp_menarik['STM'] = $list_resp_menarik['STM'] + 1;
				$total_menarik = $total_menarik + 1;
			}
			
		}
		

    	$contentVars = array(
			'base_url' => base_url(),
			'slider_count' => $i,
			'slider' => $list,
			'galeri' => $galer,
			'pengumuman'=>$isi_pengumuman,
			'jelas' => $list_resp_jelas,
			'menarik' => $list_resp_menarik,
			'total_jelas' => $total_jelas,
			'total_menarik' => $total_menarik
		);
		
		$pagedata = array(
			'title' => ' >> HOME',
			'islogin' => $islogin,
			'content' => $this->parser->parse('home', $contentVars, TRUE)
		);

        $this->parser->parse('main_home', $pagedata);
    }

	
	function review(){
		$contentVars = array(
			'base_url' => base_url()
		);

		$pagedata = array(
			'title' => ' >> HOME',
			'islogin' => '',
			
		);

        $this->parser->parse('review_site', $pagedata);
	}
	
	
	function proses_review(){
		if($this->input->post('jelas') != null && $this->input->post('menarik') != null){
			$data = array(
				'user_agent' => $_SERVER['HTTP_USER_AGENT'],
				'ip' => $_SERVER['REMOTE_ADDR'],
				'jelas' => $this->input->post('jelas'),
				'menarik' => $this->input->post('menarik')
			);
			
			$addResponden = responden_model::save('responden', $data);
		}
		redirect('home/index/#footer');
	}

}

?>

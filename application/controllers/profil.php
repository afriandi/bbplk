<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of assistant
 *
 * @author afriandizen
 */
class profil extends CI_Controller {

    private $tableName ;

    function __construct() {
      parent::__construct();
      $this->load->helper('formutil');
      $this->load->model('blk_model', '', TRUE);
		  $this->load->model('fasilitas_model', '', TRUE);
		  $this->load->model('regulasi_model', '', TRUE);
      $this->tableName = 'blk';
    }

    function index() {
        $islogin = '<li><a href="'.base_url().'login"><span class="glyphicon glyphicon-lock"></span>Login</a></li>';

		if ($this->session->userdata('username')) {
            $islogin = ' <li><a href="'.base_url().'login/logout"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>';
        }

		$blk = blk_model::get_by_id($this->tableName, 1);

		$contentVars = array(
			'base_url' =>  base_url(),
			'title_menu' =>  'Tentang BLK Bandung',
			'menu' => 'Profil',
			'submenu' =>  'Tentang',
			'profile_img' => base_url().'assets/img/'.$blk->foto,
			'profile_title' => '',
			'profile_desc' => $blk->tentang
		);

		$pagedata = array(
			'title' => ' >> Profil',
			'islogin' => $islogin,
			'content' => $this->parser->parse('profil/profil_tentang', $contentVars, TRUE)
		);

        $this->parser->parse('main', $pagedata);
    }

	function visimisi() {
        $islogin = '<li><a href="'.base_url().'login"><span class="glyphicon glyphicon-lock"></span>Login</a></li>';

		if ($this->session->userdata('username')) {
            $islogin = ' <li><a href="'.base_url().'login/logout"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>';
        }

		$blk = blk_model::get_by_id($this->tableName, 1);

		$contentVars = array(
			'base_url' =>  base_url(),
			'title_menu' =>  'Visi dan Misi BLK Bandung',
			'menu' => 'Profil',
			'submenu' =>  'Visi Misi',
			'profile_img' => base_url().'assets/img/'.$blk->foto,
			'profile_visi' => $blk->visi,
			'profile_misi' => $blk->misi
		);

		$pagedata = array(
			'title' => ' >> Profile',
			'islogin' => $islogin,
			'content' => $this->parser->parse('profil/profil_visimisi', $contentVars, TRUE)
		);

        $this->parser->parse('main', $pagedata);
    }


	function struktur() {
        $islogin = '<li><a href="'.base_url().'login"><span class="glyphicon glyphicon-lock"></span>Login</a></li>';

		if ($this->session->userdata('username')) {
            $islogin = ' <li><a href="'.base_url().'login/logout"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>';
        }

		$blk = blk_model::get_by_id($this->tableName, 1);

		$contentVars = array(
			'base_url' =>  base_url(),
			'title_menu' =>  'Struktur Organisasi BLK Bandung ',
			'menu' => 'Profil',
			'submenu' =>  'Struktur Organisasi',
			'profile_struktur_img' => base_url().'assets/img/'.$blk->struktur_org,
		);

		$pagedata = array(
			'title' => ' >> Profile',
			'islogin' => $islogin,
			'content' => $this->parser->parse('profil/profil_struktur', $contentVars, TRUE)
		);

        $this->parser->parse('main', $pagedata);
    }

    function fasilitas() {
  		$table = 'fasilitas';
  		$kepemilikan = 'umum';

      $islogin = '<li><a href="'.base_url().'login"><span class="glyphicon glyphicon-lock"></span>Login</a></li>';
  		if ($this->session->userdata('username')) {
              $islogin = ' <li><a href="'.base_url().'login/logout"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>';
          }

          $jumlah_data = $this->fasilitas_model->count_fasilitas_umum($table,$kepemilikan);
          $this->load->library('pagination');
          $config['base_url'] = site_url().'/profil/fasilitas';
          $config['total_rows'] = $jumlah_data;
          $config['per_page'] = 5;
          $config['uri_segment'] = 3;
             $config['use_page_numbers'] = true;
             $config['full_tag_open']    = '<ul class="pagination pagination-sm">';
             $config['full_tag_close']   = '</ul>';
             $config['first_link']       = 'First';
             $config['last_link']        = 'Last';
             $config['first_tag_open']   = '<li>';
             $config['first_tag_close']  = '</li>';
             $config['prev_link']        = '&laquo';
             $config['prev_tag_open']    = '<li class="prev">';
             $config['prev_tag_close']   = '</li>';
             $config['next_link']        = '&raquo';
             $config['next_tag_open']    = '<li>';
             $config['next_tag_close']   = '</li>';
             $config['last_tag_open']    = '<li>';
             $config['last_tag_close']   = '</li>';
             $config['cur_tag_open']     = '<li class="active"><a href="">';
             $config['cur_tag_close']    = '</a></li>';
             $config['num_tag_open']     = '<li>';
             $config['num_tag_close']    = '</li>';
          if($this->uri->segment(3)=="" || $this->uri->segment(3)<1){
            $from=0;
          }else{
            $from=($this->uri->segment(3)-1)*$config['per_page'];
          }
          $this->pagination->initialize($config);

  		$contentVars = array(
  			'base_url' =>  base_url(),
  			'title_menu' =>  'Fasilitas BLK Bandung ',
  			'menu' => 'Profil',
  			'submenu' =>  'Fasilitas',
        'pagination' => $this->pagination->create_links()
  		);

  		$fasilitas = fasilitas_model::get_by_kepemilikan($table,$kepemilikan,$config['per_page'],$from)->result();

  		$list = array();
  		$i=0;
  		foreach ($fasilitas as $fst) {
              $lsit[$i]['fasilitas_title'] = $fst->judul;
  			$lsit[$i]['fasilitas_desc'] = $fst->deskripsi;
  			$lsit[$i]['fasilitas_img'] = base_url().'assets/img/fasilitas/'.$fst->foto;
  			$lsit[$i]['fasilitas_detail'] = base_url().'profil/detil_fasilitas/'.$fst->id_fasilitas;
          	$i++;
  		}
  		$contentVars['fasilitas'] = $lsit;

  		$pagedata = array(
  			'title' => ' >> Profile',
  			'islogin' => $islogin,
  			'content' => $this->parser->parse('profil/profil_fasilitas', $contentVars, TRUE)
  		);

          $this->parser->parse('main', $pagedata);
      }

	function detil_fasilitas($id) {
		$table = 'fasilitas';

    $islogin = '<li><a href="'.base_url().'login"><span class="glyphicon glyphicon-lock"></span>Login</a></li>';
		if ($this->session->userdata('username')) {
        $islogin = ' <li><a href="'.base_url().'login/logout"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>';
    }

		$contentVars = array(
			'base_url' =>  base_url(),
			'title_menu' =>  'Detail Fasilitas BLK Bandung',
			'menu' => 'Profil',
			'submenu' =>  'Detail Fasilitas',
			'back' =>  base_url().'profil/fasilitas'
		);

    $fasilitas 	= fasilitas_model::get_by_id($table, $id);
    $contentVars['fasilitas_title'] = $fasilitas->judul;
		$contentVars['fasilitas_img'] = base_url().'assets/img/fasilitas/'.$fasilitas->foto;
		$contentVars['fasilitas_desc'] = $fasilitas->deskripsi;
		$contentVars['fasilitas_milik'] = "Umum";
		//$fasilitas = fasilitas_model::get_detail_fasilitas($id)->result();

		$pagedata = array(
			'title' => ' >> Profile',
			'islogin' => $islogin,
			'content' => $this->parser->parse('profil/profil_fasilitas_detil', $contentVars, TRUE)
		);

        $this->parser->parse('main', $pagedata);
    }

    function regulasi(){
  $table = 'regulasi';
  $islogin = '<li><a href="'.base_url().'login"><span class="glyphicon glyphicon-lock"></span>Login</a></li>';
  if ($this->session->userdata('username')) {
          $islogin = ' <li><a href="'.base_url().'login/logout"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>';
      }

      $jumlah_data = $this->regulasi_model->count_all($table);
      $this->load->library('pagination');
      $config['base_url'] = site_url().'/profil/regulasi';
      $config['total_rows'] = $jumlah_data;
      $config['per_page'] = 5;
      $config['uri_segment'] = 3;
         $config['use_page_numbers'] = true;
         $config['full_tag_open']    = '<ul class="pagination pagination-sm">';
         $config['full_tag_close']   = '</ul>';
         $config['first_link']       = 'First';
         $config['last_link']        = 'Last';
         $config['first_tag_open']   = '<li>';
         $config['first_tag_close']  = '</li>';
         $config['prev_link']        = '&laquo';
         $config['prev_tag_open']    = '<li class="prev">';
         $config['prev_tag_close']   = '</li>';
         $config['next_link']        = '&raquo';
         $config['next_tag_open']    = '<li>';
         $config['next_tag_close']   = '</li>';
         $config['last_tag_open']    = '<li>';
         $config['last_tag_close']   = '</li>';
         $config['cur_tag_open']     = '<li class="active"><a href="">';
         $config['cur_tag_close']    = '</a></li>';
         $config['num_tag_open']     = '<li>';
         $config['num_tag_close']    = '</li>';
      if($this->uri->segment(3)=="" || $this->uri->segment(3)<1){
        $from=0;
      }else{
        $from=($this->uri->segment(3)-1)*$config['per_page'];
      }
      $this->pagination->initialize($config);
      //$contentVars['pagination'] = $this->pagination->create_links();
      $blks = $this->regulasi_model->get_all($table,$config['per_page'],$from);

      //$blks = regulasi_model::get_paged_list($table,$config['per_page'], $from)->result();

      $this->load->library('table');
  $tabletemp['table_open'] = '<table class="table table-striped struktur_org">';
  $this->table->set_template($tabletemp);
      $this->table->set_empty("&nbsp;");
      $this->table->set_heading('NO', 'PERATURAN PERUNDANGAN', 'TENTANG', 'DOWNLOAD');
      //$i = $offset;
  $a =1;
      foreach ($blks as $blk) {
          $this->table->add_row(
        $a,
        $blk->nama,
        $blk->deskripsi,
        '<a href="'.base_url().'assets/file/regulasi/'.$blk->file.'" target="_blank"><button type="button" name="button"><span class="glyphicon glyphicon-cloud-download"></span> Download</button></a>'
          );
    $a++;
      }

  $contentVars = array(
    'base_url' =>  base_url(),
    'title_menu' =>  'Regulasi BLK Bandung ',
    'menu' => 'Profil',
    'submenu' =>  'Regulasi',
    'table' => $this->table->generate(),
    'pagination' => $this->pagination->create_links()
  );

  $pagedata = array(
    'title' => ' >> Profile',
    'islogin' => $islogin,
    'content' => $this->parser->parse('profil/profil_regulasi', $contentVars, TRUE)
  );

  $this->parser->parse('main', $pagedata);
}

}

?>

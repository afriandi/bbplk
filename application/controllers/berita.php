<?php

class berita extends CI_Controller {

    private $tableName ;

    function __construct() {
        parent::__construct();
        $this->load->model('berita_model', '', TRUE);
        $this->tableName = 'berita';
    }

	function index() {

    $islogin = '<li><a href="'.base_url().'login"><span class="glyphicon glyphicon-lock"></span>Login</a></li>';
		if ($this->session->userdata('username')) {
            $islogin = ' <li><a href="'.base_url().'login/logout"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>';
    }

		$contentVars = array(
			'base_url' =>  base_url(),
			'title_menu' =>  'Berita',
			'menu' => 'Berita'
		);

    $jumlah_data = $this->berita_model->count_all($this->tableName);
    $this->load->library('pagination');
    $config['base_url'] = site_url().'/berita/index';
    $config['total_rows'] = $jumlah_data;
    $config['per_page'] = 5;
    $config['uri_segment'] = 3;
       $config['use_page_numbers'] = true;
       $config['full_tag_open']    = '<ul class="pagination pagination-sm">';
       $config['full_tag_close']   = '</ul>';
       $config['first_link']       = 'First';
       $config['last_link']        = 'Last';
       $config['first_tag_open']   = '<li>';
       $config['first_tag_close']  = '</li>';
       $config['prev_link']        = '&laquo';
       $config['prev_tag_open']    = '<li class="prev">';
       $config['prev_tag_close']   = '</li>';
       $config['next_link']        = '&raquo';
       $config['next_tag_open']    = '<li>';
       $config['next_tag_close']   = '</li>';
       $config['last_tag_open']    = '<li>';
       $config['last_tag_close']   = '</li>';
       $config['cur_tag_open']     = '<li class="active"><a href="">';
       $config['cur_tag_close']    = '</a></li>';
       $config['num_tag_open']     = '<li>';
       $config['num_tag_close']    = '</li>';
    if($this->uri->segment(3)=="" || $this->uri->segment(3)<1){
       $from=0;
    }else{
       $from=($this->uri->segment(3)-1)*$config['per_page'];
    }
    $this->pagination->initialize($config);
    $contentVars['pagination'] = $this->pagination->create_links();

		$beritas = berita_model::get_all($this->tableName,$config['per_page'],$from);

		$list = array();
		$i=0;
		foreach ($beritas as $brt) {
      $lsit[$i]['berita_title'] = $brt->judul;
			$lsit[$i]['berita_date'] = date("d F y", strtotime($brt->tanggal));
			$lsit[$i]['berita_img'] = base_url().'assets/img/berita/'.$brt->gambar;
			$lsit[$i]['berita_desc'] = $brt->deskripsi;
      $isi_berita = $lsit[$i]['berita_desc'];
      $isi_berita = substr($isi_berita,0,140)." ...";
      $lsit[$i]['isi_berita'] = $isi_berita;
			$lsit[$i]['berita_postby'] = $brt->username;
			$lsit[$i]['berita_detail'] = base_url().'berita/detail/'.$brt->id_berita;
        	$i++;
		}
		$contentVars['berita'] = $lsit;

		$pagedata = array(
			'title' => ' >> Berita',
			'islogin' => $islogin,
			'content' => $this->parser->parse('berita/berita', $contentVars, TRUE)
		);

        $this->parser->parse('main', $pagedata);
    }

	function detail($id) {
		$table = 'detail_fasilitas';

        $islogin = '<li><a href="'.base_url().'login"><span class="glyphicon glyphicon-lock"></span>Login</a></li>';
		if ($this->session->userdata('username')) {
            $islogin = ' <li><a href="'.base_url().'login/logout"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>';
        }

		$berita = berita_model::get_by_id($this->tableName, $id);

		$contentVars = array(
			'base_url' =>  base_url(),
			'title_menu' =>  'Berita',
			'menu' =>  'Berita',
			'berita_title' => $berita->judul,
			'berita_date' => date("d F y", strtotime($berita->tanggal)),
			'berita_img' => base_url().'assets/img/berita/'.$berita->gambar,
			'berita_postby' => $berita->username,
			'berita_desc' => $berita->deskripsi,
			'berita_id' => $id
		);



		$pagedata = array(
			'title' => ' >> Berita',
			'islogin' => $islogin,
			'content' => $this->parser->parse('berita/berita_detil', $contentVars, TRUE)
		);

        $this->parser->parse('main', $pagedata);
    }

}

?>

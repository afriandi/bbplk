<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Galeri extends CI_Controller {

	private $tableName ;

    function __construct() {
        parent::__construct();
        $this->load->model('galeri_model', '', TRUE);
        $this->tableName = 'galeri';
    }


    public function index()
    {

		$islogin = '<li><a href="'.base_url().'login"><span class="glyphicon glyphicon-lock"></span>Login</a></li>';

		if ($this->session->userdata('username')) {
            $islogin = ' <li><a href="'.base_url().'login/logout"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>';
        }

		$galeri = galeri_model::list_all($this->tableName)->result();
		$list = array();
		$i=0;
		foreach ($galeri as $glr) {
            $lsit[$i]['galeri_title'] = $glr->judul;
			$lsit[$i]['galeri_img'] = base_url().'assets/img/galeri/'.$glr->foto;
			$lsit[$i]['galeri_uploaded'] = $glr->tgl_upload;
        	$i++;
		}

		$pagedata = array(
			'title' => ' >> Galeri',
			'base_url' => base_url(),
			'title_menu' =>  'Galeri',
			'menu' =>  'Galeri',
			'islogin' => $islogin,
			'galeri' => $lsit
		);

        $this->parser->parse('galeri', $pagedata);
    }

}

?>

<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class adm_user extends CI_Controller {

    private $tableName;

    function __construct() {
        parent::__construct();
        $this->load->helper('formutil');
        $this->load->model('user_model', '', TRUE);
        $this->tableName = 'user';
    }

    function index() {
		if (!$this->session->userdata('username')) {
            redirect('login');
        }
		
		$islogin = ' <li><a href="'.base_url().'login/logout"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>'; 
		
        $config['base_url'] = site_url('adm_user/index/');
        $config['total_rows'] = user_model::count_all($this->tableName);
        $config['per_page'] = 10;
        $config['num_links'] = 5;
        $config['uri_segment'] = 3;
        $config['prev_link'] = '&lt;';
        $config['next_link'] = '&gt;';
        $config['first_link'] = '&laquo;';
        $config['last_link'] = '&raquo;';
        $config['cur_tag_open'] = '<span class="pa">';
        $config['cur_tag_close'] = '</span>';
        $config['full_tag_open'] = '<div class="pagging">';
        $config['full_tag_close'] = '</div>';
        $config['use_page_numbers'] = FALSE;
        
        $this->pagination->initialize($config);

        $offset = ($this->uri->segment(3) && preg_match("/[0-9]/",$this->uri->segment(3))) ? $this->uri->segment(3) : 0;
 
        $blks = user_model::get_paged_list($this->tableName,$config['per_page'], $offset)->result();
  
        $this->load->library('table');
		$tabletemp['table_open'] = '<table class="table table-condensed table-striped struktur_org">';
		$this->table->set_template($tabletemp);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading('NO', 'USER', 'HAK AKSES', 'STATUS', 'AKSI');
		
        $i = $offset;
		$a = $i+1;
        foreach ($blks as $blk) {
            $this->table->add_row(
					$a, 
					$blk->username, 
					$blk->hak_akses, 
					getStatus($blk->status), 
                    anchor('adm_user/detail/' . $blk->username, 'Detail', array('class' => 'btn btn-success btn-xs')).' '. 
					anchor('adm_user/edit/' . $blk->username, 'Edit', array('class' => 'btn btn-warning btn-xs')).' '.
					anchor('adm_user/delete/' . $blk->username, 'Hapus', array('class' => 'btn btn-danger btn-xs', 'onclick' =>"return confirm('apakah anda yakin ingin menghapus data ini?')"))  
			);
			$a++;
        }

		$add_btn = '<a class="btn btn-primary" href="'.base_url().'adm_user/add" type="button">
						<span class="glyphicon glyphicon-plus"></span>Tambah Data
				    </a>';

		$pagedata = array(
            'title' => 'Kelola Data User | Data User',
            'title_menu' => 'Kelola Data User',
			'menu' => 'Data Master',
			'islogin' => $islogin,
			'submenu' => 'Data User',
            'table' => $this->table->generate(),
            'pagination' => $this->pagination->create_links(),
            'add_btn' => $add_btn,
			'print_btn' => '',
			'message' => $this->session->flashdata('message')
        	);

        $this->parser->parse('main_adm', $pagedata);
    }

	function add(){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }
		
		$this->_set_fields();
        $this->_set_rules();
			
		$save_btn 	= '<button type="submit" name="button" class="btn btn-primary"><span class="glyphicon glyphicon-remove"></span>&nbsp;&nbsp; Simpan Data &nbsp;&nbsp;</button>';
		$back_btn	= '<button type="button" name="button" class="btn btn-primary" onclick="location.href=\''.base_url().'adm_user\';"><span class="glyphicon glyphicon-remove"></span>&nbsp;&nbsp; Kembali &nbsp;&nbsp;</button>';
			
		$contentVars = array(
			'message' => $this->session->flashdata('message'),
			'base_url' => base_url(),
			'action' => site_url('adm_user/doAdd'),
			'combo_role' => combo_role('role', '', 'class="form-control input-sm"'),
			'radio_status' => radio_status('status'),
			'disable' => '',
			'save_btn' => $save_btn,
			'back_btn' => $back_btn
		);
			
		$pagedata = array(
            'title' => 'Kelola Data User | Detail Data User',
            'title_menu' => 'Kelola Data User',
			'menu' => 'Data Master',
			'submenu' => 'Data User',
			'content' => $this->parser->parse('adm_user_form', $contentVars, TRUE)
        );
		
		 $this->parser->parse('main_adm_form', $pagedata);
	}
	
	
	function doAdd(){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }
		
		$this->_set_fields();
        $this->_set_rules();
		
		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('message', 'Field tidak boleh kosong');
            redirect('adm_user/add');
        } else {

            $user = array(
                'username' => $this->input->post('username'),
                'password' => $this->input->post('password'),
                'status' => $this->input->post('status'),
                'hak_akses' => $this->input->post('role')
            );
             
            $doAdd = user_model::save($this->tableName, $user);
	
            if($doAdd){
				$this->session->set_flashdata('message', 'User berhasil di tambahkan');
                redirect('adm_user');
            }else{
				$this->session->set_flashdata('message', 'sedang ada pemeliharaan sistem');
				redirect('adm_user/add');
            }
            
        }
	}
	
	
	function detail($id) {
		if (!$this->session->userdata('username')) {
            redirect('login');
        }
		
        $this->_set_rules();

        $data = user_model::get_by_username($this->tableName,$id);
		
		$this->form_data = new stdClass;
        $this->form_data->nip = $data->nip;
        $this->form_data->username = $data->username;
		$this->form_data->password = $data->password;
		
		$back_btn	= '<button type="button" name="button" class="btn btn-primary" onclick="location.href=\''.base_url().'adm_user\';"><span class="glyphicon glyphicon-remove"></span>&nbsp;&nbsp; Kembali &nbsp;&nbsp;</button>';
		
		$contentVars = array(
			'message' => '',
			'base_url' => base_url(),
			'combo_role' => input_text('role', $data->hak_akses, 'class="form-control input-sm" disabled=""'),
			'radio_status' => input_text('role', getStatus($data->status), 'class="form-control input-sm" disabled=""'),
			'disable' => 'disabled=""',
			'save_btn' => '',
			'back_btn' => $back_btn
		);
			
		$pagedata = array(
            'title' => 'Kelola Data User | Detail Data User',
            'title_menu' => 'Kelola Data User',
			'menu' => 'Data Master',
			'submenu' => 'Data User',
			'content' => $this->parser->parse('adm_user_form', $contentVars, TRUE)
        );
		
		 $this->parser->parse('main_adm_form', $pagedata);
    }

	function edit($id) {
		if (!$this->session->userdata('username')) {
            redirect('login');
        }
		
        $this->_set_rules();

		$save_btn 	= '<button type="submit" name="button" class="btn btn-primary"><span class="glyphicon glyphicon-remove"></span>&nbsp;&nbsp; Simpan Data &nbsp;&nbsp;</button>';
		$back_btn	= '<button type="button" name="button" class="btn btn-primary" onclick="location.href=\''.base_url().'adm_user\';"><span class="glyphicon glyphicon-remove"></span>&nbsp;&nbsp; Kembali &nbsp;&nbsp;</button>';

        $data = user_model::get_by_username($this->tableName,$id);
		
		$this->form_data = new stdClass;
        $this->form_data->nip = $data->nip;
        $this->form_data->username = $data->username;
		$this->form_data->password = $data->password;
		
		$contentVars = array(
			'message' => $this->session->flashdata('message'),
			'action' => site_url('adm_user/doEdit'),
			'base_url' => base_url(),
			'combo_role' => combo_role('role',  $data->hak_akses, 'class="form-control input-sm"'),
			'radio_status' => radio_status('status',  $data->status),
			'disable' => '',
			'save_btn' => $save_btn,
			'back_btn' => $back_btn
		);
			
		$pagedata = array(
            'title' => 'Kelola Data User | Detail Data User',
            'title_menu' => 'Kelola Data User',
			'menu' => 'Data Master',
			'submenu' => 'Data User',
			'content' => $this->parser->parse('adm_user_form', $contentVars, TRUE)
        );
		
		 $this->parser->parse('main_adm_form', $pagedata);
    }
	
	
	function doEdit(){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }
		
		$this->_set_fields();
        $this->_set_rules();
		
		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('message', 'Field tidak boleh kosong');
            redirect('adm_user/edit');
        } else {

            $user = array(
                'username' => $this->input->post('username'),
                'password' => $this->input->post('password'),
                'status' => $this->input->post('status'),
                'hak_akses' => $this->input->post('role')
            );
             
            $doUpdate = user_model::update_by_user($this->tableName, $user, $this->input->post('username'));

			if($doUpdate){
				$this->session->set_flashdata('message', 'Data berhasil di ubah');
                redirect('adm_user');
            }else{
				$this->session->set_flashdata('message', 'sedang ada pemeliharaan sistem');
				redirect('adm_user/edit');
            }
            
        }
	}

	function delete($id){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }
		
		user_model::delete_by_user($this->tableName, $id);
		$this->session->set_flashdata('message', 'Data berhasi dihapus');
        redirect('adm_user');		
	}

    function _set_fields() {
		$this->form_data = new stdClass;
		$this->form_data->nip = '';
        $this->form_data->username = '';
        $this->form_data->password = '';
    }

    function _set_rules() {
        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

        $this->form_validation->set_error_delimiters('<span class="error">', '</span>');
    }

}

?>

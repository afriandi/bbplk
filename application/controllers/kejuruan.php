<?php

class kejuruan extends CI_Controller {

    private $tableName ;

    function __construct() {
    parent::__construct();
    $this->load->helper('formutil');
    $this->load->model('kejuruan_model', '', TRUE);
		$this->load->model('pelatihan_model', '', TRUE);
		$this->load->model('asesmen_model', '', TRUE);
		$this->load->model('instruktur_model', '', TRUE);
		$this->load->model('fasilitas_model', '', TRUE);
        $this->tableName = 'kejuruan';
    }

	function index($id){
		redirect("kejuruan/profil/".$id);
	}

    function profil($id) {
        $islogin = '<li><a href="'.base_url().'login"><span class="glyphicon glyphicon-lock"></span>Login</a></li>';
		if ($this->session->userdata('username')) {
            $islogin = ' <li><a href="'.base_url().'login/logout"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>';
        }

		$kejuruan = kejuruan_model::get_by_id($this->tableName, $id);

		$contentVars = array(
            'kejuruan_nama' => $kejuruan->nama,
            'kejuruan_desc' => $kejuruan->deskripsi
        );

		$kejur	= getKejuruan($id);

		$pagedata = array(
			'title' => ' >> Profil Kejuruan  ',
      'title_menu' => 'Profil Kejuruan '.$kejur,
			'menu' => 'Kejuruan',
			'submenu' => 'Profil',
			'islogin' => $islogin,
			'kejuruan_id' => $id,
			'content' => $this->parser->parse('kejuruan/kejuruan_profil', $contentVars, TRUE)
		);

        $this->parser->parse('main_kejuruan', $pagedata);
    }


	function pelatihan($id) {
    $islogin = '<li><a href="'.base_url().'login"><span class="glyphicon glyphicon-lock"></span>Login</a></li>';
		if ($this->session->userdata('username')) {
        $islogin = ' <li><a href="'.base_url().'login/logout"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>';
    }

		$table 		= 'detail_pelatihan';
    $jumlah_data = $this->pelatihan_model->count_pelatihan($table,$id);
    $this->load->library('pagination');
    $config['base_url'] = site_url().'/kejuruan/pelatihan/'.$id;
    $config['total_rows'] = $jumlah_data;
    $config['per_page'] = 6;
    $config['uri_segment'] = 4;
       $config['use_page_numbers'] = true;
       $config['full_tag_open']    = '<ul class="pagination pagination-sm">';
       $config['full_tag_close']   = '</ul>';
       $config['first_link']       = 'First';
       $config['last_link']        = 'Last';
       $config['first_tag_open']   = '<li>';
       $config['first_tag_close']  = '</li>';
       $config['prev_link']        = '&laquo';
       $config['prev_tag_open']    = '<li class="prev">';
       $config['prev_tag_close']   = '</li>';
       $config['next_link']        = '&raquo';
       $config['next_tag_open']    = '<li>';
       $config['next_tag_close']   = '</li>';
       $config['last_tag_open']    = '<li>';
       $config['last_tag_close']   = '</li>';
       $config['cur_tag_open']     = '<li class="active"><a href="">';
       $config['cur_tag_close']    = '</a></li>';
       $config['num_tag_open']     = '<li>';
       $config['num_tag_close']    = '</li>';
    if($this->uri->segment(4)=="" || $this->uri->segment(4)<1){
      $from=0;
    }else{
      $from=($this->uri->segment(4)-1)*$config['per_page'];
    }
    $this->pagination->initialize($config);

    $pelatihan 	= pelatihan_model::get_by_kejuruan($table,$id,$config['per_page'],$from)->result();

		$list = array();
		$i=0;
		foreach ($pelatihan as $brt) {
			$list[$i]['pelatihan_id'] = $brt->id_pelatihan;
			$list[$i]['pelatihan_detail_id'] = $brt->id_detail_pelatihan;
      $list[$i]['pelatihan_jp'] = $brt->jml_jp;
      $list[$i]['pelatihan_title'] = $brt->program_pelatihan;
  		$i++;
		}
		$contentVars['pelatihan'] = $list;
    $contentVars['kejuruan_id'] = $id;
		$contentVars['pagination'] = $this->pagination->create_links();

		$kejur	= getKejuruan($id);

		$pagedata = array(
			'title' => ' >> Pelatihan Kejuruan  ',
      'title_menu' => 'Pelatihan Kejuruan '.$kejur,
			'menu' => 'Kejuruan',
			'submenu' => 'Pelatihan',
			'kejuruan_id' => $id,
			'islogin' => $islogin,
			'content' => $this->parser->parse('kejuruan/kejuruan_pelatihan', $contentVars, TRUE)
		);

        $this->parser->parse('main_kejuruan', $pagedata);
    }


	function detail_pelatihan($id) {
        $islogin = '<li><a href="'.base_url().'login"><span class="glyphicon glyphicon-lock"></span>Login</a></li>';
		if ($this->session->userdata('username')) {
            $islogin = ' <li><a href="'.base_url().'login/logout"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>';
        }

		$table 		= 'detail_pelatihan';
		$detail_pelatihan 	= pelatihan_model::get_by_id($table, $id);

		$contentVars = array(
      'id_detail_pelatihan' => $detail_pelatihan->id_detail_pelatihan,
      'id_pelatihan' => $detail_pelatihan->id_pelatihan,
      'program_pelatihan' => $detail_pelatihan->program_pelatihan,
      'jml_jp' => $detail_pelatihan->jml_jp,
      'id_kejuruan' => $detail_pelatihan->id_kejuruan,
      'nama' => $detail_pelatihan->nama,
      'tahun' => $detail_pelatihan->tahun,
      'urutan' => $detail_pelatihan->urutan,
      'tahap' => $detail_pelatihan->tahap,
      'status' => $detail_pelatihan->status,
      'asrama' => $detail_pelatihan->asrama,
      'tgl_pendaftaran_mulai' => $detail_pelatihan->tgl_pendaftaran_mulai,
      'tgl_pendaftaran_selesai' => $detail_pelatihan->tgl_pendaftaran_selesai,
      'tgl_rekrutmen' => $detail_pelatihan->tgl_rekrutmen,
      'tgl_pengumuman' => $detail_pelatihan->tgl_pengumuman,
      'tgl_daftar_ulang' => $detail_pelatihan->tgl_daftar_ulang,
      'tgl_mulai' => $detail_pelatihan->tgl_mulai,
      'tgl_selesai' => $detail_pelatihan->tgl_selesai,
      'tgl_ujk' => $detail_pelatihan->tgl_ujk
    );

		$kejur	= getKejuruan($detail_pelatihan->id_kejuruan);

		$pagedata = array(
			'title' => ' >> Pelatihan Kejuruan  ',
      'title_menu' => 'Pelatihan Kejuruan '.$kejur,
			'menu' => 'Kejuruan',
			'submenu' => 'Pelatihan',
			'islogin' => $islogin,
			'kejuruan_id' => $detail_pelatihan->id_kejuruan,
			'content' => $this->parser->parse('kejuruan/kejuruan_pelatihan_detil', $contentVars, TRUE)
		);

        $this->parser->parse('main_kejuruan', $pagedata);
    }


	function sertifikasi($id){
		$table = 'asesmen';
		$islogin = '<li><a href="'.base_url().'login"><span class="glyphicon glyphicon-lock"></span>Login</a></li>';
		if ($this->session->userdata('username')) {
            $islogin = ' <li><a href="'.base_url().'login/logout"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>';
        }
        $jumlah_data = $this->asesmen_model->count_asesmen($table,$id);
        $this->load->library('pagination');
        $config['base_url'] = site_url().'/kejuruan/sertifikasi/'.$id;
        $config['total_rows'] = $jumlah_data;
        $config['per_page'] = 5;
        $config['uri_segment'] = 4;
           $config['use_page_numbers'] = true;
           $config['full_tag_open']    = '<ul class="pagination pagination-sm">';
           $config['full_tag_close']   = '</ul>';
           $config['first_link']       = 'First';
           $config['last_link']        = 'Last';
           $config['first_tag_open']   = '<li>';
           $config['first_tag_close']  = '</li>';
           $config['prev_link']        = '&laquo';
           $config['prev_tag_open']    = '<li class="prev">';
           $config['prev_tag_close']   = '</li>';
           $config['next_link']        = '&raquo';
           $config['next_tag_open']    = '<li>';
           $config['next_tag_close']   = '</li>';
           $config['last_tag_open']    = '<li>';
           $config['last_tag_close']   = '</li>';
           $config['cur_tag_open']     = '<li class="active"><a href="">';
           $config['cur_tag_close']    = '</a></li>';
           $config['num_tag_open']     = '<li>';
           $config['num_tag_close']    = '</li>';
        if($this->uri->segment(4)=="" || $this->uri->segment(4)<1){
          $from=0;
        }else{
          $from=($this->uri->segment(4)-1)*$config['per_page'];
        }
        $this->pagination->initialize($config);

        $blks = asesmen_model::get_by_kejuruan($table,$id,$config['per_page'],$from)->result();
//        $instruktur	= instruktur_model::get_by_kejuruan($table,$id,$config['per_page'],$from)->result();

        $this->load->library('table');
		    $tabletemp['table_open'] = '<table class="table table-striped table-bordered">';
		    $this->table->set_template($tabletemp);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading('NO', 'SKEMA', 'KETERANGAN', 'DETAIL');

        //$i = $offset;
	      $a =1;
        foreach ($blks as $blk) {
          $this->table->add_row(
  					$a,
  					$blk->judul_skema,
  					$blk->deskripsi,
  					'<a href="'.base_url().'kejuruan/detail_sertifikasi/'.$blk->id_asesmen.'">
  						<button class="btn btn-xs btn-primary" type="button" name="button"> Detail</button>
  					</a>'
          );
  			  $a++;
        }

		$contentVars = array(
			'base_url' =>  base_url(),
			'table' => $this->table->generate(),
      'pagination' => $this->pagination->create_links()
		);

		$kejur	= getKejuruan($id);

		$pagedata = array(
			'title' => ' >> Sertifikasi Kejuruan  ',
      'title_menu' => 'Sertifikasi Kejuruan '.$kejur,
			'menu' => 'Kejuruan',
			'submenu' => 'Sertifikasi',
			'islogin' => $islogin,
			'kejuruan_id' => $id,
			'content' => $this->parser->parse('kejuruan/kejuruan_sertifikasi', $contentVars, TRUE)
		);

    $this->parser->parse('main_kejuruan', $pagedata);
	}

	function detail_sertifikasi($id) {
        $islogin = '<li><a href="'.base_url().'login"><span class="glyphicon glyphicon-lock"></span>Login</a></li>';
		if ($this->session->userdata('username')) {
            $islogin = ' <li><a href="'.base_url().'login/logout"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>';
        }

		$table 			= 'asesmen';
		$sertifikasi 	= asesmen_model::get_by_id($table, $id);

		$contentVars['sertifikasi_title'] = $sertifikasi->judul_skema;
		$contentVars['sertifikasi_no_skema'] = $sertifikasi->no_skema;
		$contentVars['sertifikasi_desc'] = $sertifikasi->deskripsi;

		$kejur	= getKejuruan($sertifikasi->id_kejuruan);

		$pagedata = array(
			'title' => ' >> Pelatihan Kejuruan  ',
            'title_menu' => 'Pelatihan Kejuruan '.$kejur,
			'menu' => 'Kejuruan',
			'submenu' => 'Pelatihan',
			'islogin' => $islogin,
			'kejuruan_id' => $sertifikasi->id_kejuruan,
			'content' => $this->parser->parse('kejuruan/kejuruan_sertifikasi_detil', $contentVars, TRUE)
		);

        $this->parser->parse('main_kejuruan', $pagedata);
    }

	function instruktur($id) {
    $islogin = '<li><a href="'.base_url().'login"><span class="glyphicon glyphicon-lock"></span>Login</a></li>';
		if ($this->session->userdata('username')) {
            $islogin = ' <li><a href="'.base_url().'login/logout"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>';
        }

		$table 		= 'instruktur';
    $jumlah_data = $this->instruktur_model->count_instruktur($table,$id);
    $this->load->library('pagination');
    $config['base_url'] = site_url().'/kejuruan/instruktur/'.$id;
    $config['total_rows'] = $jumlah_data;
    $config['per_page'] = 9;
    $config['uri_segment'] = 4;
       $config['use_page_numbers'] = true;
       $config['full_tag_open']    = '<ul class="pagination pagination-sm">';
       $config['full_tag_close']   = '</ul>';
       $config['first_link']       = 'First';
       $config['last_link']        = 'Last';
       $config['first_tag_open']   = '<li>';
       $config['first_tag_close']  = '</li>';
       $config['prev_link']        = '&laquo';
       $config['prev_tag_open']    = '<li class="prev">';
       $config['prev_tag_close']   = '</li>';
       $config['next_link']        = '&raquo';
       $config['next_tag_open']    = '<li>';
       $config['next_tag_close']   = '</li>';
       $config['last_tag_open']    = '<li>';
       $config['last_tag_close']   = '</li>';
       $config['cur_tag_open']     = '<li class="active"><a href="">';
       $config['cur_tag_close']    = '</a></li>';
       $config['num_tag_open']     = '<li>';
       $config['num_tag_close']    = '</li>';
    if($this->uri->segment(4)=="" || $this->uri->segment(4)<1){
      $from=0;
    }else{
      $from=($this->uri->segment(4)-1)*$config['per_page'];
    }
    $this->pagination->initialize($config);

    $instruktur	= instruktur_model::get_by_kejuruan($table,$id,$config['per_page'],$from)->result();

		$list = array();
		$i=0;
		foreach ($instruktur as $ins) {
			$list[$i]['instruktur_nip'] = $ins->nip;
      $list[$i]['instruktur_name'] = $ins->nama;
			$list[$i]['instruktur_desc'] = $ins->keterangan;
			$list[$i]['instruktur_fb'] = $ins->facebook;
			$list[$i]['instruktur_twitter'] = $ins->twitter;
			$list[$i]['instruktur_linkedin'] = $ins->linkedin;
			$list[$i]['instruktur_img'] = base_url().'assets/img/pegawai/'.$ins->foto;
			$i++;
		}
		$contentVars['instruktur'] = $list;
    $contentVars['kejuruan_id'] = $id;
		$contentVars['pagination'] = $this->pagination->create_links();

		$kejur	= getKejuruan($id);

		$pagedata = array(
			'title' => ' >> Instruktur Kejuruan  ',
            'title_menu' => 'Instruktur Kejuruan '.$kejur,
			'menu' => 'Kejuruan',
			'submenu' => 'Instruktur',
			'kejuruan_id' => $id,
			'islogin' => $islogin,
			'content' => $this->parser->parse('kejuruan/kejuruan_instruktur', $contentVars, TRUE)
		);

        $this->parser->parse('main_kejuruan', $pagedata);
    }


	function fasilitas($id) {
    $islogin = '<li><a href="'.base_url().'login"><span class="glyphicon glyphicon-lock"></span>Login</a></li>';
		if ($this->session->userdata('username')) {
            $islogin = ' <li><a href="'.base_url().'login/logout"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>';
        }

		$table 		= 'fasilitas';
    $jumlah_data = $this->fasilitas_model->count_fasilitas_umum($table,$id);
    $this->load->library('pagination');
    $config['base_url'] = site_url().'/kejuruan/fasilitas/'.$id;
    $config['total_rows'] = $jumlah_data;
    $config['per_page'] = 5;
    $config['uri_segment'] = 4;
       $config['use_page_numbers'] = true;
       $config['full_tag_open']    = '<ul class="pagination pagination-sm">';
       $config['full_tag_close']   = '</ul>';
       $config['first_link']       = 'First';
       $config['last_link']        = 'Last';
       $config['first_tag_open']   = '<li>';
       $config['first_tag_close']  = '</li>';
       $config['prev_link']        = '&laquo';
       $config['prev_tag_open']    = '<li class="prev">';
       $config['prev_tag_close']   = '</li>';
       $config['next_link']        = '&raquo';
       $config['next_tag_open']    = '<li>';
       $config['next_tag_close']   = '</li>';
       $config['last_tag_open']    = '<li>';
       $config['last_tag_close']   = '</li>';
       $config['cur_tag_open']     = '<li class="active"><a href="">';
       $config['cur_tag_close']    = '</a></li>';
       $config['num_tag_open']     = '<li>';
       $config['num_tag_close']    = '</li>';
    if($this->uri->segment(4)=="" || $this->uri->segment(4)<1){
      $from=0;
    }else{
      $from=($this->uri->segment(4)-1)*$config['per_page'];
    }
    $this->pagination->initialize($config);

    $fasilitas	= fasilitas_model::get_by_kepemilikan($table,$id,$config['per_page'],$from)->result();

		$list = array();
		$i=0;
		foreach ($fasilitas as $ins) {
			$list[$i]['fasilitas_milik'] = $ins->kepemilikan;
            $list[$i]['fasilitas_title'] = $ins->judul;
			$list[$i]['fasilitas_desc'] = $ins->deskripsi;
			$list[$i]['fasilitas_img'] = base_url().'assets/img/fasilitas/'.$ins->foto;
			$list[$i]['fasilitas_detil'] = base_url().'kejuruan/detail_fasilitas/'.$ins->id_fasilitas;
			$i++;
		}
		$contentVars['fasilitas'] = $list;
		$contentVars['kejuruan_id'] = $id;
    $contentVars['pagination'] = $this->pagination->create_links();

		$kejur	= getKejuruan($id);

		$pagedata = array(
			'title' => ' >> Fasilitas Kejuruan  ',
      'title_menu' => 'Fasilitas Kejuruan '.$kejur,
			'menu' => 'Kejuruan',
			'submenu' => 'Fasilitas',
			'kejuruan_id' => $id,
			'islogin' => $islogin,
			'content' => $this->parser->parse('kejuruan/kejuruan_fasilitas', $contentVars, TRUE)
		);

    $this->parser->parse('main_kejuruan', $pagedata);
    }


	function detail_fasilitas($id) {
        $islogin = '<li><a href="'.base_url().'login"><span class="glyphicon glyphicon-lock"></span>Login</a></li>';
		if ($this->session->userdata('username')) {
            $islogin = ' <li><a href="'.base_url().'login/logout"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>';
        }

		$table 			= 'fasilitas';
		$fasilitas 	= fasilitas_model::get_by_id($table, $id);

		$kejur	= getKejuruan($fasilitas->kepemilikan);
		$contentVars['fasilitas_title'] = $fasilitas->judul;
		$contentVars['fasilitas_img'] = base_url().'assets/img/fasilitas/'.$fasilitas->foto;
		$contentVars['fasilitas_desc'] = $fasilitas->deskripsi;
		$contentVars['fasilitas_milik'] = $kejur;

		$pagedata = array(
			'title' => ' >> Pelatihan Kejuruan  ',
      'title_menu' => 'Pelatihan Kejuruan '.$kejur,
			'menu' => 'Kejuruan',
			'submenu' => 'Fasilitas',
			'islogin' => $islogin,
			'kejuruan_id' => $fasilitas->kepemilikan,
			'content' => $this->parser->parse('kejuruan/kejuruan_fasilitas_detil', $contentVars, TRUE)
		);

        $this->parser->parse('main_kejuruan', $pagedata);
    }

}

?>

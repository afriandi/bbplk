<?php
class Pengumuman extends CI_Controller {

    private $tableName ;

    function __construct() {
        parent::__construct();
        $this->load->helper(array('url'));
        $this->load->model('pengumuman_model', '', TRUE);
        $this->tableName = 'pengumuman';
    }

	  function index() {
		$islogin = '<li><a href="'.base_url().'login"><span class="glyphicon glyphicon-lock"></span>Login</a></li>';
		if ($this->session->userdata('username')) {
        $islogin = ' <li><a href="'.base_url().'login/logout"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>';
    }

		$contentVars = array(
			'base_url' =>  base_url(),
			'title_menu' =>  'Pengumuman',
			'menu' => 'Pengumuman'
		);

    $jumlah_data = $this->pengumuman_model->count_all($this->tableName);
		$this->load->library('pagination');
		$config['base_url'] = site_url().'/pengumuman/index';
		$config['total_rows'] = $jumlah_data;
		$config['per_page'] = 5;
		$config['uri_segment'] = 3;
       $config['use_page_numbers'] = true;
       $config['full_tag_open']    = '<ul class="pagination pagination-sm">';
       $config['full_tag_close']   = '</ul>';
       $config['first_link']       = 'First';
       $config['last_link']        = 'Last';
       $config['first_tag_open']   = '<li>';
       $config['first_tag_close']  = '</li>';
       $config['prev_link']        = '&laquo';
       $config['prev_tag_open']    = '<li class="prev">';
       $config['prev_tag_close']   = '</li>';
       $config['next_link']        = '&raquo';
       $config['next_tag_open']    = '<li>';
       $config['next_tag_close']   = '</li>';
       $config['last_tag_open']    = '<li>';
       $config['last_tag_close']   = '</li>';
       $config['cur_tag_open']     = '<li class="active"><a href="">';
       $config['cur_tag_close']    = '</a></li>';
       $config['num_tag_open']     = '<li>';
       $config['num_tag_close']    = '</li>';
		if($this->uri->segment(3)=="" || $this->uri->segment(3)<1){
      $from=0;
    }else{
      $from=($this->uri->segment(3)-1)*$config['per_page'];
    }
    $this->pagination->initialize($config);
    $contentVars['pagination'] = $this->pagination->create_links();
		$beritas = $this->pengumuman_model->get_all($this->tableName,$config['per_page'],$from);
		$list = array();
		$i=0;
		foreach ($beritas as $brt) {
      $lsit[$i]['pengumuman_title'] = $brt->judul;
			$lsit[$i]['pengumuman_date'] = date("d F y", strtotime($brt->tanggal));
			$lsit[$i]['pengumuman_img'] = base_url().'assets/img/pengumuman/'.$brt->gambar;
			$lsit[$i]['pengumuman_desc'] = $brt->deskripsi;
      $isi_pengumuman = $lsit[$i]['pengumuman_desc'];
      $isi_pengumuman = substr($isi_pengumuman,0,155)." ...";
      $lsit[$i]['isi_pengumuman'] = $isi_pengumuman;
			$lsit[$i]['pengumuman_detail'] = base_url().'pengumuman/detail/'.$brt->id_pengumuman;
      $i++;
    }
		$contentVars['pengumuman'] = $lsit;

  	$pagedata = array(
			'title' => ' >> Berita',
			'islogin' => $islogin,
			'content' => $this->parser->parse('pengumuman/pengumuman', $contentVars, TRUE)
		);

    $this->parser->parse('main', $pagedata);
    }

	function detail($id) {

    $islogin = '<li><a href="'.base_url().'login"><span class="glyphicon glyphicon-lock"></span>Login</a></li>';
		if ($this->session->userdata('username')) {
      $islogin = ' <li><a href="'.base_url().'login/logout"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>';
    }

		$pengumuman = pengumuman_model::get_by_id($this->tableName, $id);

		$contentVars = array(
			'base_url' =>  base_url(),
			'title_menu' =>  'Pengumuman',
			'menu' =>  'Pengumuman',
			'pengumuman_title' => $pengumuman->judul,
			'pengumuman_date' => date("d F y", strtotime($pengumuman->tanggal)),
			'pengumuman_img' => base_url().'assets/img/pengumuman/'.$pengumuman->gambar,
			'pengumuman_desc' => $pengumuman->deskripsi,
			'pengumuman_id' => $id
		);

		$pagedata = array(
			'title' => ' >> Pengumuman',
			'islogin' => $islogin,
			'content' => $this->parser->parse('pengumuman/pengumuman_detil', $contentVars, TRUE)
		);

        $this->parser->parse('main', $pagedata);
    }

}
?>

<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Rekap_pelatihan extends CI_Controller {

	  function __construct() {
        parent::__construct();
        $this->load->helper('simple_dom');
//				$this->load->library('dompdf_gen');
				$this->load->model('pelatihan_model');
    }

    public function index(){
        {
        $islogin = '<li><a href="'.base_url().'login"><span class="glyphicon glyphicon-lock"></span>Login</a></li>';
        if ($this->session->userdata('username')) {
            $islogin = ' <li><a href="'.base_url().'login/logout"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>';
        }

        $tableName = "detail_pelatihan";
        $id_oto = "oto";
        $id_tekman = "tekman";
        $total_oto = $this->pelatihan_model->count_pelatihan($tableName,$id_oto);
        $total_tekman = $this->pelatihan_model->count_pelatihan($tableName,$id_tekman);
        $total_siswa_oto = $total_oto * 16;
        $total_siswa_tekman = $total_tekman * 16;
    		$total_program = $total_oto + $total_tekman;
    		$total_siswa = $total_program * 16;
//echo base_url();
        $contentVars = array(
          'base_url' => base_url(),
          'title_menu' => 'Rekap Pelatihan',
          'menu' => 'Kelola Laporan',
          'submenu' =>  'Rekap Pelatihan',
          'total_oto'=> $total_oto,
          'total_tekman'=> $total_tekman,
          'total_siswa_oto'=> $total_siswa_oto,
          'total_siswa_tekman'=> $total_siswa_tekman,
          'total_program'=> $total_program,
          'total_siswa'=> $total_siswa,
        );
        $pagedata = array(
          'title' => ' >> Rekap Pelatihan',
          'islogin' => $islogin,
          'content' => $this->parser->parse('manage/laporan/rekap_pelatihan', $contentVars, TRUE)
        );
        $this->parser->parse('manage/main_rekap', $pagedata);
    }
  }

	public function cetak(){
//		$this->load->view('manage/laporan/rekap_pelatihan');
	  // dapatkan output html

//	  $html = $this->output->get_output();

	  // Load/panggil library dompdfnya
//	  $this->load->library('dompdf_gen');

	  // Convert to PDF
//	  $this->dompdf->load_html($html);
//	  $this->dompdf->render();
	  //utk menampilkan preview pdf
//	  $this->dompdf->stream("rekap_pelatihan.pdf",array('Attachment'=>0));
	  //atau jika tidak ingin menampilkan (tanpa) preview di halaman browser
	  //$this->dompdf->stream("welcome.pdf");

	}
}
?>

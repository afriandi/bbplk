<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class instruktur extends CI_Controller {

    private $tableName;
	private $base;

    function __construct() {
        parent::__construct();
        $this->load->helper('formutil');
        $this->load->model('instruktur_model', '', TRUE);
		$this->load->model('pegawai_model', '', TRUE);
        $this->tableName = 'instruktur';
		$this->base = 'manage/instruktur/';
		if ($this->session->userdata('currentmenu') != 'Input') {
             $this->session->set_userdata('currentmenu', 'Input');
        }
    }

    function index() {
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		$islogin = ' <li><a href="'.base_url().'login/logout"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>';

        $config['base_url'] = site_url('manage/instruktur/index/');
        $config['total_rows'] = instruktur_model::count_all($this->tableName);
        $config['per_page'] = 10;
        $config['num_links'] = 5;
        $config['uri_segment'] = 4;
        $config['use_page_numbers'] = true;
        $config['full_tag_open']    = '<ul class="pagination pagination-sm">';
        $config['full_tag_close']   = '</ul>';
        $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['first_tag_open']   = '<li>';
        $config['first_tag_close']  = '</li>';
        $config['prev_link']        = '&laquo';
        $config['prev_tag_open']    = '<li class="prev">';
        $config['prev_tag_close']   = '</li>';
        $config['next_link']        = '&raquo';
        $config['next_tag_open']    = '<li>';
        $config['next_tag_close']   = '</li>';
        $config['last_tag_open']    = '<li>';
        $config['last_tag_close']   = '</li>';
        $config['cur_tag_open']     = '<li class="active"><a href="">';
        $config['cur_tag_close']    = '</a></li>';
        $config['num_tag_open']     = '<li>';
        $config['num_tag_close']    = '</li>';
		$config['use_page_numbers'] = FALSE;

        $this->pagination->initialize($config);

        $offset = ($this->uri->segment(4) && preg_match("/[0-9]/",$this->uri->segment(4))) ? $this->uri->segment(4) : 0;

        $data = instruktur_model::get_paged_list_all($this->tableName,$config['per_page'], $offset)->result();

        $this->load->library('table');
		$tabletemp['table_open'] = '<table class="table table-condensed table-striped struktur_org">';
		$this->table->set_template($tabletemp);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading('NO', 'NAMA', 'NIP', 'KEJURUAN', '');

        $i = $offset;
		$a = $i+1;
        foreach ($data as $dt) {
            $this->table->add_row(
					$a,
					$dt->nama,
					$dt->nip,
					$dt->kejuruan,
          anchor($this->base.'detail/' . $dt->id_instruktur, 'Detail', array('class' => 'btn btn-success btn-xs')).' '.
					anchor($this->base.'edit/' . $dt->id_instruktur, 'Edit', array('class' => 'btn btn-warning btn-xs')).' '.
					anchor($this->base.'delete/' . $dt->id_instruktur, 'Hapus', array('class' => 'btn btn-danger btn-xs', 'onclick' =>"return confirm('apakah anda yakin ingin menghapus data ini?')"))
			);
			$a++;
        }

		$add_btn = '<a class="btn btn-primary" href="'.base_url().'manage/instruktur/add" type="button">
						<span class="glyphicon glyphicon-plus"></span>Tambah Data
				    </a>';

		$pagedata = array(
      'title' => 'Kelola Data Instruktur | Data Instruktur',
      'title_menu' => 'Kelola Data Instruktur',
			'menu' => 'Data Master',
			'islogin' => $islogin,
			'submenu' => 'Data Instruktur',
      'table' => $this->table->generate(),
      'pagination' => $this->pagination->create_links(),
      'add_btn' => $add_btn,
			'print_btn' => '',
			'message' => $this->session->flashdata('message')
        	);

        $this->parser->parse('manage/main_adm', $pagedata);
    }

	function add(){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		$this->_set_fields();
        $this->_set_rules();

		$contentVars = array(
			'message' => $this->session->flashdata('message'),
			'base_url' => base_url(),
			'action' => site_url('manage/instruktur/doAdd'),
			'combo_kejuruan' => combo_kejuruan('kejuruan', '', 'class="form-control"'),
			'datalist_pegawai' => datalist_pegawai('dtpeg'),
			'back_link' => base_url().'manage/instruktur'
		);

		$pagedata = array(
            'title' => 'Kelola Data Instruktur | Tambah Data Instruktur',
            'title_menu' => 'Kelola Data Instruktur',
			'menu' => 'Data Master',
			'submenu' => 'Tambah Instruktur',
			'content' => $this->parser->parse('manage/instruktur/instruktur_form', $contentVars, TRUE)
        );

		$this->parser->parse('manage/main_adm_form', $pagedata);
	}


	function doAdd(){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		$this->_set_fields();
        $this->_set_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('message', 'Field tidak boleh kosong');
            redirect('manage/instruktur/add');
        } else {

			$data = array(
					'nip' => $this->input->post('nip'),
					'id_kejuruan' => $this->input->post('kejuruan'),
					'keterangan' => $this->input->post('desc'),
				);

			$doAdd = instruktur_model::save($this->tableName, $data);

			if($doAdd){
				$this->session->set_flashdata('message', 'Data berhasil di tambahkan');
				redirect('manage/instruktur');
			}else{
				$this->session->set_flashdata('message', 'sedang ada pemeliharaan sistem');
				redirect('manage/instruktur/add');
			}
        }
	}


	function detail($id) {
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

        $this->_set_rules();

        $data = instruktur_model::get_by_id_detail($this->tableName,$id);
		$this->form_data = new stdClass;
    $this->form_data->id = $data->id_instruktur;
		$this->form_data->desc = $data->keterangan;
		$this->form_data->nip = $data->nip;


		$peg = pegawai_model::get_by_id('pegawai',$data->nip);
		$this->form_data->nama = $peg->nama;
		$this->form_data->tempat_lahir = $peg->tempat_lahir;
		$this->form_data->tgl_lahir = $peg->tgl_lahir;
		$this->form_data->tmt = $peg->tmt;

		$contentVars = array(
			'message' => $this->session->flashdata('message'),
			'base_url' => base_url(),
			'action' => site_url('manage/instruktur/doeEdit'),
			'combo_kejuruan' => combo_kejuruan('kejuruan', $data->id_kejuruan, 'class="form-control" disabled'),
			'back_link' => base_url().'manage/instruktur',
			'photo' => base_url().'assets/img/pegawai/'.$peg->foto,
			'combo_jabatan' => combo_jabatan('jabatan', $peg->id_jabatan, 'class="form-control" disabled'),
			'combo_golongan' => combo_golongan('golongan', $peg->id_golongan, 'class="form-control" disabled'),
			'combo_pendidikan' => combo_pendidikan('pendidikan', strtoupper($peg->pendidikan), 'class="form-control" disabled'),
			'radio_gender' => radioGender('gender', $peg->jk, 'disabled'),
		);


		$pagedata = array(
            'title' => 'Kelola Data Instruktur | Detail Data Instruktur',
            'title_menu' => 'Kelola Data Instruktur',
			'menu' => 'Data Master',
			'submenu' => 'Detail Data Instruktur',
			'content' => $this->parser->parse('manage/instruktur/instruktur_form_detail', $contentVars, TRUE)
        );

		 $this->parser->parse('manage/main_adm_form', $pagedata);
    }

	function edit($id) {
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

        $this->_set_rules();

        $data = instruktur_model::get_by_pegawai($this->tableName,$id);

		$this->form_data = new stdClass;
		$this->form_data->id = $data->id_instruktur;
        $this->form_data->nip = $data->nip . ' ( '. $data->nama .' ) ';
		$this->form_data->desc = $data->keterangan;

		$contentVars = array(
			'message' => $this->session->flashdata('message'),
			'base_url' => base_url(),
			'action' => site_url('manage/instruktur/doEdit'),
			'combo_kejuruan' => combo_kejuruan('kejuruan',  $data->id_instruktur, 'class="form-control"'),
			'back_link' => base_url().'manage/instruktur'
		);

		$pagedata = array(
            'title' => 'Kelola Data Instruktur | Edit Data Instruktur',
            'title_menu' => 'Kelola Data Instruktur',
			'menu' => 'Data Master',
			'submenu' => 'Edit Instruktur',
			'content' => $this->parser->parse('manage/instruktur/instruktur_form_edit', $contentVars, TRUE)
        );

		$this->parser->parse('manage/main_adm_form', $pagedata);
    }


	function doEdit(){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		$this->_set_fields();
        $this->_set_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('message', 'Field tidak boleh kosong');
            redirect('manage/instruktur/edit/'.$this->input->post('id'));
        } else {

            $data = array(
                'id_kejuruan' => $this->input->post('kejuruan'),
                'keterangan' => $this->input->post('desc'),
            );

            $doUpdate = instruktur_model::update($this->tableName, $data, $this->input->post('id'));

			if($doUpdate){
				$this->session->set_flashdata('message', 'Data berhasil di ubah');
                redirect('manage/instruktur');
            }else{
				$this->session->set_flashdata('message', 'sedang ada pemeliharaan sistem');
				redirect('manage/instruktur/edit/'.$this->input->post('id'));
            }

        }
	}

	function delete($id){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		instruktur_model::delete($this->tableName, $id);
		$this->session->set_flashdata('message', 'Data berhasi dihapus');
        redirect('manage/instruktur');
	}

    function _set_fields() {
		$this->form_data = new stdClass;
		$this->form_data->nip = '';
    }

    function _set_rules() {
        $this->form_validation->set_rules('nip', 'NIP', 'trim|required');

        $this->form_validation->set_error_delimiters('<span class="error">', '</span>');
    }

}

?>

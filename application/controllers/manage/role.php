<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class role extends CI_Controller {

    private $tableName ;
	private $modul;

    function __construct() {
        parent::__construct();
        $this->load->helper('formutil');
        $this->load->model('role_model', '', TRUE);
        $this->tableName = 'role';
		$this->config->load('bbplk_config');
		$this->modul = $this->config->item('bbplk_module');
		if ($this->session->userdata('currentmenu') != 'User') {
             $this->session->set_userdata('currentmenu', 'User');
        }
    }

    function index() {
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		$islogin = ' <li><a href="'.base_url().'login/logout"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>';

        $config['base_url'] = site_url('manage/role/index/');
        $config['total_rows'] = role_model::count_all($this->tableName);
        $config['per_page'] = 10;
        $config['num_links'] = 5;
        $config['uri_segment'] = 4;
        $config['use_page_numbers'] = true;
        $config['full_tag_open']    = '<ul class="pagination pagination-sm">';
        $config['full_tag_close']   = '</ul>';
        $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['first_tag_open']   = '<li>';
        $config['first_tag_close']  = '</li>';
        $config['prev_link']        = '&laquo';
        $config['prev_tag_open']    = '<li class="prev">';
        $config['prev_tag_close']   = '</li>';
        $config['next_link']        = '&raquo';
        $config['next_tag_open']    = '<li>';
        $config['next_tag_close']   = '</li>';
        $config['last_tag_open']    = '<li>';
        $config['last_tag_close']   = '</li>';
        $config['cur_tag_open']     = '<li class="active"><a href="">';
        $config['cur_tag_close']    = '</a></li>';
        $config['num_tag_open']     = '<li>';
        $config['num_tag_close']    = '</li>';
		$config['use_page_numbers'] = FALSE;

        $this->pagination->initialize($config);

        $offset = ($this->uri->segment(4) && preg_match("/[0-9]/",$this->uri->segment(4))) ? $this->uri->segment(4) : 0;

        $roles = role_model::get_paged_list($this->tableName,$config['per_page'], $offset)->result();

        $this->load->library('table');
		$tabletemp['table_open'] = '<table class="table table-condensed table-striped struktur_org">';
		$this->table->set_template($tabletemp);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading('NO', 'ROLE', 'MENU', array('data'=>'', 'class' => 'aksi'));

        $i = $offset;
        foreach ($roles as $role) {
			$menu = '';
			if($role->menu != null && $role->menu != ""){
				foreach ( unserialize($role->menu) as $module => $sections) {
					$menu .=  strtoupper($module).' : ';
					$a = count($sections);
					$b = 1;
					foreach ($sections as $section) {
						if ($section == '')
							continue;
							$menu .= $section;
							if($b < $a) $menu .= ', ';
							$b++;
					}
					$menu .='<br />';
				}
			}

            $this->table->add_row(
					++$i,
					array('data' => $role->role_name, 'class' =>'alignleft'),
					array('data' => $menu, 'class' =>'alignleft'),
                    array('data' => anchor('manage/role/edit/' . $role->id_role, 'Edit', array('class' => 'btn btn-warning btn-xs')). ' ' . anchor('manage/role/delete/' . $role->id_role, 'Hapus', array('class' => 'btn btn-danger btn-xs', 'onclick' =>"return confirm('apakah anda yakin ingin menghapus data ini?')")), 'class' => 'alignright' )

            );

        }

		$add_btn = '<a class="btn btn-primary" href="'.base_url().'manage/role/add" type="button">
						<span class="glyphicon glyphicon-plus"></span>Tambah Data
				    </a>';

		$pagedata = array(
            'title' => 'Kelola Data Role | Data Role',
            'title_menu' => 'Kelola Data Role',
			'menu' => 'Data Master',
			'islogin' => $islogin,
			'submenu' => 'Data Role',
            'table' => $this->table->generate(),
            'pagination' => $this->pagination->create_links(),
			'add_btn' => $add_btn,
			'print_btn' => '',
			'message' => $this->session->flashdata('message')
        	);


        $this->parser->parse('manage/main_adm', $pagedata);
    }

	function add(){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		$islogin = ' <li><a href="'.base_url().'login/logout"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>';

		$this->_set_fields();
        $this->_set_rules();

		$contentVars = array(
			'message' => $this->session->flashdata('message'),
			'base_url' => base_url(),
			'action' => site_url('manage/role/doAdd'),
			'back_link' => base_url().'manage/role'
		);

		$i = 1;
        foreach ($this->modul as $module => $sections) {
            $contentVars['modules'][$i]['module'] = strtoupper($module);
            $ii = 1;
            foreach ($sections as $section) {
                if ($section == '')
                    continue;
                $module_short = preg_replace("/[^[:alnum:]]/", "_", $module);
                $contentVars['modules'][$i]['module_section'][$ii]['module_short'] = $module_short;
                $contentVars['modules'][$i]['module_section'][$ii]['section'] = $section;
                $section_short = preg_replace("/[^[:alnum:]]/", "_", $section);
                $contentVars['modules'][$i]['module_section'][$ii]['section_short'] = $section_short;
                $contentVars['modules'][$i]['module_section'][$ii]['label'] = $module_short . "_" . $section_short;
                $contentVars['modules'][$i]['module_section'][$ii]['sec_checked'] = "";

                $ii++;
            }
            $i++;
        }

		$pagedata = array(
            'title' => 'Kelola Data Role | Tambah Data Role',
            'title_menu' => 'Kelola Data Role',
			'islogin' => $islogin,
			'menu' => 'Data Master',
			'submenu' => 'Tambah Role',
			'content' => $this->parser->parse('manage/role/role_form', $contentVars, TRUE)
        );

		$this->parser->parse('manage/main_adm_form', $pagedata);
	}


	function doAdd(){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		$this->_set_fields();
        $this->_set_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('message', 'Field tidak boleh kosong');
            redirect('manage/role/add');
        } else {

			$module = $this->input->post('module');

			$data = array(
				'role_name' => $this->input->post('nama'),
				'menu' => serialize($module)
			);

			$doAdd = role_model::save($this->tableName, $data);

			if($doAdd){
				$this->session->set_flashdata('message', 'Data berhasil di tambahkan');
				redirect('manage/role');
			}else{
				$this->session->set_flashdata('message', 'sedang ada pemeliharaan sistem');
				redirect('manage/role/add');
			}

        }
	}


	function edit($id) {
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

        $this->_set_rules();

        $data = role_model::get_by_id($this->tableName,$id);

		$this->form_data = new stdClass;
		$this->form_data->id = $data->id_role;
		$this->form_data->nama = $data->role_name;
		$moduledb = unserialize($data->menu);


		$contentVars = array(
			'message' => $this->session->flashdata('message'),
			'base_url' => base_url(),
			'action' => site_url('manage/role/doEdit'),
			'back_link' => base_url().'manage/role'
		);

		$i = 1;
        foreach ($this->modul as $module => $sections) {
            $contentVars['modules'][$i]['module'] = strtoupper($module);
            $ii = 1;
            foreach ($sections as $section) {
                if ($section == '')
                    continue;
                $module_short = preg_replace("/[^[:alnum:]]/", "_", $module);
                $contentVars['modules'][$i]['module_section'][$ii]['module_short'] = $module_short;
                $contentVars['modules'][$i]['module_section'][$ii]['section'] = $section;
                $section_short = preg_replace("/[^[:alnum:]]/", "_", $section);
                $contentVars['modules'][$i]['module_section'][$ii]['section_short'] = $section_short;
                $contentVars['modules'][$i]['module_section'][$ii]['label'] = $module_short . "_" . $section_short;
                $contentVars['modules'][$i]['module_section'][$ii]['sec_checked'] = array_key_exists($module_short, $moduledb) && is_array($moduledb[$module_short]) && in_array($section_short, $moduledb[$module_short]) ? 'checked="checked"' : "";

                $ii++;
            }
            $i++;
        }

		$pagedata = array(
            'title' => 'Kelola Data Role | Edit Data Role',
            'title_menu' => 'Kelola Data Role',
			'menu' => 'Data Master',
			'submenu' => 'Edit Role',
			'content' => $this->parser->parse('manage/role/role_form_edit', $contentVars, TRUE)
        );

		$this->parser->parse('manage/main_adm_form', $pagedata);
    }


	function doEdit(){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		$this->_set_fields();
        $this->_set_rules();


		$id = $this->input->post('id');
		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('message', 'Field tidak boleh kosong');
            redirect('manage/user/edit/'.$id);
        } else {

            $data = array(
				'role_name' => $this->input->post('nama'),
				'menu' => serialize($this->input->post('module'))
			);

            $doUpdate = role_model::update($this->tableName, $data, $id);

			if($doUpdate){
				$this->session->set_flashdata('message', 'Data berhasil di ubah');
                redirect('manage/role');
            }else{
				$this->session->set_flashdata('message', 'sedang ada pemeliharaan sistem');
				redirect('manage/role/edit/'.$id);
            }

        }
	}


	function delete($id){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		role_model::delete($this->tableName, $id);
		$this->session->set_flashdata('message', 'Data berhasi dihapus');
        redirect('manage/role');
	}

	function detail($id) {
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

        $this->_set_rules();

        $data = role_model::get_by_id($this->tableName,$id);

		$this->form_data = new stdClass;
        $this->form_data->id = $id;
        $this->form_data->nama = $data->role_name;


		$contentVars = array(
			'message' => $this->session->flashdata('message'),
			'base_url' => base_url(),
			'action' => site_url('manage/role/doeEdit'),
			'back_link' => base_url().'manage/role'
		);

		$pagedata = array(
            'title' => 'Kelola Data Pegawai | Detail Data Pegawai',
            'title_menu' => 'Kelola Data role',
			'menu' => 'Data Master',
			'submenu' => 'Detail Data role',
			'content' => $this->parser->parse('manage/role/role_form_detail', $contentVars, TRUE)
        );

		$this->parser->parse('manage/main_adm_form', $pagedata);
    }


    // set empty default form field values
    function _set_fields() {
		$this->form_data = new stdClass;
        $this->form_data->nama = '';
    }

    // validation rules
    function _set_rules() {
        $this->form_validation->set_rules('nama', 'Nama role', 'trim|required');

        $this->form_validation->set_error_delimiters('<span class="error">', '</span>');
    }



}

?>

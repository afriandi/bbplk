<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class user extends CI_Controller {

    private $tableName;

    function __construct() {
        parent::__construct();
        $this->load->helper('formutil');
        $this->load->model('users_model', '', TRUE);
		$this->load->model('pegawai_model', '', TRUE);
        $this->tableName = 'users';
		if ($this->session->userdata('currentmenu') != 'User') {
             $this->session->set_userdata('currentmenu', 'User');
        }
    }

    function index() {
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		$islogin = ' <li><a href="'.base_url().'login/logout"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>';

        $config['base_url'] = site_url('manage/user/index/');
        $config['total_rows'] = users_model::count_all($this->tableName);
        $config['per_page'] = 10;
        $config['num_links'] = 5;
        $config['uri_segment'] = 4;
        $config['use_page_numbers'] = true;
        $config['full_tag_open']    = '<ul class="pagination pagination-sm">';
        $config['full_tag_close']   = '</ul>';
        $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['first_tag_open']   = '<li>';
        $config['first_tag_close']  = '</li>';
        $config['prev_link']        = '&laquo';
        $config['prev_tag_open']    = '<li class="prev">';
        $config['prev_tag_close']   = '</li>';
        $config['next_link']        = '&raquo';
        $config['next_tag_open']    = '<li>';
        $config['next_tag_close']   = '</li>';
        $config['last_tag_open']    = '<li>';
        $config['last_tag_close']   = '</li>';
        $config['cur_tag_open']     = '<li class="active"><a href="">';
        $config['cur_tag_close']    = '</a></li>';
        $config['num_tag_open']     = '<li>';
        $config['num_tag_close']    = '</li>';
		$config['use_page_numbers'] = FALSE;

        $this->pagination->initialize($config);

        $offset = ($this->uri->segment(4) && preg_match("/[0-9]/",$this->uri->segment(4))) ? $this->uri->segment(4) : 0;

        $users = users_model::get_paged_list_all($this->tableName,$config['per_page'], $offset)->result();

        $this->load->library('table');
		$tabletemp['table_open'] = '<table class="table table-condensed table-striped struktur_org">';
		$this->table->set_template($tabletemp);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading('NO', 'USER', 'HAK AKSES', 'STATUS', '');

        $i = $offset;
		$a = $i+1;
        foreach ($users as $usr) {
            $this->table->add_row(
					$a,
					$usr->username,
					$usr->role_name,
					getStatus($usr->status),
                    anchor('manage/user/detail/' . $usr->id_user, 'Detail', array('class' => 'btn btn-success btn-xs')).' '.
					anchor('manage/user/edit/' . $usr->id_user, 'Edit', array('class' => 'btn btn-warning btn-xs')).' '.
					anchor('manage/user/delete/' . $usr->id_user, 'Hapus', array('class' => 'btn btn-danger btn-xs', 'onclick' =>"return confirm('apakah anda yakin ingin menghapus data ini?')"))
			);
			$a++;
        }

		$add_btn = '<a class="btn btn-primary" href="'.base_url().'manage/user/add" type="button">
						<span class="glyphicon glyphicon-plus"></span>Tambah Data
				    </a>';

		$pagedata = array(
            'title' => 'Kelola Data User | Data User',
            'title_menu' => 'Kelola Data User',
			'menu' => 'Data Master',
			'islogin' => $islogin,
			'submenu' => 'Data User',
            'table' => $this->table->generate(),
            'pagination' => $this->pagination->create_links(),
            'add_btn' => $add_btn,
			'print_btn' => '',
			'message' => $this->session->flashdata('message')
        	);

        $this->parser->parse('manage/main_adm', $pagedata);
    }

	function add(){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		$this->_set_fields();
        $this->_set_rules();

		$contentVars = array(
			'message' => $this->session->flashdata('message'),
			'base_url' => base_url(),
			'action' => site_url('manage/user/doAdd'),
			'combo_role' => combo_role('role', '', 'class="form-control input-sm"'),
			'radio_status' => radio_status('status'),
			'back_link' => base_url().'manage/user'
		);

		$pagedata = array(
            'title' => 'Kelola Data User | Tambah Data User',
            'title_menu' => 'Kelola Data User',
			'menu' => 'Data Master',
			'submenu' => 'Tambah User',
			'content' => $this->parser->parse('manage/user/user_form', $contentVars, TRUE)
        );

		$this->parser->parse('manage/main_adm_form', $pagedata);
	}


	function doAdd(){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		$this->_set_fields();
        $this->_set_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('message', 'Field tidak boleh kosong');
            redirect('manage/user/add');
        } else {

            $user = array(
				'nip' => $this->input->post('nip'),
                'username' => $this->input->post('username'),
                'password' => md5($this->input->post('password')),
                'status' => $this->input->post('status'),
                'id_role' => $this->input->post('role')
            );

            $doAdd = users_model::save($this->tableName, $user);

            if($doAdd){
				$this->session->set_flashdata('message', 'User berhasil di tambahkan');
                redirect('manage/user');
            }else{
				$this->session->set_flashdata('message', 'sedang ada pemeliharaan sistem');
				redirect('manage/user/add');
            }

        }
	}


	function detail($id) {
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

        $this->_set_rules();

        $data = users_model::get_user_detail($this->tableName,$id);
		$this->form_data = new stdClass;
        $this->form_data->nip = $data->nip;
        $this->form_data->username = $data->username;
		$this->form_data->password = $data->password;
		$this->form_data->role = $data->role_name;

		$contentVars = array(
			'message' => $this->session->flashdata('message'),
			'base_url' => base_url(),
			'action' => site_url('manage/user/doeEdit'),
			'back_link' => base_url().'manage/user'
		);

		if($data->nip != null && $data->nip != ""){
			$peg = pegawai_model::get_by_id('pegawai',$data->nip);
			$this->form_data->nama = $peg->nama;
			$this->form_data->tempat_lahir = $peg->tempat_lahir;
			$this->form_data->tgl_lahir = $peg->tgl_lahir;
			$this->form_data->tmt = $peg->tmt;
			$contentVars['user_img'] = base_url().'assets/img/pegawai/'.$peg->foto;
			$contentVars['combo_jabatan'] = combo_jabatan('jabatan', $peg->id_jabatan, 'class="form-control" disabled');
			$contentVars['combo_golongan'] = combo_golongan('golongan', $peg->id_golongan, 'class="form-control" disabled');
			$contentVars['combo_pendidikan'] = combo_pendidikan('pendidikan', strtoupper($peg->pendidikan), 'class="form-control" disabled');
			$contentVars['radio_gender'] = radioGender('gender', $peg->jk, 'disabled');

		}else{
			$this->form_data->nama = '';
			$this->form_data->tempat_lahir = '';
			$this->form_data->tgl_lahir = '';
			$this->form_data->tmt = '';
			$contentVars['user_img'] = base_url().'assets/img/pegawai/default.png';
			$contentVars['combo_jabatan'] = combo_jabatan('jabatan', '', 'class="form-control" disabled');
			$contentVars['combo_golongan'] = combo_golongan('golongan', '', 'class="form-control" disabled');
			$contentVars['combo_pendidikan'] = combo_pendidikan('pendidikan', '', 'class="form-control" disabled');
			$contentVars['radio_gender'] = radioGender('gender', '', 'disabled');
		}

		$pagedata = array(
            'title' => 'Kelola Data User | Detail Data User',
            'title_menu' => 'Kelola Data User',
			'menu' => 'Data Master',
			'submenu' => 'Detail Data User',
			'content' => $this->parser->parse('manage/user/user_form_detail', $contentVars, TRUE)
        );

		 $this->parser->parse('manage/main_adm_form', $pagedata);
    }

	function edit($id) {
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

        $this->_set_rules();

        $data = users_model::get_by_id($this->tableName,$id);

		$this->form_data = new stdClass;
		$this->form_data->id = $data->id_user;
        $this->form_data->nip = $data->nip;
        $this->form_data->username = $data->username;
		$this->form_data->password = $data->password;

		$contentVars = array(
			'message' => $this->session->flashdata('message'),
			'base_url' => base_url(),
			'action' => site_url('manage/user/doEdit'),
			'combo_role' => combo_role('role', $data->id_role, 'class="form-control input-sm"'),
			'radio_status' => radio_status('status', $data->status),
			'back_link' => base_url().'manage/user'
		);

		$pagedata = array(
            'title' => 'Kelola Data User | Edit Data User',
            'title_menu' => 'Kelola Data User',
			'menu' => 'Data Master',
			'submenu' => 'Edit User',
			'content' => $this->parser->parse('manage/user/user_form_edit', $contentVars, TRUE)
        );

		$this->parser->parse('manage/main_adm_form', $pagedata);
    }


	function doEdit(){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		$this->_set_fields();
        $this->_set_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('message', 'Field tidak boleh kosong');
            redirect('manage/user/edit/'.$this->input->post('id'));
        } else {

            $user = array(
                'username' => $this->input->post('username'),
                'status' => $this->input->post('status'),
                'id_role' => $this->input->post('role')
            );

			if($this->input->post('password') != ''){
				$user['password'] = md5($this->input->post('password'));
			}

            $doUpdate = users_model::update($this->tableName, $user, $this->input->post('id'));

			if($doUpdate){
				$this->session->set_flashdata('message', 'Data berhasil di ubah');
                redirect('manage/user');
            }else{
				$this->session->set_flashdata('message', 'sedang ada pemeliharaan sistem');
				redirect('manage/user/edit/'.$this->input->post('id'));
            }

        }
	}

	function delete($id){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		users_model::delete($this->tableName, $id);
		$this->session->set_flashdata('message', 'Data berhasi dihapus');
        redirect('manage/user');
	}

    function _set_fields() {
		$this->form_data = new stdClass;
		$this->form_data->nip = '';
        $this->form_data->username = '';
        $this->form_data->password = '';
    }

    function _set_rules() {
        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

        $this->form_validation->set_error_delimiters('<span class="error">', '</span>');
    }

}

?>

<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class info extends CI_Controller {

	  function __construct() {
        parent::__construct();
        $this->load->helper('simple_dom');
				$this->load->model('pelatihan_model');
    }

    public function index(){
		$islogin = '<li><a href="'.base_url().'login"><span class="glyphicon glyphicon-lock"></span>Login</a></li>';
		if ($this->session->userdata('username')) {
        $islogin = ' <li><a href="'.base_url().'login/logout"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>';
    }

		$tableName = "detail_pelatihan";
		$id_oto = "oto";
		$id_tekman = "tekman";

		$total_oto = $this->pelatihan_model->count_pelatihan($tableName,$id_oto);
		$total_tekman = $this->pelatihan_model->count_pelatihan($tableName,$id_tekman);
		$total_siswa_oto = $total_oto * 16;
		$total_siswa_tekman = $total_tekman * 16;
		$total_siswa = $total_siswa_oto + $total_siswa_tekman;

		$data_jk_oto = $this->pelatihan_model->get_all($tableName,$id_oto)->result();
		$info_jk_oto = array();
		$total_p_oto=0;
		$total_l_oto=0;
		$a=0;
		foreach ($data_jk_oto as $djo) {
			$info_jk_oto[$a]['p'] = $djo->p;
			$total_p_oto=$total_p_oto+$info_jk_oto[$a]['p'];
			$info_jk_oto[$a]['l'] = $djo->l;
			$total_l_oto=$total_l_oto+$info_jk_oto[$a]['l'];
			$a++;
		}

		$data_jk_tekman = $this->pelatihan_model->get_all($tableName,$id_tekman)->result();
		$info_jk_tekman = array();
		$total_p_tekman=0;
		$total_l_tekman=0;
		$b=0;
		foreach ($data_jk_tekman as $djt) {
			$info_jk_tekman[$b]['p'] = $djt->p;
			$total_p_tekman=$total_p_tekman+$info_jk_tekman[$b]['p'];
			$info_jk_tekman[$b]['l'] = $djt->l;
			$total_l_tekman=$total_l_tekman+$info_jk_tekman[$b]['l'];
			$b++;
		}

		$total_p = $total_p_oto + $total_p_tekman;
		$total_l = $total_l_oto + $total_l_tekman;

		$data_pendidikan_oto = $this->pelatihan_model->get_all($tableName,$id_oto)->result();
		$info_pendidikan_oto = array();
		$total_sd_oto=0;
		$total_smp_oto=0;
		$total_sma_oto=0;
		$total_s1_oto=0;
		$c=0;
		foreach ($data_pendidikan_oto as $dpo) {
			$info_pendidikan_oto[$c]['sd'] = $dpo->sd;
			$total_sd_oto=$total_sd_oto+$info_pendidikan_oto[$c]['sd'];
			$info_pendidikan_oto[$c]['smp'] = $dpo->smp;
			$total_smp_oto=$total_smp_oto+$info_pendidikan_oto[$c]['smp'];
			$info_pendidikan_oto[$c]['sma'] = $dpo->sma;
			$total_sma_oto=$total_sma_oto+$info_pendidikan_oto[$c]['sma'];
			$info_pendidikan_oto[$c]['s1'] = $dpo->s1;
			$total_s1_oto=$total_s1_oto+$info_pendidikan_oto[$c]['s1'];
			$c++;
		}

		$data_pendidikan_tekman = $this->pelatihan_model->get_all($tableName,$id_tekman)->result();
		$info_pendidikan_tekman = array();
		$total_sd_tekman=0;
		$total_smp_tekman=0;
		$total_sma_tekman=0;
		$total_s1_tekman=0;
		$d=0;
		foreach ($data_pendidikan_tekman as $dpt) {
			$info_pendidikan_tekman[$d]['sd'] = $dpt->sd;
			$total_sd_tekman=$total_sd_tekman+$info_pendidikan_tekman[$d]['sd'];
			$info_pendidikan_tekman[$d]['smp'] = $dpt->smp;
			$total_smp_tekman=$total_smp_tekman+$info_pendidikan_tekman[$d]['smp'];
			$info_pendidikan_tekman[$d]['sma'] = $dpt->sma;
			$total_sma_tekman=$total_sma_tekman+$info_pendidikan_tekman[$d]['sma'];
			$info_pendidikan_tekman[$d]['s1'] = $dpt->s1;
			$total_s1_tekman=$total_s1_tekman+$info_pendidikan_tekman[$d]['s1'];
			$d++;
		}

		$total_sd = $total_sd_oto + $total_sd_tekman;
		$total_smp = $total_smp_oto + $total_smp_tekman;
		$total_sma = $total_sma_oto + $total_sma_tekman;
		$total_s1 = $total_s1_oto + $total_s1_tekman;

		$contentVars = array(
			'base_url' => base_url(),
			'title_menu' => 'Rekap Penempatan',
			'menu' => 'Informasi',
			'submenu' =>  'Rekap Pelatihan',
			'total_siswa_oto'=> $total_siswa_oto,
			'total_siswa_tekman'=> $total_siswa_tekman,
			'total_siswa'=> $total_siswa,
			'total_sd_oto'=> $total_sd_oto,
			'total_smp_oto'=> $total_smp_oto,
			'total_sma_oto'=> $total_sma_oto,
			'total_s1_oto'=> $total_s1_oto,
			'total_sd_tekman'=> $total_sd_tekman,
			'total_smp_tekman'=> $total_smp_tekman,
			'total_sma_tekman'=> $total_sma_tekman,
			'total_s1_tekman'=> $total_s1_tekman,
			'total_sd'=> $total_sd,
			'total_smp'=> $total_smp,
			'total_sma'=> $total_sma,
			'total_s1'=> $total_s1,
			'total_p_oto'=> $total_p_oto,
			'total_l_oto'=> $total_l_oto,
			'total_p_tekman'=> $total_p_tekman,
			'total_l_tekman'=> $total_l_tekman,
			'total_p'=> $total_p,
			'total_l'=> $total_l,
		);

		$pagedata = array(
			'title' => ' >> Informasi',
			'islogin' => $islogin,
			'content' => $this->parser->parse('info/rekap_peserta', $contentVars, TRUE)
		);

    $this->parser->parse('main', $pagedata);
    }

	public function pelatihan()
    {
		$islogin = '<li><a href="'.base_url().'login"><span class="glyphicon glyphicon-lock"></span>Login</a></li>';
		if ($this->session->userdata('username')) {
        $islogin = ' <li><a href="'.base_url().'login/logout"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>';
    }

		$tableName = "detail_pelatihan";
		$id_oto = "oto";
		$id_tekman = "tekman";
		$total_oto = $this->pelatihan_model->count_pelatihan($tableName,$id_oto);
		$total_tekman = $this->pelatihan_model->count_pelatihan($tableName,$id_tekman);
		$total_siswa_oto = $total_oto * 16;
		$total_siswa_tekman = $total_tekman * 16;

    $contentVars = array(
			'base_url' => base_url(),
			'title_menu' => 'Rekap Pelatihan',
			'menu' => 'Informasi',
			'submenu' =>  'Rekap Pelatihan',
			'total_oto'=> $total_oto,
			'total_tekman'=> $total_tekman,
			'total_siswa_oto'=> $total_siswa_oto,
			'total_siswa_tekman'=> $total_siswa_tekman
		);

		$pagedata = array(
			'title' => ' >> Informasi',
			'islogin' => $islogin,
			'content' => $this->parser->parse('info/rekap_pelatihan', $contentVars, TRUE)
		);

    $this->parser->parse('main', $pagedata);
    }


	public function penempatan(){
		$islogin = '<li><a href="'.base_url().'login"><span class="glyphicon glyphicon-lock"></span>Login</a></li>';
		if ($this->session->userdata('username')) {
        $islogin = ' <li><a href="'.base_url().'login/logout"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>';
    }

		$tableName = "detail_pelatihan";
		$id_oto = "oto";
		$id_tekman = "tekman";

		$total_oto = $this->pelatihan_model->count_pelatihan($tableName,$id_oto);
		$total_tekman = $this->pelatihan_model->count_pelatihan($tableName,$id_tekman);
		$total_siswa_oto = $total_oto * 16;
		$total_siswa_tekman = $total_tekman * 16;
		$total_program = $total_oto + $total_tekman;
		$total_siswa = $total_program * 16;

		$data_oto = $this->pelatihan_model->get_all($tableName,$id_oto)->result();
		$data = array();
		$total_mandiri_oto=0;
		$total_industri_oto=0;
		$i=0;
		foreach ($data_oto as $if) {
			$data[$i]['mandiri'] = $if->mandiri;
			$total_mandiri_oto=$total_mandiri_oto+$data[$i]['mandiri'];
			$data[$i]['industri'] = $if->industri;
			$total_industri_oto=$total_industri_oto+$data[$i]['industri'];
			$i++;
		}

		$data_tekman = $this->pelatihan_model->get_all($tableName,$id_tekman)->result();
		$data2 = array();
		$total_mandiri_tekman=0;
		$total_industri_tekman=0;
		$j=0;
		foreach ($data_tekman as $dt) {
			$data2[$j]['mandiri'] = $dt->mandiri;
			$total_mandiri_tekman=$total_mandiri_tekman+$data2[$j]['mandiri'];
			$data2[$j]['industri'] = $dt->industri;
			$total_industri_tekman=$total_industri_tekman+$data2[$j]['industri'];
			$j++;
		}

		$total_mandiri = $total_mandiri_oto + $total_mandiri_tekman;
		$total_industri = $total_industri_oto + $total_industri_tekman;
		$total_bekerja = $total_mandiri + $total_industri;
		$total_bekerja_oto = $total_mandiri_oto + $total_industri_oto;
		$total_bekerja_tekman = $total_mandiri_tekman + $total_industri_tekman;

		$contentVars = array(
			'base_url' => base_url(),
			'title_menu' => 'Rekap Penempatan',
			'menu' => 'Informasi',
			'submenu' =>  'Rekap Pelatihan',
			'total_oto'=> $total_oto,
			'total_tekman'=> $total_tekman,
			'total_siswa_oto'=> $total_siswa_oto,
			'total_siswa_tekman'=> $total_siswa_tekman,
			'total_program'=> $total_program,
			'total_siswa'=> $total_siswa,
			'mandiri_oto'=> $total_mandiri_oto,
			'industri_oto'=> $total_industri_oto,
			'mandiri_tekman'=> $total_mandiri_tekman,
			'industri_tekman'=> $total_industri_tekman,
			'total_mandiri'=> $total_mandiri,
			'total_industri'=> $total_industri,
			'total_bekerja'=> $total_bekerja,
			'total_bekerja_oto'=> $total_bekerja_oto,
			'total_bekerja_tekman'=> $total_bekerja_tekman,
		);

		$pagedata = array(
			'title' => ' >> Informasi',
			'islogin' => $islogin,
			'content' => $this->parser->parse('info/rekap_penempatan', $contentVars, TRUE)
		);

    $this->parser->parse('main', $pagedata);
    }

	function lowker(){

		// get DOM from URL or file
		$html = file_get_html('http://www.kios3in1.net/001/kios_bursakerja/x2011lowongan_public.php');

		// find all link
		foreach($html->find('a') as $e)

		$html2 = file_get_html('http://www.kios3in1.net/001/kios_bursakerja/'.$e->href);
		// find content lowker
		foreach($html2->find('form') as $b)


		$islogin = '<li><a href="'.base_url().'login"><span class="glyphicon glyphicon-lock"></span>Login</a></li>';

		if ($this->session->userdata('username')) {
            $islogin = ' <li><a href="'.base_url().'login/logout"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>';
        }

        $contentVars = array(
			'base_url' => base_url(),
			'title_menu' =>  'Lowongan Pekerjaan',
			'menu' =>  'Informasi',
			'submenu' =>  'Lowongan Pekerjaan',
			'lowker' => $b->outertext
		);


		$pagedata = array(
			'title' => ' >> Informasi',
			'islogin' => $islogin,
			'content' => $this->parser->parse('info/lowker', $contentVars, TRUE)
		);
        $this->parser->parse('main', $pagedata);
	}
}
?>

<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class fasilitas extends CI_Controller {

    private $tableName;

    function __construct() {
        parent::__construct();
        $this->load->helper('formutil');
        $this->load->model('fasilitas_model', '', TRUE);
        $this->tableName = 'fasilitas';
		if ($this->session->userdata('currentmenu') != 'Web') {
             $this->session->set_userdata('currentmenu', 'web');
        }
    }

    function index() {
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		$islogin = ' <li><a href="'.base_url().'login/logout"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>';

        $config['base_url'] = site_url('manage/fasilitas/index/');
        $config['total_rows'] = fasilitas_model::count_all($this->tableName);
        $config['per_page'] = 5;
        $config['num_links'] = 5;
        $config['uri_segment'] = 4;
        $config['use_page_numbers'] = true;
        $config['full_tag_open']    = '<ul class="pagination pagination-sm">';
        $config['full_tag_close']   = '</ul>';
        $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['first_tag_open']   = '<li>';
        $config['first_tag_close']  = '</li>';
        $config['prev_link']        = '&laquo';
        $config['prev_tag_open']    = '<li class="prev">';
        $config['prev_tag_close']   = '</li>';
        $config['next_link']        = '&raquo';
        $config['next_tag_open']    = '<li>';
        $config['next_tag_close']   = '</li>';
        $config['last_tag_open']    = '<li>';
        $config['last_tag_close']   = '</li>';
        $config['cur_tag_open']     = '<li class="active"><a href="">';
        $config['cur_tag_close']    = '</a></li>';
        $config['num_tag_open']     = '<li>';
        $config['num_tag_close']    = '</li>';
		$config['use_page_numbers'] = FALSE;

        $this->pagination->initialize($config);

        $offset = ($this->uri->segment(4) && preg_match("/[0-9]/",$this->uri->segment(4))) ? $this->uri->segment(4) : 0;

        $data = fasilitas_model::get_paged_list($this->tableName,$config['per_page'], $offset)->result();

        $this->load->library('table');
		$tabletemp['table_open'] = '<table class="table table-condensed table-striped struktur_org">';
		$this->table->set_template($tabletemp);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading('NO', 'JUDUL', 'KEPEMILIKAN', 'DESKRIPSI', '', '');

        $i = $offset;
		$a = $i+1;
        foreach ($data as $dt) {
			$judul = $dt->judul != '' ? $dt->judul : '-';
            $this->table->add_row(
					$a,
					$dt->judul,
					getKepemilikan($dt->kepemilikan, 0, 150),
					$dt->deskripsi,
					anchor('manage/fasilitas/edit/' . $dt->id_fasilitas, 'Edit', array('class' => 'btn btn-warning btn-xs')),
					anchor('manage/fasilitas/delete/' . $dt->id_fasilitas, 'Hapus', array('class' => 'btn btn-danger btn-xs', 'onclick' =>"return confirm('apakah anda yakin ingin menghapus data ini?')"))
			);
			$a++;
        }

		$add_btn = '<a class="btn btn-primary" href="'.base_url().'manage/fasilitas/add" type="button">
						<span class="glyphicon glyphicon-plus"></span>Tambah Data
				    </a>';

		$pagedata = array(
            'title' => 'Kelola Data Fasilitas | Data Fasilitas',
            'title_menu' => 'Kelola Data Fasilitas',
			'menu' => 'Data Master',
			'islogin' => $islogin,
			'submenu' => 'Data Fasilitas',
            'table' => $this->table->generate(),
            'pagination' => $this->pagination->create_links(),
            'add_btn' => $add_btn,
			'print_btn' => '',
			'message' => $this->session->flashdata('message')
        	);

        $this->parser->parse('manage/main_adm', $pagedata);
    }

	function add(){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		$this->_set_fields();
        $this->_set_rules();

		$contentVars = array(
			'message' => $this->session->flashdata('message'),
			'base_url' => base_url(),
			'action' => site_url('manage/fasilitas/doAdd'),
			'combo_kepemilikan' => combo_kepemilikan('milik', '', 'class="form-control"'),
			'back_link' => base_url().'manage/fasilitas'
		);

		$pagedata = array(
            'title' => 'Kelola Data Fasilitas | Tambah Data Fasilitas',
            'title_menu' => 'Kelola Data Fasilitas',
			'menu' => 'Data Master',
			'submenu' => 'Tambah Fasilitas',
			'content' => $this->parser->parse('manage/fasilitas/fasilitas_form', $contentVars, TRUE)
        );

		$this->parser->parse('manage/main_adm_form', $pagedata);
	}


	function doAdd(){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		$this->_set_fields();
        $this->_set_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('message', 'Field tidak boleh kosong');
            redirect('manage/fasilitas/add');
        } else {

			$fileupload = $_FILES['photo'];
        	$path = 'assets/img/fasilitas/';

			$config['upload_path'] = $path;
            $config['allowed_types'] = '*';
            $config['file_name'] = $fileupload['name'];

            if (file_exists($path . $config['file_name'])) {
                unlink($path . $config['file_name']);
            }

			$this->load->library('upload', $config);

			if (!$this->upload->do_upload('photo')) {
               $this->session->set_flashdata('message', 'Upload Foto Gagal Silahkan ulangi kembali');
			   redirect('manage/fasilitas/add');
			} else {
				$data = array(
					'judul' => $this->input->post('judul'),
					'foto' => $config['file_name'],
					'kepemilikan' => $this->input->post('milik'),
					'deskripsi' =>$this->input->post('desc'),
				);

				$doAdd = fasilitas_model::save($this->tableName, $data);

				if($doAdd){
					$this->session->set_flashdata('message', 'Data berhasil di tambahkan');
					redirect('manage/fasilitas');
				}else{
					$this->session->set_flashdata('message', 'sedang ada pemeliharaan sistem');
					redirect('manage/fasilitas/add');
				}
			}
        }
	}


	function edit($id) {
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

        $this->_set_rules();

        $data = fasilitas_model::get_by_id($this->tableName,$id);

		$this->form_data = new stdClass;
		$this->form_data->id = $data->id_fasilitas;
		$this->form_data->judul = $data->judul;
		$this->form_data->img = $data->foto;
		$this->form_data->desc = $data->deskripsi;

		$contentVars = array(
			'message' => $this->session->flashdata('message'),
			'base_url' => base_url(),
			'action' => site_url('manage/fasilitas/doEdit'),
			'img' => base_url().'assets/img/fasilitas/'.$data->foto,
			'combo_kepemilikan' => combo_kepemilikan('milik', $data->kepemilikan, 'class="form-control"'),
			'back_link' => base_url().'manage/fasilitas'
		);

		$pagedata = array(
            'title' => 'Kelola Data fasilitas | Edit Data fasilitas',
            'title_menu' => 'Kelola Data fasilitas',
			'menu' => 'Data Master',
			'submenu' => 'Edit fasilitas',
			'content' => $this->parser->parse('manage/fasilitas/fasilitas_form_edit', $contentVars, TRUE)
        );

		$this->parser->parse('manage/main_adm_form', $pagedata);
    }


	function doEdit(){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		$this->_set_fields();
        $this->_set_rules();


		$id = $this->input->post('id');
		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('message', 'Field tidak boleh kosong');
            redirect('manage/user/edit/'.$id);
        } else {

			$fileupload = $_FILES['photo'];
        	$path = 'assets/img/fasilitas/';

			$config['upload_path'] = $path;
            $config['allowed_types'] = '*';
            $config['file_name'] = $fileupload['name'];

            if ($fileupload['name'] != null && file_exists($path . $this->input->post('img'))) {
				unlink($path . $this->input->post('img'));
			}

			$this->load->library('upload', $config);

            $data = array(
					'judul' => $this->input->post('judul'),
					'kepemilikan' => $this->input->post('milik'),
					'deskripsi' =>$this->input->post('desc'),
				);

			if($fileupload['name'] != null){
				$data['foto'] = $fileupload['name'];

				if (!$this->upload->do_upload('photo')) {
					$this->session->set_flashdata('message', 'Upload Foto Gagal');
					redirect('manage/fasilitas/edit/'.$id);
				}
			}

            $doUpdate = fasilitas_model::update($this->tableName, $data, $id);

			if($doUpdate){
				$this->session->set_flashdata('message', 'Data berhasil di ubah');
                redirect('manage/fasilitas');
            }else{
				$this->session->set_flashdata('message', 'sedang ada pemeliharaan sistem');
				redirect('manage/fasilitas/edit/'.$id);
            }

        }
	}

	function delete($id){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		$data = fasilitas_model::get_by_id($this->tableName,$id);
		$path = 'assets/img/fasilitas/';

		if (file_exists($path . $data->foto)) {
			unlink($path . $data->foto);
		}

		fasilitas_model::delete($this->tableName, $id);
		$this->session->set_flashdata('message', 'Data berhasi dihapus');
        redirect('manage/fasilitas');
	}

    function _set_fields() {
		$this->form_data = new stdClass;
		$this->form_data->judul = '';
    }

    function _set_rules() {
        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');

        $this->form_validation->set_error_delimiters('<span class="error">', '</span>');
    }

}

?>

<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class pegawai extends CI_Controller {

    private $tableName;

    function __construct() {
        parent::__construct();
        $this->load->helper('formutil');
        $this->load->model('pegawai_model', '', TRUE);
        $this->tableName = 'pegawai';
		if ($this->session->userdata('currentmenu') != 'Input') {
             $this->session->set_userdata('currentmenu', 'Input');
        }
    }

    function index() {
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		$islogin = ' <li><a href="'.base_url().'login/logout"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>';
        $config['base_url'] = site_url('manage/pegawai/index/');
        $config['total_rows'] = pegawai_model::count_all($this->tableName);
        $config['per_page'] = 10;
        $config['num_links'] = 5;
        $config['uri_segment'] = 4;
        $config['use_page_numbers'] = true;
        $config['full_tag_open']    = '<ul class="pagination pagination-sm">';
        $config['full_tag_close']   = '</ul>';
        $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['first_tag_open']   = '<li>';
        $config['first_tag_close']  = '</li>';
        $config['prev_link']        = '&laquo';
        $config['prev_tag_open']    = '<li class="prev">';
        $config['prev_tag_close']   = '</li>';
        $config['next_link']        = '&raquo';
        $config['next_tag_open']    = '<li>';
        $config['next_tag_close']   = '</li>';
        $config['last_tag_open']    = '<li>';
        $config['last_tag_close']   = '</li>';
        $config['cur_tag_open']     = '<li class="active"><a href="">';
        $config['cur_tag_close']    = '</a></li>';
        $config['num_tag_open']     = '<li>';
        $config['num_tag_close']    = '</li>';
		$config['use_page_numbers'] = FALSE;

        $this->pagination->initialize($config);

        $offset = ($this->uri->segment(4) && preg_match("/[0-9]/",$this->uri->segment(4))) ? $this->uri->segment(4) : 0;

        $pegawai = pegawai_model::get_paged_list_all($this->tableName,$config['per_page'], $offset)->result();

        $this->load->library('table');
		$tabletemp['table_open'] = '<table class="table table-condensed table-striped struktur_org">';
		$this->table->set_template($tabletemp);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading('NO', 'NIP', 'NAMA LENGKAP', 'JABATAN', '');

        $i = $offset;
		$a = $i+1;
        foreach ($pegawai as $pgw) {
            $this->table->add_row(
					$a,
					$pgw->nip,
					$pgw->nama,
					$pgw->jabatan,
                    anchor('manage/pegawai/detail/' . $pgw->nip, 'Detail', array('class' => 'btn btn-success btn-xs')).' '.
					anchor('manage/pegawai/edit/' . $pgw->nip, 'Edit', array('class' => 'btn btn-warning btn-xs')).' '.
					anchor('manage/pegawai/delete/' . $pgw->nip, 'Hapus', array('class' => 'btn btn-danger btn-xs', 'onclick' =>"return confirm('apakah anda yakin ingin menghapus data ini?')"))
			);
			$a++;
        }

		$add_btn = '<a class="btn btn-primary" href="'.base_url().'manage/pegawai/add" type="button">
						<span class="glyphicon glyphicon-plus"></span>Tambah Data
				    </a>';

		$pagedata = array(
            'title' => 'Kelola Data Pegawai | Data Pegawai',
            'title_menu' => 'Kelola Data Pegawai',
			'menu' => 'Data Master',
			'islogin' => $islogin,
			'submenu' => 'Data Pegawai',
            'table' => $this->table->generate(),
            'pagination' => $this->pagination->create_links(),
            'add_btn' => $add_btn,
			'print_btn' => '',
			'message' => $this->session->flashdata('message')
        	);

        $this->parser->parse('manage/main_adm', $pagedata);
    }

	function add(){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		$this->_set_fields();
        $this->_set_rules();

		$contentVars = array(
			'message' => $this->session->flashdata('message'),
			'base_url' => base_url(),
			'action' => site_url('manage/pegawai/doAdd'),
			'combo_jabatan' => combo_jabatan('jabatan', '', 'class="form-control"'),
			'combo_golongan' => combo_golongan('golongan', '', 'class="form-control"'),
			'combo_pendidikan' => combo_pendidikan('pendidikan', '', 'class="form-control"'),
			'radio_gender' => radioGender('gender'),
			'disable' => '',
			'back_link' => base_url().'manage/pegawai'
		);

		$pagedata = array(
            'title' => 'Kelola Data Pegawai | Data Pegawai',
            'title_menu' => 'Kelola Data Pegawai',
			'menu' => 'Data Master',
			'submenu' => 'Tambah Data Pegawai',
			'content' => $this->parser->parse('manage/pegawai/pegawai_form', $contentVars, TRUE)
        );

		 $this->parser->parse('manage/main_adm_form', $pagedata);
	}


	function doAdd(){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		$this->_set_fields();
        $this->_set_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('message', 'Field tidak boleh kosong');
			redirect('manage/pegawai/add');
        } else {

			$fileupload = $_FILES['photo'];
        	$path = 'assets/img/pegawai/';

			$config['upload_path'] = $path;
            $config['allowed_types'] = '*';
            $config['file_name'] = $fileupload['name'];

            if (file_exists($path . $config['file_name'])) {
                unlink($path . $config['file_name']);
            }

			$this->load->library('upload', $config);

			if (!$this->upload->do_upload('photo')) {
               $this->session->set_flashdata('message', 'Upload Foto Gagal Silahkan ulangi kembali');
			   redirect('manage/pegawai/add');
			} else {
				$Pegawai = array(
					'nip' => $this->input->post('nip'),
					'nama' => $this->input->post('nama'),
					'tempat_lahir' => $this->input->post('tempat_lahir'),
					'tgl_lahir' => $this->input->post('tgl_lahir'),
					'id_jabatan' => $this->input->post('jabatan'),
					'id_golongan' => $this->input->post('golongan'),
					'tmt' => $this->input->post('tmt'),
					'pendidikan' => $this->input->post('pendidikan'),
					'jk' => $this->input->post('gender'),
					'foto' => $fileupload['name']
				);

				$doAdd = pegawai_model::save($this->tableName, $Pegawai);

				if($doAdd){
					$this->session->set_flashdata('message', 'Pegawai berhasil di tambahkan');
					redirect('manage/pegawai');
				}else{
					$this->session->set_flashdata('message', 'sedang ada pemeliharaan sistem');
					redirect('manage/pegawai/add');
				}
			}
        }
	}


	function detail($id){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

        $this->_set_rules();

        $data = pegawai_model::get_by_id($this->tableName,$id);

		$this->form_data = new stdClass;
        $this->form_data->nip = $data->nip;
        $this->form_data->nama = $data->nama;
		$this->form_data->tempat_lahir = $data->tempat_lahir;
		$this->form_data->tgl_lahir = $data->tgl_lahir;
		$this->form_data->tmt = $data->tmt;

		$contentVars = array(
			'message' => $this->session->flashdata('message'),
			'base_url' => base_url(),
			'action' => site_url('manage/pegawai/doeEdit'),
			'combo_jabatan' => combo_jabatan('jabatan', $data->id_jabatan, 'class="form-control" disabled'),
			'combo_golongan' => combo_golongan('golongan', $data->id_golongan, 'class="form-control" disabled'),
			'combo_pendidikan' => combo_pendidikan('pendidikan', strtoupper($data->pendidikan), 'class="form-control" disabled'),
			'radio_gender' => radioGender('gender', $data->jk, 'disabled'),
			'pegawai_img' => base_url().'assets/img/pegawai/'.$data->foto,
			'back_link' => base_url().'manage/pegawai'
		);

		$pagedata = array(
            'title' => 'Kelola Data Pegawai | Detail Data Pegawai',
            'title_menu' => 'Kelola Data Pegawai',
			'menu' => 'Data Master',
			'submenu' => 'Detail Data Pegawai',
			'content' => $this->parser->parse('manage/pegawai/pegawai_form_detail', $contentVars, TRUE)
        );

		 $this->parser->parse('manage/main_adm_form', $pagedata);
	}


	function edit($id){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

        $this->_set_rules();

        $data = pegawai_model::get_by_id($this->tableName,$id);

		$this->form_data = new stdClass;
        $this->form_data->nip = $data->nip;
        $this->form_data->nama = $data->nama;
		$this->form_data->tempat_lahir = $data->tempat_lahir;
		$this->form_data->tgl_lahir = $data->tgl_lahir;
		$this->form_data->tmt = $data->tmt;

		$contentVars = array(
			'message' => $this->session->flashdata('message'),
			'base_url' => base_url(),
			'action' => site_url('manage/pegawai/doEdit/'.$id),
			'combo_jabatan' => combo_jabatan('jabatan', $data->id_jabatan, 'class="form-control"'),
			'combo_golongan' => combo_golongan('golongan', $data->id_golongan, 'class="form-control"s'),
			'combo_pendidikan' => combo_pendidikan('pendidikan', strtoupper($data->pendidikan), 'class="form-control"'),
			'radio_gender' => radioGender('gender', $data->jk),
			'pegawai_img' => base_url().'assets/img/pegawai/'.$data->foto,
			'back_link' => base_url().'manage/pegawai'
		);

		$pagedata = array(
            'title' => 'Kelola Data Pegawai | Edit Data Pegawai',
            'title_menu' => 'Kelola Data Pegawai',
			'menu' => 'Data Master',
			'submenu' => 'Edit Data Pegawai',
			'content' => $this->parser->parse('manage/pegawai/pegawai_form_edit', $contentVars, TRUE)
        );

		 $this->parser->parse('manage/main_adm_form', $pagedata);
	}


	function doEdit($id){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

     	$this->_set_fields();
        $this->_set_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('message', 'Field tidak boleh kosong');
            redirect('manage/pegawai/edit/'.$id);
        } else {

			$data = pegawai_model::get_by_id($this->tableName,$id);

			$fileupload = $_FILES['photo'];
			$path = 'assets/img/pegawai/';

			$config['upload_path'] = $path;
			$config['allowed_types'] = '*';
			$config['file_name'] = $fileupload['name'];

			if ($fileupload['name'] != null && file_exists($path . $data->foto)) {
				unlink($path . $data->foto);
			}

			$this->load->library('upload', $config);

			$Pegawai = array(
				'nama' => $this->input->post('nama'),
				'tempat_lahir' => $this->input->post('tempat_lahir'),
				'tgl_lahir' => $this->input->post('tgl_lahir'),
				'id_jabatan' => $this->input->post('jabatan'),
				'id_golongan' => $this->input->post('golongan'),
				'tmt' => $this->input->post('tmt'),
				'jk' => $this->input->post('gender'),
				'pendidikan' => $this->input->post('pendidikan')
			);

			if($fileupload['name'] != null){
				$Pegawai['foto'] = $fileupload['name'];

				if (!$this->upload->do_upload('photo')) {
					$this->session->set_flashdata('message', 'Data berhasil di edit Data pegawai tetapi Upload Foto Gagal');
					redirect('manage/pegawai/edit/'.$id);
				}
			}

			$doUpdate = pegawai_model::update($this->tableName, $Pegawai, $id);
			if($doUpdate){
				$this->session->set_flashdata('message', 'Data berhasil di ubah');
				redirect('manage/pegawai');
			}else{
				$this->session->set_flashdata('message', 'sedang ada pemeliharaan sistem');
				redirect('manage/pegawai/edit/'.$id);
			}

        }
	}

	function delete($id){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }
		$data = pegawai_model::get_by_id($this->tableName,$id);
		$path = 'assets/img/pegawai/';

		if (file_exists($path . $data->foto)) {
			unlink($path . $data->foto);
		}

		pegawai_model::delete($this->tableName, $id);
		$this->session->set_flashdata('message', 'Data berhasi dihapus');
        redirect('manage/pegawai');
	}

    function _set_fields() {
		$this->form_data = new stdClass;
		$this->form_data->nip = '';
        $this->form_data->nama = '';
    }

    function _set_rules() {
        $this->form_validation->set_rules('nip', 'nip', 'trim|required');
        $this->form_validation->set_rules('nama', 'nama', 'trim|required');

        $this->form_validation->set_error_delimiters('<span class="error">', '</span>');
    }

}

?>

<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class detail_pelatihan extends CI_Controller {

    private $tableName;

    function __construct() {
        parent::__construct();
        $this->load->helper('formutil');
        $this->load->model('detail_pelatihan_model', '', TRUE);
        $this->tableName = 'detail_pelatihan';
    }

    function index() {
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		$islogin = ' <li><a href="'.base_url().'login/logout"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>';
        $config['base_url'] = site_url('manage/detail_pelatihan/index/');
        $config['total_rows'] = detail_pelatihan_model::count_all($this->tableName);
        $config['per_page'] = 10;
        $config['num_links'] = 5;
        $config['uri_segment'] = 4;
        $config['use_page_numbers'] = true;
        $config['full_tag_open']    = '<ul class="pagination pagination-sm">';
        $config['full_tag_close']   = '</ul>';
        $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['first_tag_open']   = '<li>';
        $config['first_tag_close']  = '</li>';
        $config['prev_link']        = '&laquo';
        $config['prev_tag_open']    = '<li class="prev">';
        $config['prev_tag_close']   = '</li>';
        $config['next_link']        = '&raquo';
        $config['next_tag_open']    = '<li>';
        $config['next_tag_close']   = '</li>';
        $config['last_tag_open']    = '<li>';
        $config['last_tag_close']   = '</li>';
        $config['cur_tag_open']     = '<li class="active"><a href="">';
        $config['cur_tag_close']    = '</a></li>';
        $config['num_tag_open']     = '<li>';
        $config['num_tag_close']    = '</li>';
		$config['use_page_numbers'] = FALSE;

        $this->pagination->initialize($config);

        $offset = ($this->uri->segment(4) && preg_match("/[0-9]/",$this->uri->segment(4))) ? $this->uri->segment(4) : 0;

        $data = detail_pelatihan_model::get_paged_list($this->tableName,$config['per_page'], $offset)->result();

        $this->load->library('table');
		$tabletemp['table_open'] = '<table class="table table-condensed table-striped struktur_org">';
		$this->table->set_template($tabletemp);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading('NO', 'POGRAM', 'URUTAN', 'TAHAP', 'STATUS', '');

        $i = $offset;
		$a = $i+1;

        foreach ($data as $dt) {
            $this->table->add_row(
					$a,
					$dt->program_pelatihan,
					$dt->urutan,
					$dt->tahap,
					$dt->status,
                    anchor('manage/detail_pelatihan/detail/' . $dt->id_detail_pelatihan, 'Detail', array('class' => 'btn btn-success btn-xs')).' '.
					anchor('manage/detail_pelatihan/edit/' . $dt->id_detail_pelatihan, 'Edit', array('class' => 'btn btn-warning btn-xs')).' '.
					anchor('manage/detail_pelatihan/delete/' . $dt->id_detail_pelatihan, 'Hapus', array('class' => 'btn btn-danger btn-xs', 'onclick' =>"return confirm('apakah anda yakin ingin menghapus data ini?')"))
			);
			$a++;
        }

		$add_btn = '<a class="btn btn-primary" href="'.base_url().'manage/detail_pelatihan/add" type="button">
						<span class="glyphicon glyphicon-plus"></span>Tambah Data
				    </a>';

		$pagedata = array(
            'title' => 'Kelola Data Detil Pelatihan | Data Detil Pelatihan',
            'title_menu' => 'Kelola Data Detil Pelatihan',
			'menu' => 'Data Master',
			'islogin' => $islogin,
			'submenu' => 'Data Detil Pelatihan',
            'table' => $this->table->generate(),
            'pagination' => $this->pagination->create_links(),
            'add_btn' => $add_btn,
			'print_btn' => '',
			'message' => $this->session->flashdata('message')
        	);

        $this->parser->parse('manage/main_adm', $pagedata);
    }

	function add(){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		$this->_set_fields();
        $this->_set_rules();

		$contentVars = array(
			'message' => $this->session->flashdata('message'),
			'base_url' => base_url(),
			'action' => site_url('manage/detail_pelatihan/doAdd'),
			'combo_pelatihan' => combo_pelatihan('pelatihan', '', 'class="form-control"'),
			'combo_tahap' => combo_tahap('tahap', '', 'class="form-control"'),
			'combo_urutan' => combo_urutan('urutan', '', 'class="form-control"'),
			'combo_tahun' => combo_tahun('tahun', '', 'class="form-control"'),
			'combo_asrama' => combo_asrama('asrama', '', 'class="form-control"'),
			'radio_status' => radioStatusPelatihan('status'),
			'back_link' => base_url().'manage/detail_pelatihan'
		);

		$pagedata = array(
            'title' => 'Kelola Data Detail Pelatihan | Data Detail Pelatihan',
            'title_menu' => 'Kelola Data Detail Pelatihan',
			'menu' => 'Data Master',
			'submenu' => 'Tambah Data Detail Pelatihan',
			'content' => $this->parser->parse('manage/detail_pelatihan/detail_pelatihan_form', $contentVars, TRUE)
        );

		 $this->parser->parse('manage/main_adm_form', $pagedata);
	}


	function doAdd(){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		$this->_set_fields();
        $this->_set_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('message', 'Field tidak boleh kosong');
			redirect('manage/detail_pelatihan/add');
        } else {

			$detail_pelatihan = array(
				'id_pelatihan' => $this->input->post('pelatihan'),
				'program_pelatihan' => $this->input->post('program'),
				'tahun' => $this->input->post('tahun'),
				'urutan' => $this->input->post('urutan'),
				'tahap' => $this->input->post('tahap'),
				'status' => $this->input->post('status'),
				'asrama' => $this->input->post('asrama'),
				'tgl_pendaftaran_mulai' => $this->input->post('start_daftar'),
				'tgl_pendaftaran_selesai' => $this->input->post('end_daftar'),
				'tgl_rekrutmen' => $this->input->post('tgl_rekrut'),
				'tgl_pengumuman' => $this->input->post('tgl_pengumuman'),
				'tgl_daftar_ulang' => $this->input->post('tgl_daftar_ulang'),
				'tgl_mulai' => $this->input->post('tgl_mulai'),
				'tgl_selesai' => $this->input->post('tgl_selesai'),
				'tgl_ujk' => $this->input->post('tgl_ujk'),
			);

			$doAdd = detail_pelatihan_model::save($this->tableName, $detail_pelatihan);

			if($doAdd){
				$this->session->set_flashdata('message', 'Data berhasil di tambahkan');
				redirect('manage/detail_pelatihan');
			}else{
				$this->session->set_flashdata('message', 'sedang ada pemeliharaan sistem');
				redirect('manage/detail_pelatihan/add');
			}

        }
	}


	function detail($id){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

        $this->_set_rules();

        $data = detail_pelatihan_model::get_by_id($this->tableName,$id);

		$this->form_data = new stdClass;
        $this->form_data->id = $data->id_detail_pelatihan;
		$this->form_data->program = $data->program_pelatihan;
        $this->form_data->start_daftar = $data->tgl_pendaftaran_mulai;
		$this->form_data->end_daftar = $data->tgl_pendaftaran_selesai;
		$this->form_data->tgl_rekrut = $data->tgl_rekrutmen;
		$this->form_data->tgl_pengumuman = $data->tgl_pengumuman;
		$this->form_data->tgl_daftar_ulang = $data->tgl_daftar_ulang;
		$this->form_data->tgl_mulai = $data->tgl_mulai;
		$this->form_data->tgl_selesai = $data->tgl_selesai;
		$this->form_data->tgl_ujk = $data->tgl_ujk;

		$contentVars = array(
			'message' => $this->session->flashdata('message'),
			'base_url' => base_url(),
			'action' => site_url('manage/detail_pelatihan/doEdit/'.$id),
			'combo_pelatihan' => combo_pelatihan('pelatihan', $data->id_pelatihan , 'class="form-control" disabled'),
			'combo_tahap' => combo_tahap('tahap', $data->tahap, 'class="form-control" disabled'),
			'combo_urutan' => combo_urutan('urutan', $data->urutan, 'class="form-control" disabled'),
			'combo_tahun' => combo_tahun('tahun', $data->tahun, 'class="form-control" disabled'),
			'combo_asrama' => combo_asrama('asrama', $data->asrama, 'class="form-control" disabled'),
			'radio_status' => radioStatusPelatihan('status', $data->status, 'disabled'),
			'back_link' => base_url().'manage/detail_pelatihan'
		);

		$pagedata = array(
            'title' => 'Kelola Data Detail Pelatihan | Detail Data Detail Pelatihan',
            'title_menu' => 'Kelola Data Detail Pelatihan',
			'menu' => 'Data Master',
			'submenu' => 'Detail Data Detail Pelatihan',
			'content' => $this->parser->parse('manage/detail_pelatihan/detail_pelatihan_form_detail', $contentVars, TRUE)
        );

		 $this->parser->parse('manage/main_adm_form', $pagedata);
	}


	function edit($id){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

        $this->_set_rules();

        $data = detail_pelatihan_model::get_by_id($this->tableName,$id);

		$this->form_data = new stdClass;
        $this->form_data->id = $data->id_detail_pelatihan;
		$this->form_data->program = $data->program_pelatihan;
        $this->form_data->start_daftar = $data->tgl_pendaftaran_mulai;
		$this->form_data->end_daftar = $data->tgl_pendaftaran_selesai;
		$this->form_data->tgl_rekrut = $data->tgl_rekrutmen;
		$this->form_data->tgl_pengumuman = $data->tgl_pengumuman;
		$this->form_data->tgl_daftar_ulang = $data->tgl_daftar_ulang;
		$this->form_data->tgl_mulai = $data->tgl_mulai;
		$this->form_data->tgl_selesai = $data->tgl_selesai;
		$this->form_data->tgl_ujk = $data->tgl_ujk;

		$contentVars = array(
			'message' => $this->session->flashdata('message'),
			'base_url' => base_url(),
			'action' => site_url('manage/detail_pelatihan/doEdit/'.$id),
			'combo_pelatihan' => combo_pelatihan('pelatihan', $data->id_pelatihan , 'class="form-control"'),
			'combo_tahap' => combo_tahap('tahap', $data->tahap, 'class="form-control"'),
			'combo_urutan' => combo_urutan('urutan', $data->urutan, 'class="form-control"'),
			'combo_tahun' => combo_tahun('tahun', $data->tahun, 'class="form-control"'),
			'combo_asrama' => combo_asrama('asrama', $data->asrama, 'class="form-control"'),
			'radio_status' => radioStatusPelatihan('status', $data->status),
			'back_link' => base_url().'manage/detail_pelatihan'
		);

		$pagedata = array(
            'title' => 'Kelola Data Detail Pelatihan | Edit Data Detail Pelatihan',
            'title_menu' => 'Kelola Data Detail Pelatihan',
			'menu' => 'Data Master',
			'submenu' => 'Edit Data Detail Pelatihan',
			'content' => $this->parser->parse('manage/detail_pelatihan/detail_pelatihan_form_edit', $contentVars, TRUE)
        );

		 $this->parser->parse('manage/main_adm_form', $pagedata);
	}


	function doEdit(){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

     	$this->_set_fields();
        $this->_set_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('message', 'Field tidak boleh kosong');
            redirect('manage/detail_pelatihan/edit/'.$id);
        } else {

			$detail_pelatihan = array(
				'id_pelatihan' => $this->input->post('pelatihan'),
				'program_pelatihan' => $this->input->post('program'),
				'tahun' => $this->input->post('tahun'),
				'urutan' => $this->input->post('urutan'),
				'tahap' => $this->input->post('tahap'),
				'status' => $this->input->post('status'),
				'asrama' => $this->input->post('asrama'),
				'tgl_pendaftaran_mulai' => $this->input->post('start_daftar'),
				'tgl_pendaftaran_selesai' => $this->input->post('end_daftar'),
				'tgl_rekrutmen' => $this->input->post('tgl_rekrut'),
				'tgl_pengumuman' => $this->input->post('tgl_pengumuman'),
				'tgl_daftar_ulang' => $this->input->post('tgl_daftar_ulang'),
				'tgl_mulai' => $this->input->post('tgl_mulai'),
				'tgl_selesai' => $this->input->post('tgl_selesai'),
				'tgl_ujk' => $this->input->post('tgl_ujk'),
			);

			$doUpdate = detail_pelatihan_model::update($this->tableName, $detail_pelatihan, $this->input->post('id'));
			if($doUpdate){
				$this->session->set_flashdata('message', 'Data berhasil di ubah');
				redirect('manage/detail_pelatihan');
			}else{
				$this->session->set_flashdata('message', 'sedang ada pemeliharaan sistem');
				redirect('manage/detail_pelatihan/edit/'.$id);
			}

        }
	}

	function delete($id){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		detail_pelatihan_model::delete($this->tableName, $id);
		$this->session->set_flashdata('message', 'Data berhasi dihapus');
        redirect('manage/detail_pelatihan');
	}

    function _set_fields() {
		$this->form_data = new stdClass;
		$this->form_data->program = '';
        $this->form_data->pelatihan = '';
    }

    function _set_rules() {
        $this->form_validation->set_rules('program', 'Program Pelatihan', 'trim|required');
        $this->form_validation->set_rules('pelatihan', 'Pelatihan', 'trim|required');

        $this->form_validation->set_error_delimiters('<span class="error">', '</span>');
    }

}

?>

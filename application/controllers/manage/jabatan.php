<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class jabatan extends CI_Controller {

    private $tableName;
	private $base;

    function __construct() {
        parent::__construct();
        $this->load->helper('formutil');
        $this->load->model('jabatan_model', '', TRUE);
        $this->tableName = 'jabatan';
		$this->base = 'manage/jabatan/';
		if ($this->session->userdata('currentmenu') != 'Master') {
             $this->session->set_userdata('currentmenu', 'Master');
        }
    }

    function index() {
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		$islogin = ' <li><a href="'.base_url().'login/logout"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>';

        $config['base_url'] = site_url('manage/jabatan/index/');
        $config['total_rows'] = jabatan_model::count_all($this->tableName);
        $config['per_page'] = 10;
        $config['num_links'] = 5;
        $config['uri_segment'] = 4;
        $config['use_page_numbers'] = true;
        $config['full_tag_open']    = '<ul class="pagination pagination-sm">';
        $config['full_tag_close']   = '</ul>';
        $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['first_tag_open']   = '<li>';
        $config['first_tag_close']  = '</li>';
        $config['prev_link']        = '&laquo';
        $config['prev_tag_open']    = '<li class="prev">';
        $config['prev_tag_close']   = '</li>';
        $config['next_link']        = '&raquo';
        $config['next_tag_open']    = '<li>';
        $config['next_tag_close']   = '</li>';
        $config['last_tag_open']    = '<li>';
        $config['last_tag_close']   = '</li>';
        $config['cur_tag_open']     = '<li class="active"><a href="">';
        $config['cur_tag_close']    = '</a></li>';
        $config['num_tag_open']     = '<li>';
        $config['num_tag_close']    = '</li>';
		$config['use_page_numbers'] = FALSE;

        $this->pagination->initialize($config);

        $offset = ($this->uri->segment(4) && preg_match("/[0-9]/",$this->uri->segment(4))) ? $this->uri->segment(4) : 0;

        $data = jabatan_model::get_paged_list($this->tableName,$config['per_page'], $offset)->result();

        $this->load->library('table');
		$tabletemp['table_open'] = '<table class="table table-condensed table-striped struktur_org">';
		$this->table->set_template($tabletemp);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading('NO', 'JABATAN', 'JOBDESK', '');

        $i = $offset;
		$a = $i+1;
        foreach ($data as $dt) {
            $this->table->add_row(
					$a,
					$dt->jabatan,
					$dt->jobdesc,
					anchor($this->base.'edit/' . $dt->id_jabatan, 'Edit', array('class' => 'btn btn-warning btn-xs')).' '.
					anchor($this->base.'delete/' . $dt->id_jabatan, 'Hapus', array('class' => 'btn btn-danger btn-xs', 'onclick' =>"return confirm('apakah anda yakin ingin menghapus data ini?')"))
			);
			$a++;
        }

		$add_btn = '<a class="btn btn-primary" href="'.base_url().'manage/jabatan/add" type="button">
						<span class="glyphicon glyphicon-plus"></span>Tambah Data
				    </a>';

		$pagedata = array(
            'title' => 'Kelola Data Jabatan | Data Jabatan',
            'title_menu' => 'Kelola Data jabatan',
			'menu' => 'Data Master',
			'islogin' => $islogin,
			'submenu' => 'Data Jabatan',
            'table' => $this->table->generate(),
            'pagination' => $this->pagination->create_links(),
            'add_btn' => $add_btn,
			'print_btn' => '',
			'message' => $this->session->flashdata('message')
        	);

        $this->parser->parse('manage/main_adm', $pagedata);
    }

	function add(){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		$this->_set_fields();
        $this->_set_rules();

		$contentVars = array(
			'message' => $this->session->flashdata('message'),
			'base_url' => base_url(),
			'action' => site_url('manage/jabatan/doAdd'),
			'back_link' => base_url().'manage/jabatan'
		);

		$pagedata = array(
            'title' => 'Kelola Data Jabatan | Tambah Data Jabatan',
            'title_menu' => 'Kelola Data Jabatan',
			'menu' => 'Data Master',
			'submenu' => 'Tambah Jabatan',
			'content' => $this->parser->parse('manage/jabatan/jabatan_form', $contentVars, TRUE)
        );

		$this->parser->parse('manage/main_adm_form', $pagedata);
	}


	function doAdd(){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		$this->_set_fields();
        $this->_set_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('message', 'Field tidak boleh kosong');
            redirect('manage/jabatan/add');
        } else {

			$data = array(
					'jabatan' => $this->input->post('jabatan'),
					'jobdesc' => $this->input->post('jobdes')
				);

			$doAdd = jabatan_model::save($this->tableName, $data);

			if($doAdd){
				$this->session->set_flashdata('message', 'Data berhasil di tambahkan');
				redirect('manage/jabatan');
			}else{
				$this->session->set_flashdata('message', 'sedang ada pemeliharaan sistem');
				redirect('manage/jabatan/add');
			}
        }
	}


	function edit($id) {
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

        $this->_set_rules();

        $data = jabatan_model::get_by_id($this->tableName,$id);

		$this->form_data = new stdClass;
		$this->form_data->jabatan = $data->jabatan;
        $this->form_data->jobdes = $data->jobdesc;
		$this->form_data->id = $data->id_jabatan;

		$contentVars = array(
			'message' => $this->session->flashdata('message'),
			'base_url' => base_url(),
			'action' => site_url('manage/jabatan/doEdit'),
			'back_link' => base_url().'manage/jabatan'
		);

		$pagedata = array(
            'title' => 'Kelola Data Jabatan | Edit Data Jabatan',
            'title_menu' => 'Kelola Data jabatan',
			'menu' => 'Data Master',
			'submenu' => 'Edit Jabatan',
			'content' => $this->parser->parse('manage/jabatan/jabatan_form_edit', $contentVars, TRUE)
        );

		$this->parser->parse('manage/main_adm_form', $pagedata);
    }


	function doEdit(){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		$this->_set_fields();
        $this->_set_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('message', 'Field tidak boleh kosong');
            redirect('manage/jabatan/edit/'.$this->input->post('id'));
        } else {

            $data = array(
                'jabatan' => $this->input->post('jabatan'),
                'jobdesc' => $this->input->post('jobdes'),
            );

            $doUpdate = jabatan_model::update($this->tableName, $data, $this->input->post('id'));

			if($doUpdate){
				$this->session->set_flashdata('message', 'Data berhasil di ubah');
                redirect('manage/jabatan');
            }else{
				$this->session->set_flashdata('message', 'sedang ada pemeliharaan sistem');
				redirect('manage/jabatan/edit/'.$this->input->post('id'));
            }

        }
	}

	function delete($id){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		jabatan_model::delete($this->tableName, $id);
		$this->session->set_flashdata('message', 'Data berhasi dihapus');
        redirect('manage/jabatan');
	}

    function _set_fields() {
		$this->form_data = new stdClass;
		$this->form_data->jabatan = '';
		$this->form_data->jobdes = '';
    }

    function _set_rules() {
        $this->form_validation->set_rules('jabatan', 'Jabatan', 'trim|required');
		$this->form_validation->set_rules('jobdes', 'Pangkat', 'trim|required');

        $this->form_validation->set_error_delimiters('<span class="error">', '</span>');
    }

}

?>

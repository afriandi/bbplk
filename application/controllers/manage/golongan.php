<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class golongan extends CI_Controller {

    private $tableName;
	private $base;

    function __construct() {
        parent::__construct();
        $this->load->helper('formutil');
        $this->load->model('golongan_model', '', TRUE);
        $this->tableName = 'golongan';
		$this->base = 'manage/golongan/';
		if ($this->session->userdata('currentmenu') != 'Master') {
             $this->session->set_userdata('currentmenu', 'Master');
        }
    }

    function index() {
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		$islogin = ' <li><a href="'.base_url().'login/logout"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>';

        $config['base_url'] = site_url('manage/golongan/index/');
        $config['total_rows'] = golongan_model::count_all($this->tableName);
        $config['per_page'] = 10;
        $config['num_links'] = 5;
        $config['uri_segment'] = 4;
        $config['use_page_numbers'] = true;
        $config['full_tag_open']    = '<ul class="pagination pagination-sm">';
        $config['full_tag_close']   = '</ul>';
        $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['first_tag_open']   = '<li>';
        $config['first_tag_close']  = '</li>';
        $config['prev_link']        = '&laquo';
        $config['prev_tag_open']    = '<li class="prev">';
        $config['prev_tag_close']   = '</li>';
        $config['next_link']        = '&raquo';
        $config['next_tag_open']    = '<li>';
        $config['next_tag_close']   = '</li>';
        $config['last_tag_open']    = '<li>';
        $config['last_tag_close']   = '</li>';
        $config['cur_tag_open']     = '<li class="active"><a href="">';
        $config['cur_tag_close']    = '</a></li>';
        $config['num_tag_open']     = '<li>';
        $config['num_tag_close']    = '</li>';
		$config['use_page_numbers'] = FALSE;
        // $config['prev_link'] = '&lt;';
        // $config['next_link'] = '&gt;';
        // $config['first_link'] = '&laquo;';
        // $config['last_link'] = '&raquo;';
        // $config['cur_tag_open'] = '<span class="pa">';
        // $config['cur_tag_close'] = '</span>';
        // $config['full_tag_open'] = '<div class="pagging">';
        // $config['full_tag_close'] = '</div>';
        // $config['use_page_numbers'] = FALSE;

        $this->pagination->initialize($config);

        $offset = ($this->uri->segment(4) && preg_match("/[0-9]/",$this->uri->segment(4))) ? $this->uri->segment(4) : 0;

        $data = golongan_model::get_paged_list($this->tableName,$config['per_page'], $offset)->result();

        $this->load->library('table');
		$tabletemp['table_open'] = '<table class="table table-condensed table-striped struktur_org">';
		$this->table->set_template($tabletemp);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading('NO', 'GOLONGAN', 'PANGKAT', '');

        $i = $offset;
		$a = $i+1;
        foreach ($data as $dt) {
            $this->table->add_row(
					$a,
					$dt->golongan,
					$dt->pangkat,
					anchor($this->base.'edit/' . $dt->id_golongan, 'Edit', array('class' => 'btn btn-warning btn-xs')).' '.
					anchor($this->base.'delete/' . $dt->id_golongan, 'Hapus', array('class' => 'btn btn-danger btn-xs', 'onclick' =>"return confirm('apakah anda yakin ingin menghapus data ini?')"))
			);
			$a++;
        }

		$add_btn = '<a class="btn btn-primary" href="'.base_url().'manage/golongan/add" type="button">
						<span class="glyphicon glyphicon-plus"></span>Tambah Data
				    </a>';

		$pagedata = array(
            'title' => 'Kelola Data Golongan | Data Golongan',
            'title_menu' => 'Kelola Data Golongan',
			'menu' => 'Data Master',
			'islogin' => $islogin,
			'submenu' => 'Data Golongan',
            'table' => $this->table->generate(),
            'pagination' => $this->pagination->create_links(),
            'add_btn' => $add_btn,
			'print_btn' => '',
			'message' => $this->session->flashdata('message')
        	);

        $this->parser->parse('manage/main_adm', $pagedata);
    }

	function add(){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		$this->_set_fields();
        $this->_set_rules();

		$contentVars = array(
			'message' => $this->session->flashdata('message'),
			'base_url' => base_url(),
			'action' => site_url('manage/golongan/doAdd'),
			'back_link' => base_url().'manage/golongan'
		);

		$pagedata = array(
            'title' => 'Kelola Data Golongan | Tambah Data Golongan',
            'title_menu' => 'Kelola Data Golongan',
			'menu' => 'Data Master',
			'submenu' => 'Tambah Golongan',
			'content' => $this->parser->parse('manage/golongan/golongan_form', $contentVars, TRUE)
        );

		$this->parser->parse('manage/main_adm_form', $pagedata);
	}


	function doAdd(){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		$this->_set_fields();
        $this->_set_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('message', 'Field tidak boleh kosong');
            redirect('manage/golongan/add');
        } else {

			$data = array(
					'golongan' => $this->input->post('gol'),
					'pangkat' => $this->input->post('pangkat')
				);

			$doAdd = golongan_model::save($this->tableName, $data);

			if($doAdd){
				$this->session->set_flashdata('message', 'Data berhasil di tambahkan');
				redirect('manage/golongan');
			}else{
				$this->session->set_flashdata('message', 'sedang ada pemeliharaan sistem');
				redirect('manage/golongan/add');
			}
        }
	}


	function edit($id) {
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

        $this->_set_rules();

        $data = golongan_model::get_by_id($this->tableName,$id);

		$this->form_data = new stdClass;
		$this->form_data->gol = $data->golongan;
        $this->form_data->pangkat = $data->pangkat;
		$this->form_data->id = $data->id_golongan;

		$contentVars = array(
			'message' => $this->session->flashdata('message'),
			'base_url' => base_url(),
			'action' => site_url('manage/golongan/doEdit'),
			'combo_kejuruan' => combo_kejuruan('kejuruan',  $data->id_golongan, 'class="form-control"'),
			'back_link' => base_url().'manage/golongan'
		);

		$pagedata = array(
            'title' => 'Kelola Data Golongan | Edit Data Golongan',
            'title_menu' => 'Kelola Data Golongan',
			'menu' => 'Data Master',
			'submenu' => 'Edit Golongan',
			'content' => $this->parser->parse('manage/golongan/golongan_form_edit', $contentVars, TRUE)
        );

		$this->parser->parse('manage/main_adm_form', $pagedata);
    }


	function doEdit(){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		$this->_set_fields();
        $this->_set_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('message', 'Field tidak boleh kosong');
            redirect('manage/golongan/edit/'.$this->input->post('id'));
        } else {

            $data = array(
                'golongan' => $this->input->post('gol'),
                'pangkat' => $this->input->post('pangkat'),
            );

            $doUpdate = golongan_model::update($this->tableName, $data, $this->input->post('id'));

			if($doUpdate){
				$this->session->set_flashdata('message', 'Data berhasil di ubah');
                redirect('manage/golongan');
            }else{
				$this->session->set_flashdata('message', 'sedang ada pemeliharaan sistem');
				redirect('manage/golongan/edit/'.$this->input->post('id'));
            }

        }
	}

	function delete($id){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		golongan_model::delete($this->tableName, $id);
		$this->session->set_flashdata('message', 'Data berhasil dihapus');
        redirect('manage/golongan');
	}

    function _set_fields() {
		$this->form_data = new stdClass;
		$this->form_data->gol = '';
		$this->form_data->pangkat = '';
    }

    function _set_rules() {
        $this->form_validation->set_rules('gol', 'Golongan', 'trim|required');
		$this->form_validation->set_rules('pangkat', 'Pangkat', 'trim|required');

        $this->form_validation->set_error_delimiters('<span class="error">', '</span>');
    }

}

?>

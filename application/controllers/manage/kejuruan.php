<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class kejuruan extends CI_Controller {

    private $tableName;
	private $base;

    function __construct() {
        parent::__construct();
        $this->load->helper('formutil');
        $this->load->model('kejuruan_model', '', TRUE);
        $this->tableName = 'kejuruan';
		$this->base = 'manage/kejuruan/';
		if ($this->session->userdata('currentmenu') != 'Input') {
             $this->session->set_userdata('currentmenu', 'Input');
        }
    }

    function index() {
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		$islogin = ' <li><a href="'.base_url().'login/logout"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>';

        $config['base_url'] = site_url('manage/kejuruan/index/');
        $config['total_rows'] = kejuruan_model::count_all($this->tableName);
        $config['per_page'] = 10;
        $config['num_links'] = 5;
        $config['uri_segment'] = 4;
        $config['use_page_numbers'] = true;
        $config['full_tag_open']    = '<ul class="pagination pagination-sm">';
        $config['full_tag_close']   = '</ul>';
        $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['first_tag_open']   = '<li>';
        $config['first_tag_close']  = '</li>';
        $config['prev_link']        = '&laquo';
        $config['prev_tag_open']    = '<li class="prev">';
        $config['prev_tag_close']   = '</li>';
        $config['next_link']        = '&raquo';
        $config['next_tag_open']    = '<li>';
        $config['next_tag_close']   = '</li>';
        $config['last_tag_open']    = '<li>';
        $config['last_tag_close']   = '</li>';
        $config['cur_tag_open']     = '<li class="active"><a href="">';
        $config['cur_tag_close']    = '</a></li>';
        $config['num_tag_open']     = '<li>';
        $config['num_tag_close']    = '</li>';
		$config['use_page_numbers'] = FALSE;

        $this->pagination->initialize($config);

        $offset = ($this->uri->segment(4) && preg_match("/[0-9]/",$this->uri->segment(4))) ? $this->uri->segment(4) : 0;

        $data = kejuruan_model::get_paged_list($this->tableName,$config['per_page'], $offset)->result();

        $this->load->library('table');
		$tabletemp['table_open'] = '<table class="table table-condensed table-striped struktur_org">';
		$this->table->set_template($tabletemp);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading('NO', 'KEJURUAN', 'DESKRIPSI', '');

        $i = $offset;
		$a = $i+1;
        foreach ($data as $dt) {
            $this->table->add_row(
					$a,
					// $dt->id_kejuruan,
					$dt->nama,
					substr($dt->deskripsi, 0, 75).' ...',
          anchor($this->base.'detail/' . $dt->id_kejuruan, 'Detail', array('class' => 'btn btn-success btn-xs')).' '.
					anchor($this->base.'edit/' . $dt->id_kejuruan, 'Edit', array('class' => 'btn btn-warning btn-xs')).' '.
					anchor($this->base.'delete/' . $dt->id_kejuruan, 'Hapus', array('class' => 'btn btn-danger btn-xs', 'onclick' =>"return confirm('apakah anda yakin ingin menghapus data ini?')"))
			);
			$a++;
        }

		$add_btn = '<a class="btn btn-primary" href="kejuruan/add" type="button">
						<span class="glyphicon glyphicon-plus"></span>Tambah Data
				    </a>';

		$pagedata = array(
            'title' => 'Kelola Data Kejuruan | Data Kejuruan',
            'title_menu' => 'Kelola Data Kejuruan',
			'menu' => 'Data Master',
			'islogin' => $islogin,
			'submenu' => 'Data Kejuruan',
            'table' => $this->table->generate(),
            'pagination' => $this->pagination->create_links(),
            'add_btn' => $add_btn,
			'print_btn' => '',
			'message' => $this->session->flashdata('message')
        	);

        $this->parser->parse('manage/main_adm', $pagedata);
    }

	function add(){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		$this->_set_fields();
        $this->_set_rules();

		$contentVars = array(
			'message' => $this->session->flashdata('message'),
			'base_url' => base_url(),
			'action' => site_url('manage/kejuruan/doAdd'),
			'radio_status' => radio_status('status'),
			'back_link' => base_url().'manage/kejuruan'
		);

		$pagedata = array(
            'title' => 'Kelola Data Kejuruan | Tambah Data Kejuruan',
            'title_menu' => 'Kelola Data Kejuruan',
			'menu' => 'Data Master',
			'submenu' => 'Tambah Kejuruan',
			'content' => $this->parser->parse('manage/kejuruan/kejuruan_form', $contentVars, TRUE)
        );

		$this->parser->parse('manage/main_adm_form', $pagedata);
	}


	function doAdd(){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		$this->_set_fields();
        $this->_set_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('message', 'Field tidak boleh kosong');
            redirect('manage/kejuruan/add');
        } else {

            $fileupload = $_FILES['photo'];
        	$path = 'assets/img/kejuruan/';

			$config['upload_path'] = $path;
            $config['allowed_types'] = '*';
            $config['file_name'] = $fileupload['name'];

            if (file_exists($path . $config['file_name'])) {
                unlink($path . $config['file_name']);
            }

			$this->load->library('upload', $config);

			$data = array(
					'id_kejuruan' => $this->input->post('id'),
					'nama' => $this->input->post('nama'),
					'deskripsi' => $this->input->post('desc'),
				);

			if($fileupload['name'] != null){
				$data['foto'] = $fileupload['name'];

				if (!$this->upload->do_upload('photo')) {
					$this->session->set_flashdata('message', 'Upload Foto Gagal');
					redirect('manage/kajuruan/add');
				}
			}

			$doAdd = kejuruan_model::save($this->tableName, $data);

			if($doAdd){
				$this->session->set_flashdata('message', 'Data berhasil di tambahkan');
				redirect('manage/kejuruan');
			}else{
				$this->session->set_flashdata('message', 'sedang ada pemeliharaan sistem');
				redirect('manage/kejuruan/add');
			}
        }
	}


	function detail($id) {
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

        $this->_set_rules();

        $data = kejuruan_model::get_by_id($this->tableName,$id);
		$this->form_data = new stdClass;
        $this->form_data->id = $data->id_kejuruan;
        $this->form_data->nama = $data->nama;
		$this->form_data->desc = $data->deskripsi;

		$contentVars = array(
			'message' => $this->session->flashdata('message'),
			'base_url' => base_url(),
			'action' => site_url('manage/kejuruan/doeEdit'),
			'kejuruan_img' => base_url().'assets/img/kejuruan/'.$data->foto,
			'back_link' => base_url().'manage/kejuruan'
		);


		$pagedata = array(
            'title' => 'Kelola Data Kejuruan | Detail Data Kejuruan',
            'title_menu' => 'Kelola Data Kejuruan',
			'menu' => 'Data Master',
			'submenu' => 'Detail Data Kejuruan',
			'content' => $this->parser->parse('manage/kejuruan/kejuruan_form_detail', $contentVars, TRUE)
        );

		 $this->parser->parse('manage/main_adm_form', $pagedata);
    }

	function edit($id) {
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

        $this->_set_rules();

        $data = kejuruan_model::get_by_id($this->tableName,$id);

		$this->form_data = new stdClass;
		$this->form_data->id = $data->id_kejuruan;
        $this->form_data->nama = $data->nama;
		$this->form_data->desc = $data->deskripsi;
		$this->form_data->img = $data->foto;

		$contentVars = array(
			'message' => $this->session->flashdata('message'),
			'base_url' => base_url(),
			'action' => site_url('manage/kejuruan/doEdit'),
			'kejuruan_img' => base_url().'assets/img/kejuruan/'.$data->foto,
			'back_link' => base_url().'manage/kejuruan'
		);

		$pagedata = array(
            'title' => 'Kelola Data Kejuruan | Edit Data Kejuruan',
            'title_menu' => 'Kelola Data Kejuruan',
			'menu' => 'Data Master',
			'submenu' => 'Edit Kejuruan',
			'content' => $this->parser->parse('manage/kejuruan/kejuruan_form_edit', $contentVars, TRUE)
        );

		$this->parser->parse('manage/main_adm_form', $pagedata);
    }


	function doEdit(){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		$this->_set_fields();
        $this->_set_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('message', 'Field tidak boleh kosong');
            redirect('manage/kejuruan/edit/'.$this->input->post('id'));
        } else {

			$fileupload = $_FILES['photo'];
        	$path = 'assets/img/kejuruan/';

			$config['upload_path'] = $path;
            $config['allowed_types'] = '*';
            $config['file_name'] = $fileupload['name'];

            if ($fileupload['name'] != null && file_exists($path . $this->input->post('img'))) {
				unlink($path . $this->input->post('img'));
			}

			$this->load->library('upload', $config);

            $data = array(
                'nama' => $this->input->post('nama'),
                'deskripsi' => $this->input->post('desc'),
            );

			if($fileupload['name'] != null){
				$data['foto'] = $fileupload['name'];

				if (!$this->upload->do_upload('photo')) {
					$this->session->set_flashdata('message', 'Data berhasil di edit Data pegawai tetapi Upload Foto Gagal');
					redirect('manage/kajuruan/edit/'.$id);
				}
			}

            $doUpdate = kejuruan_model::update($this->tableName, $data, $this->input->post('id'));

			if($doUpdate){
				$this->session->set_flashdata('message', 'Data berhasil di ubah');
                redirect('manage/kejuruan');
            }else{
				$this->session->set_flashdata('message', 'sedang ada pemeliharaan sistem');
				redirect('manage/kejuruan/edit/'.$this->input->post('id'));
            }

        }
	}

	function delete($id){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		kejuruan_model::delete($this->tableName, $id);
		$this->session->set_flashdata('message', 'Data berhasi dihapus');
        redirect('manage/kejuruan');
	}

    function _set_fields() {
		$this->form_data = new stdClass;
		$this->form_data->id = '';
		$this->form_data->nama = '';
    }

    function _set_rules() {
        $this->form_validation->set_rules('id', 'ID Kejuruan', 'trim|required');
		$this->form_validation->set_rules('nama', 'Nama Kejuruan', 'trim|required');

        $this->form_validation->set_error_delimiters('<span class="error">', '</span>');
    }

}

?>

<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class regulasi extends CI_Controller {

    private $tableName;
	private $base;

    function __construct() {
        parent::__construct();
        $this->load->helper('formutil');
        $this->load->model('regulasi_model', '', TRUE);
        $this->tableName = 'regulasi';
		$this->base = 'manage/regulasi/';
		if ($this->session->userdata('currentmenu') != 'Web') {
             $this->session->set_userdata('currentmenu', 'Web');
        }
    }

    function index() {
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		$islogin = ' <li><a href="'.base_url().'login/logout"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>';

        $config['base_url'] = site_url('manage/regulasi/index/');
        $config['total_rows'] = regulasi_model::count_all($this->tableName);
        $config['per_page'] = 10;
        $config['num_links'] = 5;
        $config['uri_segment'] = 4;
        $config['use_page_numbers'] = true;
        $config['full_tag_open']    = '<ul class="pagination pagination-sm">';
        $config['full_tag_close']   = '</ul>';
        $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['first_tag_open']   = '<li>';
        $config['first_tag_close']  = '</li>';
        $config['prev_link']        = '&laquo';
        $config['prev_tag_open']    = '<li class="prev">';
        $config['prev_tag_close']   = '</li>';
        $config['next_link']        = '&raquo';
        $config['next_tag_open']    = '<li>';
        $config['next_tag_close']   = '</li>';
        $config['last_tag_open']    = '<li>';
        $config['last_tag_close']   = '</li>';
        $config['cur_tag_open']     = '<li class="active"><a href="">';
        $config['cur_tag_close']    = '</a></li>';
        $config['num_tag_open']     = '<li>';
        $config['num_tag_close']    = '</li>';
		$config['use_page_numbers'] = FALSE;

        $this->pagination->initialize($config);

        $offset = ($this->uri->segment(4) && preg_match("/[0-9]/",$this->uri->segment(4))) ? $this->uri->segment(4) : 0;

        $data = regulasi_model::get_paged_list($this->tableName,$config['per_page'], $offset)->result();

        $this->load->library('table');
		$tabletemp['table_open'] = '<table class="table table-condensed table-striped struktur_org">';
		$this->table->set_template($tabletemp);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading('NO', 'REGULASI', 'DESKRIPSI', '');

        $i = $offset;
		$a = $i+1;
        foreach ($data as $dt) {
            $this->table->add_row(
					$a,
					$dt->nama,
					$dt->deskripsi,
					anchor($this->base.'edit/' . $dt->id_regulasi, 'Edit', array('class' => 'btn btn-warning btn-xs')).' '.
					anchor($this->base.'delete/' . $dt->id_regulasi, 'Hapus', array('class' => 'btn btn-danger btn-xs', 'onclick' =>"return confirm('apakah anda yakin ingin menghapus data ini?')"))
			);
			$a++;
        }

		$add_btn = '<a class="btn btn-primary" href="'.base_url().'manage/regulasi/add" type="button">
						<span class="glyphicon glyphicon-plus"></span>Tambah Data
				    </a>';

		$pagedata = array(
            'title' => 'Kelola Data Regulasi | Data Regulasi',
            'title_menu' => 'Kelola Data Regulasi',
			'menu' => 'Data Master',
			'islogin' => $islogin,
			'submenu' => 'Data Regulasi',
            'table' => $this->table->generate(),
            'pagination' => $this->pagination->create_links(),
            'add_btn' => $add_btn,
			'print_btn' => '',
			'message' => $this->session->flashdata('message')
        	);

        $this->parser->parse('manage/main_adm', $pagedata);
    }

	function add(){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		$this->_set_fields();
        $this->_set_rules();

		$contentVars = array(
			'message' => $this->session->flashdata('message'),
			'base_url' => base_url(),
			'action' => site_url('manage/regulasi/doAdd'),
			'back_link' => base_url().'manage/regulasi'
		);

		$pagedata = array(
            'title' => 'Kelola Data Regulasi | Tambah Data Regulasi',
            'title_menu' => 'Kelola Data Regulasi',
			'menu' => 'Data Master',
			'submenu' => 'Tambah Regulasi',
			'content' => $this->parser->parse('manage/regulasi/regulasi_form', $contentVars, TRUE)
        );

		$this->parser->parse('manage/main_adm_form', $pagedata);
	}


	function doAdd(){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		$this->_set_fields();
        $this->_set_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('message', 'Field tidak boleh kosong');
            redirect('manage/regulasi/add');
        } else {

			$fileupload = $_FILES['photo'];
        	$path = 'assets/file/regulasi/';

			$config['upload_path'] = $path;
            $config['allowed_types'] = '*';
            $config['file_name'] = $fileupload['name'];

            if (file_exists($path . $config['file_name'])) {
                unlink($path . $config['file_name']);
            }

			$this->load->library('upload', $config);
			if (!$this->upload->do_upload('photo')) {
               $this->session->set_flashdata('message', 'Upload File Gagal Silahkan ulangi kembali');
			   redirect('manage/galeri/add');
			} else {
				$data = array(
					'nama' => $this->input->post('nama'),
					'deskripsi' => $this->input->post('desc'),
					'file' => $config['file_name'],
				);

				$doAdd = regulasi_model::save($this->tableName, $data);
	
				if($doAdd){
					$this->session->set_flashdata('message', 'Data berhasil di tambahkan');
					redirect('manage/regulasi');
				}else{
					$this->session->set_flashdata('message', 'sedang ada pemeliharaan sistem');
					redirect('manage/regulasi/add');
				}
			}
        }
	}


	function edit($id) {
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

        $this->_set_rules();

        $data = regulasi_model::get_by_id($this->tableName,$id);

		$this->form_data = new stdClass;
		$this->form_data->nama = $data->nama;
        $this->form_data->desc = $data->deskripsi;
		$this->form_data->img = $data->file;
		$this->form_data->id = $data->id_regulasi;

		$contentVars = array(
			'message' => $this->session->flashdata('message'),
			'base_url' => base_url(),
			'action' => site_url('manage/regulasi/doEdit'),
			'back_link' => base_url().'manage/regulasi',
			'regulasi_file' => $data->file
		);

		$pagedata = array(
            'title' => 'Kelola Data Regulasi | Edit Data Regulasi',
            'title_menu' => 'Kelola Data Regulasi',
			'menu' => 'Data Master',
			'submenu' => 'Edit Regulasi',
			'content' => $this->parser->parse('manage/regulasi/regulasi_form_edit', $contentVars, TRUE)
        );

		$this->parser->parse('manage/main_adm_form', $pagedata);
    }


	function doEdit(){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		$this->_set_fields();
        $this->_set_rules();
		
		$id = $this->input->post('id');
		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('message', 'Field tidak boleh kosong');
            redirect('manage/regulasi/edit/'.$this->input->post('id'));
        } else {

			$fileupload = $_FILES['photo'];
        	$path = 'assets/file/regulasi/';

			$config['upload_path'] = $path;
            $config['allowed_types'] = '*';
            $config['file_name'] = $fileupload['name'];

            if ($fileupload['name'] != null && file_exists($path . $this->input->post('img'))) {
				unlink($path . $this->input->post('img'));
			}

			$this->load->library('upload', $config);

            $data = array(
                'nama' => $this->input->post('nama'),
                'deskripsi' => $this->input->post('desc'),
            );
			
			if($fileupload['name'] != null){
				$data['file'] = $fileupload['name'];

				if (!$this->upload->do_upload('photo')) {
					$this->session->set_flashdata('message', 'Data berhasil di edit Data pegawai tetapi Upload Foto Gagal');
					redirect('manage/regulasi/edit/'.$id);
				}
			}

            $doUpdate = regulasi_model::update($this->tableName, $data, $this->input->post('id'));

			if($doUpdate){
				$this->session->set_flashdata('message', 'Data berhasil di ubah');
                redirect('manage/regulasi');
            }else{
				$this->session->set_flashdata('message', 'sedang ada pemeliharaan sistem');
				redirect('manage/regulasi/edit/'.$this->input->post('id'));
            }

        }
	}

	function delete($id){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }
		
		$data = regulasi_model::get_by_id($this->tableName,$id);
		$path = 'assets/file/regulasi/';

		if (file_exists($path . $data->file)) {
			unlink($path . $data->file);
		}

		regulasi_model::delete($this->tableName, $id);
		$this->session->set_flashdata('message', 'Data berhasi dihapus');
        redirect('manage/regulasi');
	}

    function _set_fields() {
		$this->form_data = new stdClass;
		$this->form_data->nama = '';
		$this->form_data->desc = '';
    }

    function _set_rules() {
        $this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		$this->form_validation->set_rules('desc', 'Deskripsi', 'trim|required');

        $this->form_validation->set_error_delimiters('<span class="error">', '</span>');
    }

}

?>

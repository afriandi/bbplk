<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Rekap_penempatan extends CI_Controller {

	  function __construct() {
        parent::__construct();
        $this->load->helper('simple_dom');
				$this->load->model('pelatihan_model');
    }

    public function index(){
			{
			$islogin = '<li><a href="'.base_url().'login"><span class="glyphicon glyphicon-lock"></span>Login</a></li>';
			if ($this->session->userdata('username')) {
					$islogin = ' <li><a href="'.base_url().'login/logout"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>';
			}

			$tableName = "detail_pelatihan";
			$id_oto = "oto";
			$id_tekman = "tekman";

			$total_oto = $this->pelatihan_model->count_pelatihan($tableName,$id_oto);
			$total_tekman = $this->pelatihan_model->count_pelatihan($tableName,$id_tekman);
			$total_siswa_oto = $total_oto * 16;
			$total_siswa_tekman = $total_tekman * 16;
			$total_program = $total_oto + $total_tekman;
			$total_siswa = $total_program * 16;

			$data_oto = $this->pelatihan_model->get_all($tableName,$id_oto)->result();
			$data = array();
			$total_mandiri_oto=0;
			$total_industri_oto=0;
			$i=0;
			foreach ($data_oto as $if) {
				$data[$i]['mandiri'] = $if->mandiri;
				$total_mandiri_oto=$total_mandiri_oto+$data[$i]['mandiri'];
				$data[$i]['industri'] = $if->industri;
				$total_industri_oto=$total_industri_oto+$data[$i]['industri'];
				$i++;
			}

			$data_tekman = $this->pelatihan_model->get_all($tableName,$id_tekman)->result();
			$data2 = array();
			$total_mandiri_tekman=0;
			$total_industri_tekman=0;
			$j=0;
			foreach ($data_tekman as $dt) {
				$data2[$j]['mandiri'] = $dt->mandiri;
				$total_mandiri_tekman=$total_mandiri_tekman+$data2[$j]['mandiri'];
				$data2[$j]['industri'] = $dt->industri;
				$total_industri_tekman=$total_industri_tekman+$data2[$j]['industri'];
				$j++;
			}

			$total_mandiri = $total_mandiri_oto + $total_mandiri_tekman;
			$total_industri = $total_industri_oto + $total_industri_tekman;
			$total_bekerja = $total_mandiri + $total_industri;
			$total_bekerja_oto = $total_mandiri_oto + $total_industri_oto;
			$total_bekerja_tekman = $total_mandiri_tekman + $total_industri_tekman;

			$contentVars = array(
				'base_url' => base_url(),
				'title_menu' => 'Rekap Penempatan',
				'menu' => 'Kelola Laporan',
				'submenu' =>  'Rekap Pelatihan',
				'total_oto'=> $total_oto,
				'total_tekman'=> $total_tekman,
				'total_siswa_oto'=> $total_siswa_oto,
				'total_siswa_tekman'=> $total_siswa_tekman,
				'total_program'=> $total_program,
				'total_siswa'=> $total_siswa,
				'mandiri_oto'=> $total_mandiri_oto,
				'industri_oto'=> $total_industri_oto,
				'mandiri_tekman'=> $total_mandiri_tekman,
				'industri_tekman'=> $total_industri_tekman,
				'total_mandiri'=> $total_mandiri,
				'total_industri'=> $total_industri,
				'total_bekerja'=> $total_bekerja,
				'total_bekerja_oto'=> $total_bekerja_oto,
				'total_bekerja_tekman'=> $total_bekerja_tekman,
			);

			$pagedata = array(
				'title' => ' >> Rekap Penempatan',
				'islogin' => $islogin,
				'content' => $this->parser->parse('manage/laporan/rekap_penempatan', $contentVars, TRUE)
			);
			$this->parser->parse('manage/main_rekap', $pagedata);
			}
		}
}
?>

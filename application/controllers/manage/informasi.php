<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Informasi extends CI_Controller {

    private $tableName;

    function __construct() {
        parent::__construct();
        $this->load->helper('formutil');
        $this->load->model('blk_model', '', TRUE);
        $this->tableName = 'blk';
		if ($this->session->userdata('currentmenu') != 'Web') {
             $this->session->set_userdata('currentmenu', 'Web');
        }
    }

    function index() {
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		$islogin = ' <li><a href="'.base_url().'login/logout"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>';

        $this->_set_rules();

        $data = blk_model::get_by_id($this->tableName,1);

		$this->form_data = new stdClass;
		$this->form_data->id = $data->id_blk;
		$this->form_data->nama = $data->nama;
		$this->form_data->info = $data->info;
		$this->form_data->tentang = $data->tentang;
		$this->form_data->visi = $data->visi;
		$this->form_data->misi = $data->misi;
		$this->form_data->img = $data->foto;

		$contentVars = array(
			'message' => $this->session->flashdata('message'),
			'base_url' => base_url(),
			'action' => site_url('manage/informasi/edit'),
			'img' => base_url().'assets/img/home/'.$data->foto,
			'back_link' => base_url().'manage/informasi'
		);

		$pagedata = array(
            'title' => 'Kelola Data Informasi | Edit Data Informasi',
            'title_menu' => 'Kelola Data Informasi',
			'menu' => 'Data Web',
			'submenu' => 'Edit Informasi',
			'islogin' => $islogin,
			'content' => $this->parser->parse('manage/informasi/informasi_form_detail', $contentVars, TRUE)
        );

		$this->parser->parse('manage/main_adm_form', $pagedata);
    }


	function edit() {
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		$islogin = ' <li><a href="'.base_url().'login/logout"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>';

        $this->_set_rules();

        $data = blk_model::get_by_id($this->tableName,1);
		$this->form_data = new stdClass;
		$this->form_data->id = $data->id_blk;
		$this->form_data->nama = $data->nama;
		$this->form_data->info = $data->info;
		$this->form_data->tentang = $data->tentang;
		$this->form_data->visi = $data->visi;
		$this->form_data->misi = $data->misi;
		$this->form_data->img = $data->foto;

		$contentVars = array(
			'message' => $this->session->flashdata('message'),
			'base_url' => base_url(),
			'action' => site_url('manage/informasi/doEdit'),
			'img' => base_url().'assets/img/home/'.$data->foto,
			'back_link' => base_url().'manage/informasi'
		);

		$pagedata = array(
            'title' => 'Kelola Data Informasi | Edit Data Informasi',
            'title_menu' => 'Kelola Data Informasi',
			'menu' => 'Data Web',
			'submenu' => 'Edit Informasi',
			'islogin' => $islogin,
			'content' => $this->parser->parse('manage/informasi/informasi_form_edit', $contentVars, TRUE)
        );

		$this->parser->parse('manage/main_adm_form', $pagedata);
    }


	function doEdit(){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		$this->_set_fields();
        $this->_set_rules();

		$id = $this->input->post('id');
		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('message', 'Field tidak boleh kosong');
            redirect('manage/informasi/edit');
        } else {

			$fileupload = $_FILES['photo'];
        	$path = 'assets/img/home/';

			$config['upload_path'] = $path;
            $config['allowed_types'] = '*';
            $config['file_name'] = $fileupload['name'];

            if ($fileupload['name'] != null && file_exists($path . $this->input->post('img'))) {
				unlink($path . $this->input->post('img'));
			}

			$this->load->library('upload', $config);

            $data = array(
					'nama' => $this->input->post('nama'),
					'info' => $this->input->post('info'),
					'tentang' => $this->input->post('tentang'),
					'visi' => $this->input->post('visi'),
					'misi' => $this->input->post('misi'),
				);

			if($fileupload['name'] != null){
				$data['foto'] = $fileupload['name'];

				if (!$this->upload->do_upload('photo')) {
					$this->session->set_flashdata('message', 'Upload Foto Gagal');
					redirect('manage/informasi/edit');
				}
			}

            $doUpdate = blk_model::update($this->tableName, $data, $id);

			if($doUpdate){
				$this->session->set_flashdata('message', 'Data berhasil di ubah');
                redirect('manage/informasi');
            }else{
				$this->session->set_flashdata('message', 'sedang ada pemeliharaan sistem');
				redirect('manage/informasi/edit');
            }

        }
	}

    function _set_fields() {
		$this->form_data = new stdClass;
		$this->form_data->nama = '';
		$this->form_data->info = '';
		$this->form_data->tentang = '';
		$this->form_data->visi = '';
		$this->form_data->misi = '';
    }

    function _set_rules() {
        $this->form_validation->set_rules('nama', 'Nama BLK', 'trim|required');
		$this->form_validation->set_rules('info', 'Info Home', 'trim|required');
		$this->form_validation->set_rules('tentang', 'Tentang', 'trim|required');
		$this->form_validation->set_rules('visi', 'Visi', 'trim|required');
		$this->form_validation->set_rules('misi', 'Misi', 'trim|required');

        $this->form_validation->set_error_delimiters('<span class="error">', '</span>');
    }

}

?>

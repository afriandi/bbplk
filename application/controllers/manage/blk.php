<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class blk extends CI_Controller {

    private $tableName ;

    function __construct() {
        parent::__construct();
        $this->load->helper('formutil');
        $this->load->model('blk_model', '', TRUE);
        $this->tableName = 'blk';
		if ($this->session->userdata('currentmenu') != 'Master') {
             $this->session->set_userdata('currentmenu', 'Master');
        }
    }

    function index() {
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		$islogin = ' <li><a href="'.base_url().'login/logout"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>';

        $config['base_url'] = site_url('adm_blk/index/');
        $config['total_rows'] = blk_model::count_all($this->tableName);
        $config['per_page'] = 10;
        $config['num_links'] = 5;
        $config['uri_segment'] = 4;
        $config['use_page_numbers'] = true;
        $config['full_tag_open']    = '<ul class="pagination pagination-sm">';
        $config['full_tag_close']   = '</ul>';
        $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['first_tag_open']   = '<li>';
        $config['first_tag_close']  = '</li>';
        $config['prev_link']        = '&laquo';
        $config['prev_tag_open']    = '<li class="prev">';
        $config['prev_tag_close']   = '</li>';
        $config['next_link']        = '&raquo';
        $config['next_tag_open']    = '<li>';
        $config['next_tag_close']   = '</li>';
        $config['last_tag_open']    = '<li>';
        $config['last_tag_close']   = '</li>';
        $config['cur_tag_open']     = '<li class="active"><a href="">';
        $config['cur_tag_close']    = '</a></li>';
        $config['num_tag_open']     = '<li>';
        $config['num_tag_close']    = '</li>';
		$config['use_page_numbers'] = FALSE;

        $this->pagination->initialize($config);

        $offset = ($this->uri->segment(4) && preg_match("/[0-9]/",$this->uri->segment(4))) ? $this->uri->segment(4) : 0;

        $blks = blk_model::get_paged_list($this->tableName,$config['per_page'], $offset)->result();

        $this->load->library('table');
		$tabletemp['table_open'] = '<table class="table table-condensed table-striped struktur_org">';
		$this->table->set_template($tabletemp);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading('NAMA', 'ALAMAT', 'NO TELEPON', '');

        $i = $offset;
        foreach ($blks as $blk) {
            $this->table->add_row(
					$blk->nama,
					$blk->alamat,
					$blk->no_tlp,
                    anchor('manage/blk/detail/' . $blk->id_blk, 'detail', array('class' => 'btn btn-success btn-xs'))
            );
        }

		$pagedata = array(
            'title' => 'Kelola Data BLK | Data BLK',
            'title_menu' => 'Kelola Data BLK',
			'menu' => 'Data Master',
			'islogin' => $islogin,
			'submenu' => 'Data BLK',
            'table' => $this->table->generate(),
            'pagination' => $this->pagination->create_links(),
			'add_btn' => '',
			'print_btn' => '',
			'message' => $this->session->flashdata('message')
        	);


        $this->parser->parse('manage/main_adm', $pagedata);
    }


	function detail($id) {
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

        $this->_set_rules();

        $data = blk_model::get_by_id($this->tableName,$id);

		$this->form_data = new stdClass;
        $this->form_data->id = $id;
        $this->form_data->nama = $data->nama;
		$this->form_data->alamat = $data->alamat;
        $this->form_data->telp = $data->no_tlp;
		$this->form_data->email = $data->email;
		$this->form_data->jam = $data->jam_kerja;
		$this->form_data->fb = $data->facebook;
		$this->form_data->tw = $data->twitter;
		$this->form_data->in = $data->instagram;

		$contentVars = array(
			'message' => $this->session->flashdata('message'),
			'base_url' => base_url(),
			'action' => site_url('manage/blk/doeEdit'),
			'back_link' => base_url().'manage/blk'
		);

		$pagedata = array(
            'title' => 'Kelola Data Pegawai | Detail Data Pegawai',
            'title_menu' => 'Kelola Data BLK',
			'menu' => 'Data Master',
			'submenu' => 'Detail Data BLK',
			'content' => $this->parser->parse('manage/blk/blk_form_detail', $contentVars, TRUE)
        );

		$this->parser->parse('manage/main_adm_form', $pagedata);
    }


    // set empty default form field values
    function _set_fields() {
        $this->form_data->id_blk = '';

        $this->form_data->nama = '';
        $this->form_data->no_tlp = '';
        $this->form_data->email = '';
		$this->form_data->jam_kerja = '';
		$this->form_data->facebook = '';
		$this->form_data->twiter = '';
		$this->form_data->instagram = '';
    }

    // validation rules
    function _set_rules() {
        $this->form_validation->set_rules('nama', 'Nama BLK', 'trim|required');
        $this->form_validation->set_rules('no_telp', 'No Telp', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required');


        $this->form_validation->set_error_delimiters('<span class="error">', '</span>');
    }



}

?>

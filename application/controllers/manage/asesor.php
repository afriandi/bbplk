<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class asesor extends CI_Controller {

    private $tableName;
	private $base;

    function __construct() {
        parent::__construct();
        $this->load->helper('formutil');
        $this->load->model('asesor_model', '', TRUE);
        $this->tableName = 'asesor';
		$this->base = 'manage/asesor/';
		if ($this->session->userdata('currentmenu') != 'Input') {
             $this->session->set_userdata('currentmenu', 'Input');
        }
    }

    function index() {
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		$islogin = ' <li><a href="'.base_url().'login/logout"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>';

        $config['base_url'] = site_url('manage/asesor/index/');
        $config['total_rows'] = asesor_model::count_all($this->tableName);
        $config['per_page'] = 10;
        $config['num_links'] = 5;
        $config['uri_segment'] = 4;
        $config['use_page_numbers'] = true;
        $config['full_tag_open']    = '<ul class="pagination pagination-sm">';
        $config['full_tag_close']   = '</ul>';
        $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['first_tag_open']   = '<li>';
        $config['first_tag_close']  = '</li>';
        $config['prev_link']        = '&laquo';
        $config['prev_tag_open']    = '<li class="prev">';
        $config['prev_tag_close']   = '</li>';
        $config['next_link']        = '&raquo';
        $config['next_tag_open']    = '<li>';
        $config['next_tag_close']   = '</li>';
        $config['last_tag_open']    = '<li>';
        $config['last_tag_close']   = '</li>';
        $config['cur_tag_open']     = '<li class="active"><a href="">';
        $config['cur_tag_close']    = '</a></li>';
        $config['num_tag_open']     = '<li>';
        $config['num_tag_close']    = '</li>';
		$config['use_page_numbers'] = FALSE;

        $this->pagination->initialize($config);

        $offset = ($this->uri->segment(4) && preg_match("/[0-9]/",$this->uri->segment(4))) ? $this->uri->segment(4) : 0;

        $data = asesor_model::get_paged_list_all($this->tableName,$config['per_page'], $offset)->result();

        $this->load->library('table');
		$tabletemp['table_open'] = '<table class="table table-condensed table-striped struktur_org">';
		$this->table->set_template($tabletemp);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading('NO', 'NAMA', 'NIP', 'NO MET', 'NO LSP', 'TGL EXP','');

        $i = $offset;
		$a = $i+1;
        foreach ($data as $dt) {
            $this->table->add_row(
					$a,
					$dt->nama,
					$dt->nip,
					$dt->no_met,
					$dt->no_lsp,
					$dt->tgl_exp,
                    anchor($this->base.'detail/' . $dt->id_asesor, 'Detail', array('class' => 'btn btn-success btn-xs')).' '.
					anchor($this->base.'edit/' . $dt->id_asesor, 'Edit', array('class' => 'btn btn-warning btn-xs')).' '.
					anchor($this->base.'delete/' . $dt->id_asesor, 'Hapus', array('class' => 'btn btn-danger btn-xs', 'onclick' =>"return confirm('apakah anda yakin ingin menghapus data ini?')"))
			);
			$a++;
        }

		$add_btn = '<a class="btn btn-primary" href="'.base_url().'manage/asesor/add" type="button">
						<span class="glyphicon glyphicon-plus"></span>Tambah Data
				    </a>';

		$pagedata = array(
            'title' => 'Kelola Data asesor | Data asesor',
            'title_menu' => 'Kelola Data asesor',
			'menu' => 'Data Master',
			'islogin' => $islogin,
			'submenu' => 'Data asesor',
            'table' => $this->table->generate(),
            'pagination' => $this->pagination->create_links(),
            'add_btn' => $add_btn,
			'print_btn' => '',
			'message' => $this->session->flashdata('message')
        	);

        $this->parser->parse('manage/main_adm', $pagedata);
    }

	function add(){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		$this->_set_fields();
        $this->_set_rules();

		$contentVars = array(
			'message' => $this->session->flashdata('message'),
			'base_url' => base_url(),
			'action' => site_url('manage/asesor/doAdd'),
			'datalist_pegawai' => datalist_pegawai('dtpeg'),
			'back_link' => base_url().'manage/asesor'
		);

		$pagedata = array(
            'title' => 'Kelola Data Asesor | Tambah Data Asesor',
            'title_menu' => 'Kelola Data Asesor',
			'menu' => 'Data Master',
			'submenu' => 'Tambah Asesor',
			'content' => $this->parser->parse('manage/asesor/asesor_form', $contentVars, TRUE)
        );

		$this->parser->parse('manage/main_adm_form', $pagedata);
	}


	function doAdd(){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		$this->_set_fields();
        $this->_set_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('message', 'Field tidak boleh kosong');
            redirect('manage/asesor/add');
        } else {

			$data = array(
					'nip' => $this->input->post('nip'),
					'no_met' => $this->input->post('met'),
					'no_lsp' => $this->input->post('lsp'),
					'tgl_exp' => $this->input->post('exp'),
				);

			$doAdd = asesor_model::save($this->tableName, $data);

			if($doAdd){
				$this->session->set_flashdata('message', 'Data berhasil di tambahkan');
				redirect('manage/asesor');
			}else{
				$this->session->set_flashdata('message', 'sedang ada pemeliharaan sistem');
				redirect('manage/asesor/add');
			}
        }
	}


	function detail($id) {
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

        $this->_set_rules();

        $data = asesor_model::get_by_id($this->tableName,$id);
		$this->form_data = new stdClass;
        $this->form_data->id = $data->id_asesor;
		$this->form_data->nip = $data->nip;
		$this->form_data->nama = $data->nama;
		$this->form_data->tempat_lahir = $data->tempat_lahir;
		$this->form_data->tgl_lahir = $data->tgl_lahir;
		$this->form_data->tmt = $data->tmt;
		$this->form_data->met = $data->no_met;
		$this->form_data->lsp = $data->no_lsp;
		$this->form_data->exp = $data->tgl_exp;

		$contentVars = array(
			'message' => $this->session->flashdata('message'),
			'base_url' => base_url(),
			'action' => site_url('manage/asesor/doeEdit'),
			'back_link' => base_url().'manage/asesor',
			'photo' => base_url().'assets/img/pegawai/'.$data->foto,
			'combo_jabatan' => combo_jabatan('jabatan', $data->id_jabatan, 'class="form-control" disabled'),
			'combo_golongan' => combo_golongan('golongan', $data->id_golongan, 'class="form-control" disabled'),
			'combo_pendidikan' => combo_pendidikan('pendidikan', strtoupper($data->pendidikan), 'class="form-control" disabled'),
			'radio_gender' => radioGender('gender', $data->jk, 'disabled'),
		);


		$pagedata = array(
            'title' => 'Kelola Data Asesor | Detail Data Asesor',
            'title_menu' => 'Kelola Data Asesor',
			'menu' => 'Data Master',
			'submenu' => 'Detail Data Asesor',
			'content' => $this->parser->parse('manage/asesor/asesor_form_detail', $contentVars, TRUE)
        );

		 $this->parser->parse('manage/main_adm_form', $pagedata);
    }

	function edit($id) {
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

        $this->_set_rules();

        $data = asesor_model::get_by_id($this->tableName,$id);

		$this->form_data = new stdClass;
		$this->form_data->id = $data->id_asesor;
        $this->form_data->nip = $data->nip . ' ( '. $data->nama .' ) ';
		$this->form_data->met = $data->no_met;
		$this->form_data->lsp = $data->no_lsp;
		$this->form_data->exp = $data->tgl_exp;

		$contentVars = array(
			'message' => $this->session->flashdata('message'),
			'base_url' => base_url(),
			'action' => site_url('manage/asesor/doEdit'),
			'back_link' => base_url().'manage/asesor'
		);

		$pagedata = array(
            'title' => 'Kelola Data Asesor | Edit Data Asesor',
            'title_menu' => 'Kelola Data Asesor',
			'menu' => 'Data Master',
			'submenu' => 'Edit Asesor',
			'content' => $this->parser->parse('manage/asesor/asesor_form_edit', $contentVars, TRUE)
        );

		$this->parser->parse('manage/main_adm_form', $pagedata);
    }


	function doEdit(){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		$this->_set_fields();
        $this->_set_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('message', 'Field tidak boleh kosong');
            redirect('manage/asesor/edit/'.$this->input->post('id'));
        } else {

            $data = array(
					'nip' => $this->input->post('nip'),
					'no_met' => $this->input->post('met'),
					'no_lsp' => $this->input->post('lsp'),
					'tgl_exp' => $this->input->post('exp'),
				);

            $doUpdate = asesor_model::update($this->tableName, $data, $this->input->post('id'));

			if($doUpdate){
				$this->session->set_flashdata('message', 'Data berhasil di ubah');
                redirect('manage/asesor');
            }else{
				$this->session->set_flashdata('message', 'sedang ada pemeliharaan sistem');
				redirect('manage/asesor/edit/'.$this->input->post('id'));
            }

        }
	}

	function delete($id){
		if (!$this->session->userdata('username')) {
            redirect('login');
        }

		asesor_model::delete($this->tableName, $id);
		$this->session->set_flashdata('message', 'Data berhasi dihapus');
        redirect('manage/asesor');
	}

    function _set_fields() {
		$this->form_data = new stdClass;
		$this->form_data->nip = '';
    }

    function _set_rules() {
        $this->form_validation->set_rules('nip', 'NIP', 'trim|required');

        $this->form_validation->set_error_delimiters('<span class="error">', '</span>');
    }

}

?>

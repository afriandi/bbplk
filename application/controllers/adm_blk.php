<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of assistant
 *
 * @author afriandizen
 */
class adm_blk extends CI_Controller {

    private $tableName ;
    
    function __construct() {
        parent::__construct();
        $this->load->helper('formutil');
        $this->load->model('blk_model', '', TRUE);
        $this->tableName = 'blk';
    }

    function index() {
		if (!$this->session->userdata('username')) {
            redirect('login');
        }
		
		$islogin = ' <li><a href="'.base_url().'login/logout"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>';
        
        $config['base_url'] = site_url('adm_blk/index/');
        $config['total_rows'] = blk_model::count_all($this->tableName);
        $config['per_page'] = 10;
        $config['num_links'] = 5;
        $config['uri_segment'] = 4;
        $config['prev_link'] = '&lt;';
        $config['next_link'] = '&gt;';
        $config['first_link'] = '&laquo;';
        $config['last_link'] = '&raquo;';
        $config['cur_tag_open'] = '<span class="pa">';
        $config['cur_tag_close'] = '</span>';
        $config['full_tag_open'] = '<div class="pagging">';
        $config['full_tag_close'] = '</div>';
        $config['use_page_numbers'] = FALSE;
        
        $this->pagination->initialize($config);

        $offset = ($this->uri->segment(4) && preg_match("/[0-9]/",$this->uri->segment(4))) ? $this->uri->segment(4) : 0;
 
        $blks = blk_model::get_paged_list($this->tableName,$config['per_page'], $offset)->result();
  
        $this->load->library('table');
		$tabletemp['table_open'] = '<table class="table table-condensed table-striped struktur_org">';
		$this->table->set_template($tabletemp);
        $this->table->set_empty("&nbsp;");
        $this->table->set_heading('NAMA', 'ALAMAT', 'NO TELEPON', 'AKSI');
		
        $i = $offset;
        foreach ($blks as $blk) {
            $this->table->add_row(
					$blk->nama, 
					$blk->alamat, 
					$blk->no_tlp, 
                    anchor('adm_blk/detail/' . $blk->id_blk, 'detail', array('class' => 'btn btn-success btn-xs')) 
            );
        }

		$pagedata = array(
            'title' => 'Kelola Data BLK | Data BLK',
            'title_menu' => 'Kelola Data BLK',
			'menu' => 'Data Master',
			'islogin' => $islogin,
			'submenu' => 'Data BLK',
            'table' => $this->table->generate(),
            'pagination' => $this->pagination->create_links(),
			'add_btn' => '',
			'print_btn' => '',
			'message' => $this->session->flashdata('message')
        	);


        $this->parser->parse('main_adm', $pagedata);
    }
	
	
	function detail($id) {
		
       	$pagedata = array(
            'title' => 'Kelola Data BLK | Detail Data BLK',
            'title_menu' => 'Kelola Data BLK',
			'menu' => 'Data Master',
			'submenu' => 'Data BLK',
            'message' => '',
            'action' => '',
            'link_back' => anchor('adm_blk/index/', 'Back to list of User', array('class' => 'back')),
        );

        $this->_set_rules();

        $data = blk_model::get_by_id($this->tableName,$id);
		
		$this->form_data = new stdClass;
        $this->form_data->id_blk = $id;
        $this->form_data->nama = $data->nama;
		$this->form_data->alamat = $data->alamat;
        $this->form_data->no_tlp = $data->no_tlp;
		$this->form_data->email = $data->email;
		$this->form_data->jam_kerja = $data->jam_kerja;
		$this->form_data->facebook = $data->facebook;
		$this->form_data->twitter = $data->twitter;
		$this->form_data->instagram = $data->instagram;
        
		$contentVars = array(
			'message' => '',
			'base_url' => base_url()
		);
		
		$pagedata['content'] = $this->parser->parse('adm_blk_form', $contentVars, TRUE);
		
        $this->parser->parse('main_adm_form', $pagedata);
    }
	

    // set empty default form field values
    function _set_fields() {
        $this->form_data->id_blk = '';
        $this->form_data->nama = '';
        $this->form_data->no_tlp = '';
        $this->form_data->email = '';
		$this->form_data->jam_kerja = '';
		$this->form_data->facebook = '';
		$this->form_data->twiter = '';
		$this->form_data->instagram = '';
    }

    // validation rules
    function _set_rules() {
        $this->form_validation->set_rules('nama', 'Nama BLK', 'trim|required');
        $this->form_validation->set_rules('no_telp', 'No Telp', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required');
		
   
        $this->form_validation->set_error_delimiters('<span class="error">', '</span>');
    }

 

}

?>

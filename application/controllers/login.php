<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class login extends CI_Controller {

    private $tableName;

    function __construct() {
        parent::__construct();
        $this->load->helper('formutil');
        $this->load->model('users_model', '', TRUE);
        $this->tableName = 'users';
    }

    function index() {
		$islogin = '<li><a href="'.base_url().'login"><span class="glyphicon glyphicon-lock"></span>Login</a></li>';

		if ($this->session->userdata('username')) {
			$islogin = ' <li><a href="'.base_url().'login/logout"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>';
		}

        if (!$this->session->userdata('username')) {
			$contentVars = array(
				'title_menu' => 'Login',
				'menu' => 'Login',
				'message' => '',
				'action' => site_url('login/doLogin')
			);

			$pagedata = array(
				'title' => ' >> Login',
				'islogin' => $islogin,
				'content' => $this->parser->parse('login', $contentVars, TRUE)
			);

			$this->parser->parse('main', $pagedata);
        } else {
            redirect('manage/pegawai');
        }
    }

    function doLogin() {
		$islogin = '<li><a href="'.base_url().'login"><span class="glyphicon glyphicon-lock"></span>Login</a></li>';

		if ($this->session->userdata('username')) {
			$islogin = ' <li><a href="'.base_url().'logout"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>';
		}

        $contentVars = array
         	(
            'action' => site_url('login/doLogin'),
            'title_menu' => 'Login',
			'menu' => 'Login',
			'message' => ''
        );

        $this->_set_fields();
        $this->_set_rules();

        if ($this->form_validation->run() == FALSE) {
			redirect('login');
        } else {
            $user = $this->input->post('username');
            $pass = $this->input->post('password');

            $userWhere = array(
                $this->tableName.'.username' => $user,
                $this->tableName.'.password' => md5($pass),
				$this->tableName.'.status' => '1'
            );

            $userData = users_model::get_user_login($this->tableName, $userWhere);

            if (count($userData) > 0) {
				$menu = unserialize($userData->menu);
				$currentMenu = '';
				$defaultMenu = '';
				if(array_key_exists('Master',$menu)){
					$currentMenu = 'Master';
					$defaultMenu = 'blk';
				}elseif(array_key_exists('Input',$menu)){
					$currentMenu = 'Input';
					$defaultMenu = 'pegawai';
				}elseif(array_key_exists('Web',$menu)){
					$currentMenu = 'Web';
					$defaultMenu = 'berita';
				}elseif(array_key_exists('User',$menu)){
					$currentMenu = 'user';
					$defaultMenu = 'user';
				}elseif(array_key_exists('Laporan',$menu)){
					$currentMenu = 'Laporan';
					$defaultMenu = 'rekap_pelatihan';
				}

                $data = array(
					'username' => $userData->username,
                    'nip' => $userData->nip,
					'menu' => $menu,
					'currentmenu' => $currentMenu
                );

                $this->session->set_userdata($data);
                redirect('manage/'.$defaultMenu);
            }else{
				$contentVars['message'] = 'Username atau password anda salah, silahkan ulangi kembali';

				$pagedata = array(
					'title' => ' >> Login',
					'islogin' => $islogin,
					'content' => $this->parser->parse('login', $contentVars, TRUE)
				);

				$this->parser->parse('main', $pagedata);

			}
        }

    }

    function logout(){
        $this->session->sess_destroy();
        redirect('login');
    }

    function _set_fields() {
		$this->form_data = new stdClass;
        $this->form_data->username = '';
        $this->form_data->pass = '';
    }

    function _set_rules() {
        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');


        $this->form_validation->set_error_delimiters('<span class="error">', '</span>');
    }

}

?>

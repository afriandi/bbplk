<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pendaftaran extends CI_Controller {

    private $tableName ;

    function __construct() {
        parent::__construct();
        $this->load->helper(array('url'));
        $this->load->model('pendaftaran_model', '', TRUE);
        $this->tableName = 'detail_pelatihan';
    }

    public function index(){
  		$islogin = '<li><a href="'.base_url().'login"><span class="glyphicon glyphicon-lock"></span>Login</a></li>';
  		if ($this->session->userdata('username')) {
          $islogin = '<li><a href="'.base_url().'login/logout"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>';
      }
      $contentVars = array(
  			'base_url' => base_url(),
  			'title_menu' => 'Informasi Pendaftaran',
  			'menu' => 'Pendaftaran',
  			'submenu' => 'Informasi',
  		);
      $info = pendaftaran_model::list_all($this->tableName)->result();
  		$lsit = array();
  		$i=0;
  		foreach ($info as $if) {
        $lsit[$i]['no'] = $i+1;
        $lsit[$i]['id_detail_pelatihan'] = $if->id_detail_pelatihan;
        $lsit[$i]['program_pelatihan'] = $if->program_pelatihan;
  			$lsit[$i]['tahun'] = $if->tahun;
  			$lsit[$i]['urutan'] = $if->urutan;
  			$lsit[$i]['tahap'] = $if->tahap;
  			$lsit[$i]['asrama'] = $if->asrama;
  			$lsit[$i]['tgl_pendaftaran_mulai'] = $if->tgl_pendaftaran_mulai;
  			$lsit[$i]['tgl_pendaftaran_selesai'] = $if->tgl_pendaftaran_selesai;
  			$lsit[$i]['tgl_rekrutmen'] = $if->tgl_rekrutmen;
  			$lsit[$i]['tgl_pengumuman'] = $if->tgl_pengumuman;
  			$lsit[$i]['tgl_daftar_ulang'] = $if->tgl_daftar_ulang;
  			$lsit[$i]['tgl_mulai'] = $if->tgl_mulai;
  			$lsit[$i]['tgl_selesai'] = $if->tgl_selesai;
  			$lsit[$i]['status'] = $if->status;
        $i++;
  		}
  		$contentVars['info_pendaftaran'] = $lsit;
  		$pagedata = array(
  			'title' => ' >> Informasi Pendaftaran',
  			'islogin' => $islogin,
  			'content' => $this->parser->parse('pendaftaran/pendaftaran_info', $contentVars, TRUE)
  		);
      $this->parser->parse('main', $pagedata);
    }

    public function detail_pendaftaran($id){
      $islogin = '<li><a href="'.base_url().'login"><span class="glyphicon glyphicon-lock"></span>Login</a></li>';
      if ($this->session->userdata('username')) {
          $islogin = '<li><a href="'.base_url().'login/logout"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>';
      }
      $detail_pelatihan = pendaftaran_model::get_by_id($this->tableName,$id);

      $contentVars = array(
        'base_url' =>  base_url(),
        'title_menu' => 'Detail Program Pelatihan',
        'menu' => 'Pendaftaran',
        'submenu' => 'Informasi',
        'id_detail_pelatihan' => $detail_pelatihan->id_detail_pelatihan,
        'id_pelatihan' => $detail_pelatihan->id_pelatihan,
        'program_pelatihan' => $detail_pelatihan->program_pelatihan,
        'jml_jp' => $detail_pelatihan->jml_jp,
        'id_kejuruan' => $detail_pelatihan->id_kejuruan,
        'nama' => $detail_pelatihan->nama,
        'tahun' => $detail_pelatihan->tahun,
        'urutan' => $detail_pelatihan->urutan,
        'tahap' => $detail_pelatihan->tahap,
        'status' => $detail_pelatihan->status,
        'asrama' => $detail_pelatihan->asrama,
        'tgl_pendaftaran_mulai' => $detail_pelatihan->tgl_pendaftaran_mulai,
        'tgl_pendaftaran_selesai' => $detail_pelatihan->tgl_pendaftaran_selesai,
        'tgl_rekrutmen' => $detail_pelatihan->tgl_rekrutmen,
        'tgl_pengumuman' => $detail_pelatihan->tgl_pengumuman,
        'tgl_daftar_ulang' => $detail_pelatihan->tgl_daftar_ulang,
        'tgl_mulai' => $detail_pelatihan->tgl_mulai,
        'tgl_selesai' => $detail_pelatihan->tgl_selesai,
        'tgl_ujk' => $detail_pelatihan->tgl_ujk
      );

      $pagedata = array(
        'title' => ' >> Informasi Pendaftaran',
        'islogin' => $islogin,
        'content' => $this->parser->parse('pendaftaran/pendaftaran_detail', $contentVars, TRUE)
      );
      $this->parser->parse('main', $pagedata);
    }

	  public function daftar(){
  		$islogin = '<li><a href="'.base_url().'login"><span class="glyphicon glyphicon-lock"></span>Login</a></li>';
  		if ($this->session->userdata('username')) {
          $islogin = ' <li><a href="'.base_url().'login/logout"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>';
      }
      $contentVars = array(
  			'base_url' => base_url(),
  			'title_menu' => 'Daftar',
  			'menu' => 'Pendaftaran',
  			'submenu' => 'Daftar',
  		);
  		$pagedata = array(
  			'title' => ' >> Pendaftaran',
  			'islogin' => $islogin,
  			'content' => $this->parser->parse('pendaftaran/pendaftaran_daftar', $contentVars, TRUE)
  		);
      $this->parser->parse('main', $pagedata);
    }

}
?>

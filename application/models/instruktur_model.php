<?php

/**
 * Description of instruktur_model
 *
 * @author afriandizen
 */
class instruktur_model extends CI_Model{

    function __construct() {
        parent::__construct();
    }

    function list_all($tableName) {
        $this->db->order_by('last_update', 'desc');
        return $this->db->get($tableName);
    }

    function count_all($tableName) {
        return $this->db->count_all($tableName);
    }

    function get_paged_list($tableName, $limit = 10, $offset = 0) {
        $this->db->order_by('last_update', 'desc');
        return $this->db->get($tableName, $limit, $offset);
    }

	function get_paged_list_all($tableName, $limit = 10, $offset = 0) {
		$this->db->select($tableName.'.id_instruktur, pegawai.nip, pegawai.nama as nama, kejuruan.nama as kejuruan,'.$tableName.'.keterangan');
        $this->db->order_by($tableName.'.last_update', 'desc');
		$this->db->join('pegawai', 'pegawai.nip = '.$tableName.'.nip', 'left');
		$this->db->join('kejuruan', 'kejuruan.id_kejuruan = '.$tableName.'.id_kejuruan', 'left');
        return $this->db->get($tableName, $limit, $offset);
    }

    function get_by_id($tableName,$id,$number,$offset) {
    $this->db->where('id_instruktur', $id);
		$this->db->join('kejuruan', 'kejuruan.id_kejuruan = '.$tableName.'.id_kejuruan', 'left');
    //    return $this->db->get($tableName)->row();
    return $this->db->get($tableName,$number,$offset);
    }

    function get_by_id_detail($tableName,$id) {
    $this->db->where('id_instruktur', $id);
		$this->db->join('kejuruan', 'kejuruan.id_kejuruan = '.$tableName.'.id_kejuruan', 'left');
    return $this->db->get($tableName)->row();
    //return $this->db->get($tableName);
    }

    function count_instruktur($tableName,$id) {
          $this->db->where('id_kejuruan', $id);
          return $this->db->count_all_results($tableName);
      }

	  function get_by_pegawai($tableName,$id) {
        $this->db->where('id_instruktur', $id);
		$this->db->join('pegawai', 'pegawai.nip = '.$tableName.'.nip', 'left');
        return $this->db->get($tableName)->row();
    }

    function get_by_kejuruan($tableName,$id,$number,$offset) {
  		$this->db->select('*');
          $this->db->where('id_kejuruan', $id);
  		$this->db->join('pegawai', 'pegawai.nip = '.$tableName.'.nip', 'left');
          return $this->db->get($tableName,$number,$offset);
      }

    function save($tableName, $data) {
        $this->db->insert($tableName, $data);
        return $this->db->insert_id();
    }

    function update($tableName, $data, $id) {
        $this->db->where('id_instruktur', $id);
        return $this->db->update($tableName, $data);
    }

    function delete($tableName, $id) {
        $this->db->where('id_instruktur', $id);
        $this->db->delete($tableName);
    }
}

?>

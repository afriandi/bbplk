<?php

/**
 * Description of pelatihan_model
 *
 * @author afriandizen
 */
class pelatihan_model extends CI_Model{

    function __construct() {
        parent::__construct();
    }

    function list_all($tableName) {
        $this->db->order_by('last_update', 'desc');
        return $this->db->get($tableName);
    }

    function count_all($tableName) {
        return $this->db->count_all($tableName);
    }

    function get_paged_list($tableName, $limit = 10, $offset = 0) {
        $this->db->order_by('last_update', 'desc');
        return $this->db->get($tableName, $limit, $offset);
    }

	function get_paged_list_all($tableName, $limit = 10, $offset = 0) {
        $this->db->order_by($tableName.'.last_update', 'desc');
		$this->db->join('kejuruan', 'kejuruan.id_kejuruan = '.$tableName.'.id_kejuruan', 'left');
        return $this->db->get($tableName, $limit, $offset);
    }
	/*
    function get_by_id($tableName,$id) {
        $this->db->where('id_pelatihan', $id);
        return $this->db->get($tableName)->row();
    }

	function get_by_kejuruan($tableName,$id) {
        $this->db->where('id_kejuruan', $id);
        return $this->db->get($tableName);
    }*/

    function get_by_id($tableName,$id) {
        $this->db->join('pelatihan', 'detail_pelatihan.id_pelatihan = pelatihan.id_pelatihan');
        $this->db->join('kejuruan', 'pelatihan.id_kejuruan = kejuruan.id_kejuruan');
        $this->db->where('id_detail_pelatihan', $id);
        return $this->db->get($tableName)->row();
    }

    function get_by_id_edit($tableName,$id) {
        $this->db->where('id_pelatihan', $id);
        return $this->db->get($tableName)->row();
    }

	  function get_by_kejuruan($tableName,$id,$number,$offset) {
        $this->db->join('pelatihan', 'detail_pelatihan.id_pelatihan = pelatihan.id_pelatihan');
        $this->db->where('id_kejuruan', $id);
        return $this->db->get($tableName,$number,$offset);
    }

    function count_pelatihan($tableName,$id) {
        $this->db->join('pelatihan', 'detail_pelatihan.id_pelatihan = pelatihan.id_pelatihan');
        $this->db->where('id_kejuruan', $id);
        return $this->db->count_all_results($tableName);
    }

    function get_all($tableName,$id) {
        $this->db->join('pelatihan', 'detail_pelatihan.id_pelatihan = pelatihan.id_pelatihan');
        $this->db->where('id_kejuruan', $id);
        return $this->db->get($tableName);
    }

    function save($tableName, $data) {
        $this->db->insert($tableName, $data);
        return $this->db->insert_id();
    }

    function update($tableName, $data, $id) {
        $this->db->where('id_pelatihan', $id);
        return $this->db->update($tableName, $data);
    }

    function delete($tableName, $id) {
        $this->db->where('id_pelatihan', $id);
        $this->db->delete($tableName);
    }
}

?>

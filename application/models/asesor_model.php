<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of instruktur_model
 *
 * @author afriandizen
 */
class asesor_model extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }

    function list_all($tableName) {
        $this->db->order_by('last_update', 'desc');
        return $this->db->get($tableName);
    }

    function count_all($tableName) {
        return $this->db->count_all($tableName);
    }

    function get_paged_list($tableName, $limit = 10, $offset = 0) {
        $this->db->order_by('last_update', 'desc');
        return $this->db->get($tableName, $limit, $offset);
    }
	
	function get_paged_list_all($tableName, $limit = 10, $offset = 0) {
        $this->db->order_by($tableName.'.last_update', 'desc');
		$this->db->join('pegawai', 'pegawai.nip = '.$tableName.'.nip', 'left');
        return $this->db->get($tableName, $limit, $offset);
    }

    function get_by_id($tableName,$id) {
        $this->db->where('id_asesor', $id);
		$this->db->join('pegawai', 'pegawai.nip = '.$tableName.'.nip', 'left');
        return $this->db->get($tableName)->row();
    }

    function save($tableName, $data) {
        $this->db->insert($tableName, $data);
        return $this->db->insert_id();
    }

    function update($tableName, $data, $id) {
        $this->db->where('id_asesor', $id);    
        return $this->db->update($tableName, $data);
    }

    function delete($tableName, $id) {
        $this->db->where('id_asesor', $id);
        $this->db->delete($tableName);
    }
}

?>

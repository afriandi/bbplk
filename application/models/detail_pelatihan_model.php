<?php

/**
 * Description of pelatihan_model
 *
 * @author afriandizen
 */
class detail_pelatihan_model extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }

    function list_all($tableName) {
        $this->db->order_by('last_update', 'desc');
        return $this->db->get($tableName);
    }

    function count_all($tableName) {
        return $this->db->count_all($tableName);
    }

    function get_paged_list($tableName, $limit = 10, $offset = 0) {
        $this->db->order_by('last_update', 'desc');
        return $this->db->get($tableName, $limit, $offset);
    }

	function get_paged_list_all($tableName, $limit = 10, $offset = 0) {
        $this->db->order_by($tableName.'.last_update', 'desc');
		$this->db->join('pelatihan', 'pelatihan.id_pelatihan = '.$tableName.'.id_pelatihan', 'left');
        return $this->db->get($tableName, $limit, $offset);
    }
	
    function get_by_id($tableName,$id) {
        $this->db->where('id_detail_pelatihan', $id);
        return $this->db->get($tableName)->row();
    }
	
    function save($tableName, $data) {
        return $this->db->insert($tableName, $data);
    }

    function update($tableName, $data, $id) {
        $this->db->where('id_detail_pelatihan', $id);    
        return $this->db->update($tableName, $data);
    }

    function delete($tableName, $id) {
        $this->db->where('id_detail_pelatihan', $id);
        $this->db->delete($tableName);
    }
}

?>

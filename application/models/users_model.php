<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class users_model extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }

    function list_all($tableName) {
        $this->db->order_by('last_update', 'desc');
        return $this->db->get($tableName);
    }

    function count_all($tableName) {
        return $this->db->count_all($tableName);
    }

    function get_paged_list($tableName, $limit = 10, $offset = 0) {
        $this->db->order_by('last_update', 'desc');
        return $this->db->get($tableName, $limit, $offset);
    }
	
	function get_paged_list_all($tableName, $limit = 10, $offset = 0) {
        $this->db->order_by($tableName.'.last_update', 'desc');
		$this->db->join('role', 'role.id_role = '.$tableName.'.id_role', 'left');
        return $this->db->get($tableName, $limit, $offset);
    }

    function get_by_id($tableName,$id) {
        $this->db->where('id_user', $id);
        return $this->db->get($tableName)->row();
    }
	
    function get_user_active($tableName,$where) {
        return $this->db->get_where($tableName,$where)->row();
    }
	
	function get_user_login($tableName,$where) {
		$this->db->join('role', 'role.id_role = '.$tableName.'.id_role', 'left');
        return $this->db->get_where($tableName,$where)->row();
    }
	
	function get_user_detail($tableName,$id) {
		$this->db->where($tableName.'.id_user', $id);
		$this->db->join('role', 'role.id_role = '.$tableName.'.id_role', 'left');
        return $this->db->get_where($tableName)->row();
    }
	
    function save($tableName, $data) {
        return $this->db->insert($tableName, $data);
    }

    function update($tableName, $data, $id) {
        $this->db->where('id_user', $id);    
        return $this->db->update($tableName, $data);
    }
	
    function delete($tableName, $id) {
        $this->db->where('id_user', $id);
        $this->db->delete($tableName);
    }
	
}

?>

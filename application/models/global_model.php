<?php

class global_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }
	
    function list_all($tableName) {
        $this->db->order_by('last_update', 'desc');
        return $this->db->get($tableName);
    }

    function count_all($tableName) {
        return $this->db->count_all($tableName);
    }

	function get_paged_list($tableName, $limit = 10, $offset = 0) {
        $this->db->order_by('last_update', 'desc');
        return $this->db->get($tableName, $limit, $offset);
    }

	function save($tableName, $data) {
        $this->db->insert($tableName, $data);
        return $this->db->insert_id();
    }

}

?>

<?php
class Pendaftaran_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function list_all($tableName) {
        $this->db->order_by('last_update', 'desc');
        return $this->db->get($tableName);
    }

    function get_by_id($tableName,$id) {
        $this->db->join('pelatihan', 'detail_pelatihan.id_pelatihan = pelatihan.id_pelatihan');
        $this->db->join('kejuruan', 'pelatihan.id_kejuruan = kejuruan.id_kejuruan');
        $this->db->where('id_detail_pelatihan', $id);
        return $this->db->get($tableName)->row();
    }

}
?>

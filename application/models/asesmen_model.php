<?php

/**
 * Description of asesmen_model
 *
 * @author afriandizen
 */
class asesmen_model extends CI_Model{

    function __construct() {
        parent::__construct();
    }

    function list_all($tableName) {
        $this->db->order_by('last_update', 'desc');
        return $this->db->get($tableName);
    }

    function count_all($tableName) {
        return $this->db->count_all($tableName);
    }

    function get_paged_list($tableName, $limit = 10, $offset = 0) {
        $this->db->order_by('last_update', 'desc');
        return $this->db->get($tableName, $limit, $offset);
    }

	function get_paged_list_all($tableName, $limit = 10, $offset = 0) {
        $this->db->select($tableName.'.deskripsi, '.$tableName.'.judul_skema, '.$tableName.'.no_skema, '.$tableName.'.id_asesmen, '.$tableName.'.id_kejuruan, kejuruan.nama');
		$this->db->order_by($tableName.'.last_update', 'desc');
		$this->db->join('kejuruan', 'kejuruan.id_kejuruan = '.$tableName.'.id_kejuruan', 'left');
        return $this->db->get($tableName, $limit, $offset);
    }

	function get_paged_list_by_kejuruan($tableName, $idkejur, $limit = 10, $offset = 0) {
        $this->db->order_by('last_update', 'desc');
		$this->db->where('id_kejuruan', $idkejur);
        return $this->db->get($tableName, $limit, $offset);
    }

    function get_by_id($tableName,$id) {
        $this->db->where('id_asesmen', $id);
        return $this->db->get($tableName)->row();
    }

    function get_by_kejuruan($tableName,$id,$number,$offset) {
        $this->db->where('id_kejuruan', $id);
        return $this->db->get($tableName,$number,$offset);
    }

    function count_asesmen($tableName,$id) {
        $this->db->where('id_kejuruan', $id);
        return $this->db->count_all_results($tableName);
    }

    function save($tableName, $data) {
        $this->db->insert($tableName, $data);
        return $this->db->insert_id();
    }

    function update($tableName, $data, $id) {
        $this->db->where('id_asesmen', $id);
        return $this->db->update($tableName, $data);
    }

    function delete($tableName, $id) {
        $this->db->where('id_asesmen', $id);
        $this->db->delete($tableName);
    }
}

?>

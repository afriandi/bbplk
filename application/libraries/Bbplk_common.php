<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * MSAP Common function
 *
 * This class contains functions which commonly use in MSAP
 */
class BBPLK_Common {

    var $CI = null;
    public $BBPLK_config = array();

    /**
     * Constructor
     *
     * @access   public
     * @return  boolean
     */
    function FBSMS_Common() {

        $this->CI = & get_instance();

        foreach ($this->CI->config->config as $item => $value) {
            if (!preg_match('/^BBPLK_/', $item))
                continue;
            $this->BBPLK_config[preg_replace('/^BBPLK_/', '', $item)] = $value;
        }

    }
}

/* End of file Msap_common.php */
/* Location: ./system/application/libraries/Msap_common.php */